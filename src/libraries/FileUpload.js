import fs from "fs";
import uniqid from "uniqid";
import commonConstants from "~/constants/comman";

/**
 * File Upload Library
 */
export default class FileUpload {
    /**
     * Upload File.
     *
     * @param  {Object} fileObject
     * @param  {String} folder
     * @returns {Boolean}
     */
    uploadFile( fileObject, folder ) {

        return new Promise( ( resolve, reject ) => {
            if( fileObject !== "" || fileObject == null ) {
                const ext = fileObject.name.split( "." ).pop(),
                    directory = commonConstants.STORAGE_PATH + folder,
                    fileName = `${uniqid()}.${ext}`;

                // Check Directory
                if ( !fs.existsSync( directory ) ) {
                    fs.mkdirSync( directory, { "recursive": true } );
                }

                const fileNameWithPath = `${directory}/${fileName}`;

                fileObject.mv( fileNameWithPath, ( err ) => {
                    if ( err ) {
                        reject( err );
                    }

                    const output = { "name": fileName, "path": fileNameWithPath };

                    resolve( output );
                } );
            }else{
                const output = { "name": "" };

                resolve( output );
            }
        } );
    }

    /**
     * Upload Multiple Files.
     *
     * @param  {Object} fileObject
     * @param  {String} folder
     * @returns {Boolean}
     */
    uploadMultipleFile( fileObject, folder ) {
        fileObject.map( async( file ) => {
            const ext = file.name.split( "." ).pop(),
                directory = commonConstants.STORAGE_PATH + folder,
                fileName = `${uniqid()}.${ext}`;

            // Check Directory
            if ( !fs.existsSync( directory ) ) {
                fs.mkdirSync( directory );
            }

            const fileNameWithPath = `${directory}/${fileName}`;

            await file.mv( fileNameWithPath, ( err ) => {
                if ( err ) {
                    return false;
                }

                return true;
            } );
        } );
    }

    /**
     * File unlink.
     *
     * @param  {Object} fileName
     * @param  {String} folder
     * @returns {Boolean}
     */
    unlinkFile( fileName, folder ) {
        const directory = commonConstants.STORAGE_PATH + folder,
            path = `${directory}/${fileName}`;

        fs.unlink( path, ( err ) => {
            if ( err ) {
                console.error( err );

                return;
            }

            // file removed
        } );
    }
}
