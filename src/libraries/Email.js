import emailConfig from "~/config/emailConfig";

/**
* Email Library
*/
export default class Email {
    /**
    * Send Email.
    *
    * @param {Object} mailDetails Which includes to, template and fields which needs to be dynamic.
    */
    async sendEmail( mailDetails ) {

    await emailConfig
    .send( {
    "template": mailDetails.template,
    "message": {
    "to": mailDetails.to,
    "subject": mailDetails.subject
    },
    "locals": mailDetails.dynamicFields
    } )
    .then( ( res ) => {
        console.log("res", res)
    return res;
    } )
    .catch( () => {} );
    }
}