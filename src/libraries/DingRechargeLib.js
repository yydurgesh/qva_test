import util from "~/utils/util";
import request from "request";
//import { convertData } from "~/utils/convertData";
import envConstants from "~/constants/envConstants";
import paymentConstants from "~/constants/paymentConstants";
import axios from "axios";
import https from "https";

//const convertDataObj = new convertData();

/**
 *  Dtone AirTime Recharge Library
 */
export class DingRechargeLib {
    
    /**
     * ding connect API.
     *
     * @returns {String}
     */
     GetErrorCodeDescriptions(){
      const host = envConstants.DING_BASE_URL,
           token = envConstants.DING_API_KEY;
        return axios({
            method  : 'GET',
            url     : host+'GetErrorCodeDescriptions',
            //data    : xmlFormData,
            headers: {
                'Content-Type' : 'application/json; charset=utf-8',
                 'api_key': token
            },
            httpsAgent: new https.Agent({ rejectUnauthorized: false })
        }).then(response => {
                return response.data;
        }).catch(function (error) {
      // const response = error.response.data
         const response = error
            return response;
        });

     }
    
    GetRegions( CountryIso ){
        const host = envConstants.DING_BASE_URL,
           token = envConstants.DING_API_KEY,
           formData = {
                    countryIsos: CountryIso,
           }; 
        return axios({
            method  : 'GET',
            url     : host+'GetRegions',
            params    : formData,
            headers: {
                'Content-Type' : 'application/json; charset=utf-8',
                 'api_key': token
            },
            httpsAgent: new https.Agent({ rejectUnauthorized: false })
        }).then(response => {
                return response.data;
        }).catch(function (error) {
      // const response = error.response.data +'?countryIsos=IN'
         const response = error
            return response;
        });
    }
    GetProviders( countryIsos, RegionCodes, contact_number ){
          const host = envConstants.DING_BASE_URL,
           token = envConstants.DING_API_KEY,
           formData = {
                    countryIsos: countryIsos,
                    RegionCodes: RegionCodes,
                    accountNumber: contact_number
            }; 
        return axios({
            method  : 'GET',
            url     : host+'GetProviders',
            params    : formData,
            headers: {
                'Content-Type' : 'application/json; charset=utf-8',
                 'api_key': token
            },
            httpsAgent: new https.Agent({ rejectUnauthorized: false })
        }).then(response => {
                return response.data;
        }).catch(function (error) {
      // const response = error.response.data +'?countryIsos=IN'
         const response = error
            return response;
        });
    }

    /**old */
    GetProducts( countryIsos, RegionCodes,provider_codes, contact_number ){
          const host = envConstants.DING_BASE_URL,
           token = envConstants.DING_API_KEY,
           formData = {
                    countryIsos: countryIsos,
                    RegionCodes: RegionCodes,
                    providerCodes:provider_codes,
                    accountNumber: contact_number,

            }; 
        return axios({
            method  : 'GET',
            url     : host+'GetProducts',
            params    : formData,
            headers: {
                'Content-Type' : 'application/json; charset=utf-8',
                'api_key': token,
                'insecureHTTPParser': true
            },
            httpsAgent: new https.Agent({ rejectUnauthorized: false })
        }).then(response => {
            console.log("response:::::::", response)
                return response.data;
        }).catch(function (error) {
            console.log("error:::::::", error)
            const response = error
            return response;
        });
    }
 
    SendTransfer( sku_code, send_value, send_currency_iso, contact_number ){
          const host = envConstants.DING_BASE_URL,
           token = envConstants.DING_API_KEY,
           formData = {
                        "SkuCode": sku_code,
                        "SendValue": send_value,
                        "SendCurrencyIso":send_currency_iso,
                        "AccountNumber": contact_number,
                        "DistributorRef": "cuaring",
                        "Settings": [
                            {
                            "Name": "mindiii",
                            "Value": "0"
                            }
                        ],
                        "ValidateOnly": envConstants.DING_API_MODE

            }; 

        return axios({
            method  : 'POST',
            url     : host+'SendTransfer',
            data    : formData,
            headers: {
                'Content-Type' : 'application/json',
                 'api_key': token
            },
            httpsAgent: new https.Agent({ rejectUnauthorized: false })
        }).then(response => {
                return response.data;
        }).catch(function (error) {
         const response = error
            return response;
        });
    }
    SendTransferLiv( sku_code, send_value, send_currency_iso, contact_number ){
          const host = envConstants.DING_BASE_URL,
           token = envConstants.DING_API_KEY,
           formData = {
                        "SkuCode": sku_code,
                        "SendValue": send_value,
                        "SendCurrencyIso":send_currency_iso,
                        "AccountNumber": contact_number,
                        "DistributorRef": "cubaring",
                        "Settings": [
                            {
                            "Name": "mindiii",
                            "Value": "0"
                            }
                        ],
                        "ValidateOnly": true

            }; 
        return axios({
            method  : 'POST',
            url     : host+'SendTransfer',
            data    : formData,
            headers: {
                'Content-Type' : 'application/json',
                 'api_key': token
            },
            httpsAgent: new https.Agent({ rejectUnauthorized: false })
        }).then(response => {
                return response.data;
        }).catch(function (error) {
         const response = error
            return response;
        });
    }

    /**
     * New phase function start from here
     * 
     */

     /**
     * ding connect API.
     *
     * @returns {String}
     */
     GetAccountLookup( contact_number ){
      const host = envConstants.DING_BASE_URL,
             formData = {
                  accountNumber: contact_number

            },
           token = envConstants.DING_API_KEY;
        return axios({
            method  : 'GET',
            url     : host+'GetAccountLookup',
            params    : formData,
            headers: {
                'Content-Type' : 'application/json; charset=utf-8',
                 'api_key': token
            },
            httpsAgent: new https.Agent({ rejectUnauthorized: false })
        }).then(response => {
                return response.data;
        }).catch(function (error) {
         const response = error
            return response;
        });

     }

     /**
     * ding connect API.
     *
     * @returns {Obj}
     */
     Getpromotion() {
        
        const host = envConstants.DING_BASE_URL,
            action = "GetPromotions",
            // formData = {
            //     "accountNumber": contactNumber

            // },
            token = envConstants.DING_API_KEY;

        return axios( {
            "method": "GET",
            "url": host + action,
           // "params": formData,
            "headers": {
                "Content-Type": "application/json; charset=utf-8",
                "api_key": token
            },
            "httpsAgent": new https.Agent( { "rejectUnauthorized": false } )
        } ).then( ( response ) => {
            return response.data;
        } ).catch( ( error ) => {
            const response = error;

            return response;
        } );

    }


     /**
     * ding connect API.
     *
     * @returns {Obj}
     */
    GetPromotionDescriptions() {
     // GET   /api/V1/GetPromotionDescriptions
        const host = envConstants.DING_BASE_URL,
            action = "GetPromotionDescriptions",
         formData = {
                 "LanguageCode": "en"

             },
            token = envConstants.DING_API_KEY;

        return axios( {
            "method": "GET",
            "url": host + action,
           // "params": formData,
            "headers": {
                "Content-Type": "application/json; charset=utf-8",
                "api_key": token
            },
            "httpsAgent": new https.Agent( { "rejectUnauthorized": false } )
        } ).then( ( response ) => {
            return response.data;
        } ).catch( ( error ) => {
            const response = error;

            return response;
        } );

    }

    /**
     * ding connect API.
     *
     * @returns {Obj}
     */
    GetAllProviders(){
        const host = envConstants.DING_BASE_URL,
         token = envConstants.DING_API_KEY;
       
      return axios({
          method  : 'GET',
          url     : host+'GetProviders',
         // params    : formData,
          headers: {
              'Content-Type' : 'application/json; charset=utf-8',
               'api_key': token
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false })
      }).then(response => {
              return response.data;
      }).catch(function (error) {
    // const response = error.response.data +'?countryIsos=IN'
       const response = error
          return response;
      });
  }

    /**
     * Get all supported plans and top up
     * @returns plans objects
     * 
     */
        GetAllProducts(){
            const host = envConstants.DING_BASE_URL,
            token = envConstants.DING_API_KEY
             
        return axios({
            method  : 'GET',
            url     : host+'GetProducts',
            //params    : formData,
            headers: {
                'Content-Type' : 'application/json; charset=utf-8',
                'api_key': token,
                'insecureHTTPParser': true
            },
            httpsAgent: new https.Agent({ rejectUnauthorized: false })
        }).then(response => {
           
                return response.data;
        }).catch(function (error) {
           
            const response = error
            return response;
        });
     }


    /**
     *  Get all Ding supported countries
     */
     GetCountries(){
        const host = envConstants.DING_BASE_URL,
         token = envConstants.DING_API_KEY;
      return axios({
          method  : 'GET',
          url     : host+'GetCountries',
          //data    : xmlFormData,
          headers: {
              'Content-Type' : 'application/json; charset=utf-8',
               'api_key': token
          },
          httpsAgent: new https.Agent({ rejectUnauthorized: false })
      }).then(response => {
              return response.data;
      }).catch(function (error) {
    // const response = error.response.data
       const response = error
          return response;
      });
    
    
    }
}




