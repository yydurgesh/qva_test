// //var User = require('../app/models/front/home.js');
// //var appUser = require('../app/controllers/api/user.js');
// import * as firebase from 'firebase';
// import * as fcm from 'fcm-node';
import FCM from "fcm-node";
import envConstants from "~/constants/envConstants";

// const fcm_key = new fcm( envConstants.FIREBASE_SERVER_KEY );
var fcm = new FCM(envConstants.FIREBASE_SERVER_KEY);

export class FirebaseLib {

//     constructor() {
//       this.firebase.initializeApp({
//              apiKey: envConstants.API_KEY,
//              authDomain: envConstants.AUTH_DOMAIN,
//              databaseURL:envConstants.DATABASE_URL,
//              projectId: envConstants.PROJECT_ID,
//              storageBucket:envConstants.STORAGE_BUCKET,
//              messagingSenderId:envConstants.MESSAGE_SENDER_ID,
//              gcm_sender_id :envConstants.GCM_SENDER_ID
//            });
//       this.notifications: 'webnotification';
//       this.users : 'users';
   
//     }
//     register(userId, data) {
//       firebase.database().ref().child('users').child(userId).once('value',function(rdata){

//               if(rdata.val()){
                  
//                   banAdmin = rdata.val().banAdmin ? rdata.val().banAdmin : 0;
              
//               }else{

//                   banAdmin = 0;
//               }

//               data.banAdmin = banAdmin;    
//               firebase.database().ref('users').child(userId).set(data);
//               firebase.database().ref().child('myGroup').child(userId).on('child_added',function(ddata){

//                   var s = ddata.val();
//                   firebase.database().ref('group').child(s).child('member').child(userId).child('firebaseToken').set(data.firebaseToken);
//                   firebase.database().ref('group').child(s).child('member').child(userId).child('profilePic').set(data.profilePic);
//                   firebase.database().ref('group').child(s).child('member').child(userId).child('userName').set(data.userName);

//                   return true;

//               });
          
//       });
//     }

    /**
     * Send notification using Firebase created by @hemant_solanki .
     *
     * @param  {string} deviceToken
     * @param  {string} notificationPayload
     * @returns {Object}
     */
    sendNotification(deviceToken, notificationPayload, redirectUrl) {

       
        var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
            to: deviceToken,
            collapse_key: 'your_collapse_key',
            delay_while_idle: false,
            priority: "high",
            content_available: true,
            click_action: redirectUrl,
            notification: notificationPayload,//{ title: 'Title of your push notification',body: 'Body of your push notification' 
            //}
            data: notificationPayload,  //you can send only notification or only data(or include both)
        
            mutable_content: true,
            category: ''
        };
        
        fcm.send(message, function (err, response) {
            if (err) {
                return err;
            } else {
                return response;
            }
        });
    }
}    


 

