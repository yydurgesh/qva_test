import Hashids from "hashids-bn";
import commonConstants from "~/constants/comman";
import jwt from "jsonwebtoken";
import Boom from "boom";
import i18n from "~/config/i18n.config";
import { LocaleService } from "~/utils/localeService";
const localeService = new LocaleService(i18n),
    hashidObj = new Hashids("", 6);

/**
 * define DateTime Library
 */
export class Security {
    /**
     * Get a encrypted string.
     *
     * @param  {Number|String} text
     * @returns {String}
     */
    encrypt(text) {
        return hashidObj.encode(text);
    }

    /**
     * Get a decrypted string.
     *
     * @param  {Number|String} text
     * @returns {String}
     */
    decrypt(text) {
        const output = hashidObj.decode(text);

        if (output.length === 0) {
            return 0;
        }

        return output[0];
    }

    /**
     * Get a decrypted string.
     *
     * @param  {Number|String} enrcyptedIdArr
     * @returns {String}
     */
    arraydecrypt(enrcyptedIdArr) {

        return Promise.all(
            enrcyptedIdArr.map((encryptedID) => {
                const decodedID = hashidObj.decode(encryptedID);

                return decodedID[0];
            })
        );
    }

    /**
     * Get a encrypted string.
     *
     * @param  {Number|String} decryptedIdArr
     * @returns {String}
     */
    arrayencrypt(decryptedIdArr) {

        return Promise.all(
            decryptedIdArr.map((decryptedID) => {
                const encodedID = hashidObj.encode(decryptedID);

                return encodedID;
            })
        );
    }

    /**
     * Get user auth token.
     *
     * @param  {Array} user
     * @returns {string}
     */
    static getUserAuthToken(user) {
        
        const token = jwt.sign(JSON.parse(JSON.stringify(user)), process.env.JWT_SECRET, {
            "expiresIn": commonConstants.JWT_EXPIRE_TIME
        });
        
        return `${token}`;
    }

    /**
     * Check auth token user Id.
     *
     * @param  {Object} req Request.
     * @param  {Object} res Response.
     * @param  {Object} next Next request.
     * @returns {string}
     */
    static checkAuthUserId(req, res, next) {
     
        const authUser = req.user,
            requestUser = hashidObj.decode(req.body.user_id)[0].toString();
       
        if (authUser !== requestUser) {
            throw Boom.unauthorized(localeService.translate("INVALID_USER"));
        }
        req.body.user_id = requestUser;

        return next();
    }

    /**
     * Decrypt Id.
     *
     * @param  {string} id
     * @returns {string}
     */
    static decryptId(id) {

        const decryptedId = hashidObj.decode(id)[0].toString();

        return decryptedId;
    }
}
