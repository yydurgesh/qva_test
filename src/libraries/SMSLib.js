import twilio from "twilio";
import envConstants from "~/constants/envConstants";
import logger from "~/utils/logger";

/**
 *  SMS Library twilio
 */
export default class SMSLib {
    /**
     *  Constructor.
     */
    constructor() {
        this.accountSid = process.env.TWILIO_ID;
        this.authToken = process.env.TWILIO_TOKEN;
        this.fromNumber = process.env.TWILIO_FROM_NUMBER;
        this.client = twilio( this.accountSid, this.authToken );
        
    }

    /**
     * Delete Object from  s3 .
     *
     * @param  {string} toNumber
     * @param  {string} message
     * @param  {Object} extra
     * @returns {Object}
     */
    sendSms( toNumber, message, extra = {} ) {
        const smsJson = {
            "body": message,
            "to": toNumber,
            "from": this.fromNumber
        };

        return new Promise( ( resolve, reject ) => {
            return this.client.messages.create( smsJson, ( error, send ) => {
                if ( error ) {
                    logger.error( error );

                    return reject( error );
                }else{
                    return resolve( true ); 
                }              
            
            } );
        } );
    }

    /**
     * Message body format .
     *
     * @param  {string} SenderMessage
     * @param  {string} senderName
     * @returns {Object}
     */
    getUserMessageBody( SenderMessage, senderName ) {
        const msgString = "\r\n-- \r\nFrom ",
            message = SenderMessage + msgString + senderName;

        return message;
    }
}                           
                                                                   
