import AWS from "aws-sdk";
import uniqid from "uniqid";

// const s3Obj = new AWS.S3( {
//     "accessKeyId": process.env.ACCESS_Key_ID,
//     "secretAccessKey": process.env.SECRET_ACCESS_Key,
//     "region": process.env.REGION
// } );

const s3Obj = new AWS.S3( {
    "accessKeyId": process.env.AWS_S3_ACCESS_KEY_ID,
    "secretAccessKey": process.env.AWS_S3_SECRET_ACCESS_KEY,
    "region": process.env.AWS_S3_REGION
} );


/**
 * S3 Maniuplation
 */
export class S3Lib {
    /**
     * Upload Image to s3 bucket.
     *
     * @param  {Object} file
     * @param  {Object} directory
     * @param  {Object} name
     * @returns {Object}
     */
    uploadToS3( file, directory, name ) {
        // const extension = file.name.split( "." ).pop();
        
        const extension = name.split( "." ).pop();
        let fileName;

        if ( name !== undefined && name !== "" ) {
            fileName = name;
        } else {
            fileName = `${uniqid()}.${extension}`;
        }

        return new Promise( ( resolve, reject ) => {
            const params = {
                "Bucket": process.env.AWS_S3_BUCKET_NAME,//BUCKET_NAME,
                "Key": `${directory}/${fileName}`,
                "Body": file.data,
                "ACL": 'public-read'
            };

            s3Obj.upload( params, ( err, data ) => {
                if ( err ) {
                    reject( err );
                }
                resolve( data );
            } );
        } );
    }

    /**
     * Delete Object from  s3 .
     *
     * @param  {Object} file
     * @returns {Object}
     */
    deleteFile( file ) {
        
        return new Promise( ( resolve, reject ) => {
            const params = {
                "Bucket": process.env.AWS_S3_BUCKET_NAME,//process.env.BUCKET_NAME,
                "Key": file
            };

            s3Obj.deleteObject( params, ( err, data ) => {
                if ( err ) {
                    reject( err );
                }
                resolve( data );
            } );
        } );
    }
}
