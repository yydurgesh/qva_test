
import express from "express";
import path from "path";
import fs from "fs";
import bodyParser from "body-parser";
import logger, { logStream } from "~/utils/logger";
import cors from "cors";
import json from "~/middlewares/json";
import stringHandler from "~/middlewares/stringHandler";
import { responseHandler } from "~/middlewares/responseHandler";
import * as errorHandler from "~/middlewares/errorHandler";
import favicon from "serve-favicon";
 import swaggerUiExpress from "swagger-ui-express";
import YAML from "yamljs";
import morgan from "morgan";
import helmet from "helmet";
import {  authRoutes, webRoutes,apiRoutes  } from "./v1/routes";
//require( "dotenv" ).config( { "path": `${__dirname}/../.env` } );
import fileUpload from "express-fileupload";
import cookieParser from "cookie-parser";
// process.binding('http_parser').HTTPParser = require('http-parser-js').HTTPParser;
import http from "http";
import session from "express-session";

import commonConstants from "~/constants/comman";
import passport from "passport";
import expressSanitizer from "express-sanitizer";
//if ( process.env.NODE_ENV !== "production" ) {
   



const app = express(),
    server = http.Server( app ),
    APP_PORT = process.env.APP_PORT || "3000",
    APP_HOST = process.env.APP_HOST || "192.168.10.28";
    

app.set( "port", APP_PORT );
app.set( "host", APP_HOST );
app.set( "view engine", "ejs" );
// app.set( "view engine", "html" );
app.set( "views", path.join( __dirname, "/views" ) );

app.locals.title = process.env.APP_NAME;
app.locals.version = process.env.APP_VERSION;
const swaggerDocument = YAML.load( "./src/docs/swagger.yaml" );

app.use( morgan( "tiny", { "stream": logStream } ) );
app.use( favicon( path.join( __dirname, "/../public/assets/favicons", "favicon.ico" ) ) );
app.use( "/file", express.static( path.join( __dirname, "/../storage" ) ) );
app.use( "/public", express.static( path.join( __dirname, "/../public" ) ) );
app.use( "firebase-messaging-sw.js", express.static( path.join( __dirname, "" ) ) );
app.use( "sucuri-31871d9abc0f415adacc831c16e4fcac.php", express.static( path.join( __dirname, "" ) ) );
app.use( "/", express.static( path.join( __dirname, "/../" ) ) );
app.use( cors() );
app.options( "*", cors() );
app.use( responseHandler );
app.use( bodyParser.json({limit: '50mb'}) );
app.use( expressSanitizer() );
app.use( bodyParser.urlencoded( {limit: '50mb', "extended": false } ) );
app.use( errorHandler.bodyParser );
app.use( session( {
    "secret": process.env.JWT_SECRET,
    "resave": true,
    "saveUninitialized": true
} ) );
app.use( passport.initialize() );
app.use( json );

app.use( stringHandler );
app.use( fileUpload() );
app.use(cookieParser());
app.use( "/", authRoutes);
app.use( "/", webRoutes);

app.use( "/api", apiRoutes);

// Error Middlewares
app.use( errorHandler.genericErrorHandler );
app.use( errorHandler.methodNotAllowed );
app.use( ( req, res, next ) => {
    res.set( "Cache-Control", "no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0" );
    next();
} );

if ( process.env.NODE_ENV === "production" ) {
    server.listen( app.get( "port" ), () => {
        logger.info( `Server started at http://${app.get( "port" )}/api` );
    } );
} else {

    server.listen( app.get( "port" ), app.get( "host" ), () => {
        logger.info( `Server started at http://${app.get( "host" )}:${app.get( "port" )}/api` );
    } );

    const directory = `${commonConstants.STORAGE_PATH}development`;

    if ( !fs.existsSync( directory ) ) {
        fs.mkdirSync( directory, { "recursive": true } );
    }
}

process.on( "uncaughtException", ( err ) => {
    logger.error( err.message );
} );

process.on( "unhandledRejection", ( reason ) => {
    logger.error( reason );
} );


export default app;
