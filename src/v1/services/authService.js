import Boom from "boom";
import bcrypt from "bcrypt";
import HttpStatus from "http-status-codes";
import UserModel from "~/v1/models/UserModel";
import OTPModel from "~/v1/models/OTPModel";
import CallModel from "~/v1/models/CallModel";
import AuthModel from "~/v1/models/AuthModel";
import MessageModel from "~/v1/models/MessageModel";
import RechargeSettingModel from "~/v1/models/RechargeSettingModel";
import DeveloperModel from "~/v1/models/DeveloperModel";
import VisiterModel from "~/v1/models/VisiterModel";
import CommissionOperatorModel from "~/v1/models/CommissionOperatorModel";
import DeveloperBalanceCommissionModel from "~/v1/models/DeveloperBalanceCommissionModel";
import CountryModel from "~/v1/models/CountryModel";
import WalletModel from "~/v1/models/WalletModel";
import RechargeModel from "~/v1/models/RechargeModel";
import DeveloperRechargeModel from "~/v1/models/DeveloperRechargeModel";
import FirebaseAuthModel from "~/v1/models/FirebaseAuthModel";
import UserApprovalModel from "~/v1/models/UserApprovalModel";
import InvalidAttemptedModel from "~/v1/models/InvalidAttemptedModel";
const moment = require( "moment-timezone" );


import { Security } from "~/libraries/Security";
import { DateTime } from "~/libraries/DateTime";
import configData from "~/config/configData";
import databaseConstants from "~/constants/databaseConstants";
import dataConstants from "~/constants/dataConstants";
import envConstants from "~/constants/envConstants";
import commonConstants from "~/constants/comman";
import statusConstants from "~/constants/statusConstants";
import folderConstants from "~/constants/folderConstants";
import DateTimeUtil from "~/utils/DateTimeUtil";
import logger from "~/utils/logger";
import SMSLib from "~/libraries/SMSLib";
//import { DingRechargeLib } from "~/libraries/DingRechargeLib";
import { convertData } from "~/utils/convertData";
import FileUpload from "~/libraries/FileUpload";


import util from "~/utils/util";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
import responseHelper from "~/utils/responseHelper";
import password from "~/utils/password";
import { S3Lib } from "~/libraries/S3Lib";
import { date } from "joi";

const basePath = process.env.ASSETS_URL_BASE,
    awsS3BaseUrl = process.env.S3_BASE_PATH,
    directory = commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR, //s3 profile folder
    staticPath = commonConstants.FILE_STATIC_PATH,
    fileUploadFolder = commonConstants.FILE_UPLOAD_FOLDER,
    defaultUserImage = commonConstants.DEFAULT_USER_IMAGE,
    s3PlaceholderImage = commonConstants.USER_PLACEHOLDER_IMAGE,
    fileUploadObj = new FileUpload(),
    UserModelObj = new UserModel(),
    OTPModelobj = new OTPModel(),
    MessageModelObj = new MessageModel(),
    RechargeSettingModelObj = new RechargeSettingModel(),
    CallModelObj = new CallModel(),
    AuthModelObj = new AuthModel(),
    VisiterModelObj = new VisiterModel(),
    DeveloperModelObj = new DeveloperModel(),
    CommissionOperatorModelObj = new CommissionOperatorModel(),
    DeveloperBalanceObj = new DeveloperBalanceCommissionModel(),
    CountryModelObj = new CountryModel(),
    WalletModelObj = new WalletModel(),
    DeveloperRechargeObj       = new DeveloperRechargeModel(),
    RechargeModelObj = new RechargeModel(),
    FirebaseAuthModelObj = new FirebaseAuthModel(),
    UserApprovalModelObj = new UserApprovalModel(),
    InvalidAttemptedModelObj = new InvalidAttemptedModel(),
    S3LibObj = new S3Lib(),
    
   
    SMSLibObj = new SMSLib(),
   // dingRechargeLibObj      = new DingRechargeLib(),
    convertDataObj          = new convertData(),
    localeService = new LocaleService( i18n ),
    datetimeObj = new DateTime();

/**
 * Send Otp.
 *
 * @param  {Object}     requestData 
 * @returns {Promise}
 */
export const sendOtp = function( request ) {
    const requestData = request.body;
    const otp = util.generatOtp( commonConstants.INTEGER_RANDOME_NUMBER, commonConstants.RANDOME_NUMBER_LENGTH ),
    countryCode = "+"+requestData.dial_code,
        userType = request.headers['x-user-type'],
        contact = requestData.contact_number,
        userContact = countryCode + contact,
        
        msg = localeService.translate( "TWILIO_VERIFICATION" ) + otp,
        whereData = {
            "country_dial_code": countryCode,
            "contact_number": contact,
            "user_type": userType
        },
        selectData = [ 
            "country_code", 
            "contact_number",
            "user_type", 
            "is_profile_done"
        ],
        response = { "data": { } };
         // change language 
        
    return UserModelObj.checkContactNumber( whereData, selectData ).then( ( userData ) => {
       
        // update status is_mobile_verified.
        if ( userData.length > 0 && userData[0].is_profile_done === statusConstants.IS_PROFILE.COMPLETED ) {
            throw Boom.badRequest( localeService.translate( "Phone number already registered" ) );

        }
        return SMSLibObj.sendSms( userContact, msg ).then( (result) => {
           
            const otpExpiry = DateTimeUtil.getCurrentWithAddMinutes( commonConstants.OTP_EXP_AT );
            const ct = DateTimeUtil.getCurrentTimeObjForDB();
            const column = {
                "otp_code": otp,
                "login_otp_expire_at": otpExpiry,
                "created_at": ct,
                "updated_at": ct


            };
            
            
            var selectOtpData = [ 
                "country_dial_code	", 
                "contact_number",
                
            ];
            if ( !userData.length ) {
              
                // if( userType == dataConstants.USER_TYPE.USER_CONSUMER ) {
                //     column.is_approval = statusConstants.USER_APPROVAL.IS_APPROVED;
                // }

                column.country_dial_code = countryCode;
                column.contact_number = contact;
              //  column.user_type = userType;
               // column.status = commonConstants.DEFAULT_USER_STATUS;
                
                return OTPModelobj.createObj( column, selectOtpData ).then( () => {
                    response.code = HttpStatus.OK;
                    response.message = "Qvaring verification code sent successfully." ;
                    return response;
                } ).catch( ( err ) => {
                    logger.error( err );
                    throw Boom.badImplementation( localeService.translate( "INTERNAL_SERVER_ERROR" ) );
                } ); 
            }else{
                const updateWhereCondition = {
                    "country_code": countryCode,
                    "contact_number": contact,
                    "user_type": userType
                };
                
                return UserModelObj.updateObj( column, updateWhereCondition, selectData ).then( () => {
                    response.code = HttpStatus.OK;
                    
                    return response;
                } ).catch( ( err ) => {
                    
                    logger.error( err );
                    throw Boom.badImplementation( localeService.translate( "INTERNAL_SERVER_ERROR" ) );
                } );
            }
        } ).catch( ( err ) => {    
                throw Boom.badRequest( localeService.translate("INVALID_TO_NUMBER") );
            } );
    } );
};


/**
 * Verify Otp.
 *
 * @param  {Object}  request
 * @returns {Promise}
 */
export const verifyOtp = function( request, next ) {
   
    // Variable Delaration
     
    var remotAddress;
    if (request.headers['x-forwarded-for']) {
        remotAddress = request.headers['x-forwarded-for'].split(",")[0];
    } else if (request.connection && request.connection.remoteAddress) {
        remotAddress = request.connection.remoteAddress;
    } else {
        remotAddress = req.ip;
    }
    const requestData = request.body,
       deviceId = request.headers['x-device-id'],
       userType = request.headers['x-user-type'],
       deviceType = request.headers['X-DEVICE-TYPE'],
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        otp_code = requestData.user_loginotp ,
        fetchColumns = [ "otp_code", "login_otp_expire_at" ],
        whereData = {
           
            "contact_number": requestData.user_contact,
            
        },
        response = { "data": { } };
        
       
    return OTPModelobj.fetchOtpObjWithSelectedFields( whereData, fetchColumns ).then( async ( user ) => {
     
       if(typeof user == "undefined" || user.length < 1 ){
          
         throw Boom.badRequest( localeService.translate( "INVALID_OTP" ) );

       }
       
       if(otp_code !== user.otp_code ){
        
        // Check OTP
            /* simple logic for invalid otp start here */
            var column = {};
            column.contact_number = requestData.user_contact;
            column.country_dial_code = '+'+requestData.dial_code;
            column.otp_code	 = requestData.user_loginotp;
            column.login_otp_expire_at = user.login_otp_expire_at;
            column.user_type = userType;
            column.created_at = currentTime;
            column.updated_at = currentTime;
            
            column.status = 0;
            var selectData = [ "contact_number", "otp_code" ]; 
            await InvalidAttemptedModelObj.createObj( column, selectData );
            /* end here */
            let whereCondition = {
                "contact_number":  requestData.user_contact,
                "status" : "0",
                "user_type" : userType
            };
            var invalidAttempted =  await InvalidAttemptedModelObj.getCount(whereCondition,["contact_number"]);
            
            if(invalidAttempted>=3){
                await  InvalidAttemptedModelObj.updateObj( {"status": "1"}, {"contact_number": requestData.user_contact, "user_type" : userType }, [ "contact_number", "otp_code" ] ) ;    

                response.code = HttpStatus.OK;
                response.message = localeService.translate( "INVALID_OTP" );
                response.attemptedCount = invalidAttempted[0];
                return response;
            }
            throw Boom.badRequest( localeService.translate( "INVALID_OTP" ) );
        
       }
       const loginOtpExpiry = user.login_otp_expire_at,
       expirytime = datetimeObj.changeFormat( loginOtpExpiry, databaseConstants.TIME_FORMAT );
       
       if ( expirytime < currentTime ) {
         
           throw Boom.badRequest( localeService.translate( 'EXPIRE_OTP' ) )
        }
       
        await OTPModelobj.updateObj({"status" :1}, whereData, ["id"])
        response.code = HttpStatus.OK;
              
        return response;
    
    } ).catch( ( err ) => {
        logger.error( err );
        throw err;
    } ); 
};



/**
 * sign up User.
 *
 * @param  {Object}  req 
 * @returns {Promise}
 */
export const userSignup = async function( req , next) {
  
    var ermsg = "Something went wrong" ;
    const firebaseToken = req.headers['x-firebase-token'],
          deviceId = req.headers['x-device-id'],
        //  deviceType = req.headers['x-device-type'],
          deviceType = req.headers['X-DEVICE-TYPE'],
          userType = req.headers['x-user-type'],
          currentYear = DateTimeUtil.getCurrentYear(),
          nextYear = DateTimeUtil.getNextYear();

          var ip;
        if (req.headers['x-forwarded-for']) {
            ip = req.headers['x-forwarded-for'].split(",")[0];
        } else if (req.connection && req.connection.remoteAddress) {
            ip = req.connection.remoteAddress;
        } else {
            ip = req.ip;
        }
         
       
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    let image;
    let profileImage;
   
    if( req.files ) {
       
        image = req.files.profileImage;

        // return fileUploadObj.uploadFile( image, fileUploadFolder )
        // .then( ( fileData ) => {
          
        const fileData = await fileUploadObj.uploadFile( image, fileUploadFolder );
       
            if( fileData.name !== "" ) {
                profileImage = fileData.name;
                const s3Data = await S3LibObj.uploadToS3( image, directory, fileData.name );
               
            } else {
                throw Boom.serverUnavailable( localeService.translate( "FAIL_FILE_UPLOAD" ) );
            }
           
            }else{
                
                image = "";
                profileImage = "";
            }
   
                const requestData = req.body,
                  
                    name = requestData.name,
                    user_loginotp = requestData.user_loginotp,
                    email = requestData.email,
                    hashPassword = password.cryptPassword( requestData.password ),
                    a_token = util.generatRandomString( commonConstants.UPPER_CASE_RANDOME_NUMBER, 50 ),
                    response = { "data": { } };

                   const fetchotpColumns = [ "otp_code", "login_otp_expire_at" ],
                    whereotpData = {
           
                        "contact_number": requestData.user_contact,
                        
                    };
                   
                    const is_otp = await OTPModelobj.fetchOtpObjWithSelectedFields( whereotpData, fetchotpColumns ) ;
                  
                   if(typeof is_otp == "undefined" || user_loginotp !== is_otp.otp_code ){
                        
                        throw Boom.badRequest( localeService.translate( "INVALID_OTP" ) );
                   }
                   
                   

                    const is_email = await  UserModelObj.fetchObj( { "email":email, "user_type" : userType } );
                    const is_number = await UserModelObj.fetchObj( { "contact_number": requestData.user_contact, "user_type" : userType } );
                        response.code = HttpStatus.OK
                      if(is_number.length > 0){
                        throw Boom.badRequest( localeService.translate( "Phone number already registered" ) );
                      }
                      if(is_email.length > 0){
                        throw Boom.badRequest( localeService.translate( "Email  already registered" ) );
                      }
                     
                const column = {
                    "name": name,
                    "email": email,
                    "password" : hashPassword,
                    "api_token":a_token,
                    "is_profile_done": statusConstants.IS_PROFILE.COMPLETED,
                    "profile_image": profileImage,
                    "is_approval" : 1,
                    "country_code" :requestData.country_code,
                    "country_dial_code" : '+'+requestData.dial_code,
                    "contact_number" : requestData.user_contact,
                    "is_mail_verified" : 1,
                    "status" : 2,
                    "user_type" : 1,
                    "is_mobile_verified" : 1,
                    "is_profile_done" : 1,
                    "ip_expire_time	" : datetimeObj.appendDaysInDate(currentDateTime , 1, 'days'),
                    "is_approval" : 1,
                    "auth_last_ip" : ip,
                    "created_at	" :currentDateTime,
                    "updated_at" : currentDateTime
                };

                const fetchColumns = [
                    "id",
                    "user_type",
                    "status",
                    "name",
                    "profile_image",
                    "email",
                    "country_code",
                    "contact_number",
                    "country_dial_code",
                    "is_mail_verified",
                    "is_mobile_verified",
                    "api_token",
                    "is_profile_done",
                    "is_approval",
                    "language"
                ];
            
            return UserModelObj.createObj( column, fetchColumns ).then( async (updateUserData) => {
                        response.code = HttpStatus.OK;
                      
                    // trim profile name.
                    const trimProfile = updateUserData.profile_image !== null ? updateUserData.profile_image.trim() : updateUserData.profile_image;
                    awsS3BaseUrl
                  
                    if( trimProfile === "" || trimProfile == null ) {
                        // updateUserData[ 0 ].profile_image = `${basePath}/${defaultUserImage}`;
                        updateUserData.profile_image = `${awsS3BaseUrl}${s3PlaceholderImage}`;
                    }else {
                        // updateUserData[ 0 ].profile_image = `${basePath}/${staticPath}/${fileUploadFolder}/${updateUserData[ 0 ].profile_image}`;
                        updateUserData.profile_image = `${awsS3BaseUrl}${directory}/${updateUserData.profile_image}`;
                    }
                   // code for operatror wise commission 
                   updateUserData.mobile_number = updateUserData.country_dial_code+updateUserData.contact_number;
                    //add for stroe providerinfo and operator start
                   
                    // add dvice details 

                    const fieebaseColumn = {
                        "user_id": updateUserData.id,
                        'device_token': firebaseToken,
                        "device_id": deviceId,
                        "device_type": deviceType,
                        "created_at": currentDateTime,
                        "updated_at": currentDateTime
                    },
                    fetchDevice = [
                        'user_id',
                        'device_token',
                        'device_id',
                        'device_type'
                       
                    ];
                    updateUserData.device_id = deviceId
                response.data.userData = updateUserData ;
                return  FirebaseAuthModelObj.createObj( fieebaseColumn, fetchDevice ).then( async (deviceData) => { 
                    response.data.deviceData = deviceData ;
                   
                 
                    const authData = {
                        "id": updateUserData.id,
                        "deviceType": deviceType,
                        "device_id": deviceId
                    },
                    authToken = Security.getUserAuthToken( authData );
                    response.token = authToken;
                //  response.deviceData = device_data ;
                response.code = HttpStatus.OK;
                  return response;
                });
             } ).catch( ( updateErr ) => {
                
                return next(Boom.badRequest( updateErr ))
            } ); 
};


/**
 * Get a user.
 *
 * @param  {object} req
 * @param  {object} res
 * @param  {Number|String}  authId
 * @returns {Promise}
 */
export const getWebUser = function( req, res, authId ) {
    return UserModelObj.fetchObj( { "id": authId } ).then( ( user ) => {
      
        if ( user.length < 1 ) {
            const token = req.session.userAuthToken;// req.headers['x-access-token'];
            
            if ( token ) {
                req.session.destroy();
                req.user = null;       
            }
            
            return res.redirect( "/" );
        }

        return user;
    } );
};



/**
 * resendSend Otp.
 *
 * @param  {Object}     requestData 
 * @returns {Promise}
 */
export const resendOtp = function( request ) {
    const requestData = request.body;
    const otp = util.generatOtp( commonConstants.INTEGER_RANDOME_NUMBER, commonConstants.RANDOME_NUMBER_LENGTH ),
    countryCode = "+"+requestData.dial_code,
        userType = request.headers['x-user-type'],
        contact = requestData.user_contact,
        userContact = countryCode + contact,
        
        msg = localeService.translate( "TWILIO_VERIFICATION" ) + otp,
        whereData = {
            "country_dial_code": countryCode,
            "contact_number": contact,
            "user_type": userType
        },
        selectData = [ 
            "country_code", 
            "contact_number",
            "user_type", 
            "is_profile_done"
        ],
        response = { "data": { } };
         // change language 
        return SMSLibObj.sendSms( userContact, msg ).then( (result) => {
           
            const otpExpiry = DateTimeUtil.getCurrentWithAddMinutes( commonConstants.OTP_EXP_AT );
            const ct = DateTimeUtil.getCurrentTimeObjForDB();
            const column = {
                "otp_code": otp,
                "login_otp_expire_at": otpExpiry,
                "created_at": ct,
                "updated_at": ct

            };
            
            var selectOtpData = [ 
                "country_dial_code	", 
                "contact_number",
                
            ];
            
                // if( userType == dataConstants.USER_TYPE.USER_CONSUMER ) {
                //     column.is_approval = statusConstants.USER_APPROVAL.IS_APPROVED;
                // }

                column.country_dial_code = countryCode;
                column.contact_number = contact;
              //  column.user_type = userType;
               // column.status = commonConstants.DEFAULT_USER_STATUS;
                
                return OTPModelobj.createObj( column, selectOtpData ).then( () => {
                    response.code = HttpStatus.OK;
                    response.message = "Qvaring verification code sent successfully." ;
                    return response;
                } ).catch( ( err ) => {
                    logger.error( err );
                    throw Boom.badImplementation( localeService.translate( "INTERNAL_SERVER_ERROR" ) );
                } ); 
            
        } ).catch( ( err ) => {    
                throw Boom.badRequest( localeService.translate("INVALID_TO_NUMBER") );
            } );
   
};


export const login = function( request ) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const req = request.body,
    currentTime = DateTimeUtil.getCurrentTimeObjForDB(), 
        userType = request.headers['x-user-type'],
        deviceType = request.headers['X-DEVICE-TYPE'],
        firebaseToken = request.headers['x-firebase-token'],
        device_id = request.headers['x-device-id'],
        isApp = req.is_app,
        deviceId = util.generatRandomString( commonConstants.LOWER_CASE_RANDOME_NUMBER, 50 ),
        fetchColumns = [
            "qva_users.id as id",
            "user_type",
            "qva_users.status as status",
            "name",
            "country_dial_code",
            "api_token",
            "profile_image",
            "password",
            "email",
            "qva_users.country_code as country_code",
            "qva_users.contact_number as contact_number",
           // "login_otp_expire_at",
            "is_mail_verified",
            "is_mobile_verified",
            "is_profile_done",
          //  "wallet_amount",
           // "admin_commission_amount",
          //  "wallet_id",
            "stripe_customer_id",
            "qva_users.created_at",
            "auth_last_ip",
          
            "ip_expire_time",
            "is_approval",
            "language"
        ],        
        whereCondition = {
            "contact_number": req.user_contact
           
        },
        methodWhere = {
            is_default:1
        },
        columnMethod = ["method"],
        response = { "data": { } },
        errorData ={"error":{}};
        

        var isExpired = 1;
        var ip;
        if (request.headers['x-forwarded-for']) {
            ip = request.headers['x-forwarded-for'].split(",")[0];
        } else if (request.connection && request.connection.remoteAddress) {
            ip = request.connection.remoteAddress;
        } else {
            ip = request.ip;
        }
        
        
     return UserModelObj.getUserInfo( whereCondition, fetchColumns ).then( async ( user ) => {

      //  const getMethod = await RechargeSettingModelObj.fetchObjWithSingleRecord( methodWhere, columnMethod ); 
        
       // check user Valid or not
        if ( user === undefined || user === null ) {
           
            throw Boom.badRequest( localeService.translate( "Mobile number is not registered, Please signup first") );
        }

        if( user != undefined && user != null ) {
            
            if( user.is_profile_done == statusConstants.IS_PROFILE.NOT_COMPLETED) {
                throw Boom.badRequest( localeService.translate( "INVALID_USER" ) );  
            }
            if( userType != dataConstants.USER_TYPE.USER_CONSUMER ) {
                if( user.is_approval == statusConstants.USER_APPROVAL.NOT_APPROVED ) {
                    throw Boom.badRequest( localeService.translate( "USER_NOT_APPROVED" ) );
                }
            }
        }
  
        
              user.rechargeMethod =2; 
           

       
        const trimProfile = user.profile_image !== null ? user.profile_image.trim() : user.profile_image, 
              isMatch = bcrypt.compareSync( req.password, user.password );

        // Check Password
        if ( !isMatch ) {
            throw Boom.badRequest( localeService.translate( "WRONG_PASSWORD" ) );
        }

        // set Admin full Profile_image path 
        if( trimProfile == null || trimProfile === "" ) {
            // user.profile_image = `${basePath}/${defaultUserImage}`;
            user.profile_image = `${awsS3BaseUrl}${s3PlaceholderImage}`;
        }else {
            // user.profile_image = `${basePath}/${staticPath}/${fileUploadFolder}/${user.profile_image}`;
            user.profile_image = `${awsS3BaseUrl}${directory}/${user.profile_image}`;
        }
        var web_user = user ;
       
        var otpSent;
        var currentDate = new Date();
        currentDate =new Date(currentDate).toISOString();
        var ipExpireTime = user.ip_expire_time;
        var diff_ms = moment(currentDate).diff(moment(ipExpireTime));
        var dur_obj = moment.duration(diff_ms);
        var hour = Math.round(dur_obj.asHours());
      
        //check ip is change
        if( user.auth_last_ip != ip ) {
            isExpired = 2;
        }

        //check login hours
        if( ipExpireTime == null || hour > 6 ) {
            isExpired = 2;
        }

        //send otp for app
        if( isExpired == 2 ) {
            //if( req.is_app == 'app') {
                await resendOtp( request );
           // }
        }
        if(isApp == ''){
            response.data.web_user = web_user
            console.log("user", web_user)
        }else{

        delete user.password;
        delete user.status;
        delete user.login_otp;
        //delete user.login_otp_expire_at;
        delete user.is_mail_verified;
        delete user.is_mobile_verified;
       // delete user.admin_commission_amount;
        delete user.stripe_customer_id;
     }
        
        response.code = HttpStatus.OK;
        
            user.deviceId = device_id ;
        

      
        // if(isApp == ''){

        //     var ipExpireTime = user.ip_expire_time;

        //     if (ipExpireTime == null || hour > 12) {
        //         is_web_exp = 2;
        //     }

        //     // check ip address
            
        //     if (user.auth_last_ip != ipAddress) {
        //         is_web_exp = 2;
        //     }

        // }
        
        //new work
       // isExpired = 1;

        if( isExpired == 1 ) {
            response.token = Security.getUserAuthToken( user );
            var isexp  = datetimeObj.appendDaysInDate(currentDateTime , 1, 'days');
            
            const  fetchDevice = [
                'user_id',
                'device_token',
                'device_id',
                'device_type'
               
            ],

             column = {
                "user_id": user.id,
                "device_token": firebaseToken,
                "device_id": device_id,
                "device_type": deviceType,
                "created_at": currentTime,
                "updated_at": currentTime
            } ;
            
           

       var isDevice =  await FirebaseAuthModelObj.fetchObj( {'device_id':device_id }) ;

       if(isDevice.length > 0){
                var authData = await FirebaseAuthModelObj.updateObj( column, {'device_id':device_id }, fetchDevice );
                user.device_id = authData[0].device_id;
            }else{
                var authData = await FirebaseAuthModelObj.createObj( column, fetchDevice );
                user.device_id = authData.device_id;
        }
        
        await UserModelObj.updateObj( {"ip_expire_time" : isexp}, whereCondition,  ["id"] );
           
    }
        //new work
        
        response.data.user = user;
        response.currentTime = currentTime;
        response.isExpired = isExpired;
        responseHelper.encryptAllKeys( response.data.user, [ databaseConstants.USER_KEY ] );

        return response;
    } );
};



/**
 * Send Otp for ip address.
 *
 * @param  {Object}     requestData 
 * @returns {Promise}
 */
export const sendOtpToIp = function( request ) {
    
    const requestData = request.body;
    const otp = util.generatOtp( commonConstants.INTEGER_RANDOME_NUMBER, commonConstants.RANDOME_NUMBER_LENGTH ),
    countryCode = "+"+requestData.dial_code,
        userType = request.headers['x-user-type'],
        deviceType = request.headers['X-DEVICE-TYPE'],
        contact = requestData.user_contact,
        userContact = countryCode + contact,
        
        msg = localeService.translate( "TWILIO_VERIFICATION" ) + otp,
        whereData = {
            "country_dial_code": countryCode,
            "contact_number": contact,
            "user_type": userType
        },
        selectData = [ 
            "country_code", 
            "contact_number",
            "user_type", 
            "is_profile_done"
        ],
        response = { "data": { } };
         // change language 
        
    return UserModelObj.checkContactNumber( whereData, selectData ).then( ( userData ) => {
       
        // update status is_mobile_verified.
        if ( userData.length > 0 && userData[0].is_profile_done === statusConstants.IS_PROFILE.COMPLETED ) {
            throw Boom.badRequest( localeService.translate( "Phone number already registered" ) );

        }
        return SMSLibObj.sendSms( userContact, msg ).then( (result) => {
           
            const otpExpiry = DateTimeUtil.getCurrentWithAddMinutes( commonConstants.OTP_EXP_AT );
            const ct = DateTimeUtil.getCurrentTimeObjForDB();
            const column = {
                "otp_code": otp,
                "login_otp_expire_at": otpExpiry,
                "created_at": ct,
                "updated_at": ct


            };
            
            
            var selectOtpData = [ 
                "country_dial_code	", 
                "contact_number",
                
            ];
            if ( !userData.length ) {
              
                // if( userType == dataConstants.USER_TYPE.USER_CONSUMER ) {
                //     column.is_approval = statusConstants.USER_APPROVAL.IS_APPROVED;
                // }

                column.country_dial_code = countryCode;
                column.contact_number = contact;
              //  column.user_type = userType;
               // column.status = commonConstants.DEFAULT_USER_STATUS;
                
                return OTPModelobj.createObj( column, selectOtpData ).then( () => {
                    response.code = HttpStatus.OK;
                    response.message = "Qvaring verification code sent successfully." ;
                    return response;
                } ).catch( ( err ) => {
                    logger.error( err );
                    throw Boom.badImplementation( localeService.translate( "INTERNAL_SERVER_ERROR" ) );
                } ); 
            }else{
                const updateWhereCondition = {
                    "country_code": countryCode,
                    "contact_number": contact,
                    "user_type": userType
                };
                
                return UserModelObj.updateObj( column, updateWhereCondition, selectData ).then( () => {
                    response.code = HttpStatus.OK;
                    
                    return response;
                } ).catch( ( err ) => {
                    
                    logger.error( err );
                    throw Boom.badImplementation( localeService.translate( "INTERNAL_SERVER_ERROR" ) );
                } );
            }
        } ).catch( ( err ) => {    
                throw Boom.badRequest( localeService.translate("INVALID_TO_NUMBER") );
            } );
    } );
};


/**
 * Verify User Otp.
 *
 * @param  {Object}  request
 * @returns {Promise}
 */
export const verifyUserOtp = async function( request ) {
   
    // Variable Delaration
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const firebaseToken = request.headers['x-firebase-token'],
          deviceType = request.headers['x-device-type'],
          localTimeZone = request.headers['x-local-timezone'],
          device_id = request.headers['x-device-id'],
         
          userType = request.headers['x-user-type'];
           
    var remotAddress;

    if (request.headers['x-forwarded-for']) {
        remotAddress = request.headers['x-forwarded-for'].split(",")[0];
    } else if (request.connection && request.connection.remoteAddress) {
        remotAddress = request.connection.remoteAddress;
    } else {
        remotAddress = request.ip;
    }
    
  
    const requestData = request.body,
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        fetchColumns = [
            "id",
            "user_type",
            "status",
            "name",
            "profile_image",
            "email",
            "country_code",
            "country_dial_code",
            "contact_number",
            "is_mail_verified",
            "is_mobile_verified",
            "is_profile_done",
            "api_token",
            "created_at",
            "language"
        ],
       
        
        updateWhereCondition = {
            "contact_number": requestData.user_contact,
            "user_type":userType,
        },
        updateWhereOtpCondition = {
            "contact_number": requestData.user_contact,
          
        },
        fetchOtpColumns = [ "otp_code", "login_otp_expire_at" ],
        response = { "data": { } };
    return UserModelObj.fetchObjWithSingleRecord( updateWhereCondition, fetchColumns ).then( async ( user ) => {
        
        return OTPModelobj.fetchOtpObjWithSelectedFields( updateWhereOtpCondition, fetchOtpColumns ).then( async ( userOtp ) => {
            
       
       // const getMethod = await RechargeSettingModelObj.fetchObjWithSingleRecord( methodWhere, columnMethod ); 
        if(user){
            
              user.rechargeMethod =2; 
           
        }
        const loginOtpExpiry = userOtp.login_otp_expire_at,
            loginOtp = userOtp.otp_code	,
            expirytime = datetimeObj.changeFormat( loginOtpExpiry, databaseConstants.TIME_FORMAT ),
            trimProfile = user.profile_image !== null ? user.profile_image.trim() : user.profile_image;
            
        // Check login otp expiry.
        if ( expirytime < currentTime ) {
           
            throw Boom.badRequest( localeService.translate( "EXPIRE_OTP" ) );
        }
        var invalidAttempted = '0';
      
        // Check OTP
        if ( loginOtp !== requestData.user_loginotp ) {
            /* simple logic for invalid otp start here */
            var column = {};
            column.contact_number = requestData.user_contact;
            column.country_dial_code = '+'+requestData.dial_code;
            column.otp_code	 = requestData.user_loginotp;
            column.login_otp_expire_at = userOtp.login_otp_expire_at;
            column.user_type = userType;
            column.status = 0;
            column.created_at = currentTime;
            column.updated_at = currentTime;
            var selectData = [ "contact_number", "otp_code" ]; 
           
           
            console.log("column", column)
            await InvalidAttemptedModelObj.createObj( column, selectData );
            /* end here */
            let whereCondition = {
                "contact_number":  requestData.user_contact,
                "status" : "0",
                "user_type" : userType
            };
            var invalidAttempted =  await InvalidAttemptedModelObj.getCount(whereCondition,["contact_number"]);
            
            if(invalidAttempted>=3){
              
                await  InvalidAttemptedModelObj.updateObj( {"status": "1"}, {"contact_number": requestData.user_contact, "user_type" : userType }, [ "contact_number", "otp_code" ] ) ;    

                response.code = HttpStatus.OK;
                response.message = localeService.translate( "INVALID_OTP" );
                response.attemptedCount = invalidAttempted[0];
                return response;
            }
            throw Boom.badRequest( localeService.translate( "INVALID_OTP" ) );
        }

       
        var isexp  = datetimeObj.appendDaysInDate(currentDateTime , 1, 'days');
        await UserModelObj.updateObj( {"auth_last_ip" : remotAddress, "ip_expire_time" : isexp}, updateWhereCondition, ['id'] );
        //console.log("updateData", updateData)
        // set and remove user properties 
        if( trimProfile == null || trimProfile === "" ) {
            // user.profile_image = `${basePath}/${defaultUserImage}`;
            user.profile_image = `${awsS3BaseUrl}${s3PlaceholderImage}`;
        }else {
            // user.profile_image = `${basePath}/${staticPath}/${fileUploadFolder}/${user.profile_image}`;
            user.profile_image = `${awsS3BaseUrl}${directory}/${user.profile_image}`;
        }
        
        const  fetchDevice = [
            'user_id',
            'device_token',
            'device_id',
            'device_type'
           
        ],

         columns = {
            "user_id": user.user_id,
            "device_token": firebaseToken,
            "device_id": device_id,
            "device_type": deviceType,
            "created_at": currentTime,
            "updated_at": currentTime
        } ;
        
       

            var isDevice =  await FirebaseAuthModelObj.fetchObj( {'device_id':device_id }) ;

            if(isDevice.length > 0){
                        var authData = await FirebaseAuthModelObj.updateObj( columns, {'device_id':device_id }, fetchDevice );
                        user.device_id = authData[0].device_id;
                    }else{
                        var authData = await FirebaseAuthModelObj.createObj( columns, fetchDevice );
                        user.device_id = authData.device_id;
                }
      
        user.usertype_value = configData.USER_TYPE[ user.user_type ];
        user.userstatus_value = configData.USER_STATUS[ user.status ];
        response.user = user ;
        response.token = Security.getUserAuthToken( user );
        response.code = HttpStatus.OK;
        return response
  //  })
    } ).catch( ( err ) => { 
        throw err;
    } );
    } );
// });
};


//Logout
export const logout = function( request ) {
   
    const req = request.body,
    currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        deviceId = request.headers[ "X-DEVICE-ID" ],
      
        updateWhere = {
            'device_id':deviceId
        },
       
        response = { "data": { } };
        console.log(deviceId)
        return FirebaseAuthModelObj.deleteObj( updateWhere ).then( async ( authData ) => {
           
        response.code = HttpStatus.OK;
        response.message = localeService.translate( "LOG_OUT" ) ;

        return response;
    })
}

