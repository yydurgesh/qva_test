import Boom from "boom";
import bcrypt from "bcrypt";
import HttpStatus from "http-status-codes";
import UserModel from "~/v1/models/UserModel";


import { LocaleService } from "~/utils/localeService";
import logger from "~/utils/logger";
import FileUpload from "~/libraries/FileUpload";
import i18n from "~/config/i18n.config";
//import { StripeLib } from "~/libraries/StripeLib";   
import commonConstants from "~/constants/comman";
import envConstants from "~/constants/envConstants";
import responseHelper from "~/utils/responseHelper";
import { Security } from "~/libraries/Security";
import statusConstants from "~/constants/statusConstants";
import databaseConstants from "~/constants/databaseConstants";
import dataConstants from "~/constants/dataConstants";
import { DateTime } from "~/libraries/DateTime";
import CommonFunction from "~/utils/CommonFunctions";
import Page from "twilio/lib/base/Page";
import { response } from "express";
import password from "~/utils/password";
import DateTimeUtil from "~/utils/DateTimeUtil";
import Email from "~/libraries/Email";
import SMSLib from "~/libraries/SMSLib";
import util from "~/utils/util";
import { DingRechargeLib } from "~/libraries/DingRechargeLib";
import { convertData } from "~/utils/convertData";
//import { FirebaseLib } from "~/libraries/FirebaseLib";
import folderConstants from "~/constants/folderConstants";
import { S3Lib } from "~/libraries/S3Lib";
import FirebaseAuthModel from "~/v1/models/FirebaseAuthModel";

const basePath = envConstants.ASSETS_BASE_URL,
    awsS3BaseUrl = envConstants.AWS_S3_BASE_URL,
    FirebaseAuthModelObj = new FirebaseAuthModel(),
    directory = commonConstants.PROFILE_MEDIA_UPLOAD_FOLDEFR, //s3 profile folder
    documentUploadDirectory = commonConstants.DOCUMENT_UPLOAD_FOLDEFR,
    staticPath = commonConstants.FILE_STATIC_PATH,
    fileUploadFolder = commonConstants.FILE_UPLOAD_FOLDER,
    verifyFileUploadFolder = commonConstants.VERIFY_FILE_UPLOAD_FOLDER,
    documentUploadFolder = commonConstants.DOCUMENT_UPLOAD_FOLDER,
    profileImagePath = `${basePath}/${staticPath}/${fileUploadFolder}/`,
    verifyImagePath = `${basePath}/${staticPath}/${verifyFileUploadFolder}/`,
    defaultUserImage = commonConstants.DEFAULT_USER_IMAGE,
    s3PlaceholderImage = commonConstants.USER_PLACEHOLDER_IMAGE,
    defaultUserImagePath = `${basePath}/${defaultUserImage}`,
    UserModelObj = new UserModel(),
    convertDataObj          = new convertData(),
    dingRechargeLibObj      = new DingRechargeLib(),
    SMSLibObj = new SMSLib(),
   
    localeService = new LocaleService( i18n ),
    fileUploadObj = new FileUpload(),
    CommonFnObj = new CommonFunction(),
    emailObj     = new Email(),
    datetimeObj = new DateTime(),
  //  firebaseLibObj = new FirebaseLib(),
    S3LibObj = new S3Lib();

/**
 * Update User.
 *
 * @param  {Object}  req 
 * @returns {Promise}
 */
export const updateUser = async function( req , next) {
    
    const firebaseToken = req.headers['x-firebase-token'],
        deviceType =  req.headers['X-DEVICE-TYPE'],
          deviceId = req.headers['x-device-id'],
          currentYear = DateTimeUtil.getCurrentYear(),
          user_id =  Security.decryptId(req.body.user_id)
        //  nextYear = DateTimeUtil.getNextYear();
         
       
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    let image;
    let profileImage;

    if( req.files ) {
       
        image = req.files.profileImage;
       
        const fileData = await fileUploadObj.uploadFile( image, fileUploadFolder );
      
            if( fileData.name !== "" ) {
                profileImage = fileData.name;

                const s3Data = await S3LibObj.uploadToS3( image, directory, fileData.name );
                await fileUploadObj.unlinkFile(fileData.name, fileUploadFolder);
            
            } else {
                throw Boom.serverUnavailable( localeService.translate( "FAIL_FILE_UPLOAD" ) );
            }
            
            
    }else{
        
        image = "";
        profileImage = "";
    }
    
    return UserModelObj.fetchObj( { "id": user_id } ).then( async ( data ) => {


                const requestData = req.body,
                    id = user_id,
                    name = requestData.name,
                    email = requestData.email,
                  //  hashPassword = password.cryptPassword( requestData.password ),
                    a_token = util.generatRandomString( commonConstants.UPPER_CASE_RANDOME_NUMBER, 50 ),
                    response = { "data": { } };


                const Updatecolumn = {
                    "name": name,
                    "email": email,
                   // "password" : hashPassword,
                    "api_token":a_token,
                    "is_profile_done": statusConstants.IS_PROFILE.COMPLETED
                   
                };
                if(profileImage){
                    Updatecolumn.profile_image = profileImage ;
                }
               

            const fetchColumns = [
                "id",
                "user_type",
                "status",
                "name",
                "profile_image",
                "email",
                "country_code",
                "country_dial_code",
                "contact_number",
                "is_mail_verified",
                "is_mobile_verified",
                "api_token",
                "is_profile_done",
                "is_approval"
            ];
            const updateWhereCondition = {
                "id": user_id
            };
     
            return UserModelObj.updateObj( Updatecolumn, updateWhereCondition, fetchColumns )
                .then( async ( updateUserData ) => {
                    
                    const trimProfile = updateUserData[ 0 ].profile_image !== null ? updateUserData[ 0 ].profile_image.trim() : updateUserData[ 0 ].profile_image;
                    awsS3BaseUrl
                    if( trimProfile === "" || trimProfile == null ) {
                        // updateUserData[ 0 ].profile_image = `${basePath}/${defaultUserImage}`;
                        updateUserData[ 0 ].profile_image = `${awsS3BaseUrl}${s3PlaceholderImage}`;
                    }else {
                        // updateUserData[ 0 ].profile_image = `${basePath}/${staticPath}/${fileUploadFolder}/${updateUserData[ 0 ].profile_image}`;
                        updateUserData[ 0 ].profile_image = `${awsS3BaseUrl}${directory}/${updateUserData[ 0 ].profile_image}`;
                    }
                 
                   
                    const authTokenData = {
                        "id": updateUserData[0].id,
                        "deviceType": deviceType,
                        "device_id": req.session.loginUserData.device_id
                    },
                    authToken = Security.getUserAuthToken( authTokenData );
                        // var authData = await FirebaseAuthModelObj.createObj( fieebaseColumn, firebaseSelectData );
                        
                        updateUserData[0].device_id = req.session.loginUserData.device_id;
                       
                        response.token = authToken;
                
                        response.code = HttpStatus.OK;
                        response.data.user = updateUserData[ 0 ];
                        responseHelper.encryptAllKeys( response.data.user, [ databaseConstants.USER_KEY ] );
                      
                        return response;
                    
          
            } ).catch( ( updateErr ) => {
                
                return next(Boom.badRequest( localeService.translate( updateErr ) ))
            } ); 
      
    })   
};


/**
 * Get a user.
 *
 * @param  {Number|String}  userId
 * @returns {Promise}
 */
export const getUser = function( userId ) {
    return UserModelObj.fetchObj( { "user_id": userId } ).then( ( user ) => {
        if ( user.length < 1 ) {
            throw Boom.notFound( localeService.translate( "USER_NOT_FOUND" ) );
        }

        return user;
    } );
};

/**
 * Check email for user update.
 *
 * @param  {String}  userEmail
 * @param  {String}  req
 * @returns {Promise}   
 */
export const checkEmail = function( userEmail, req ) {
    const selectColumns = [ "email" ],
        whereData = {
            "email": userEmail,
           // "user_type":req.headers['x-user-type']
        },
        whereNotData = {
            "id":   Security.decryptId(req.body.user_id)
        };
       // console.log("whereNotData", req.body)

    return UserModelObj.checkEmailForUpdateProfile( whereData, whereNotData, selectColumns ).then( ( emailData ) => {
        if ( emailData.length > 0 ) {
            throw Boom.forbidden( localeService.translate( "EMAIL_DUPLICATE" ) );
        }

        return emailData;
    } );
};
/**
 * Check email .
 *
 * @param  {String}  userEmail
 * @param  {String}  req
 * @returns {Promise}   
 */
export const checkEmailValid = function( userEmail, req ) {
    const selectColumns = [ "email" ],
        whereData = {
            "email": userEmail
        };
    return UserModelObj.fetchObjWithSingleRecord( whereData,  selectColumns ).then( ( emailData ) => {
       
        if ( !emailData ) {
            throw Boom.forbidden( localeService.translate( "EMAIL_NOT_EXIST" ) );
        }

        return emailData;
    } );
};

/**
 * Get all users for ADMIN.
 *
 * @param  {Object}   request 
 * @returns {Promise}
 */
export const getAllUsers = function( request ) {
  
    const whereData = {
        "user_id": request.user,
        "user_type": dataConstants.USER_TYPE.USER_ADMIN
    };

    return UserModelObj.fetchObj( whereData ).then( ( userAdmin ) => {
        
        // check user_type
        if( userAdmin.length > 0 ) {
            const requestData = request.query,
                page = requestData.start,
                countColumn = [ "user_id" ],
                columns = [ 
                    "users.user_id",
                    "users.name", 
                    "users.profile_image", 
                    "users.email",
                    "users.country_code",
                    "users.contact_number",
                    "users.created_at",
                    "wallets.wallet_amount"
                ],
                limit = databaseConstants.LIMIT,
                response = { "data": { } };
    
                let offset = databaseConstants.OFFSET;
                if(Page !== undefined) {
                    offset = page;
                }

            const whereMatch = {
                "user_type": dataConstants.USER_TYPE.USER_CONSUMER 
            } 
            
            return UserModelObj.getAllUsers( whereMatch, columns, limit, offset, requestData ).then( ( resdata ) => {
              
                response.code = HttpStatus.OK;
                
                return datetimeObj.changeDatabaseTimestampFormat( resdata, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( ( userData ) => {
                    // return UserModelObj.getCountUsers( countColumn ).then( ( count ) => {
                    return UserModelObj.getCountUsersData( whereMatch, countColumn, requestData ).then( ( count ) => {
                        response.recordsTotal = count[ 0 ];
                        
                        return responseHelper.encryptobjectAllKeys( userData, databaseConstants.USER_KEY ).then( ( message ) => {
                          
                            response.data.users = message;
                            response.recordsFiltered = count[ 0 ];
                            response.profileImagePath = profileImagePath;
                            response.defaultUserImage = defaultUserImagePath;
                         
                            
                            return response;
                        } ).catch( ( error ) => {
                            throw Boom.badImplementation( localeService.translate( error ) );
                        } );
                    } ).catch( ( error ) => {
                        logger.error( error );
                        throw Boom.badImplementation( localeService.translate( error ) );
                    } );
                } ).catch( ( error ) => {
                    logger.error( error );
                    throw Boom.notImplemented( localeService.translate( error ) );
                } );
            } ).catch( ( err ) => {
                throw Boom.failedDependency( localeService.translate( err ) );
            } ); 
        }else{
            throw Boom.unauthorized( localeService.translate( "INVALID_USER_TYPE" ) );
        }
    } ).catch( (err) => {
        throw Boom.unauthorized( localeService.translate( "INVALID_USER_TYPE" ) );
    } ); 
};

/*
 * User detail for Admin.
 *
 * @param  {Object}  req 
 * @returns {Promise}
 */
export const getUserDetail = function( req ) {
    // Variable Declarations
    const requestData = req.query,
        userId = Security.decryptId( requestData.user_id ),
        fetchColumns = [
            "users.user_id",
            "users.status",
            "users.name",
            "users.profile_image",
            "users.email",
            "users.country_code",
            "users.contact_number",
            "users.is_mail_verified",
            "users.is_mobile_verified",
            "users.is_profile_done",
            "users.user_type",
            "verify.verify_image",
            "verify.verify_status",
            "wallets.wallet_amount",
            "wallets.admin_commission_amount",
            "wallets.ding_commission_amount",
        ],
        whereCondition = {
            "users.user_id": userId
        },
        response = { "data": { } };

    return UserModelObj.getMyProfile( whereCondition, fetchColumns ).then( ( user ) => {
        // return UserModelObj.fetchObjWithSingleRecord( whereCondition, fetchColumns ).then( ( user ) => {
            
        return UserModelObj.getRechargeCount( userId ).then( ( totalRecharge ) => {
            return UserModelObj.getDeveloperRechargeCount( userId ).then( ( developerTotalRecharge ) => {
                const trimProfile = user.profile_image !== null ? user.profile_image.trim() : user.profile_image;
                const trimVerifyImage = user.verify_image !== null ? user.verify_image.trim() : user.verify_image;

                if( trimProfile == null || trimProfile === "" ) {
                    // user.profile_image = `${basePath}/${defaultUserImage}`;
                    user.profile_image = `${awsS3BaseUrl}${s3PlaceholderImage}`;
                }else {
                    // user.profile_image = `${basePath}/${staticPath}/${fileUploadFolder}/${user.profile_image}`;
                    user.profile_image = `${awsS3BaseUrl}${directory}/${user.profile_image}`;
                }
                
                if( trimVerifyImage != null && trimVerifyImage != "" ) {
                    // user.verify_image = `${basePath}/${staticPath}/${verifyFileUploadFolder}/${user.verify_image}`;
                    user.verify_image = `${awsS3BaseUrl}${documentUploadFolder}/${user.verify_image}`;
                }

                response.code = HttpStatus.OK;
                response.data = user;
                responseHelper.encryptAllKeys( response.data, [ databaseConstants.USER_KEY ] );
                response.totalRecharge = Number(totalRecharge[0].count) + Number(developerTotalRecharge[0].count);
                
                return response;
            } ).catch( ( err ) => {
                throw Boom.badImplementation( localeService.translate( err ) );
            } );
        } ).catch( ( err ) => {
            throw Boom.badImplementation( localeService.translate( err ) );
        } );
    } ).catch( ( error ) => {
        throw Boom.badImplementation( localeService.translate( error ) );
    } ); 
};

/**
 * Check Admin user type.
 *
 * @param  {String}  id
 * @returns {Promise}   
 */
export const checkUserType = function( id ) {
    const selectColumns = [ "user_type" ],
        whereData = {
            "user_id": id
        };

    return UserModelObj.fetchObjWithSingleRecord( whereData, selectColumns ).then( ( user ) => {
        if ( user.user_type !== dataConstants.USER_TYPE.USER_ADMIN ) {
            throw Boom.unauthorized( localeService.translate( "INVALID_USER_TYPE" ) );
        }

        return user.user_type;
    } );
};

/**
 * Update Admin user status
 * 
 * @param {String} id
 */
export const updateUserStatus = function( req ) {
    
    const requestData = req.body,
        id = Security.decryptId( requestData.user_id ), 
        fetchColumns = [
            "user_id",
            "user_type",
            "status",
            "name",
            "profile_image",
            "email",
            "country_code",
            "contact_number",
            "is_mail_verified",
            "is_mobile_verified",
            "is_profile_done"
        ],
        Updatecolumn = { "status": requestData.status },
        updateWhere = { "user_id": id },
        response = { "data": { } };

    return UserModelObj.updateObj( Updatecolumn, updateWhere, fetchColumns )
        .then( ( updateUserData ) => {
            response.code = HttpStatus.OK;
            response.data.user = updateUserData[0];
            response.message = localeService.translate( "UPDATE_STATUS" );
            
            return response;
        }).catch( ( updateErr ) => {
            throw Boom.badImplementation( localeService.translate( updateErr ) );
        } );
}

/**
 * Get all recharges for ADMIN.
 *
 * @param  {Object}   request 
 * @returns {Promise}
 */
export const getAllRecharges = function( request ) {
  
    const whereData = {
        "user_id": request.user,
        "user_type": dataConstants.USER_TYPE.USER_ADMIN
    };

    return UserModelObj.fetchObj( whereData ).then( ( userAdmin ) => {
        // check user_type
        if( userAdmin.length > 0 ) {
            
            const requestData = request.query,
                page = requestData.start,
                countColumn = [ "recharge_id" ],
                columns = [ 
                    "recharges.recharge_id",
                    "recharges.recharge_operator", 
                    "recharges.recharge_mobile_number", 
                    "recharges.recharge_currency",
                    "recharges.recharge_product_price",
                    "recharges.commission_value",
                    "recharges.user_id",
                    "recharges.recharge_status",
                    "recharges.recharge_payment_status",
                    "recharges.created_at",
                    "users.name",
                    "users.profile_image",
                    "users.country_code",
                    "users.contact_number"
                ],
                limit = databaseConstants.LIMIT,
                response = { "data": { } };
            
                let offset = databaseConstants.OFFSET;
                if(Page !== undefined) {
                    offset = page;
                }
            return RechargeModelObj.getAllRecharges( columns, limit, offset, requestData ).then( ( resdata ) => {
              
                response.code = HttpStatus.OK;
                
                return datetimeObj.changeDatabaseTimestampFormat( resdata, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( ( userData ) => {
                   
                    return RechargeModelObj.getCountRechargesData( countColumn, requestData ).then( ( count ) => {
        
                        response.recordsTotal = count[ 0 ];
                        
                        return responseHelper.encryptobjectAllKeys( userData ).then( ( message ) => {
                            response.data.recharges = message;
                            response.recordsFiltered = count[ 0 ];
                            response.profileImagePath = profileImagePath;
                            response.defaultUserImage = defaultUserImagePath;
            
                            response.s3ProfileImagePath = `${awsS3BaseUrl}${directory}/`;
                            response.s3DefaultUserImage = `${awsS3BaseUrl}${s3PlaceholderImage}`;

                            return response;
                        } ).catch( ( error ) => {
                            throw Boom.badImplementation( localeService.translate( error ) );
                        } );
                    } ).catch( ( error ) => {
                        logger.error( error );
                        throw Boom.badImplementation( localeService.translate( error ) );
                    } );
                } ).catch( ( error ) => {
                    logger.error( error );
                    throw Boom.notImplemented( localeService.translate( error ) );
                } );
            } ).catch( ( err ) => {
                throw Boom.failedDependency( localeService.translate( err ) );
            } ); 
        }else{
            throw Boom.unauthorized( localeService.translate( "INVALID_USER_TYPE" ) );
        }
    } ).catch( (err) => {
        throw Boom.unauthorized( localeService.translate( "INVALID_USER_TYPE" ) );
    } ); 
};

/**
 * Get all recharges for ADMIN.
 *
 * @param  {Object}   request 
 * @returns {Promise}
 */
export const getRechargePaymentList = function( request ) {
  
    const whereData = {
        "user_id": request.user,
        "user_type": dataConstants.USER_TYPE.USER_ADMIN
    };
   
    return UserModelObj.fetchObj( whereData ).then( ( userAdmin ) => {
        
        // check user_type
        if( userAdmin.length > 0 ) {
            const requestData = request.query,
            columns = [ 
                "rp_id",
                "recharge_id", 
                "rp_currency", 
                "rp_charge_id",
                "rp_transaction_id",
                "rp_product_price",
                "rp_payment_status",
                "user_id",
                "created_at"
            ],
            orderBy = "created_at",
            order = "Asc",
            wherePaymentData = {
                "recharge_id": requestData.recharge_id,
            },

            response = { "data": { } };
           
            return RechargePaymentModelObj.fetchObjWithSelectedFields( wherePaymentData, columns, orderBy, order ).then( ( paymentData ) => {
                return datetimeObj.changeDatabaseTimestampFormat( paymentData, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( ( transactionData ) => {
                    return responseHelper.encryptobjectAllKeys( transactionData ).then( ( message ) => {
                        response.code = HttpStatus.OK;
                        response.data.payments = message;
                        
                        return response;
                    } ).catch( ( error ) => {
                        throw Boom.badImplementation( localeService.translate( error ) );
                    } );
                } ).catch( ( error ) => {
                    throw Boom.notImplemented( localeService.translate( error ) );
                } );
            } ).catch( (err) => {
                throw Boom.unauthorized( localeService.translate( err ) );
            } ); 
        }else{
            throw Boom.unauthorized( localeService.translate( "INVALID_USER_TYPE" ) );
        }
    } ).catch( (err) => {
        throw Boom.unauthorized( localeService.translate( "INVALID_USER_TYPE" ) );
    } ); 
};


const refundRechargeAmount = async function(rechargeRefundObj, ChargeID){
    rechargeRefundObj['rp_payment_status'] = statusConstants.RECHARGE_PAYMENT_STATUS.REFUND_INITIATED;
    var recharge_refund_insert_obj = await RechargePaymentModelObj.createObj(rechargeRefundObj, [ "rp_id"]);
    var rechargeRefundStatusObj = {
            "recharge_payment_status":statusConstants.RECHARGE_PAYMENT_STATUS.REFUND_INITIATED
        }

    await RechargeModelObj.updateObj(rechargeRefundStatusObj, {"recharge_id": rechargeRefundObj.recharge_id}, [ "recharge_id"]);

    var refundResponse = await StripeLibObj.refundByChargeID(ChargeID);
    if(refundResponse.status){
       
        var rechargeRefundUpdateObj = {
            "recharge_payment_status":statusConstants.RECHARGE_PAYMENT_STATUS.REFUND_COMPLETED
        }

        var rechargeRefundPayUpdateObj = {
            "rp_transaction_id":refundResponse.data.balance_transaction,
            "rp_payment_status":statusConstants.RECHARGE_PAYMENT_STATUS.REFUND_COMPLETED,
            "rp_response":refundResponse.data
        }

    }else{
        var rechargeRefundUpdateObj = {
            "recharge_payment_status":statusConstants.RECHARGE_PAYMENT_STATUS.REFUND_FAILED
        }

        var rechargeRefundPayUpdateObj = {
            "rp_payment_status":statusConstants.RECHARGE_PAYMENT_STATUS.REFUND_FAILED,
            "rp_response":refundResponse.data
        }
  
    }
    await RechargeModelObj.updateObj(rechargeRefundUpdateObj, {"recharge_id": rechargeRefundObj.recharge_id}, [ "recharge_id"]);
    await RechargePaymentModelObj.updateObj(rechargeRefundPayUpdateObj, {"rp_id": recharge_refund_insert_obj.rp_id}, [ "rp_id"]);
    return true;
}

/**
 * Payment refund by ADMIN.
 *
 * @param  {Object}   request 
 * @returns {Promise}
 */
export const rechargeAmountRefund = async function( request ){
    
    const whereData = {
        "user_id": request.user,
        "user_type": dataConstants.USER_TYPE.USER_ADMIN
    };

    return UserModelObj.fetchObj( whereData ).then( ( userAdmin ) => {
        
        // check user_type
        if( userAdmin.length > 0 ) {
            const requestData = request.body,
            columns = [ 
                "rp_id",
                "recharge_id", 
                "rp_currency", 
                "rp_charge_id",
                "rp_transaction_id",
                "rp_product_price",
                "rp_payment_status",
                "user_id",
                "created_at"
            ],
            orderBy = "rp_id",
            order = "ASC",
            wherePaymentData = {
                "recharge_id": requestData.recharge_id,
            },

            response = { "data": { } };
           
            return RechargePaymentModelObj.fetchObjWithSelectedFields( wherePaymentData, columns, orderBy, order ).then( async ( paymentData ) => {
                return datetimeObj.changeDatabaseTimestampFormat( paymentData, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( async ( transactionData ) => {
                    return responseHelper.encryptobjectAllKeys( transactionData ).then( async ( message ) => {

                        const rechargeRefundObj = {
                            "recharge_id":message[0].recharge_id,
                            "rp_currency":message[0].rp_currency,
                            "rp_product_price":message[0].rp_product_price,
                            "user_id":message[0].user_id
                        },
                        ChargeID = message[0].rp_charge_id; 
                        
                        await refundRechargeAmount(rechargeRefundObj, ChargeID);
                        
                        response.code = HttpStatus.OK;

                        return response;
                    } ).catch( ( error ) => {
                        throw Boom.badImplementation( localeService.translate( error ) );
                    } );
                } ).catch( ( error ) => {
                    throw Boom.notImplemented( localeService.translate( error ) );
                } );
            } ).catch( (err) => {
                throw Boom.unauthorized( localeService.translate( err ) );
            } ); 
        }else{
            throw Boom.unauthorized( localeService.translate( "INVALID_USER_TYPE" ) );
        }
    } ).catch( (err) => {
        throw Boom.unauthorized( localeService.translate( "INVALID_USER_TYPE" ) );
    } ); 
}
/**
* Reset password
* 
* @param {Object} request 
* @returns {Promise}
*/
export const resetPassword = function( request, next ) {
    // Variable declaration
    const requestData = request.body,
    currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
    currentYear = DateTimeUtil.getCurrentYear(),
    nextYear = DateTimeUtil.getNextYear(),
    randomPasswords = password.generateRandomPassword(),
    userType = request.headers['x-user-type'],
     randomstr = randomPasswords.hashPassword ,
    updatePassword = { 
       "password_token" : randomstr,
        "updated_at": currentTime
    },
    updateWhere = { 
        "email": requestData.email,
        "user_type" : userType
    }, 
    fetchColumns = [
         "id"
    ],
    response = { "data": {} };

    
   
    return UserModelObj.fetchObj( updateWhere ).then( ( user ) => {
        // Check user email exist or not
        if ( user.length < 1 ) {
           
            // const data = { "error_code": "EMAIL_NOT_EXIST" };
            
             throw Boom.badRequest( localeService.translate( "Oops ! looking like email is invalid Please try to enter a valid Email or make sure you select the correct user type ." ));
        }
       
        return UserModelObj.updateObj( updatePassword, updateWhere, fetchColumns ).then( () => {
            const mailDetails = {
                "template": "reset-password",
                "to": requestData.email,
                "subject": "Qvaring - Reset password",
                "dynamicFields": {
                    "title": "Qvaring",
                    "cubaring_icon": `${basePath}/public/email_icon/qvaring_logo.png`,
                    "name": `${user[0].name}`,
                    "randomstr": randomstr,
                    "basepath" : `${basePath}/reset-password?token=${randomstr}`,
                    "nextYear": `${nextYear}`,
                    "year": `${currentYear}`
                }
            };
         // console.log("mailDetails", mailDetails)
            // Sent Reset Password mail
            return emailObj.sendEmail( mailDetails ).then( () => {

                // Set response
                response.code = HttpStatus.OK;
                response.message = localeService.translate( "RESET_PASSWORD_MAIL" );

                return response;
            } );
        } ).catch( ( error ) => {
            logger.error( error );
            next( error );
        } );
    } )
    // .catch( ( error ) => {
    //     logger.error( error );
    //     next();
    // } );
}

export const getUserList = async function ( request ){
    var req = request.body,
    userType = request.headers['x-user-type'];
    var user_id  = req.user_id;
    var login_user_id = req.login_user_id;
    var user_name = req.user_name;
    var page = req.page;
    var limit = req.limit;

     const column = [ 
            "user_id",
            "name", 
            "country_code",
            "contact_number",
        ],
     
        response = {};
        const totalCount = await UserModelObj.getNumberSearchCount( req, userType );
        
        return UserModelObj.getNumberSearchList( req, userType ).then(( checkData ) => {
            response.status ="success";
            response.message ="OK";
            response.userList = checkData;
            response.recordsTotal = totalCount[0].count;
            response.recordsFiltered  = totalCount[0].count;
           
            return response;
        }).catch(function(error) {
            throw Boom.serverUnavailable(localeService.translate("FAIL"));
       });
            

}
export const getMyProfile = function ( request ){
    const req = request.body,
        userType = request.headers['x-user-type'],
        userId = req.user_id,
        fetchColumns = [
            "user_id",
            "user_type",
            "status",
            "name",
            "profile_image",
            "email",
            "country_code",
            "contact_number",
            "login_otp",
            "login_otp_expire_at",
            "is_mail_verified",
            "is_mobile_verified",
            "is_profile_done",
            "stripe_customer_id",
            "wallet_id",
            "wallet_amount",
            "admin_commission_amount",
            "verify_id",
            "verify_status",
            "verify_image",
            "user_country_id",
            "api_token",
            "users.created_at",
            "auth.device_id"
        ],
        whereCondition = {
            "user_id": userId
        },
        methodWhere = {
            is_default:1
        },
        columnMethod = ["method"],
        response = { "data": { } },
        errorData ={"error":{}};

     return UserModelObj.getMyProfile( whereCondition, fetchColumns ).then( async ( user ) => {

        const getMethod = await RechargeSettingModelObj.fetchObjWithSingleRecord( methodWhere, columnMethod ); 
       
        if( user != undefined && user != null ) {
            if( getMethod.method =='dtone' ){
               user.rechargeMethod =1; 
            }else{
              user.rechargeMethod =2; 
            }
            if( user.is_profile_done == statusConstants.IS_PROFILE.NOT_COMPLETED) {
                throw Boom.badRequest( localeService.translate( "INVALID_USER" ) );  
            }
        }
       
        const trimProfile = user.profile_image !== null ? user.profile_image.trim() : user.profile_image; 
        
        // set Admin full Profile_image path 
        if( trimProfile == null || trimProfile === "" ) {
            // user.profile_image = `${basePath}/${defaultUserImage}`;
            user.profile_image = `${awsS3BaseUrl}${s3PlaceholderImage}`;
        }else {
            // user.profile_image = `${basePath}/${staticPath}/${fileUploadFolder}/${user.profile_image}`;
            user.profile_image = `${awsS3BaseUrl}${directory}/${user.profile_image}`;
        }
        
        const  trimDocument = user.verify_image !== null ? user.verify_image.trim() : user.verify_image;
      
        if( trimDocument === "" || trimDocument == null ) {
           // verify[ 0 ].verify_image = `${basePath}/${defaultUserImage}`;
        }else {
            // user.verify_image = `${basePath}/${staticPath}/${documentUploadFolder}/${user.verify_image}`;   
            user.verify_image = `${awsS3BaseUrl}${documentUploadFolder}/${user.verify_image}`;
        }
        
        user.wallet_amount = (user.wallet_amount).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
       
        response.code = HttpStatus.OK
        response.status = 'success';
        response.userInfo = user;
        responseHelper.encryptAllKeys( response.userInfo, [ databaseConstants.USER_KEY ] );
      
        return response;
    
      } );
            

}
/**
 * updateProfile User.
 *
 * @param  {Object}  req 
 * @returns {Promise}
 */
export const updateProfile = async function( req ) {
    
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    let image;
    let profileImage;

    if( req.files ) {
        image = req.files.profileImage;
        
        const fileData = await fileUploadObj.uploadFile( image, fileUploadFolder );
       
        if( fileData.name !== "" ) {
            profileImage = fileData.name;
            const s3Data = await S3LibObj.uploadToS3( image, directory, fileData.name );
        } else {
            throw Boom.serverUnavailable( localeService.translate( "FAIL_FILE_UPLOAD" ) );
        }
    }else{
        image = "";
        // profileImage = "";
    }
  
    //    return fileUploadObj.uploadFile( image, fileUploadFolder )
    //     .then( ( fileData ) => {

            // let profileImage;

            const requestData = req.body,
                id = requestData.user_id,
                name = requestData.name,
                email = requestData.email,
                response = { "data": { } };

            const Updatecolumn = {
                "name": name,
                "email": email
            };

            if( profileImage != "" && profileImage != undefined ) {
                Updatecolumn.profile_image = profileImage;
            }

            // if( fileData.name !== "" ) {
            //     profileImage = fileData.name;
            //     Updatecolumn.profile_image = profileImage;
            // }else{
            //     profileImage = "";
            // }

            const fetchColumns = [
            "user_id",
            "user_type",
            "status",
            "name",
            "profile_image",
            "email",
            "country_code",
            "contact_number",
            "is_profile_done",
            "wallet_amount",
            "wallet_id",
            "verify_id",
            "verify_status",
            "users.created_at"
            ];
            const updateWhereCondition = {
                "user_id": id
            };
        return UserModelObj.fetchObj( {  "user_id": id } ).then( async ( user ) => {
             
            if( user[0].profile_image != '' ) {
               
                let deleteImage = `${directory}/${user[0].profile_image}`;
                const s3DeleteData = await S3LibObj.deleteFile( deleteImage );
            }

            return UserModelObj.updateObj( Updatecolumn, updateWhereCondition, ["user_id"] )
                .then( ( updateUserData ) => {
                
                    return UserModelObj.getMyProfile( updateWhereCondition, fetchColumns ).then( ( user ) => {    
                       
                 const trimProfile = user.profile_image !== null ? user.profile_image.trim() : user.profile_image; 
                      
                    // set Admin full Profile_image path 
                    if( trimProfile == null || trimProfile === "" ) {
                        // user.profile_image = `${basePath}/${defaultUserImage}`;
                        user.profile_image = `${awsS3BaseUrl}${s3PlaceholderImage}`;
                    }else {
                        // user.profile_image = `${basePath}/${staticPath}/${fileUploadFolder}/${user.profile_image}`;
                        user.profile_image = `${awsS3BaseUrl}${directory}/${user.profile_image}`;
                    }
                   
                        response.code = HttpStatus.OK;
                        response.data = user;
                        responseHelper.encryptAllKeys( response.data, [ databaseConstants.USER_KEY ] );
                        return response;
                    })
                } ).catch( ( updateErr ) => {
                    // const profileName = req.files.profileImage.name;
                            
                    // fileUploadObj.unlinkFile( profileName, fileUploadFolder );
                    logger.error( updateErr );
                    throw Boom.badRequest( localeService.translate( "FAIL_USER_UPDATE" ) );
                } ); 
            } )
            .catch( ( uploadErr ) => {
                logger.error( uploadErr );
                throw Boom.serverUnavailable( localeService.translate( "INVALID_USER" ) );
            } );
            
        // } )
        // .catch( ( uploadErr ) => {
        //     logger.error( uploadErr );
        //     throw Boom.serverUnavailable( localeService.translate( "FAIL_FILE_UPLOAD" ) );
        // } );
};

/**
* Reset password
* 
* @param {Object} request 
* @returns {Promise}
*/
export const changePassword = function( request ) {
   
    // Variable declaration
    const requestData = request.body,
    currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
    user_id =requestData.user_id,
    old_password = requestData.old_password,
    new_password = password.cryptPassword( requestData.new_password),
    userType = request.headers['x-user-type'],

    updatePassword = {
        "password": new_password,
        "updated_at": currentTime
    },
    updateWhere = { 
        "user_id": user_id
    }, 
    fetchColumns = [
         "user_id"
    ],
    response = { "data": {} };

    if( request.cookies.user_profile != undefined ) {
        var token = request.cookies.user_profile;
        var profileData = JSON.parse(token);
    }
    return UserModelObj.fetchObj( updateWhere ).then( ( user ) => {
       const  isMatch = bcrypt.compareSync( old_password, user[0].password );
        // Check user  exist or not
        if ( !isMatch ) {
           
            throw Boom.badRequest( localeService.translate( "NOT_MATCH_OLD_PASS" ) );
        }
        return UserModelObj.updateObj( updatePassword, updateWhere, fetchColumns ).then( async () => {
                // Set response
                response.code = HttpStatus.OK;
                response.message = localeService.translate( "SUCCESS_UPDATE_PASS" );

                if( request.cookies.user_profile != undefined ) {
                    if(profileData.user){
                    logoutWebuser(request, profileData.user) ;
                        
                    }
                }else{
                    
                   await FirebaseAuthModelObj.deleteObj( {"firebase_auth_user_id": user_id} ).then( updateAuthData => {} ) 
                }

                return response;
          
        } )
    } );
   
}

/**
* create customer
* 
* @param {Object} request 
* @returns {Promise}
*/
export const createCustomer = function( request ) {
   
    // Variable declaration
    const requestData = request.body,
    currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
    user_id =requestData.user_id,
    email = requestData.email,
    userType = request.headers['x-user-type'],
    updateData = {
        
        "updated_at": currentTime
    },
    where = { 
        "user_id": user_id
    }, 
    fetchColumns = [
         "stripe_customer_id"
    ],
    response = { "data": {} };
   
    return UserModelObj.fetchObjWithSingleRecord( where, fetchColumns ).then( async ( user ) => {
       const  customer_id =  user.stripe_customer_id;
        // Check user  exist or not
        if ( customer_id ) {
            
              response.code = HttpStatus.OK;
              response.data = user;
              responseHelper.encryptAllKeys( response.data, [ databaseConstants.USER_KEY ] ); 
             
              return response; 
        }else{
    
            const stripe_id = await StripeLibObj.createCustomer(email);
              updateData.stripe_customer_id = stripe_id;
            return UserModelObj.updateObj( updateData, where, fetchColumns ).then( ( updateRecord ) => {
  
              response.code = HttpStatus.OK;
              response.data = updateRecord[0];
              responseHelper.encryptAllKeys( response.data, [ databaseConstants.USER_KEY ] ); 
                
                return response;
            } );
        }
       

    } );
   
}

/**
 * Get all consomer users for ADMIN.
 *
 * @param  {Object}   request 
 * @returns {Promise}
 */
export const getAllConsumerUser = function( request ) {
    
    const whereData = {
        "user_id": request.user,
        "user_type": dataConstants.USER_TYPE.USER_ADMIN
    };

    return UserModelObj.fetchObj( whereData ).then( ( userAdmin ) => {
        
        // check user_type
        if( userAdmin.length > 0 ) {
            const requestData = request.body,
                page = requestData.start,
                countColumn = [ "user_id" ],
                columns = [ 
                    "users.user_id",
                    "users.user_type",
                    "users.name", 
                    "users.profile_image", 
                    "users.email",
                    "users.country_code",
                    "users.contact_number",
                    "users.is_approval",
                    "users.created_at",
                    "wallets.wallet_id",
                    "wallets.wallet_amount",
                    "verify.verify_image"
                ],
                limit = databaseConstants.LIMIT,
                response = { "data": { } };
    
                let offset = databaseConstants.OFFSET;
                if(Page !== undefined) {
                    offset = page;
                }

            const whereMatch = {
                "user_type": request.headers['x-user-type'],
                "is_profile_done":1
            } 
            
            return UserModelObj.getAllConsumerUser( whereMatch, columns, limit, offset, requestData ).then( ( resdata ) => {
              
                response.code = HttpStatus.OK;
                
                return datetimeObj.changeDatabaseTimestampFormat( resdata, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( ( userData ) => {
                    // return UserModelObj.getCountUsers( countColumn ).then( ( count ) => {
                    return UserModelObj.getCountConsumerUserData( whereMatch, countColumn, requestData ).then( ( count ) => {
                        response.recordsTotal = count[ 0 ];
                        
                        return responseHelper.encryptobjectAllKeys( userData, databaseConstants.USER_KEY ).then( ( message ) => {
                          
                            response.data.users = message;
                            response.recordsFiltered = count[ 0 ];
                            response.profileImagePath = profileImagePath;
                            response.defaultUserImage = defaultUserImagePath;
                            response.verifyImage = verifyImagePath;

                            response.s3ProfileImagePath = `${awsS3BaseUrl}${directory}/`;
                            response.s3DefaultUserImage = `${awsS3BaseUrl}${s3PlaceholderImage}`;
                            response.s3VerifyImage = `${awsS3BaseUrl}${documentUploadFolder}/`;

                            return response;
                        } ).catch( ( error ) => {
                            throw Boom.badImplementation( localeService.translate( error ) );
                        } );
                    } ).catch( ( error ) => {
                        logger.error( error );
                        throw Boom.badImplementation( localeService.translate( error ) );
                    } );
                } ).catch( ( error ) => {
                    logger.error( error );
                    throw Boom.notImplemented( localeService.translate( error ) );
                } );
            } ).catch( ( err ) => {
                throw Boom.failedDependency( localeService.translate( err ) );
            } ); 
        }else{
            throw Boom.unauthorized( localeService.translate( "INVALID_USER_TYPE" ) );
        }
    } ).catch( (err) => {
        throw Boom.unauthorized( localeService.translate( "INVALID_USER_TYPE" ) );
    } ); 
};

/**
 * Get all reseller users for ADMIN.
 *
 * @param  {Object}   request 
 * @returns {Promise}
 */
export const getAllResellerUser = function( request ) {
  
    const whereData = {
        "user_id": request.user,
        "user_type": dataConstants.USER_TYPE.USER_ADMIN
    };

    return UserModelObj.fetchObj( whereData ).then( ( userAdmin ) => {
        
        // check user_type
        if( userAdmin.length > 0 ) {
            const requestData = request.body,
                page = requestData.start,
                countColumn = [ "user_id" ],
                columns = [ 
                    "users.user_id",
                    "users.user_type",
                    "users.name", 
                    "users.profile_image", 
                    "users.email",
                    "users.country_code",
                    "users.contact_number",
                    "users.is_approval",
                    "users.created_at",
                    "wallets.wallet_id",
                    "wallets.wallet_amount",
                ],
                limit = databaseConstants.LIMIT,
                response = { "data": { } };
    
                let offset = databaseConstants.OFFSET;
                if(Page !== undefined) {
                    offset = page;
                }

            const whereMatch = {
                "user_type": request.headers['x-user-type']
            } 
            
            return UserModelObj.getAllResellerUser( whereMatch, columns, limit, offset, requestData ).then( ( resdata ) => {
              
                response.code = HttpStatus.OK;
                
                return datetimeObj.changeDatabaseTimestampFormat( resdata, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( ( userData ) => {
                    // return UserModelObj.getCountUsers( countColumn ).then( ( count ) => {
                    return UserModelObj.getCountResellerUserData( whereMatch, countColumn, requestData ).then( ( count ) => {
                        response.recordsTotal = count[ 0 ];
                        
                        return responseHelper.encryptobjectAllKeys( userData, databaseConstants.USER_KEY ).then( ( message ) => {
                          
                            response.data.users = message;
                            response.recordsFiltered = count[ 0 ];
                            response.profileImagePath = profileImagePath;
                            response.defaultUserImage = defaultUserImagePath;
                            response.s3ProfileImagePath = `${awsS3BaseUrl}${directory}/`;
                            response.s3DefaultUserImage = `${awsS3BaseUrl}${s3PlaceholderImage}`;
                            return response;
                        } ).catch( ( error ) => {
                            throw Boom.badImplementation( localeService.translate( error ) );
                        } );
                    } ).catch( ( error ) => {
                        logger.error( error );
                        throw Boom.badImplementation( localeService.translate( error ) );
                    } );
                } ).catch( ( error ) => {
                    logger.error( error );
                    throw Boom.notImplemented( localeService.translate( error ) );
                } );
            } ).catch( ( err ) => {
                throw Boom.failedDependency( localeService.translate( err ) );
            } ); 
        }else{
            throw Boom.unauthorized( localeService.translate( "INVALID_USER_TYPE" ) );
        }
    } ).catch( (err) => {
        throw Boom.unauthorized( localeService.translate( "INVALID_USER_TYPE" ) );
    } ); 
};

export const getRechargeMethod = function ( request ){
    const req = request.body,
        methodWhere = {
            is_default:1
        },
        columnMethod = ["method"],
        response = { "data": { } },
        errorData ={"error":{}};

     return RechargeSettingModelObj.fetchObj().then( ( method ) => {
    
        response.code = HttpStatus.OK
        response.status = 'success';
        response.methodList = method;
        return response;
        
      } );
            

}
export const setDefaultMethod = async function( request ) {
    
    const requestData = request.body,
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        method_id = requestData.method_id,
        getWwhere = {
            "rs_id":method_id
        },
        columns =["rs_id","method","is_default"],
        response = { "data": { } };
        // update old card
          await RechargeSettingModelObj.updateObj({'is_default':0,"updated_at" :currentTime}, {'is_default':1},columns );
        // set default card
        const setdefault = await RechargeSettingModelObj.updateObj({'is_default':1,"updated_at" :currentTime},getWwhere,columns);
        response.code = HttpStatus.OK;
        response.data = setdefault;
        response.message = "Method set successfully.";

        return response;  
};
/**
 * Send Otp for change contact number.
 *
 * @param  {Object}     requestData 
 * @returns {Promise}
 */
export const changeNumber = function( request ) {
 
    let currentTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body;
    const otp = util.generatOtp( commonConstants.INTEGER_RANDOME_NUMBER, commonConstants.RANDOME_NUMBER_LENGTH ),
        countryCode = requestData.country_code,
        userType = request.headers['x-user-type'],
        contact = requestData.contact_number,
        userContact = countryCode + contact,
        msg = localeService.translate( "TWILIO_VERIFICATION" ) + otp,
        whereData = {
            "country_code": countryCode,
            "contact_number": contact,
            "user_type": userType
        },
        whereNotData ={
       "user_id":requestData.user_id
        },   
        selectData = [ 
            "country_code", 
            "contact_number",
            "user_type", 
            "is_profile_done"
        ],
        selectVerify =["vn_id","verify_otp","verify_otp_expiry_at"],
        response = { "data": { } };
    return UserModelObj.checkEmailForUpdateProfile( whereData, whereNotData, selectData ).then(async ( userData ) => {
       
         // update status is_mobile_verified.
        if ( userData.length > 0 && userData[0].is_profile_done === statusConstants.IS_PROFILE.COMPLETED ) {
            throw Boom.badRequest( localeService.translate( "NUMBER_DUPLICATE" ) );

        }

       return SMSLibObj.sendSms( userContact, msg ).then( async ( twi ) => {
            const checkData = await ContactVerifyModelObj.fetchObjWithSingleRecord(whereNotData,selectVerify);
 
            const otpExpiry = DateTimeUtil.getCurrentWithAddMinutes( commonConstants.OTP_EXP_AT );
            const column = {
                "user_id":requestData.user_id,
                "verify_otp": otp,
                "verify_otp_expiry_at": otpExpiry,
                "updated_at":currentTime
            };
      
            if ( !checkData ) {
                column.country_code = countryCode;
                column.contact_number = contact;
                column.created_at = currentTime;
                //column.status = commonConstants.DEFAULT_USER_STATUS;
                return ContactVerifyModelObj.createObj( column, selectVerify ).then( () => {
                    response.code = HttpStatus.OK;

                    return response;
                } ).catch( ( err ) => {
                    //logger.error( err );
                    throw Boom.badImplementation( localeService.translate( "INTERNAL_SERVER_ERROR" ) );
                } ); 
            }else{
                const updateWhereCondition = {
                  "user_id":requestData.user_id,

                };
                
                return ContactVerifyModelObj.updateObj( column, updateWhereCondition, selectVerify ).then( () => {
                    response.code = HttpStatus.OK;
                    
                    return response;
                } ).catch( ( err ) => {
                    logger.error( err );
                    throw Boom.badImplementation( localeService.translate( "INTERNAL_SERVER_ERROR" ) );
                } );
           }
        } ).catch( ( err ) => {
                   // logger.error( err );
                   throw Boom.badRequest( localeService.translate("INVALID_TO_NUMBER") );
            } );
    // } ).catch( ( err ) => {
    //     logger.error( err );
    //     return err;
        //throw Boom.badImplementation( err );
    } );
};

/**
 * Verify Otp.
 *
 * @param  {Object}  request
 * @returns {Promise}
 */
export const verifyNumber = function( request ) {
    
    // Variable Delaration
    const userType = request.headers['x-user-type'],
          requestData = request.body,
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        fetchColumns = [
            "vn_id",
            "user_id", 
            "verify_otp",
            "verify_otp_expiry_at",
            "country_code",
            "contact_number",
            "created_at"
        ],
        updateColumns = [ "user_id" ],
        updateProperties = {
             "country_code": requestData.country_code,
             "contact_number": requestData.contact_number,
        },
        updateWhereCondition = {
            // "country_code": requestData.user_countrycode,
            // "contact_number": requestData.user_contact,
            "user_id": requestData.user_id
        },
        response = { "data": { } };
     
    return ContactVerifyModelObj.fetchObjWithSingleRecord( updateWhereCondition, fetchColumns ).then( ( user ) => {
        
        const loginOtpExpiry = user.verify_otp_expiry_at,
            loginOtp = user.verify_otp,
            expirytime = datetimeObj.changeFormat( loginOtpExpiry, databaseConstants.TIME_FORMAT );
        
        // Check login otp expiry.
        if ( expirytime < currentTime ) {
            throw Boom.badRequest( localeService.translate( "EXPIRE_OTP" ) );
        }

        // Check OTP
        if ( loginOtp !== requestData.verify_otp ) {
            throw Boom.badRequest( localeService.translate( "INVALID_OTP" ) );
        }
        
          return  UserModelObj.updateObj( updateProperties, updateWhereCondition, updateColumns ).then( ( versuc ) => {  
              
            response.code = HttpStatus.OK;
            response.message =localeService.translate( "CONTACT_CHANGE" );
            return response;
            } ).catch( ( err ) => { 

                throw Boom.badImplementation( localeService.translate( err ) );
            } );
        
            
      
    } );
};

export const stripeFee =  async function( request ) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
   
    const requestData = request.body,
          amount = requestData.amount,
          currency = requestData.currency,
          userType = request.headers['x-user-type'],
          response = { "data": { } };
          response.code = HttpStatus.OK;
          
    const stripe_fee = await StripeLibObj.stripeFeeCalculator( amount, currency );
    
    const stripeFeeTax = await StripeFeeTaxModelObj.fetchObj( { "user_type": userType } );
   
    let totalExtraCharge = Number(stripeFeeTax[0].stripe_fee) + Number(stripeFeeTax[0].tax);    
 
    response.data.stripeFee = stripe_fee;
    response.message = `You would be charged ${totalExtraCharge}% qvaring fee extra to pay through credit card.`;
    // response.message = `You would be charged ${stripeFeeTax[0].stripe_fee}% stripe fee and ${stripeFeeTax[0].tax}% tax qvaring fee extra to pay through credit card.`;
    return response;
};

export const getcommissionSetting =  async function( request ) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
          response = { "data": { } };
          response.code = HttpStatus.OK;
         return SettingModelObj.fetchObj().then( ( method ) => {
               response.status = 'success';
               response.commission = method;
              return response;
        
      } );
   
  
};
export const updateCommission = async function( request ) {
    
    const requestData = request.body,
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        id = requestData.setting_id,
        getWwhere = {
            "setting_id":id
        },
        columns =["setting_id","user_commission","res_dev_commission"],
        updateData ={
           "user_commission":requestData.user_commission,
           "res_dev_commission":requestData.res_dev_commission,
           "updated_at" :currentTime 
        },
        response = { "data": { } };
        // update old card
          await SettingModelObj.updateObj( updateData, getWwhere,columns );
        // set default card
        response.code = HttpStatus.OK;
//response.data = setdefault;
        response.message = "Commission set successfully.";

        return response;  
};
export const getCommissionOperator =  async function( request ) {
  
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
          where = {
            userType : request.headers['x-user-type'],
            view_user_id : Security.decryptId( requestData.view_user_id )
          },
          response = { "data": { } };
           //if(request.headers['x-user-type'] == 4){  
            // var id = requestData.view_user_id;

          //      const countryId = await DeveloperModelObj.fetchFirstObj( {'developer_id':requestData.view_user_id }, ["developer_country_id"] );
          //       where.country_id = countryId.developer_country_id;
          // }else{
                var id = Security.decryptId( requestData.view_user_id );
               const countryId = await UserModelObj.fetchFirstObj( {'user_id':id }, ["user_country_id"] );
               where.country_id = countryId.user_country_id;

          //}
    response.code = HttpStatus.OK;
    
    const totalCount = await CommissionOperatorModelObj.getCommissionOperatorCount( where );
    const country = await CountryModelObj.fetchObj(); 

    return CommissionOperatorModelObj.getCommissionOperator( where ).then( ( operatorList ) => {
      response.data.operatorList = operatorList;
      response.data.country = country;
      response.data.selectCountry = where.country_id;
      response.recordsTotal = totalCount[0].count;
      response.recordsFiltered  = totalCount[0].count;

      return response;
    }) 
};
export const updateCommissionOperator = async function( request ) {
     
    const requestData = request.body,
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        id = requestData.cos_id,
        commission = requestData.commission,
        where = {
            "cos_id":id
        },
        columns =["cos_id"],
        updateData ={
           "cos_commission":commission,
           "updated_at" :currentTime 
        },
        response = { "data": { } };
       
        // update old card
          await CommissionOperatorModelObj.updateObj( updateData, where, columns );
        // set default card
        response.code = HttpStatus.OK;
        response.message = "Commission set successfully.";

        return response;  
};
export const userCommissionSetting =  async function( request ) {
 const requestData = request.body,
          response = { "data": { } };
          
        // if(request.headers['x-user-type'] == 4){
        //     var id = requestData.view_user_id

        // }else{
          var id = Security.decryptId( requestData.view_user_id );
        //}
         const   where = {
           "commission_user_id":  id,
           "commission_user_type":request.headers['x-user-type']
          };
          response.code = HttpStatus.OK;
         return UserCommissionModelObj.fetchObj(where).then( ( method ) => {
               response.status = 'success';
               response.commission = method;
              return response;
        
      } );
};

export const updateUserCommission = async function ( request ) {
        const requestData = request.body,
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        commission = requestData.commission,
        columns =["commission_id"],
        updateData ={
           "commission":commission,
           "updated_at" :currentTime 
        },
        response = { "data": { } };
        // if(request.headers['x-user-type'] == 4){
        //     var id = requestData.view_user_id

        // }else{
          var id = Security.decryptId( requestData.view_user_id );
        //}
        const where = {
           "commission_user_id": id,
           "commission_user_type":request.headers['x-user-type']
          };
        // update old card
         const checkUsr = await UserCommissionModelObj.fetchObj(where);
         if(checkUsr.length>0){
            await UserCommissionModelObj.updateObj( updateData, where, columns );

         }else{
            const createData = {
            "commission_user_id": id,
            "commission":commission,
            "commission_user_type":request.headers['x-user-type'],
            "created_at": currentTime,
            "updated_at": currentTime 
            };
            await UserCommissionModelObj.createObj( createData,["commission_id"]  );

         }
        // // set default card
        response.code = HttpStatus.OK;
        response.message = "Commission set successfully.";

        return response;

}
export const getCountryWiseOperator =  async function( request ) {
    const requestData = request.body,
          where = {
            userType : request.headers['x-user-type'],
            view_user_id : requestData.view_user_id,
            country_id:requestData.country_id
          },
          response = { "data": { } };
          response.code = HttpStatus.OK;
    const totalCount = await CommissionOperatorModelObj.getCommissionOperatorCount( where );
    return CommissionOperatorModelObj.getCommissionOperator( where ).then( ( operatorList ) => {
      response.data.operatorList = operatorList;
      response.recordsTotal = totalCount[0].count;
      response.recordsFiltered  = totalCount[0].count;

      return response;
    }) 
};

export const getDeveloperIp =  function( request ) {
    const requestData = request.body,
          where = {
            developer_id: Security.decryptId(requestData.developer_id)
          },
          response = { "data": { } };
          response.code = HttpStatus.OK;
    return DeveloperIPModelObj.fetchObj( where ).then( ( ipList ) => {
      response.data.ipList = ipList;
      return response;
    }) 
};

/**
 * Send app link to mobile number created by. @heamnt_solanki
 *
 * @param  {Object}     requestData 
 * @returns {Promise}
 */
export const sendAppLink = function( request ) {
    
    //"SEND_APP_LINK": "Click on this link and install cubaring application in your mobile for recharge and tranfer money:"
    let currentTime = DateTimeUtil.getCurrentTimeObjForDB();
    let msg;
    const requestData = request.body;
    //  otp = util.generatOtp( commonConstants.INTEGER_RANDOME_NUMBER, commonConstants.RANDOME_NUMBER_LENGTH ),
    const countryCode = requestData.country_code,
        deviceType = requestData.device_type,
        contact = requestData.contact_number,
        userContact = countryCode + contact,
        //msg = localeService.translate( "TWILIO_VERIFICATION" ),
        column = {
            "country_code": countryCode,
            "contact_number": contact,
            "device_type": deviceType,
            "created_at": currentTime,
            "updated_at": currentTime,
        },
        selectData = [ 
            "country_code", 
            "contact_number",
            "device_type"
        ],
        response = { "data": { } };

    if( deviceType == 1 ) {
        msg = requestData.message + '  ' +'https://play.google.com/store/apps/details?id=com.cubaring';
    }
    if( deviceType == 2 ) {
        msg = requestData.message + '  ' +'https://apps.apple.com/us/app/cubaring/id1493044955?ls=1';
    }
    if( deviceType == 3 ) {
        msg = requestData.message +' '+"Anroid Link:"+' '+'https://play.google.com/store/apps/details?id=com.cubaring'+' '+"Apple Link:"+' '+'https://apps.apple.com/us/app/cubaring/id1493044955?ls=1';
    }

    return SMSLibObj.sendSms( userContact, msg ).then( () => {
        column.message = msg;
        return AppUrlSendModelObj.createObj( column, selectData ).then( () => {
            response.code = HttpStatus.OK;

            return response;
        } ).catch( ( err ) => {
            logger.error( err );
            throw Boom.badImplementation( localeService.translate( "INTERNAL_SERVER_ERROR" ) );
        } ); 
        
    } ).catch( ( err ) => {
                // logger.error( err );
                throw Boom.badRequest( localeService.translate("INVALID_TO_NUMBER") );
        } );
};

/**
 * Send app link to mobile number created by. @heamnt_solanki
 *
 * @param  {Object}     requestData 
 * @returns {Promise}
 */
export const sendNotification = function( request ) {
    
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
    token = request.cookies.admin_profile,
    profileData = JSON.parse(token),
    adminId = Security.decryptId( profileData.user.user_id),
    userId = Security.decryptId( requestData.user_id),
    fetchWhere = { "auth_user_id": userId },
    notificationColumn = {
        "notification_by": adminId,
        "notification_for": userId,
        "notification_type": 6,
        "read_status": 2,
        "notification_status": 2,
        "delete_status": 2,
        "created_at": currentDateTime,
    },
    selectNotificationData = [
        "notification_by",
        "notification_for",
        "notification_type" 
    ],
    notificationType = 'Limited recharge',
    //senderUserName = profileData.user.name,
    byUserId = parseInt(adminId),
    response = { "data": { } };
    
    return AuthModelObj.fetchObj( fetchWhere )
        .then( ( userDetail ) => {
            var deviceToken = userDetail[0].auth_firebase_token;
            // var deviceToken = 'ejqatVk6QPaGTtrEe-cO3S:APA91bGsyAwsqY_rqAxW4PbE67AVnCGMtUdhXPJ6rHmwOD0tVXP8c1XATn66_wmkaHBmRZUuWaRqEUtvp_fcQ6DkuoqUxL71llXG9SdMdGQQjlLlbFfW31pvzcla2RukK5DAYX7IwQLb';//userDetail[0].auth_firebase_token;
            var referenceId = '';
            var notification = {
                recharge_id: referenceId,
                by_user_id: byUserId,
                notification_type: notificationType,
                click_action: "MainActivity",
                title: 'Limited recharge',
                body: 'You have reached your maximum instant recharge limit of the month, Verify yourself for hassle free recharge experience'
            }
            
            var objSavePayload = JSON.stringify(notification);
            firebaseLibObj.sendNotification(deviceToken, notification);
                notificationColumn.notification_msg = "You have reached your maximum instant recharge limit of the month, Verify yourself for hassle free recharge experience";
                notificationColumn.notification_payload = objSavePayload;

                return NotificationModelObj.createObj( notificationColumn, selectNotificationData )
                    .then( ( ) => {
                        
                        response.code = HttpStatus.OK;
                        response.message = localeService.translate( "Notification sent successfully" );
                        
                        return response;
                } ).catch( ( error ) => {
                    throw Boom.badImplementation( localeService.translate( error ) );
                } );
            }).catch( ( updateErr ) => {
                throw Boom.badImplementation( localeService.translate( updateErr ) );
            } );
}

/**
 * Get dashboard data created by @hemant_solanki
 */

export const getDashboardData = function( request ) {
    let columnName = [
        "user_id"
    ],
    countConsumerWhere = { "user_type": dataConstants.USER_TYPE.USER_CONSUMER, "is_profile_done": 1 },
    countResellerWhere = { "user_type": dataConstants.USER_TYPE.USER_RESELLER, "is_profile_done": 1 },
    response = { "data": { } };

    return UserModelObj.getCount( countResellerWhere, columnName ).then( ( resellerCount ) => {
        
        return UserModelObj.getCount( countConsumerWhere, columnName ).then( ( userCount ) => {
            columnName = [ "msg_id" ];
            return MessageModelObj.getCount( {}, columnName ).then( ( msgCount ) => {
                columnName = [ "call_id" ];
                return CallModelObj.getCount( {}, columnName ).then( async ( callCount ) => {
                    const devCount = await UserModelObj.getCount({"user_type":4, "is_profile_done": 1},["user_id"]);
                    const usrReg = await RechargeModelObj.getCount({},["recharge_id"]);
                    const devReg = await DeveloperRechargeObj.getCount({},["dav_recharge_id"]);
                    const devSum = await AuthModelObj.getSum('wallets','admin_commission_amount');

                    response.code = HttpStatus.OK;
                   
                    response.data.totalUser = userCount[0];
                    response.data.totalReseller = resellerCount[0];
                    response.data.totalMessage = msgCount[0];
                    response.data.totalCall = callCount[0];
                    response.data.totalDeveloper = devCount[0];
                    response.data.totalProfit =Number( devSum[0].profit_sum ).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
                    response.data.totalRecharge = Number( usrReg[0] ) +Number( devReg[0] );

                    return response;
                } ).catch( ( error ) => {
                    throw Boom.badImplementation( localeService.translate( error ) );
                } );
            } ).catch( ( error ) => {
                throw Boom.badImplementation( localeService.translate( error ) );
            } );
        } ).catch( ( error ) => {
            throw Boom.badImplementation( localeService.translate( error ) );
        } );
    } ).catch( ( error ) => {
        throw Boom.badImplementation( localeService.translate( error ) );
    } );
}


export const getAdminDetail = function( req ) {
    // Variable Declarations
    const requestData = req.body,
        userId = Security.decryptId( requestData.user_id ),
        fetchColumns = [
            "user_id",
            "status",
            "name",
            "profile_image",
            "email",
            "country_code",
            "contact_number",
            "is_mail_verified",
            "is_mobile_verified",
            "is_profile_done",
            "user_type",
            "user_country_id"
        ],
        whereCondition = {
            "user_id": userId
        },
        response = { "data": { } };

    return UserModelObj.fetchObjWithSingleRecord( whereCondition, fetchColumns ).then( ( user ) => {

        const trimProfile = user.profile_image !== null ? user.profile_image.trim() : user.profile_image;

        if( trimProfile == null || trimProfile === "" ) {
            user.profile_image = `${basePath}/${defaultUserImage}`;
        }else {
            user.profile_image = `${basePath}/${staticPath}/${fileUploadFolder}/${user.profile_image}`;
        }
        
        response.code = HttpStatus.OK;
        response.data = user;
        responseHelper.encryptAllKeys( response.data, [ databaseConstants.USER_KEY ] );
        return response;
    } ).catch( ( error ) => {
        throw Boom.badImplementation( localeService.translate( error ) );
    } ); 
};

//Get user for transaction history in admin
export const getUserData = function ( request ){
    
    const req = request.body,
        response = { "data": { } };

    return UserModelObj.getUserData( req ).then( user => {
        response.code = HttpStatus.OK
        response.userList = user;
        
        return response;
    } ).catch( ( error ) => {
         throw Boom.badImplementation( localeService.translate( error ) );
      } );
            

}

/**
 * update Admin Profile.
 *
 * @param  {Object}  req 
 * @returns {Promise}
 */
export const updateAdminProfile = function( req ) {
    
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    // let image;

    // if( req.files ) {
    //     image = req.files.profileImage;
    // }else{
    //     image = "";
    // }

    //    return fileUploadObj.uploadFile( image, fileUploadFolder )
    //     .then( ( fileData ) => {
            let profileImage;

            const requestData = req.body,
                id = Security.decryptId( requestData.user_id),
                name = requestData.name,
                email = requestData.email,
                phone = requestData.contact_number,
                countryCode = requestData.country_code,
                countryDial = requestData.country_dial,
                response = { "data": { } };

            const Updatecolumn = {
                "name": name,
                "email": email,
                "contact_number": phone,
                "country_code": countryDial,
                "user_country_id":countryCode,
            };

            // if( fileData.name !== "" ) {
            //     profileImage = fileData.name;
            //     Updatecolumn.profile_image = profileImage;
            // }else{
            //     profileImage = "";
            // }

            const fetchColumns = [
                "user_id",
                "user_type",
                "status",
                "name",
                "profile_image",
                "email",
                "country_code",
                "contact_number",
                "user_country_id",
                "is_profile_done",
            ];

            const updateWhereCondition = {
                "user_id": id
            };

            return UserModelObj.updateObj( Updatecolumn, updateWhereCondition, fetchColumns )
                .then( ( user ) => {
                
                    const trimProfile = user[0].profile_image !== null ? user[0].profile_image.trim() : user[0].profile_image; 
                      
                    // set Admin full Profile_image path 
                    if( trimProfile == null || trimProfile === "" ) {
                        user[0].profile_image = `${basePath}/${defaultUserImage}`;
                    }else {
                        user[0].profile_image = `${basePath}/${staticPath}/${fileUploadFolder}/${user[0].profile_image}`;
                    }
                    
                        response.code = HttpStatus.OK;
                        response.data.user = user[0];
                        // response.token = Security.getUserAuthToken( user );
                        responseHelper.encryptAllKeys( response.data.user, [ databaseConstants.USER_KEY ] );
                        
                        return response;
                    
                } ).catch( ( updateErr ) => {
                    // const profileName = req.files.profileImage.name;
                            
                    // fileUploadObj.unlinkFile( profileName, fileUploadFolder );
                    logger.error( updateErr );
                    throw Boom.badRequest( localeService.translate( "FAIL_USER_UPDATE" ) );
                } ); 
 
};

/**
 * update user profile by admin.
 *
 * @param  {Object}  req 
 * @returns {Promise}
 */
export const updateUserProfileByAdmin = function( req ) {
   
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    // let image;

    // if( req.files ) {
    //     image = req.files.profileImage;
    // }else{
    //     image = "";
    // }

    //    return fileUploadObj.uploadFile( image, fileUploadFolder )
    //     .then( ( fileData ) => {
            // let profileImage;

            const requestData = req.body,
                id = Security.decryptId( requestData.update_user_id),
                userType = requestData.user_type,
                name = requestData.name,
                email = requestData.email,
                // newPassword = password.cryptPassword(requestData.new_password),
                newPassword = requestData.new_password,
                response = { "data": { } };
            
            
            const Updatecolumn = {
                "name": name,
                "email": email,
                // "password": newPassword
            };

            // if( fileData.name !== "" ) {
            //     profileImage = fileData.name;
            //     Updatecolumn.profile_image = profileImage;
            // }else{
            //     profileImage = "";
            // }

            const fetchColumns = [
                "user_id",
                "user_type",
                "status",
                "name",
                "profile_image",
                "email",
                "country_code",
                "contact_number",
                "user_country_id",
                "is_profile_done",
            ];

            const updateWhereCondition = {
                "user_id": id
            };

            const whereCondition = {
                "user_id": id,
                "user_type": userType,
                "email": email
            };
            
            if( newPassword != undefined && newPassword != "" ) {
                Updatecolumn.password = password.cryptPassword(newPassword);
            }

            // return UserModelObj.fetchObj( whereCondition ).then( ( userDetail ) => {
            return UserModelObj.checkEmailIsExist( whereCondition ).then( ( userDetail ) => {
                if( userDetail.length > 0 ) {
                    throw Boom.badRequest( localeService.translate( "EMAIL_ALREADY_EXIST" ) );
                }
                return UserModelObj.updateObj( Updatecolumn, updateWhereCondition, fetchColumns )
                    .then( ( user ) => {
                    
                        const trimProfile = user[0].profile_image !== null ? user[0].profile_image.trim() : user[0].profile_image; 
                        
                        // set Admin full Profile_image path 
                        if( trimProfile == null || trimProfile === "" ) {
                            user[0].profile_image = `${basePath}/${defaultUserImage}`;
                        }else {
                            user[0].profile_image = `${basePath}/${staticPath}/${fileUploadFolder}/${user[0].profile_image}`;
                        }
                        
                            response.code = HttpStatus.OK;
                            response.data.user = user[0];
                            response.message = 'Profile updated successfully.'
                            responseHelper.encryptAllKeys( response.data.user, [ databaseConstants.USER_KEY ] );
                            
                            return response;
                        
                } ).catch( ( updateErr ) => {
                     logger.error( updateErr );
                    throw Boom.badRequest( localeService.translate( "FAIL_USER_UPDATE" ) );
                } ); 
            } )
           
    
};

/**
 * get stripe fee and tax detail
 */
export const getStripeFeeTax = function ( request, userType ){
   
    if( userType == 4 ) {
        userType = 3;
    }
    
    const response = { "data": { } };

    return StripeFeeTaxModelObj.fetchObj( { "user_type": userType } ).then( stripetaxDetail => {
        
        response.code = HttpStatus.OK
        response.stripetaxDetail = stripetaxDetail;
        
        return response;
    } ).catch( ( error ) => {
         throw Boom.badImplementation( localeService.translate( error ) );
      } );       
}

//Update stripe fee tax
export const updateStripeFeeTax = function ( request ){
    
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();

    const requestData = request.body,
            stripefeeTaxId = requestData.stripefeeTaxId,
            userType = requestData.userType,
            stripeFee = requestData.stripeFee,
            tax = requestData.tax,
            response = { "data": { } };

    const Updatecolumn = {
        "stripe_fee": stripeFee,
        "tax": tax,
        "updated_at" : currentDateTime
        
    };

    const updateWhereCondition = {
        "stripefee_tax_id": stripefeeTaxId,
        "user_type": userType
    };

    const fetchColumns = [
        "stripefee_tax_id",
        "user_type",
        "stripe_fee",
        "tax"
    ];

    return StripeFeeTaxModelObj.updateObj( Updatecolumn, updateWhereCondition, fetchColumns ).then( stripetaxDetail => {
        
        response.code = HttpStatus.OK
        response.message = "Record updated successfully.";
        response.stripetaxDetail = stripetaxDetail[0];

        return response;
    } ).catch( ( error ) => {
         throw Boom.badImplementation( localeService.translate( error ) );
      } );       
}


export const deleteUserFirebaseAuthData = function ( request, profileData ){
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const userId = Security.decryptId(profileData.user_id),
        where = {
            // "device_id": profileData.device_id,
            "firebase_auth_user_id" : userId
        },
        Updatecolumn = {
            "device_id": null,
            "auth_firebase_token": null,
            "updated_at" : currentDateTime
        },
        updateWhereCondition = {
            "auth_user_id": userId,
        },
        fetchColumns = [
            "auth_id",
            "auth_user_id"  
        ],
        response = { "data": { } };
   
    return FirebaseAuthModelObj.deleteObj( where ).then( ( deleteRecord ) => {
       
        return AuthModelObj.updateObj( Updatecolumn, updateWhereCondition, fetchColumns ).then( updateAuthData => {
        
            response.code = HttpStatus.OK;
            return response;
        } ).catch( ( error ) => {
            throw Boom.badImplementation( localeService.translate( error ) );
        } );
    } ).catch( ( error ) => {
        throw Boom.badImplementation( localeService.translate( error ) );
    } );  
}


/**
 * Get transfer limit for admin created_by @Vihag_kumar
 * 
 * @param {*} request 
 * @param {*} userType 
 */
export const getTransferLimits = function ( request, userType ){
    
    const response = { "data": { } };

    return SettingModelObj.fetchObj( {} ).then( stripetaxDetail => {
        
        response.code = HttpStatus.OK
        response.stripetaxDetail = stripetaxDetail;
        
        return response;
    } ).catch( ( error ) => {
         throw Boom.badImplementation( localeService.translate( error ) );
      } );       
}

/**
 * Get transfer limit for admin created_by @Vihag_kumar
 * 
 * @param {*} request 
 * @param {*} userType 
 */
export const updateTransferLimit = function ( request ){
    
    const requestData = request.body,
        currentDateTime = DateTimeUtil.getCurrentTimeObjForDB(),
        userType = parseInt( requestData.userType ),
        updateSettingData = {
            "updated_at": currentDateTime
        },
        updateWhere = {
            "setting_id": requestData.transferLimitId
        },
        fetchColumns = [
            "setting_id",
        ],
        response = { "data": { } };

    if( userType == 1 ) {
        updateSettingData.user_transfer_amount = requestData.maxTransferAmount;
        updateSettingData.user_transfer_limit = requestData.maxTransferLimit;
    } else {
        updateSettingData.reseller_transfer_amount = requestData.maxTransferAmount;
        updateSettingData.reseller_transfer_limit = requestData.maxTransferLimit;
    }

    return SettingModelObj.updateObj( updateSettingData, updateWhere, fetchColumns ).then( ( stripetaxDetail ) => {
        
        // Set response.
        response.code = HttpStatus.OK
        response.message = "Updated successfully.";
        response.transferLimitsDetail = stripetaxDetail;
        
        return response;
    } ).catch( ( error ) => {
        throw Boom.badImplementation( localeService.translate( error ) );
    } );       
}


/**
* update user language
* 
* @param {Object} request 
* @returns {Promise}
*/
export const updateLanguage = function( request, user ) {
    // Variable declaration
    const requestData = request.body,
    currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
    user_id =user,
    language = requestData.lang,
    
    Updatecolumn = {
        "language": language,
        "updated_at": currentTime
    },
    updateWhereCondition = { 
        "id": user_id
    }, 
    fetchColumns = [
         "id"
    ],
    response = { "data": {} };
    
    return UserModelObj.updateObj( Updatecolumn, updateWhereCondition, fetchColumns ).then( ( user ) => {
        
        return response.data = user ;
    
    } );
   
}



/**
* get user info vial link
* 
* @param {Object} request 
* @returns {Promise}
*/
export const updatePassword = function( request) {
    // Variable declaration
    const requestData = request.query,
    
    password_token  = requestData.token,
    response = { "data": {} };
   
    return UserModelObj.fetchObj( { "password_token": password_token } ).then( ( user ) => {
        
        return response.user = user;
    } );
    
   // } );
   
}


/**
* update password via mail
* 
* @param {Object} request 
* @returns {Promise}
*/
export const updateUserPassword = function( request, next) {
    // Variable declaration
   
    const requestData = request.body,
    currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
    email = requestData.email,
    type  = requestData.user_type,
    password_token  = requestData.password_token,
    passwords = password.cryptPassword( requestData.password) ,
    response = { "data": {} };
    
   const Updatecolumn = {
        "password": passwords,
        "password_token" : "",
        "updated_at": currentTime
    },
     updateWhereCondition = { 
        "user_type": type,
        "email" :email,
        "password_token" :password_token
        
    },
     fetchColumns = [
         "id",
         "password"
    ];

    return UserModelObj.fetchObj( updateWhereCondition ).then( ( user ) => {
       if(user.length < 1){
        return "link_expired";
       }else
        if(user[0].password_token == null || user[0].password_token == ''){
           
            return "link_expired";
        }else{
            return  UserModelObj.updateObj( Updatecolumn, updateWhereCondition, fetchColumns )
                    .then( async ( updateUserData ) => {  
              
                  await FirebaseAuthModelObj.deleteObj( {"user_id": updateUserData[0].id} ).then( updateAuthData => {} )        
                return response.user = updateUserData;
            } );
        }
        
    })
    
   // } );
   
}

/**
 * Get a user device.
 * Add by durgesh
 * @param  {Number|String}  userId ,deviceid
 * @returns {Promise}
 */
export const checkDeviceId = function( userId , deviceId) {
  
    return FirebaseAuthModelObj.fetchObj( { "firebase_auth_user_id": userId, "device_id": deviceId } ).then( ( user ) => {
        if ( user.length < 1 ) {
            throw Boom.notFound( localeService.translate( "USER_NOT_FOUND" ) );
        }
        return user;
    } );
};

var logoutWebuser = function ( request, profileData ){
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const userId = Security.decryptId(profileData.user_id),
        where = {
            "firebase_auth_user_id": userId,
        },
        Updatecolumn = {
            "device_id": null,
            "auth_firebase_token": null,
            "updated_at" : currentDateTime
        },
        updateWhereCondition = {
            "auth_user_id": userId,
        },
        fetchColumns = [
            "auth_id",
            "auth_user_id"  
        ],
        response = { "data": { } };
    
     FirebaseAuthModelObj.deleteObj( where ).then( ( deleteRecord ) => {
       
         AuthModelObj.updateObj( Updatecolumn, updateWhereCondition, fetchColumns ).then( updateAuthData => {
        
            response.code = HttpStatus.OK;
            
             response;
        } )
    } )
}


//Check user exist or not 
export const is_exist_user = function ( request, type){
   
    const req = request.body,
        response = { "data": { } };

    return UserModelObj.fetchObj( { "email": request.email, "user_type" : type } ).then( async user => {
        const is_number = await UserModelObj.fetchObj( { "email": request.email, "user_type" : type } );
        response.code = HttpStatus.OK
      
        response.is_email = user;
        response.is_number = user;
        
        return response;
    } ).catch( ( error ) => {
         throw Boom.badImplementation( localeService.translate( error ) );
    } );
            

}

