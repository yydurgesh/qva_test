import Boom from "boom";
import { DateTime } from "~/libraries/DateTime";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
import CommonFunction from "~/utils/CommonFunctions";
import { DingRechargeLib } from "~/libraries/DingRechargeLib";
import { convertData } from "~/utils/convertData";
import RechargeModel from "~/v1/models/RechargeModel";
import DingeModel from "~/v1/models/DingModel";

import HttpStatus from "http-status-codes";
var _ = require('lodash');
//new-work
import AWS from "aws-sdk";

//new work
import SettingModel from "~/v1/models/SettingModel";
import CommissionOperatorModel from "~/v1/models/CommissionOperatorModel";
import UserCommissionModel from "~/v1/models/UserCommissionModel";
import DeveloperModel from "~/v1/models/DeveloperModel";
import DeveloperBalanceCommissionModel from "~/v1/models/DeveloperBalanceCommissionModel";
import DeveloperRechargeModel from "~/v1/models/DeveloperRechargeModel";
import DeveloperTransactionModel from "~/v1/models/DeveloperTransactionModel";
import WalletModel from "~/v1/models/WalletModel";
import WalletTransactionModel from "~/v1/models/WalletTransactionModel";
import UserModel from "~/v1/models/UserModel";
import TempRechargeModel from "~/v1/models/TempRechargeModel";

import dataConstants from "~/constants/dataConstants";
import DateTimeUtil from "~/utils/DateTimeUtil";
import logger from "~/utils/logger";
import { json } from "express";

//new work
// Configure the region
AWS.config.update(
    {
        accessKeyId: 'AKIA242OUIV5MZVE5BAD', 
        accessSecretKey: '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',
        region: 'us-west-1'
    }
);
// Create an SQS service object
const sqs = new AWS.SQS({apiVersion: '2012-11-05'});
const queueUrl = "https://sqs.us-west-1.amazonaws.com/749099828602/RechargeTest.fifo";
//new work


const dingRechargeLibObj         = new DingRechargeLib(),
     
      RechargeModelObj           = new RechargeModel(),
      DingeModelobj           = new DingeModel(),
     
      SettingModelObj            = new SettingModel(),
      UserModelObj               = new UserModel(),
      CommissionOperatorModelObj = new CommissionOperatorModel(),
      UserCommissionModelObj     = new UserCommissionModel(),
    
      DeveloperRechargeObj       = new DeveloperRechargeModel(),
      DeveloperTransactionObj    = new DeveloperTransactionModel(),   
      WalletModelObj             = new WalletModel(),
      convertDataObj = new convertData(),
      WalletTransactionObj    = new WalletTransactionModel();
     

export const GetErrorCodeDescriptions = function( request ){
        const requestData = request.body,
        response = { "data": { } };
    return dingRechargeLibObj.GetErrorCodeDescriptions().then( async ( responseData ) => {
        
        response.data = responseData;
        return response;
    } ).catch( ( error ) => {
        throw Boom.badRequest( error );
    } )

};

export const GetRegions = function( request ){
        const requestData = request.body,
        CountryIso = requestData.countryIso,
        response = { "data": { } };
    return dingRechargeLibObj.GetRegions( CountryIso ).then( async ( responseData ) => {
        response.data = responseData;
        return response;
    } ).catch( ( error ) => {
        throw Boom.badRequest( error );
    } )

};

export const GetCountries = function( request ){
        const requestData = request.body,
        response = { "data": { } };
    return dingRechargeLibObj.GetCountries().then( async ( responseData ) => {
        response.data = responseData;
        return response;
    } ).catch( ( error ) => {
        throw Boom.badRequest( error );
    } )

};

export const GetProviders = function( request ){
        const requestData = request.body,
        countryIsos = requestData.countryIso,
        RegionCodes = requestData.regionCodes,
        contact_number = requestData.accountNumber,
        response = { "data": { } };
    return dingRechargeLibObj.GetProviders( countryIsos, RegionCodes, contact_number ).then( async ( responseData ) => {
        response.data = responseData;
        return response;
    } ).catch( ( error ) => {
        throw Boom.badRequest( error );
    } )

};

export const getproducts = function( request ){
        const requestData = request.body,
        countryIsos = requestData.countryIso,
        RegionCodes = requestData.regionCodes,
        contact_number = requestData.accountNumber,
        provider_codes = requestData.providerCodes,
        response = { "data": { } };
    return dingRechargeLibObj.GetProducts( countryIsos, RegionCodes, provider_codes, contact_number ).then( async ( responseData ) => {
        response.data = responseData;
        return response;
    } ).catch( ( error ) => {
        throw Boom.badRequest( error );
    } )

};

export const SendTransferFinal = async function(request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        sku_code = requestData.skuCode,
        send_value = requestData.sendValue,
        contact_number = requestData.accountNumber,
        send_currency_iso = requestData.sendCurrencyIso,
        devWhere = {
            'commission_user_id': requestData.developer_id,
            //'commission_user_type': 4
        },
        createRecharge = {
            "developer_id": requestData.developer_id,
            "sku_code": sku_code,
            "send_value": send_value,
            "send_currency_iso": send_currency_iso,
            "account_number": contact_number,
            "created_at": currentDateTime,
            "updated_at": currentDateTime
        },
        create_dev_trs = {
            "developer_id": requestData.developer_id,
            "dt_type": 2,
            "currency": send_currency_iso,
            "reference_type": 1,
            "payment_status": 2,
            "created_at": currentDateTime,
            "updated_at": currentDateTime

        },
        createWalletTrs = {
            "user_id": requestData.developer_id,
            "wt_currency": send_currency_iso,
            "created_at": currentDateTime,
            "updated_at": currentDateTime

        },
        updateBal = {
            "updated_at": currentDateTime
        },
        updateRecharge = {
            "updated_at": currentDateTime
        },
        response = {
            "data": {}
        };
    const globalSetting = await SettingModelObj.fetchObjWithSingleRecord('', ["res_dev_commission"]);
    
    const devCommission = await UserCommissionModelObj.fetchObjWithSingleRecord(devWhere, ["commission_id", "commission"]);

          const getProvider = await dingRechargeLibObj.GetProviders('', '', contact_number);
          if (getProvider.Items.length == 0) {

              const errRes = {
                  "TransferRecord": {
                      "TransferId": null,
                      "SkuCode": sku_code,
                      "Price": null,
                      "CommissionApplied": 0,
                      "StartedUtc": null,
                      "CompletedUtc": null,
                      "ProcessingState": "Failed",
                      "ReceiptText": null,
                      "ReceiptParams": {},
                      "AccountNumber": contact_number
                  },
                  "ResultCode": 4,
                  "ErrorCodes": [{
                      "Code": "AccountNumberInvalid",
                      "Context": "AccountNumberFailedRegex"
                  }]
              };

              response.data = errRes;
              return response;

          }
          // need to manage else condition
          const porviderCode = getProvider.Items[0].ProviderCode,
                whereOpr = {
                  'operator_id': porviderCode,
                  'cos_user_id': requestData.developer_id,
                  //'cos_user_type':4
              };
          const opratorCom = await CommissionOperatorModelObj.fetchObjWithSingleRecord(whereOpr, ['cos_commission', 'cos_id']);
          

    const devBalance = await WalletModelObj.fetchObjWithSingleRecord({
        'wallet_user_id': requestData.developer_id
    }, ["wallet_id", "wallet_amount", "admin_commission_amount", "ding_commission_amount"]);

    //created by hemant soalnki
    const totalDeveloperRechargeAmount = await RechargeModelObj.getDeveloperRechargeAmount(requestData, currentDateTime);
    const totalRechargelimit = await RechargeModelObj.getDeveloperRechargeLimit(requestData);
    
    if (totalDeveloperRechargeAmount[0].total_developer_recharge_amount == null) {
        totalDeveloperRechargeAmount[0].total_developer_recharge_amount = 0.0;
    }

    if (totalRechargelimit.length > 0) {
        var rechargeLimit = Number(totalRechargelimit[0].recharge_limit_amount);
        var totalRechargeAmount = Number(totalDeveloperRechargeAmount[0].total_developer_recharge_amount) + Number(send_value);

        if (totalRechargeAmount >= rechargeLimit) {
            throw Boom.badRequest("You have reached your maximum instant recharge amount limit of today.");
        }
    }
    //end
    // need to manage else 
    if (devBalance) {
        var apply_commission;
        var commission_type;
       
        if (Number(devBalance.wallet_amount) < send_value) {
            throw Boom.badRequest("insufficient credit, please recharge your account.");
        } else {
         
            const fetchDevRecharge = [
                "dav_recharge_id",
                "sku_code",
                "send_value",
                "commission_value",
                "commission_type"
            ];
            const insert_obj = await DeveloperRechargeObj.createObj(createRecharge, fetchDevRecharge);
         
            var responseData = await dingRechargeLibObj.SendTransfer(sku_code, send_value, send_currency_iso, contact_number);
            if (responseData.ResultCode == 1) {
              
                const getProvider = await dingRechargeLibObj.GetProviders('', '', contact_number);

                // need to manage else condition
                const porviderCode = getProvider.Items[0].ProviderCode,
                    whereOpr = {
                        'operator_id': porviderCode,
                        'cos_user_id': requestData.developer_id,
                        //'cos_user_type':4
                    };

                if (porviderCode == "NUCU") {
                    updateRecharge.recharge_for = 2;
                }    
                const opratorCom = await CommissionOperatorModelObj.fetchObjWithSingleRecord(whereOpr, ['cos_commission', 'cos_id']);

                if( opratorCom.cos_commission > 0 && (opratorCom.cos_commission != null || opratorCom.cos_commission != 'undefined') ) {
                    apply_commission = opratorCom.cos_commission;
                    commission_type = 3;
                    
                } else if( devCommission.commission > 0 ) {
                    apply_commission = devCommission.commission;
                    commission_type = 2;
                    
                } else {
                    apply_commission = globalSetting.res_dev_commission;
                    commission_type = 1;
                    
                }
                 var commissionValue = (responseData.TransferRecord.Price.SendValue * (apply_commission / 100)); // calculate commission
                // working on current balance status  
                const currentBalance = await WalletModelObj.fetchObjWithSingleRecord({
                    'wallet_user_id': requestData.developer_id
                }, ["wallet_id", "wallet_amount", "admin_commission_amount", "ding_commission_amount"]);
                const addDieCom = Number(currentBalance.admin_commission_amount) + Number(commissionValue);

                var dingCommission = currentBalance.ding_commission_amount;
                
                const balance_dud = Number(currentBalance.wallet_amount) - (Number(responseData.TransferRecord.Price.SendValue) - Number(commissionValue));
                
                create_dev_trs.amount = (Number(responseData.TransferRecord.Price.SendValue) - Number(commissionValue));

                //wt trs data
                createWalletTrs.wallet_id = currentBalance.wallet_id;
                createWalletTrs.wt_amount = (Number(responseData.TransferRecord.Price.SendValue) - Number(commissionValue));
                createWalletTrs.wt_reference_type = 1;
                createWalletTrs.wt_payment_status = 3;
                createWalletTrs.wt_status = 1;
                createWalletTrs.wt_type = 2;
                createWalletTrs.wt_user_type = 2;

                dingCommission = Number(dingCommission) + Number(responseData.TransferRecord.CommissionApplied);
                
                updateRecharge.reg_response_data = responseData;
                updateRecharge.dev_recharge_status = 2;
                updateRecharge.commission_value = apply_commission;
                updateRecharge.commission_type = commission_type;
                updateRecharge.operator_id = porviderCode;
              


                //code added by @hemant_Solanki
                if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                    if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                        if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                            updateRecharge.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                        }
                    }

                    if (responseData.TransferRecord.CommissionApplied != null && responseData.TransferRecord.CommissionApplied != undefined) {

                        const rechargeCommissionValue = (responseData.TransferRecord.Price.SendValue * (apply_commission / 100));

                        updateRecharge.admin_commission_applied = rechargeCommissionValue;

                        updateRecharge.ding_commission_applied = responseData.TransferRecord.CommissionApplied;

                        updateRecharge.actual_commission = Number(responseData.TransferRecord.CommissionApplied) - Number(rechargeCommissionValue);

                    }
                }

                //code added by @hemant_Solanki
                await DeveloperRechargeObj.updateObj(updateRecharge, {
                    'dav_recharge_id': insert_obj.dav_recharge_id
                }, ["dav_recharge_id"]);
               
                updateBal.wallet_amount = balance_dud;
                updateBal.admin_commission_amount = addDieCom;
                updateBal.ding_commission_amount = dingCommission;

                create_dev_trs.reference_id = insert_obj.dav_recharge_id;

                createWalletTrs.wt_reference_id = insert_obj.dav_recharge_id;

                createWalletTrs.wt_pre_amount = currentBalance.wallet_amount;
                createWalletTrs.wt_balance = balance_dud;

                await WalletTransactionObj.createObj(createWalletTrs, ["wt_id"]);

                await DeveloperTransactionObj.createObj(create_dev_trs, ["dt_id"]);
                await WalletModelObj.updateObj(updateBal, {
                    'wallet_id': currentBalance.wallet_id
                }, ["wallet_id"]);
               
                responseData.OwnCredit = balance_dud;
                responseData.OwnDiscount = commissionValue;
                delete responseData.TransferRecord.TransferId.DistributorRef;
                if (responseData.TransferRecord != null) {
                    if (responseData.TransferRecord.CommissionApplied != null) {
                        delete responseData.TransferRecord.CommissionApplied
                    }
                }
                response.data = responseData;
            } else {
                // delete responseData.stack;
                // delete responseData.config;
                updateRecharge.reg_response_data = responseData.response.data;
                updateRecharge.dev_recharge_status = 3;

                //code added by @hemant_Solanki
                if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                    if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                        if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                            updateRecharge.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                        }
                    }
                }
                //code added by @hemant_Solanki

                await DeveloperRechargeObj.updateObj(updateRecharge, {
                    'dav_recharge_id': insert_obj.dav_recharge_id
                }, ["dav_recharge_id"]);

                if (responseData.TransferRecord != null) {
                    if (responseData.TransferRecord.CommissionApplied != null) {
                        delete responseData.TransferRecord.CommissionApplied;
                    }
                }
                response.data = responseData.response.data;
            }
        }
    }else{
        throw Boom.badRequest("insufficient credit, please recharge your account.");
    }
    return response;
};

export const SendTransfer = async function(request) {
    
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        sku_code = requestData.skuCode,
        send_value = requestData.sendValue,
        contact_number = requestData.accountNumber,
        send_currency_iso = requestData.sendCurrencyIso,
        devWhere = {
            'commission_user_id': requestData.developer_id,
            //'commission_user_type': 4
        },
        createRecharge = {
            "developer_id": requestData.developer_id,
            "sku_code": sku_code,
            "send_value": send_value,
            "send_currency_iso": send_currency_iso,
            "account_number": contact_number,
            "created_at": currentDateTime,
            "updated_at": currentDateTime
        },
        create_dev_trs = {
            "developer_id": requestData.developer_id,
            "dt_type": 2,
            "currency": send_currency_iso,
            "reference_type": 1,
            "payment_status": 2,
            "created_at": currentDateTime,
            "updated_at": currentDateTime

        },
        createWalletTrs = {
            "user_id": requestData.developer_id,
            "wt_currency": send_currency_iso,
            "created_at": currentDateTime,
            "updated_at": currentDateTime

        },
        updateBal = {
            "updated_at": currentDateTime
        },
        updateRecharge = {
            "updated_at": currentDateTime
        },
        // createTempRecharge =  {
        //     "developer_id": requestData.developer_id,
        //     "send_value": send_value,
        //     "account_number": contact_number,
        //     "created_at": currentDateTime,
        //     "updated_at": currentDateTime
        // },
        // fetchTempRecharge = [
        //     "temp_recharge_id",
        //     "recharge_status"
        // ],
        response = {
            "data": {}
        };
    //Code for insert all request for reharge start
    // const temp_rech_insert_obj = await TempRechargeModelObj.createObj(createTempRecharge, fetchTempRecharge);
    //Code for insert all request for reharge end
    
    const globalSetting = await SettingModelObj.fetchObjWithSingleRecord('', ["res_dev_commission"]);
    
    const devCommission = await UserCommissionModelObj.fetchObjWithSingleRecord(devWhere, ["commission_id", "commission"]);
   
          const getProvider = await dingRechargeLibObj.GetProviders('', '', contact_number);
          if (getProvider.Items.length == 0) {

              const errRes = {
                  "TransferRecord": {
                      "TransferId": null,
                      "SkuCode": sku_code,
                      "Price": null,
                      "CommissionApplied": 0,
                      "StartedUtc": null,
                      "CompletedUtc": null,
                      "ProcessingState": "Failed",
                      "ReceiptText": null,
                      "ReceiptParams": {},
                      "AccountNumber": contact_number
                  },
                  "ResultCode": 4,
                  "ErrorCodes": [{
                      "Code": "AccountNumberInvalid",
                      "Context": "AccountNumberFailedRegex"
                  }]
              };

              response.data = errRes;
              return response;

          }
          // need to manage else condition
          const porviderCode = getProvider.Items[0].ProviderCode,
                whereOpr = {
                  'operator_id': porviderCode,
                  'cos_user_id': requestData.developer_id,
                  //'cos_user_type':4
              };
          const opratorCom = await CommissionOperatorModelObj.fetchObjWithSingleRecord(whereOpr, ['cos_commission', 'cos_id']);
          
    const devBalance = await WalletModelObj.fetchObjWithSingleRecord({
        'wallet_user_id': requestData.developer_id
    }, ["wallet_id", "wallet_amount", "admin_commission_amount", "ding_commission_amount"]);

    //created by hemant soalnki
    const totalDeveloperRechargeAmount = await RechargeModelObj.getDeveloperRechargeAmount(requestData, currentDateTime);
    const totalRechargelimit = await RechargeModelObj.getDeveloperRechargeLimit(requestData);
    
    if (totalDeveloperRechargeAmount[0].total_developer_recharge_amount == null) {
        totalDeveloperRechargeAmount[0].total_developer_recharge_amount = 0.0;
    }

    if (totalRechargelimit.length > 0) {
        var rechargeLimit = Number(totalRechargelimit[0].recharge_limit_amount);
        var totalRechargeAmount = Number(totalDeveloperRechargeAmount[0].total_developer_recharge_amount) + Number(send_value);

        if (totalRechargeAmount >= rechargeLimit) {
            throw Boom.badRequest("You have reached your maximum instant recharge amount limit of today.");
        }
    }
    //end
    // need to manage else 
    if (devBalance) {
        var apply_commission;
        var commission_type;
       
        if (Number(devBalance.wallet_amount) < send_value) {
            throw Boom.badRequest("insufficient credit, please recharge your account.");
        } else {
         
            const fetchDevRecharge = [
                "dav_recharge_id",
                "sku_code",
                "send_value",
                "commission_value",
                "commission_type"
            ];
            const insert_obj = await DeveloperRechargeObj.createObj(createRecharge, fetchDevRecharge);
         
            var responseData = await dingRechargeLibObj.SendTransfer(sku_code, send_value, send_currency_iso, contact_number);
            if (responseData.ResultCode == 1) {
              
                const getProvider = await dingRechargeLibObj.GetProviders('', '', contact_number);

                // need to manage else condition
                const porviderCode = getProvider.Items[0].ProviderCode,
                    whereOpr = {
                        'operator_id': porviderCode,
                        'cos_user_id': requestData.developer_id,
                        //'cos_user_type':4
                    };

                if (porviderCode == "NUCU") {
                    updateRecharge.recharge_for = 2;
                }    
                const opratorCom = await CommissionOperatorModelObj.fetchObjWithSingleRecord(whereOpr, ['cos_commission', 'cos_id']);

                if( opratorCom.cos_commission != null && opratorCom.cos_commission != 'undefined' ) {
                    apply_commission = opratorCom.cos_commission;
                    commission_type = 3;
                    
                } else if( devCommission && ( opratorCom.cos_commission == null || opratorCom.cos_commission =='undefined' )) {
                    apply_commission = devCommission.commission;
                    commission_type = 2;
                    
                } else {
                    apply_commission = globalSetting.res_dev_commission;
                    commission_type = 1;
                    
                }
                 var commissionValue = (responseData.TransferRecord.Price.SendValue * (apply_commission / 100)); // calculate commission
                // working on current balance status  
                const currentBalance = await WalletModelObj.fetchObjWithSingleRecord({
                    'wallet_user_id': requestData.developer_id
                }, ["wallet_id", "wallet_amount", "admin_commission_amount", "ding_commission_amount"]);
                const addDieCom = Number(currentBalance.admin_commission_amount) + Number(commissionValue);

                var dingCommission = currentBalance.ding_commission_amount;
                
                const balance_dud = Number(currentBalance.wallet_amount) - (Number(responseData.TransferRecord.Price.SendValue) - Number(commissionValue));
                
                create_dev_trs.amount = (Number(responseData.TransferRecord.Price.SendValue) - Number(commissionValue));

                //wt trs data
                createWalletTrs.wallet_id = currentBalance.wallet_id;
                createWalletTrs.wt_amount = (Number(responseData.TransferRecord.Price.SendValue) - Number(commissionValue));
                createWalletTrs.wt_reference_type = 1;
                createWalletTrs.wt_payment_status = 3;
                createWalletTrs.wt_status = 1;
                createWalletTrs.wt_type = 2;
                createWalletTrs.wt_user_type = 2;

                dingCommission = Number(dingCommission) + Number(responseData.TransferRecord.CommissionApplied);
                
                updateRecharge.reg_response_data = responseData;
                updateRecharge.dev_recharge_status = 2;
                updateRecharge.commission_value = apply_commission;
                updateRecharge.commission_type = commission_type;
                updateRecharge.operator_id = porviderCode;
              


                //code added by @hemant_Solanki
                if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                    if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                        if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                            updateRecharge.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                        }
                    }

                    if (responseData.TransferRecord.CommissionApplied != null && responseData.TransferRecord.CommissionApplied != undefined) {

                        const rechargeCommissionValue = (responseData.TransferRecord.Price.SendValue * (apply_commission / 100));

                        updateRecharge.admin_commission_applied = rechargeCommissionValue;

                        updateRecharge.ding_commission_applied = responseData.TransferRecord.CommissionApplied;

                        updateRecharge.actual_commission = Number(responseData.TransferRecord.CommissionApplied) - Number(rechargeCommissionValue);

                    }
                }

                //code added by @hemant_Solanki
                await DeveloperRechargeObj.updateObj(updateRecharge, {
                    'dav_recharge_id': insert_obj.dav_recharge_id
                }, ["dav_recharge_id"]);
               
                updateBal.wallet_amount = balance_dud;
                updateBal.admin_commission_amount = addDieCom;
                updateBal.ding_commission_amount = dingCommission;

                create_dev_trs.reference_id = insert_obj.dav_recharge_id;

                createWalletTrs.wt_reference_id = insert_obj.dav_recharge_id;

                createWalletTrs.wt_pre_amount = currentBalance.wallet_amount;
                createWalletTrs.wt_balance = balance_dud;

                const inster_wallet_tran = await WalletTransactionObj.createObj(createWalletTrs, ["wt_id"]);

                await DeveloperTransactionObj.createObj(create_dev_trs, ["dt_id"]);
                await WalletModelObj.updateObj(updateBal, {
                    'wallet_id': currentBalance.wallet_id
                }, ["wallet_id"]);
               
                //Code for update all request for reharge start
                // const deductedAmunt = (Number(responseData.TransferRecord.Price.SendValue) - Number(commissionValue));
                // const updateTempRecharge = {
                //     "commission_amount": deductedAmunt,
                //     "prev_wallet_amout": currentBalance.wallet_amount,
                //     "dev_recharge_id": insert_obj.dav_recharge_id,
                //     "wall_trans_id": inster_wallet_tran.wt_id,
                //     "recharge_status": 2,
                //     "reg_response_data": responseData,
                //     "current_wallet_amount": balance_dud,
                //     "updated_at": currentDateTime
                // };
                // await TempRechargeModelObj.updateObj(updateTempRecharge, {
                //     'temp_recharge_id': temp_rech_insert_obj.temp_recharge_id
                // }, ["temp_recharge_id"]);    
                //Code for update all request for reharge end

                responseData.OwnCredit = balance_dud;
                responseData.OwnDiscount = commissionValue;
                delete responseData.TransferRecord.TransferId.DistributorRef;
                if (responseData.TransferRecord != null) {
                    if (responseData.TransferRecord.CommissionApplied != null) {
                        delete responseData.TransferRecord.CommissionApplied
                    }
                }
                response.data = responseData;
            } else {
                // delete responseData.stack;
                // delete responseData.config;
                updateRecharge.reg_response_data = responseData.response.data;
                updateRecharge.dev_recharge_status = 3;

                //code added by @hemant_Solanki
                if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                    if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                        if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                            updateRecharge.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                        }
                    }
                }
                //code added by @hemant_Solanki

                await DeveloperRechargeObj.updateObj(updateRecharge, {
                    'dav_recharge_id': insert_obj.dav_recharge_id
                }, ["dav_recharge_id"]);

                //Code for update all request for reharge start
                // const deductedAmunt = (Number(responseData.TransferRecord.Price.SendValue) - Number(commissionValue));
                // const updateTempRecharge = {
                //     "deduct_amount": deductedAmunt,
                //     "dev_recharge_id": insert_obj.dav_recharge_id,
                //     "recharge_status": 3,
                //     "reg_response_data": responseData.response.data,
                //     "updated_at": currentDateTime
                // };
                // await TempRechargeModelObj.updateObj(updateTempRecharge, {
                //     'temp_recharge_id': temp_rech_insert_obj.temp_recharge_id
                // }, ["temp_recharge_id"]);    
                //Code for update all request for reharge end

                if (responseData.TransferRecord != null) {
                    if (responseData.TransferRecord.CommissionApplied != null) {
                        delete responseData.TransferRecord.CommissionApplied;
                    }
                }
                response.data = responseData.response.data;
            }
        }
    }else{
        throw Boom.badRequest("insufficient credit, please recharge your account.");
    }
    return response;
};

export const SendTransfer3112020 = async function( request ){
        let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();

        const requestData = request.body,
        sku_code = requestData.skuCode,
        send_value = requestData.sendValue,
        contact_number = requestData.accountNumber,
        send_currency_iso = requestData.sendCurrencyIso,
        devWhere = {
        'commission_user_id':requestData.developer_id,
        //'commission_user_type': 4
        },
        createRecharge = {
         "developer_id":requestData.developer_id,   
         "sku_code":sku_code,
         "send_value":send_value,
         "send_currency_iso":send_currency_iso,
         "account_number":contact_number,
          "created_at": currentDateTime,
          "updated_at": currentDateTime 
        },
        create_dev_trs = {
          "developer_id":requestData.developer_id,
          "dt_type":2,
          "currency":send_currency_iso,
          "reference_type":1,
          "payment_status":2,
          "created_at": currentDateTime,
          "updated_at": currentDateTime 

        },
        createWalletTrs = {
            "user_id": requestData.developer_id,
            "wt_currency":send_currency_iso,
            "created_at":currentDateTime,
            "updated_at":currentDateTime
           
        },
        updateBal = {
           "updated_at": currentDateTime  
        },
        updateRecharge = {
           "updated_at": currentDateTime  
        },
        response = { "data": { } };
        const globalSetting = await SettingModelObj.fetchObjWithSingleRecord('',["res_dev_commission"]);
        const devCommission = await UserCommissionModelObj.fetchObjWithSingleRecord( devWhere, ["commission_id","commission"] ); 
        const getProvider   = await dingRechargeLibObj.GetProviders( '','', contact_number );
        if(getProvider.Items.length==0){

         const errRes = {
          "TransferRecord":{
                            "TransferId":null,
                            "SkuCode":sku_code,
                            "Price":null,
                            "CommissionApplied":0,
                            "StartedUtc":null,
                            "CompletedUtc":null,
                            "ProcessingState":"Failed",
                            "ReceiptText":null,
                            "ReceiptParams":{},
                            "AccountNumber":contact_number
                          },
                            "ResultCode":4,
                            "ErrorCodes":[{"Code":"AccountNumberInvalid","Context":"AccountNumberFailedRegex"}]
                        };

           response.data = errRes;
           return response;

        }
        const porviderCode  = getProvider.Items[0].ProviderCode,
              whereOpr = {
                'operator_id':porviderCode,
                'cos_user_id':requestData.developer_id,
                //'cos_user_type':4
              };
        const userData    = await UserModelObj.fetchObjWithSingleRecord({ 'user_id':requestData.developer_id },['user_type','status']); 
        const opratorCom    = await CommissionOperatorModelObj.fetchObjWithSingleRecord(whereOpr,['cos_commission','cos_id']); 
        const devBalance    = await WalletModelObj.fetchObjWithSingleRecord({'wallet_user_id':requestData.developer_id},["wallet_id","wallet_amount","admin_commission_amount","ding_commission_amount"]);
        
        //created by hemant soalnki
        const totalDeveloperRechargeAmount = await RechargeModelObj.getDeveloperRechargeAmount( requestData, currentDateTime );
        const totalRechargelimit = await RechargeModelObj.getDeveloperRechargeLimit( requestData );
        
        if( totalDeveloperRechargeAmount[0].total_developer_recharge_amount == null ) {
            totalDeveloperRechargeAmount[0].total_developer_recharge_amount = 0.0;
        }

        if( totalRechargelimit.length > 0 ) {
            var rechargeLimit = Number(totalRechargelimit[0].recharge_limit_amount);
            var totalRechargeAmount = Number(totalDeveloperRechargeAmount[0].total_developer_recharge_amount) + Number( send_value );

            if( totalRechargeAmount >= rechargeLimit ) {
                throw Boom.badRequest("You have reached your maximum instant recharge amount limit of today.");
            }
        }
        //end
      
        if(devBalance){
            var apply_commission= globalSetting.res_dev_commission;
            var commission_type = 1;
            if( opratorCom.cos_commission ){
                apply_commission = opratorCom.cos_commission;
                commission_type = 3;


            }
            if(devCommission && ( opratorCom.cos_commission == null || opratorCom.cos_commission =='undefined' )){
                apply_commission = devCommission.commission;
                commission_type = 2;

            }
            var commissionValue = (send_value*(apply_commission/100));// calculate commission
           

             const addDieCom = Number(devBalance.admin_commission_amount) + Number(commissionValue);


            //check dev commission for recharge 

            if(Number(devBalance.wallet_amount)< send_value){
                throw Boom.badRequest("insufficient credit, please recharge your account.");
            }else{

                // working on current balance status  
                 const currentBalance    = await WalletModelObj.fetchObjWithSingleRecord({'wallet_user_id':requestData.developer_id},["wallet_id","wallet_amount","admin_commission_amount","ding_commission_amount"]);

                var dingCommission = currentBalance.ding_commission_amount;
                // calculation for when recharge success and mangege balance and admin commission
                //const balance_dud = Number(devBalance.balance) - Number(send_value);
                const balance_dud = Number(currentBalance.wallet_amount) - (Number(send_value)-Number(commissionValue));
                create_dev_trs.amount = (Number(send_value)-Number(commissionValue));
                //wt trs data
                createWalletTrs.wallet_id = currentBalance.wallet_id;
                createWalletTrs.wt_amount =(Number(send_value)-Number(commissionValue));
                createWalletTrs.wt_reference_type = 1;
                createWalletTrs.wt_payment_status =3;
                createWalletTrs.wt_status = 1;
                createWalletTrs.wt_type = 2;
                createWalletTrs.wt_user_type = 2;


                // 
                 createRecharge.commission_value =apply_commission;
                 createRecharge.commission_type = commission_type;
                 createRecharge.operator_id = porviderCode;

                 if( porviderCode == "NUCU" ) {
                    createRecharge.recharge_for = 2;
                 }

                 const fetchDevRecharge = [
                        "dav_recharge_id", 
                        "sku_code",
                        "send_value",
                        "commission_value",
                        "commission_type"
                    ];
                 const insert_obj =  await DeveloperRechargeObj.createObj( createRecharge, fetchDevRecharge); 
                                 
                 //if( userData.user_type == 3 ){
                 //   var responseData = await dingRechargeLibObj.SendTransferL( sku_code, send_value, send_currency_iso, contact_number );
                 //}else{
                    var responseData = await dingRechargeLibObj.SendTransfer( sku_code, send_value, send_currency_iso, contact_number );
                 //}
                if(responseData.ResultCode ==1){
                  dingCommission = Number(dingCommission) + Number(responseData.TransferRecord.CommissionApplied);
                  updateRecharge.reg_response_data = responseData;
                  updateRecharge.dev_recharge_status =2;

                                 
                  //code added by @hemant_Solanki
                  if( responseData.TransferRecord != null && responseData.TransferRecord != undefined ) {
                    if( responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                        if( responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                            updateRecharge.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                        }
                    }

                    if( responseData.TransferRecord.CommissionApplied != null && responseData.TransferRecord.CommissionApplied != undefined ) {
                                
                        const rechargeCommissionValue = (insert_obj.send_value*(insert_obj.commission_value/100));

                            updateRecharge.admin_commission_applied = rechargeCommissionValue;

                            updateRecharge.ding_commission_applied = responseData.TransferRecord.CommissionApplied;
                        
                            updateRecharge.actual_commission = Number( responseData.TransferRecord.CommissionApplied ) - Number( rechargeCommissionValue );

                    }
                   }
                   
                  //code added by @hemant_Solanki
                   await DeveloperRechargeObj.updateObj(updateRecharge,{'dav_recharge_id':insert_obj.dav_recharge_id},["dav_recharge_id"]);
                   // code for manage ding comission


                  //=====================================new================================
                  updateBal.wallet_amount = balance_dud;
                  updateBal.admin_commission_amount = addDieCom ;
                  updateBal.ding_commission_amount = dingCommission;
                  
                  create_dev_trs.reference_id = insert_obj.dav_recharge_id;
                  
                  createWalletTrs.wt_reference_id = insert_obj.dav_recharge_id;

                  createWalletTrs.wt_pre_amount = currentBalance.wallet_amount;
                  createWalletTrs.wt_balance = balance_dud;

                  await WalletTransactionObj.createObj(createWalletTrs,["wt_id"]);

                  await DeveloperTransactionObj.createObj(create_dev_trs,["dt_id"]); 
                  await WalletModelObj.updateObj(updateBal,{'wallet_id':currentBalance.wallet_id}, ["wallet_id"]);
               
                 responseData.OwnCredit = balance_dud;
                 responseData.OwnDiscount =commissionValue;
                 delete responseData.TransferRecord.TransferId.DistributorRef;
                 if( responseData.TransferRecord != null) {
                     if( responseData.TransferRecord.CommissionApplied != null ) {
                        delete responseData.TransferRecord.CommissionApplied
                     }
                 }
                  response.data = responseData;
                } else{
                      // delete responseData.stack;
                      // delete responseData.config;
                      updateRecharge.reg_response_data = responseData.response.data;
                      updateRecharge.dev_recharge_status =3;

                      //code added by @hemant_Solanki
                      if( responseData.TransferRecord != null && responseData.TransferRecord != undefined ) {
                        if( responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                            if( responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                updateRecharge.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                            }
                        }
                       }
                      //code added by @hemant_Solanki

                      await DeveloperRechargeObj.updateObj(updateRecharge,{'dav_recharge_id':insert_obj.dav_recharge_id},["dav_recharge_id"]);

                        if( responseData.TransferRecord != null) {
                            if( responseData.TransferRecord.CommissionApplied != null ) {
                                delete responseData.TransferRecord.CommissionApplied;
                            }
                        }
                      response.data = responseData.response.data;
                }
            }
        }
        return response;
};

export const SendTransferOld3 = async function( request ){
  
        let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();

        const requestData = request.body,
        sku_code = requestData.skuCode,
        send_value = requestData.sendValue,
        contact_number = requestData.accountNumber,
        send_currency_iso = requestData.sendCurrencyIso,
        devWhere = {
        'commission_user_id':requestData.developer_id,
        //'commission_user_type': 4
        },
        createRecharge = {
         "developer_id":requestData.developer_id,   
         "sku_code":sku_code,
         "send_value":send_value,
         "send_currency_iso":send_currency_iso,
         "account_number":contact_number,
          "created_at": currentDateTime,
          "updated_at": currentDateTime 
        },
        create_dev_trs = {
          "developer_id":requestData.developer_id,
          "dt_type":2,
          "currency":send_currency_iso,
          "reference_type":1,
          "payment_status":2,
          "created_at": currentDateTime,
          "updated_at": currentDateTime 

        },
        createWalletTrs = {
            "user_id": requestData.developer_id,
            "wt_currency":send_currency_iso,
            "created_at":currentDateTime,
            "updated_at":currentDateTime
           
        },
        updateBal = {
           "updated_at": currentDateTime  
        },
        updateRecharge = {
           "updated_at": currentDateTime  
        },
        response = { "data": { } };
        const globalSetting = await SettingModelObj.fetchObjWithSingleRecord('',["res_dev_commission"]);
        const devCommission = await UserCommissionModelObj.fetchObjWithSingleRecord( devWhere, ["commission_id","commission"] ); 
        const getProvider   = await dingRechargeLibObj.GetProviders( '','', contact_number );
        if(getProvider.Items.length==0){

         const errRes = {
          "TransferRecord":{
                            "TransferId":null,
                            "SkuCode":sku_code,
                            "Price":null,
                            "CommissionApplied":0,
                            "StartedUtc":null,
                            "CompletedUtc":null,
                            "ProcessingState":"Failed",
                            "ReceiptText":null,
                            "ReceiptParams":{},
                            "AccountNumber":contact_number
                          },
                            "ResultCode":4,
                            "ErrorCodes":[{"Code":"AccountNumberInvalid","Context":"AccountNumberFailedRegex"}]
                        };

           response.data = errRes;
           return response;

        }
        const porviderCode  = getProvider.Items[0].ProviderCode,
              whereOpr = {
                'operator_id':porviderCode,
                'cos_user_id':requestData.developer_id,
                //'cos_user_type':4
              };
        const userData    = await UserModelObj.fetchObjWithSingleRecord({ 'user_id':requestData.developer_id },['user_type','status']); 
        const opratorCom    = await CommissionOperatorModelObj.fetchObjWithSingleRecord(whereOpr,['cos_commission','cos_id']); 
        const devBalance    = await WalletModelObj.fetchObjWithSingleRecord({'wallet_user_id':requestData.developer_id},["wallet_id","wallet_amount","admin_commission_amount","ding_commission_amount"]);
        
        //created by hemant soalnki
        const totalDeveloperRechargeAmount = await RechargeModelObj.getDeveloperRechargeAmount( requestData, currentDateTime );
        const totalRechargelimit = await RechargeModelObj.getDeveloperRechargeLimit( requestData );
        
        if( totalDeveloperRechargeAmount[0].total_developer_recharge_amount == null ) {
            totalDeveloperRechargeAmount[0].total_developer_recharge_amount = 0.0;
        }

        if( totalRechargelimit.length > 0 ) {
            var rechargeLimit = Number(totalRechargelimit[0].recharge_limit_amount);
            var totalRechargeAmount = Number(totalDeveloperRechargeAmount[0].total_developer_recharge_amount) + Number( send_value );

            if( totalRechargeAmount >= rechargeLimit ) {
                throw Boom.badRequest("You have reached your maximum instant recharge amount limit of today.");
            }
        }
        //end
      
        if(devBalance){
            var apply_commission= globalSetting.res_dev_commission;
            var commission_type = 1;
            if( opratorCom.cos_commission ){
                apply_commission = opratorCom.cos_commission;
                commission_type = 3;


            }
            if(devCommission && ( opratorCom.cos_commission == null || opratorCom.cos_commission =='undefined' )){
                apply_commission = devCommission.commission;
                commission_type = 2;

            }
            var commissionValue = (send_value*(apply_commission/100));// calculate commission
            

             const addDieCom = Number(devBalance.admin_commission_amount) + Number(commissionValue);


            //check dev commission for recharge 

            if(Number(devBalance.wallet_amount)< send_value){
                throw Boom.badRequest("insufficient credit, please recharge your account.");
            }else{

                // working on current balance status  
                 const currentBalance    = await WalletModelObj.fetchObjWithSingleRecord({'wallet_user_id':requestData.developer_id},["wallet_id","wallet_amount","admin_commission_amount","ding_commission_amount"]);

                var dingCommission = currentBalance.ding_commission_amount;
                // calculation for when recharge success and mangege balance and admin commission
                //const balance_dud = Number(devBalance.balance) - Number(send_value);
                const balance_dud = Number(currentBalance.wallet_amount) - (Number(send_value)-Number(commissionValue));
                create_dev_trs.amount = (Number(send_value)-Number(commissionValue));
                //wt trs data
                createWalletTrs.wallet_id = currentBalance.wallet_id;
                createWalletTrs.wt_amount =(Number(send_value)-Number(commissionValue));
                createWalletTrs.wt_reference_type = 1;
                createWalletTrs.wt_payment_status =3;
                createWalletTrs.wt_status = 1;
                createWalletTrs.wt_type = 2;
                createWalletTrs.wt_user_type = 2;

                // 
                 createRecharge.commission_value =apply_commission;
                 createRecharge.commission_type = commission_type;
                 createRecharge.operator_id = porviderCode;

                 if( porviderCode == "NUCU" ) {
                    createRecharge.recharge_for = 2;
                 }

                 const fetchDevRecharge = [
                        "dav_recharge_id", 
                        "sku_code",
                        "send_value",
                        "commission_value",
                        "commission_type"
                    ];
                 const insert_obj =  await DeveloperRechargeObj.createObj( createRecharge, fetchDevRecharge); 
                  // changerrrrrrr
                  updateBal.wallet_amount = balance_dud;
                  updateBal.admin_commission_amount = addDieCom ;
                  //updateBal.ding_commission_amount = dingCommission;
                  
                  create_dev_trs.reference_id = insert_obj.dav_recharge_id;
                  
                  createWalletTrs.wt_reference_id = insert_obj.dav_recharge_id;

                  createWalletTrs.wt_pre_amount = currentBalance.wallet_amount;
                  createWalletTrs.wt_balance = balance_dud;

                  await WalletTransactionObj.createObj(createWalletTrs,["wt_id"]);

                  await DeveloperTransactionObj.createObj(create_dev_trs,["dt_id"]); 
                  await WalletModelObj.updateObj(updateBal,{'wallet_id':currentBalance.wallet_id}, ["wallet_id"]);
                  //changeeeeeeeeeeeeeeeeeeeee
                 
                 //if( userData.user_type == 3 ){
                 //   var responseData = await dingRechargeLibObj.SendTransferL( sku_code, send_value, send_currency_iso, contact_number );
                 //}else{
                    var responseData = await dingRechargeLibObj.SendTransfer( sku_code, send_value, send_currency_iso, contact_number );
                 //}
                if(responseData.ResultCode ==1){
                  dingCommission = Number(dingCommission) + Number(responseData.TransferRecord.CommissionApplied);
                  updateRecharge.reg_response_data = responseData;
                  updateRecharge.dev_recharge_status =2;
                  
                  //code added by @hemant_Solanki
                  if( responseData.TransferRecord != null && responseData.TransferRecord != undefined ) {
                    if( responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                        if( responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                            updateRecharge.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                        }
                    }

                    if( responseData.TransferRecord.CommissionApplied != null && responseData.TransferRecord.CommissionApplied != undefined ) {
                                
                        const rechargeCommissionValue = (insert_obj.send_value*(insert_obj.commission_value/100));

                            updateRecharge.admin_commission_applied = rechargeCommissionValue;

                            updateRecharge.ding_commission_applied = responseData.TransferRecord.CommissionApplied;
                        
                            updateRecharge.actual_commission = Number( responseData.TransferRecord.CommissionApplied ) - Number( rechargeCommissionValue );

                    }
                   }
                   
                  //code added by @hemant_Solanki
                   await DeveloperRechargeObj.updateObj(updateRecharge,{'dav_recharge_id':insert_obj.dav_recharge_id},["dav_recharge_id"]);
                   // code for manage ding comission
                   await WalletModelObj.updateObj({'ding_commission_amount':dingCommission},{'wallet_id':currentBalance.wallet_id}, ["wallet_id"]);

                 responseData.OwnCredit = balance_dud;
                 responseData.OwnDiscount =commissionValue;
                 delete responseData.TransferRecord.TransferId.DistributorRef;
                 if( responseData.TransferRecord != null) {
                     if( responseData.TransferRecord.CommissionApplied != null ) {
                        delete responseData.TransferRecord.CommissionApplied
                     }
                 }
                  response.data = responseData;
                }else{
                      // delete responseData.stack;
                      // delete responseData.config;
                      updateRecharge.reg_response_data = responseData.response.data;
                      updateRecharge.dev_recharge_status =3;

                      //code added by @hemant_Solanki
                      if( responseData.TransferRecord != null && responseData.TransferRecord != undefined ) {
                        if( responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                            if( responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                updateRecharge.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                            }
                        }
                       }
                      //code added by @hemant_Solanki

                      await DeveloperRechargeObj.updateObj(updateRecharge,{'dav_recharge_id':insert_obj.dav_recharge_id},["dav_recharge_id"]);

                        if( responseData.TransferRecord != null) {
                            if( responseData.TransferRecord.CommissionApplied != null ) {
                                delete responseData.TransferRecord.CommissionApplied;
                            }
                        }
                      response.data = responseData.response.data;

                }
                

            }
          
        }
        return response;
  
};

export const SendTransferOld2 = async function( request ){
   
        let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();

        const requestData = request.body,
        sku_code = requestData.skuCode,
        send_value = requestData.sendValue,
        contact_number = requestData.accountNumber,
        send_currency_iso = requestData.sendCurrencyIso,
        devWhere = {
        'commission_user_id':requestData.developer_id,
        //'commission_user_type': 4
        },
        createRecharge = {
         "developer_id":requestData.developer_id,   
         "sku_code":sku_code,
         "send_value":send_value,
         "send_currency_iso":send_currency_iso,
         "account_number":contact_number,
          "created_at": currentDateTime,
          "updated_at": currentDateTime 
        },
        create_dev_trs = {
          "developer_id":requestData.developer_id,
          "dt_type":2,
          "currency":send_currency_iso,
          "reference_type":1,
          "payment_status":2,
          "created_at": currentDateTime,
          "updated_at": currentDateTime 

        },
        createWalletTrs = {
            "user_id": requestData.developer_id,
            "wt_currency":send_currency_iso,
            "created_at":currentDateTime,
            "updated_at":currentDateTime
           
        },
        updateBal = {
           "updated_at": currentDateTime  
        },
        updateRecharge = {
           "updated_at": currentDateTime  
        },
        response = { "data": { } };
        const globalSetting = await SettingModelObj.fetchObjWithSingleRecord('',["res_dev_commission"]);
        
        const devCommission = await UserCommissionModelObj.fetchObjWithSingleRecord( devWhere, ["commission_id","commission"] ); 
        
        const getProvider   = await dingRechargeLibObj.GetProviders( '','', contact_number );

        
        if(getProvider.Items.length==0){

         const errRes = {
          "TransferRecord":{
                            "TransferId":null,
                            "SkuCode":sku_code,
                            "Price":null,
                            "CommissionApplied":0,
                            "StartedUtc":null,
                            "CompletedUtc":null,
                            "ProcessingState":"Failed",
                            "ReceiptText":null,
                            "ReceiptParams":{},
                            "AccountNumber":contact_number
                          },
                            "ResultCode":4,
                            "ErrorCodes":[{"Code":"AccountNumberInvalid","Context":"AccountNumberFailedRegex"}]
                        };

           response.data = errRes;
           return response;

        }
        const porviderCode  = getProvider.Items[0].ProviderCode,
              whereOpr = {
                'operator_id':porviderCode,
                'cos_user_id':requestData.developer_id,
                //'cos_user_type':4
              };
        const userData    = await UserModelObj.fetchObjWithSingleRecord({ 'user_id':requestData.developer_id },['user_type','status']); 
        const opratorCom    = await CommissionOperatorModelObj.fetchObjWithSingleRecord(whereOpr,['cos_commission','cos_id']); 
        
        const devBalance    = await WalletModelObj.fetchObjWithSingleRecord({'wallet_user_id':requestData.developer_id},["wallet_id","wallet_amount","admin_commission_amount","ding_commission_amount"]);
        
        //created by hemant soalnki
        const totalDeveloperRechargeAmount = await RechargeModelObj.getDeveloperRechargeAmount( requestData, currentDateTime );
        const totalRechargelimit = await RechargeModelObj.getDeveloperRechargeLimit( requestData );
        
        if( totalDeveloperRechargeAmount[0].total_developer_recharge_amount == null ) {
            totalDeveloperRechargeAmount[0].total_developer_recharge_amount = 0.0;
        }

        if( totalRechargelimit.length > 0 ) {
            var rechargeLimit = Number(totalRechargelimit[0].recharge_limit_amount);
            var totalRechargeAmount = Number(totalDeveloperRechargeAmount[0].total_developer_recharge_amount) + Number( send_value );

            if( totalRechargeAmount >= rechargeLimit ) {
                throw Boom.badRequest("You have reached your maximum instant recharge amount limit of today.");
            }
        }
        //end
      
        if(devBalance){
            var apply_commission= globalSetting.res_dev_commission;
            var commission_type = 1;
            if( opratorCom.cos_commission ){
                apply_commission = opratorCom.cos_commission;
                commission_type = 3;


            }
            if(devCommission && ( opratorCom.cos_commission == null || opratorCom.cos_commission =='undefined' )){
                apply_commission = devCommission.commission;
                commission_type = 2;

            }
            
            var commissionValue = (send_value*(apply_commission/100));// calculate commission
           
            
             const addDieCom = Number(devBalance.admin_commission_amount) + Number(commissionValue);

            return false;
            //check dev commission for recharge 

            if(Number(devBalance.wallet_amount)< send_value){
                throw Boom.badRequest("insufficient credit, please recharge your account.");
            }else{

                // working on current balance status  
                 const currentBalance    = await WalletModelObj.fetchObjWithSingleRecord({'wallet_user_id':requestData.developer_id},["wallet_id","wallet_amount","admin_commission_amount","ding_commission_amount"]);

                var dingCommission = currentBalance.ding_commission_amount;
                // calculation for when recharge success and mangege balance and admin commission
                //const balance_dud = Number(devBalance.balance) - Number(send_value);
                const balance_dud = Number(currentBalance.wallet_amount) - (Number(send_value)-Number(commissionValue));
                create_dev_trs.amount = (Number(send_value)-Number(commissionValue));
                //wt trs data
                createWalletTrs.wallet_id = currentBalance.wallet_id;
                createWalletTrs.wt_amount =(Number(send_value)-Number(commissionValue));
                createWalletTrs.wt_reference_type = 1;
                createWalletTrs.wt_payment_status =3;
                createWalletTrs.wt_status = 1;
                createWalletTrs.wt_type = 2;
                createWalletTrs.wt_user_type = 2;


                // 
                 createRecharge.commission_value =apply_commission;
                 createRecharge.commission_type = commission_type;
                 createRecharge.operator_id = porviderCode;

                 if( porviderCode == "NUCU" ) {
                    createRecharge.recharge_for = 2;
                 }

                 const fetchDevRecharge = [
                        "dav_recharge_id", 
                        "sku_code",
                        "send_value",
                        "commission_value",
                        "commission_type"
                    ];
                 const insert_obj =  await DeveloperRechargeObj.createObj( createRecharge, fetchDevRecharge); 

                  // changerrrrrrr
                  updateBal.wallet_amount = balance_dud;
                  updateBal.admin_commission_amount = addDieCom ;
                  //updateBal.ding_commission_amount = dingCommission;
                  
                  create_dev_trs.reference_id = insert_obj.dav_recharge_id;
                  
                  createWalletTrs.wt_reference_id = insert_obj.dav_recharge_id;

                  createWalletTrs.wt_pre_amount = currentBalance.wallet_amount;
                  createWalletTrs.wt_balance = balance_dud;

                  await WalletTransactionObj.createObj(createWalletTrs,["wt_id"]);

                  await DeveloperTransactionObj.createObj(create_dev_trs,["dt_id"]); 
                  await WalletModelObj.updateObj(updateBal,{'wallet_id':currentBalance.wallet_id}, ["wallet_id"]);
                  //changeeeeeeeeeeeeeeeeeeeee
                 
                 //if( userData.user_type == 3 ){
                 //   var responseData = await dingRechargeLibObj.SendTransferL( sku_code, send_value, send_currency_iso, contact_number );
                 //}else{
                    var responseData = await dingRechargeLibObj.SendTransfer( sku_code, send_value, send_currency_iso, contact_number );
                 //}
                if(responseData.ResultCode ==1){
                  dingCommission = Number(dingCommission) + Number(responseData.TransferRecord.CommissionApplied);
                  
                  updateRecharge.reg_response_data = responseData;
                  updateRecharge.dev_recharge_status =2;
                  
                  //code added by @hemant_Solanki
                  if( responseData.TransferRecord != null && responseData.TransferRecord != undefined ) {
                    if( responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                        if( responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                            updateRecharge.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                        }
                    }

                    if( responseData.TransferRecord.CommissionApplied != null && responseData.TransferRecord.CommissionApplied != undefined ) {
                                
                        const rechargeCommissionValue = (insert_obj.send_value*(insert_obj.commission_value/100));

                            updateRecharge.admin_commission_applied = rechargeCommissionValue;

                            updateRecharge.ding_commission_applied = responseData.TransferRecord.CommissionApplied;
                        
                            updateRecharge.actual_commission = Number( responseData.TransferRecord.CommissionApplied ) - Number( rechargeCommissionValue );

                    }
                   }
                   
                  //code added by @hemant_Solanki
                   await DeveloperRechargeObj.updateObj(updateRecharge,{'dav_recharge_id':insert_obj.dav_recharge_id},["dav_recharge_id"]);
                   // code for manage ding comission
                   await WalletModelObj.updateObj({'ding_commission_amount':dingCommission},{'wallet_id':currentBalance.wallet_id}, ["wallet_id"]);

                 responseData.OwnCredit = balance_dud;
                 responseData.OwnDiscount =commissionValue;
                 delete responseData.TransferRecord.TransferId.DistributorRef;
                 if( responseData.TransferRecord != null) {
                     if( responseData.TransferRecord.CommissionApplied != null ) {
                        delete responseData.TransferRecord.CommissionApplied
                     }
                 }
                  response.data = responseData;
                }else{
                      // delete responseData.stack;
                      // delete responseData.config;
                      updateRecharge.reg_response_data = responseData.response.data;
                      updateRecharge.dev_recharge_status =3;

                      //code added by @hemant_Solanki
                      if( responseData.TransferRecord != null && responseData.TransferRecord != undefined ) {
                        if( responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                            if( responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                updateRecharge.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                            }
                        }
                       }
                      //code added by @hemant_Solanki

                      await DeveloperRechargeObj.updateObj(updateRecharge,{'dav_recharge_id':insert_obj.dav_recharge_id},["dav_recharge_id"]);

                        if( responseData.TransferRecord != null) {
                            if( responseData.TransferRecord.CommissionApplied != null ) {
                                delete responseData.TransferRecord.CommissionApplied;
                            }
                        }
                      response.data = responseData.response.data;

                }
                

            }
          
        }
        // return response;
  
};

export const SendTransferOld1 = async function( request ){

        let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
        
        const requestData = request.body,
        sku_code = requestData.skuCode,
        send_value = requestData.sendValue,
        contact_number = requestData.accountNumber,
        send_currency_iso = requestData.sendCurrencyIso,
        devWhere = {
        'commission_user_id':requestData.developer_id,
        //'commission_user_type': 4
        },
        createRecharge = {
         "developer_id":requestData.developer_id,   
         "sku_code":sku_code,
         "send_value":send_value,
         "send_currency_iso":send_currency_iso,
         "account_number":contact_number,
          "created_at": currentDateTime,
          "updated_at": currentDateTime 
        },
        create_dev_trs = {
          "developer_id":requestData.developer_id,
          "dt_type":2,
          "currency":send_currency_iso,
          "reference_type":1,
          "payment_status":2,
          "created_at": currentDateTime,
          "updated_at": currentDateTime 

        },
        createWalletTrs = {
            "user_id": requestData.developer_id,
            "wt_currency":send_currency_iso,
            "created_at":currentDateTime,
            "updated_at":currentDateTime
           
        },
        updateBal = {
           "updated_at": currentDateTime  
        },
        updateRecharge = {
           "updated_at": currentDateTime  
        },
        response = { "data": { } };
        const globalSetting = await SettingModelObj.fetchObjWithSingleRecord('',["res_dev_commission"]);
        const devCommission = await UserCommissionModelObj.fetchObjWithSingleRecord( devWhere, ["commission_id","commission"] ); 
        const getProvider   = await dingRechargeLibObj.GetProviders( '','', contact_number );
        if(getProvider.Items.length==0){

         const errRes = {
          "TransferRecord":{
                            "TransferId":null,
                            "SkuCode":sku_code,
                            "Price":null,
                            "CommissionApplied":0,
                            "StartedUtc":null,
                            "CompletedUtc":null,
                            "ProcessingState":"Failed",
                            "ReceiptText":null,
                            "ReceiptParams":{},
                            "AccountNumber":contact_number
                          },
                            "ResultCode":4,
                            "ErrorCodes":[{"Code":"AccountNumberInvalid","Context":"AccountNumberFailedRegex"}]
                        };

           response.data = errRes;
           return response;

        }
        const porviderCode  = getProvider.Items[0].ProviderCode,
              whereOpr = {
                'operator_id':porviderCode,
                'cos_user_id':requestData.developer_id,
                //'cos_user_type':4
              };
        const userData    = await UserModelObj.fetchObjWithSingleRecord({ 'user_id':requestData.developer_id },['user_type','status']); 
        const opratorCom    = await CommissionOperatorModelObj.fetchObjWithSingleRecord(whereOpr,['cos_commission','cos_id']); 
        const devBalance    = await WalletModelObj.fetchObjWithSingleRecord({'wallet_user_id':requestData.developer_id},["wallet_id","wallet_amount","admin_commission_amount","ding_commission_amount"]);
        
        //created by hemant soalnki
        const totalDeveloperRechargeAmount = await RechargeModelObj.getDeveloperRechargeAmount( requestData, currentDateTime );
        const totalRechargelimit = await RechargeModelObj.getDeveloperRechargeLimit( requestData );
        
        if( totalDeveloperRechargeAmount[0].total_developer_recharge_amount == null ) {
            totalDeveloperRechargeAmount[0].total_developer_recharge_amount = 0.0;
        }

        if( totalRechargelimit.length > 0 ) {
            var rechargeLimit = Number(totalRechargelimit[0].recharge_limit_amount);
            var totalRechargeAmount = Number(totalDeveloperRechargeAmount[0].total_developer_recharge_amount) + Number( send_value );

            if( totalRechargeAmount >= rechargeLimit ) {
                throw Boom.badRequest("You have reached your maximum instant recharge amount limit of today.");
            }
        }
        //end
       
        if(devBalance){
            var apply_commission= globalSetting.res_dev_commission;
            var commission_type = 1;
            if( opratorCom.cos_commission ){
                apply_commission = opratorCom.cos_commission;
                commission_type = 3;


            }
            if(devCommission && ( opratorCom.cos_commission == null || opratorCom.cos_commission =='undefined' )){
                apply_commission = devCommission.commission;
                commission_type = 2;

            }
            var commissionValue = (send_value*(apply_commission/100));// calculate commission
           
             const addDieCom = Number(devBalance.admin_commission_amount) + Number(commissionValue);


            //check dev commission for recharge 

            if(Number(devBalance.wallet_amount)< send_value){
                throw Boom.badRequest("insufficient credit, please recharge your account.");
            }else{
                var dingCommission = devBalance.ding_commission_amount;
                // calculation for when recharge success and mangege balance and admin commission
                //const balance_dud = Number(devBalance.balance) - Number(send_value);
                const balance_dud = Number(devBalance.wallet_amount) - (Number(send_value)-Number(commissionValue));
                create_dev_trs.amount = (Number(send_value)-Number(commissionValue));
                //wt trs data
                createWalletTrs.wallet_id = devBalance.wallet_id;
                createWalletTrs.wt_amount =(Number(send_value)-Number(commissionValue));
                createWalletTrs.wt_reference_type = 1;
                createWalletTrs.wt_payment_status =3;
                createWalletTrs.wt_status = 1;
                createWalletTrs.wt_type = 2;
                createWalletTrs.wt_user_type = 2;


                // 
                 createRecharge.commission_value =apply_commission;
                 createRecharge.commission_type = commission_type;
                 createRecharge.operator_id = porviderCode;
                 const insert_obj =  await DeveloperRechargeObj.createObj( createRecharge, ["dav_recharge_id"]); 
                 
                 //if( userData.user_type == 3 ){
                 //   var responseData = await dingRechargeLibObj.SendTransferL( sku_code, send_value, send_currency_iso, contact_number );
                 //}else{
                    var responseData = await dingRechargeLibObj.SendTransfer( sku_code, send_value, send_currency_iso, contact_number );
                 //}
                if(responseData.ResultCode ==1){
                  dingCommission = Number(dingCommission) + Number(responseData.TransferRecord.CommissionApplied);
                  updateRecharge.reg_response_data = responseData;
                  updateRecharge.dev_recharge_status =2;

                  updateBal.wallet_amount = balance_dud;
                  updateBal.admin_commission_amount = addDieCom ;
                  updateBal.ding_commission_amount = dingCommission;
                  
                  create_dev_trs.reference_id = insert_obj.dav_recharge_id;
                  
                  createWalletTrs.wt_reference_id = insert_obj.dav_recharge_id;

                  createWalletTrs.wt_pre_amount = devBalance.wallet_amount;
                  createWalletTrs.wt_balance = balance_dud;
                  
                  //code added by @hemant_Solanki
                  if( responseData.TransferRecord != null && responseData.TransferRecord != undefined ) {
                    if( responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                        if( responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                            updateRecharge.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                        }
                    }
                   }
                  //code added by @hemant_Solanki

                  await WalletTransactionObj.createObj(createWalletTrs,["wt_id"]);

                  await DeveloperTransactionObj.createObj(create_dev_trs,["dt_id"]); 
                  await DeveloperRechargeObj.updateObj(updateRecharge,{'dav_recharge_id':insert_obj.dav_recharge_id},["dav_recharge_id"]);
                  await WalletModelObj.updateObj(updateBal,{'wallet_id':devBalance.wallet_id}, ["wallet_id"]);
                  //response.data.message = 'Recharge has been done successfully.';
                 responseData.OwnCredit = balance_dud;
                 responseData.OwnDiscount =commissionValue;
                 delete responseData.TransferRecord.TransferId.DistributorRef;
                  response.data = responseData;
                }else{
                      // delete responseData.stack;
                      // delete responseData.config;
                      updateRecharge.reg_response_data = responseData.response.data;
                      updateRecharge.dev_recharge_status =3;

                      //code added by @hemant_Solanki
                      if( responseData.TransferRecord != null && responseData.TransferRecord != undefined ) {
                        if( responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                            if( responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                updateRecharge.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                            }
                        }
                       }
                      //code added by @hemant_Solanki

                      await DeveloperRechargeObj.updateObj(updateRecharge,{'dav_recharge_id':insert_obj.dav_recharge_id},["dav_recharge_id"]);
                      response.data = responseData.response.data;

                }
                

            }
          
        }

        return response;
 
};

export const getProfiderInfo = function( request ){
    const requestData = request.body,
        accountNumber = requestData.accountNumber,
        response = { "data": { } };
    return dingRechargeLibObj.GetAccountLookup( accountNumber ).then( async ( responseData ) => {
        if(responseData){
           delete responseData.stack;
           delete responseData.config;

        }
        response.data = responseData;
        return response;
    } ).catch( ( error ) => {
        throw Boom.badRequest( error );
    } )

};

/*
* Add promotion and promotion description data to db
*/
export const addPromotions = function( request ){
    const requestData = request.body,
     currentDateTime = DateTimeUtil.getCurrentTimeObjForDB(),
        accountNumber = requestData.accountNumber,
        response = { "data": { } };
    return dingRechargeLibObj.Getpromotion().then( async ( responseData ) => {
    return dingRechargeLibObj.GetPromotionDescriptions().then( async ( desc ) => {
     
        var merge = _.unionBy(responseData.Items, desc.Items, 'LocalizationKey');
       

        var arrResult = _.map(responseData.Items, function(obj) {
            return _.assign(obj, _.find(desc.Items, {
                LocalizationKey: obj.LocalizationKey
            }));
        });
        var finalArr = [] ;
       // console.log("arrResult.length", arrResult.length)
     
        for(let i =0 ; arrResult.length > i; i++){

            var column = {
                "provider_code": arrResult[i].ProviderCode,
                "headline": arrResult[i].Headline.replace(+/\//g, " "),
                "currency_iso": arrResult[i].CurrencyIso,
                "minimum_send_amount": arrResult[i].MinimumSendAmount,
               "localization_key": arrResult[i].LocalizationKey,
              "terms_conditions": JSON.stringify(arrResult[i].TermsAndConditionsMarkDown),
               "bonus_validity": arrResult[i].BonusValidity,
                "promotion_type": arrResult[i].PromotionType.replace(/\//g, " "),
                "language_code": arrResult[i].LanguageCode,
                "start_utc": arrResult[i].StartUtc,
                "end_utc": arrResult[i].EndUtc,
                "created_at": currentDateTime,
                "updated_at": currentDateTime
            } ;
            
          // JSON.stringify(column)
           finalArr.push(column);
        }

        //console.log("Allconti", finalArr)
        var deletepromotions =   await DingeModelobj.deleteDingDataObj("qva_promotion_desc") ;
        const res =  await DingeModelobj.batchPromotionInsertObj(finalArr)
       
        response.data = res ;
        return response;
    })
    } ).catch( ( error ) => {
        throw Boom.badRequest( error );
    } )

};

/*
* Add providers  data to db
*/
export const addProviders = function( request ){
    const requestData = request.body,
     currentDateTime = DateTimeUtil.getCurrentTimeObjForDB(),
      
        response = { "data": { } };
    return dingRechargeLibObj.GetAllProviders().then( async ( resp ) => {

      var deleteProvider =   await DingeModelobj.deleteDingDataObj("qva_providers") ;
        
       // console.log("responseDatalength", resp.Items.length)
        var arrResult = resp.Items ;
        var finalArr = [] ;
       // console.log("arrResult.length", arrResult.length)
     
        for(let i =0 ; arrResult.length > i; i++){

            var column = {
                "provider_code": arrResult[i].ProviderCode,
                "countries_iso": arrResult[i].CountryIso,
                "logo_url": arrResult[i].LogoUrl,
                "provider_name": arrResult[i].Name,
                "validation_regex": arrResult[i].ValidationRegex,
                "created_at": currentDateTime,
                "updated_at": currentDateTime
            } ;
            
          // JSON.stringify(column)
           finalArr.push(column);
        }
        //console.log("finalArr", finalArr)
        return DingeModelobj.batchProvidersInsertObj(finalArr).then((row) => {
            response.data = row ;
            return response;
        }).catch((err) => {

            throw Boom.badRequest( err );
    })

    }).catch((err) => {

        throw Boom.badRequest( err );
    })
    

};


/*
* Add Continet wise promotions  data to db
*/
export const addContinentPromotion = function( request ){
    const requestData = request.body,
     currentDateTime = DateTimeUtil.getCurrentTimeObjForDB(),
      
        response = { "data": { } };
       
        return DingeModelobj.GetAllContinentPromotions().then( async ( arrResult ) => {

           
            var finalArr = [] ;
       // console.log("arrResult.length", arrResult.length)
     
        for(let i =0 ; arrResult.length > i; i++){

            var column = {
                "continent_id": arrResult[i].continent_id,
                "countries_iso": arrResult[i].countries_iso,
                "provider_code": arrResult[i].provider_code,
                "currency_type": arrResult[i].currency_iso,
                "minimum_send_amount": arrResult[i].minimum_send_amount,
                "localization_key": arrResult[i].localization_key,
                "headline": arrResult[i].headline,
                "terms_conditions": arrResult[i].terms_conditions,
                "bonus_validity": arrResult[i].bonus_validity,
                "promotion_type": arrResult[i].promotion_type,
                "language_code": arrResult[i].language_code,
                "start_utc": arrResult[i].start_utc,
                "end_utc": arrResult[i].end_utc,
                "created_at": currentDateTime,
                "updated_at": currentDateTime
            } ;
            
          // JSON.stringify(column)
           finalArr.push(column);
        }
        var deleteProvider =   await DingeModelobj.deleteDingDataObj("qva_continent_promotion") ;

        return DingeModelobj.batchDataInsertObj(finalArr, "qva_continent_promotion").then((row) => {
            response.data = row ;
            response.message = "Continent Promotions added successfully" ;
            return response;
        }).catch((err) => {

            throw Boom.badRequest( err );
       })
    })
    

};

/**
 * Add ding products to db
 * @param  request obj
 */

export const addProducts = function( request ){
    const requestData = request.body,
     currentDateTime = DateTimeUtil.getCurrentTimeObjForDB(),
      
        response = { "data": { } };
       
        return dingRechargeLibObj.GetAllProducts().then( async ( allProduct ) => {
          
            var arrResult = allProduct.Items ;
            var productData = await convertDataObj.dingProductsjson(allProduct);
            return false ;
            var finalArr = [] ;
       // console.log("arrResult.length",arrResult)
     
        for(let i =0 ; arrResult.length > i; i++){
            var product_type = 2 ;
            if(arrResult[i].Benefits.includes("Data")){
                product_type =1 ;
            }
            var column = {
                "provider_code": arrResult[i].ProviderCode,
                "recharge_amount": arrResult[i].Minimum.ReceiveValue,
                "receive_currency_iso": arrResult[i].Minimum.ReceiveCurrencyIso,
                "sku_code": arrResult[i].SkuCode,
                "localization_key": arrResult[i].LocalizationKey,
                "region_code": arrResult[i].RegionCode,
                "default_text": arrResult[i].DefaultDisplayText,
                "rcharge_type": product_type,
                "created_at": currentDateTime,
                "updated_at": currentDateTime
            } ;
            
          // JSON.stringify(column)
           finalArr.push(column);
        }
        console.log("finalArr", finalArr)
        return false ;
        var deleteProvider =   await DingeModelobj.deleteDingDataObj("qva_continent_promotion") ;

        return DingeModelobj.batchDataInsertObj(finalArr, "qva_continent_promotion").then((row) => {
            response.data = row ;
            response.message = "Continent Promotions added successfully" ;
            return response;
        }).catch((err) => {

            throw Boom.badRequest( err );
       })
    })
    

};

/**
 *  get continent list
 * @param {*} request 
 */
export const getContinent = function( request ){
    const response = { "data": { } };
    return DingeModelobj.fetchResults('qva_continent',{}, "name", "ASC").then( async ( responseData ) => {
        response.data = responseData;
       
        return response;
    } ).catch( ( error ) => {
        throw Boom.badRequest( error );
    } )

};


/**
 *  get continent description  list
 * @param {*} request 
 */
export const continentPromotionsDescription = function( request ){
    const response = { "data": { } };
    var continent_id = request.body.continent_id ;
    
    return DingeModelobj.fetchPromotionsData(continent_id).then( async ( responseData ) => {
        response.code = HttpStatus.OK;
        response.data = responseData;
       
        return response;
    } ).catch( ( error ) => {
        throw Boom.badRequest( error );
    } )

};



/*
* Add Continet wise promotions  data to db
*/
export const addCountries = function( request ){
    const requestData = request.body,
     currentDateTime = DateTimeUtil.getCurrentTimeObjForDB(),
      
        response = { "data": { } };
       
        return dingRechargeLibObj.GetCountries().then( async ( countryData ) => {
          var arrResult = countryData.Items ;
           
            var finalArr = [] ;
       // console.log("arrResult.length", arrResult.length)
     
        for(let i =0 ; arrResult.length > i; i++){

            var column = {
                "countries_iso": arrResult[i].CountryIso,
                "name": arrResult[i].CountryName,
                "region_codes": arrResult[i].RegionCodes.toString(),
                "logo_url": `https://imagerepo.ding.com/flag/${arrResult[i].CountryIso}.png?height=46&mask=circle&compress=none`,
                "created_at": currentDateTime,
                "updated_at": currentDateTime
            } ;
            
          // JSON.stringify(column)
           finalArr.push(column);
        }
        var deleteProvider =   await DingeModelobj.deleteDingDataObj(" qva_countries") ;

        return DingeModelobj.batchDataInsertObj(finalArr, " qva_countries").then((row) => {
            response.data = row ;
            response.message = "Countries added successfully" ;
            return response;
        }).catch((err) => {

            throw Boom.badRequest( err );
       })
    })
    

};
