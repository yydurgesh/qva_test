import Boom from "boom";
import HttpStatus from "http-status-codes";
import logger from "~/utils/logger";
import VerifyModel from "~/v2/models/VerifyModel";
import WalletTransactionModel from "~/v2/models/WalletTransactionModel";
import statusConstants from "~/constants/statusConstants";
import databaseConstants from "~/constants/databaseConstants";
import DateTimeUtil from "~/utils/DateTimeUtil";
import FileUpload from "~/libraries/FileUpload";
import commonConstants from "~/constants/comman";
import envConstants from "~/constants/envConstants";
import AuthModel from "~/v2/models/AuthModel";
import NotificationModel from "~/v2/models/NotificationModel";

import { DateTime } from "~/libraries/DateTime";
import responseHelper from "~/utils/responseHelper";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
import { Security } from "~/libraries/Security";
import CommonFunction from "~/utils/CommonFunctions";
import { FirebaseLib } from "~/libraries/FirebaseLib";
import { S3Lib } from "~/libraries/S3Lib";

const basePath = envConstants.ASSETS_BASE_URL,
     awsS3BaseUrl = envConstants.AWS_S3_BASE_URL,
     staticPath = commonConstants.FILE_STATIC_PATH,
     fileUploadFolder = commonConstants.DOCUMENT_UPLOAD_FOLDER,
     profileImagePath = `${basePath}/${staticPath}/${fileUploadFolder}/`,
     defaultUserImage = commonConstants.DEFAULT_USER_IMAGE,
     defaultUserImagePath = `${basePath}/${defaultUserImage}`, 
     VerifyModelObj = new VerifyModel(),
     WalletTransactionObj = new WalletTransactionModel(),
     fileUploadObj = new FileUpload(),
     datetimeObj = new DateTime(),
     CommonFnObj = new CommonFunction(),
     AuthModelObj = new AuthModel(),
     firebaseLibObj = new FirebaseLib(),
     NotificationModelObj = new NotificationModel(),
     localeService = new LocaleService( i18n ),
     S3LibObj = new S3Lib();

/**
 * Send user message.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const addDocument = async function( req ) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    let image;
    let verify_image;

    if( req.files ) {
        image = req.files.verify_image;
        const fileData = await fileUploadObj.uploadFile( image, fileUploadFolder );
        
        if( fileData.name !== "" ) {
            verify_image = fileData.name;

            const s3Data = await S3LibObj.uploadToS3( image, fileUploadFolder, fileData.name );
           
        } else {
            throw Boom.serverUnavailable( localeService.translate( "FAIL_FILE_UPLOAD" ) );
        }
    }else{
        image = "";
    }
   
//    return fileUploadObj.uploadFile( image, fileUploadFolder )
//     .then( ( fileData ) => {
       
        // let verify_image;

        const requestData = req.body,
            user_id = requestData.user_id,
            response = { "data": { } };

        const createcolumn = {
            "verify_user_id": user_id,
            "created_at":currentDateTime,
            "updated_at":currentDateTime
        };

        if( verify_image != "" && verify_image != undefined ) {
            createcolumn.verify_image = verify_image;
        }

        // if( fileData.name !== "" ) {
        //     verify_image = fileData.name;
        //     createcolumn.verify_image = verify_image;
        // }else{
        //     verify_image = "";
        // }

        const fetchColumns = [
            "verify_id",
            "verify_status",
           
        ];
        const updateWhereCondition = {
            "user_id": user_id
        };

        return VerifyModelObj.createObj( createcolumn, fetchColumns )
            .then( ( updateUserData ) => {
                
                response.code = HttpStatus.OK;
                response.data = updateUserData;
                response.message = localeService.translate( "DOCUMENT_UPLOADED" );
                return response;
            } ).catch( ( updateErr ) => {
                const verify_image =fileData.name;
                        
                fileUploadObj.unlinkFile( verify_image, fileUploadFolder );
                logger.error( updateErr );
                throw Boom.badRequest( localeService.translate( "FAIL_USER_UPDATE" ) );
            } ); 
    // } )
    // .catch( ( uploadErr ) => {
    //     logger.error( uploadErr );
    //     throw Boom.serverUnavailable( localeService.translate( "FAIL_FILE_UPLOAD" ) );
    // } );
        
};
/**
 * Send user message.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const getDocument = function( request ) {
  
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
          user_id = requestData.user_id,
         fetchColumns = [
            "verify_id",
            "verify_user_id",
            "verify_image",
            "verify_status"
        ],
        where = {
         "verify_user_id": user_id,
         },
          response = { "data": { } };
     
    return VerifyModelObj.fetchObj( where ).then( ( verify ) => {
      
            // trim profile name.
        return VerifyModelObj.getRechargeCount( user_id ).then( ( totalRecharge ) => {   
                if( verify.length>0 ){
                    const  trimProfile = verify[ 0 ].verify_image !== null ? verify[ 0 ].verify_image.trim() : verify[ 0 ].verify_image;
                    if( trimProfile === "" || trimProfile == null ) {
                        verify[ 0 ].verify_image = `${basePath}/${defaultUserImage}`;
                    }else {
                        // verify[ 0 ].verify_image = `${basePath}/${staticPath}/${fileUploadFolder}/${verify[ 0 ].verify_image}`;
                        verify[ 0 ].verify_image = `${awsS3BaseUrl}${fileUploadFolder}/${verify[ 0 ].verify_image}`;
                        
                    }
                }
                response.data.verifyData = verify;
                response.totalRecharge = totalRecharge[0].count;

                return response;
        });
    }) 
};

export const deleteDocument = async function( req ) {
    const requestData = req.body,
        user_id = requestData.user_id,
        verify_id = requestData.verify_id, 
        fetchColumns = [
            "user_id",
            "verify_id"
        ],
        where = {
         "verify_user_id": user_id,
         "verify_id":verify_id
         },
        response = { "data": { } };
        const verifyData = await VerifyModelObj.fetchObj( where );
        if( verifyData.length > 0 ) {
            let deleteImage = `${fileUploadFolder}/${verifyData[0].verify_image}`;
            
            const s3DeleteData = await S3LibObj.deleteFile( deleteImage );
            
            await  fileUploadObj.unlinkFile( verifyData[0].verify_image, fileUploadFolder );
               
            return VerifyModelObj.deleteObj( where )
                .then( ( status ) => {
                    response.code = HttpStatus.OK;
                    response.message = localeService.translate( "DOCUMENT_DELETE" );
                    return response;
            })
        }else {
                throw Boom.unauthorized( localeService.translate( "Document not found." ) );
        }
}

/**
 * Update document status by Admin created by @hemant_solanki
 * 
 * @param {String} id
 */
export const updateDocumentStatus = function( req ) {
    
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = req.body,
        // token = req.cookies.admin_profile,
        // profileData = JSON.parse(token),
        adminId = Security.decryptId( requestData.admin_id),
        id = Security.decryptId( requestData.user_id ), 
        fetchColumns = [
            "verify_id",
            "verify_user_id",
            "verify_status"
        ],
        Updatecolumn = { "verify_status": requestData.verify_status, "updated_at": currentDateTime },
        updateWhere = { "verify_user_id": id },
        fetchWhere = { "auth_user_id": id },
        notificationColumn = {
            "notification_by": adminId,
            "notification_for": id,
            "read_status": 2,
            "notification_status": 2,
            "delete_status": 2,
            "created_at": currentDateTime,
        },
        selectNotificationData = [
            "notification_by",
            "notification_for",
            "notification_type" 
        ],
        
        byUserId = parseInt(adminId),
        response = { "data": { } };

    return VerifyModelObj.updateObj( Updatecolumn, updateWhere, fetchColumns )
        .then( ( updateVerifyData ) => {
            return AuthModelObj.fetchObj( fetchWhere )
            .then( ( userDetail ) => {
                
                var deviceToken = userDetail[0].auth_firebase_token;
                var referenceId = updateVerifyData[0].verify_id;
                var notificationType;
                var type;
                var title;
                var notificatioMsg;

                if( requestData.verify_status == 2) {
                    notificationType = 'Document verify';
                    title = 'Document verified';
                    notificatioMsg = 'Your uploaded document is verified, enjoy recharging';
                    type = 5;
                }
                if( requestData.verify_status == 3) {
                    notificationType = 'Document reject';
                    title = 'Document rejected';
                    notificatioMsg = 'Your uploaded document rejected by Admin';
                    type = 7;
                }

                var notification = {
                    verify_id: referenceId,
                    by_user_id: byUserId,
                    notification_type: notificationType,
                    click_action: "MainActivity",
                    title: title,
                    body: notificatioMsg
                }
                
                var objSavePayload = JSON.stringify(notification);
                firebaseLibObj.sendNotification(deviceToken, notification);
                notificationColumn.notification_msg = notificatioMsg;
                notificationColumn.notification_payload = objSavePayload;
                notificationColumn.notification_type = type;
                return NotificationModelObj.createObj( notificationColumn, selectNotificationData )
                    .then( ( ) => {
                    
                    response.code = HttpStatus.OK;
                    response.data.transfer = updateVerifyData[0];
                    response.message = localeService.translate( "UPDATE_DOCUMENT_STATUS" );
                    
                    return response;
                } ) .catch( ( error ) => {
                    throw Boom.badImplementation( localeService.translate( error ) );
                } );
            }).catch( ( err ) => {
                throw Boom.badImplementation( localeService.translate( err ) );
            } );
        }).catch( ( updateErr ) => {
            throw Boom.badImplementation( localeService.translate( updateErr ) );
        } );
}