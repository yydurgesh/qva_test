import Boom from "boom";
import HttpStatus from "http-status-codes";
import UserModel from "~/v2/models/UserModel";
import MessageModel from "~/v2/models/MessageModel";
import databaseConstants from "~/constants/databaseConstants";
import statusConstants from "~/constants/statusConstants";
import DateTimeUtil from "~/utils/DateTimeUtil";
import { Security } from "~/libraries/Security";
import { DateTime } from "~/libraries/DateTime";
import SMSLib from "~/libraries/SMSLib";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
import responseHelper from "~/utils/responseHelper";
import CommonFunction from "~/utils/CommonFunctions";

const UserModelObj = new UserModel(),
    MessageModelObj = new MessageModel(),
    SMSLibObj = new SMSLib(),
    datetimeObj = new DateTime(),
    CommonFnObj = new CommonFunction(),
    localeService = new LocaleService(i18n);

/**
 * Send user message.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const sendMessage = function (request) {
    const requestData = request.body,
        recieverContact = requestData.to_number,
        UserColumns = ["name"],
        userWhereCondition = { "user_id": requestData.user_id },
        msgColumns = ["msg_id"],
        msgData = {
            "msg_user_id": requestData.user_id,
            "msg_contact_id": request.contactId
        },
        response = { "data": {} };

    return UserModelObj.fetchObjWithSingleRecord(userWhereCondition, UserColumns).then((userData) => {
        const message = SMSLibObj.getUserMessageBody(requestData.message, userData.name);

        return SMSLibObj.sendSms(recieverContact, message).then(() => {
            msgData.msg_text = requestData.message;
            msgData.msg_status = statusConstants.MESSAGE_STATUS.SENT;
            msgData.created_at = DateTimeUtil.getCurrentTimeObjForDB();

            return MessageModelObj.createObj(msgData, msgColumns).then(() => {
                response.code = HttpStatus.OK;
                response.message = localeService.translate("MESSAGE_SENT");
                response.data.message = requestData.message;
                response.data.created_at = datetimeObj.changeFormat(msgData.created_at, databaseConstants.TIMESTAMP_FORMAT);

                return response;
            }).catch((error) => {
                throw Boom.badImplementation(localeService.translate(error));
            });
        }).catch((error) => {
            if (error.status === 400 && error.code === 21211) {
                throw Boom.badRequest(error.message, { "code": 21211 });
            } else {
                throw Boom.badImplementation(localeService.translate(error));
            }
        });

    }).catch((error) => {
        if (error.output.statusCode === 400) {
            throw Boom.badRequest(error.message);
        } else {
            throw Boom.badImplementation(localeService.translate(error));
        }
    });
};

/**
 * Get user messages.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const getUserMessages = function (request) {

    const requestData = request.query,
        userId = CommonFnObj.getUserId(request),
        page = requestData.page_no,
        countColumn = ["msg_contact_id"],
        whereCondition = { "messages.msg_user_id": userId },
        limit = databaseConstants.LIMIT,
        response = { "data": {} };
    const offset = CommonFnObj.getPageOffset(page, limit);



    return MessageModelObj.getUserMessageHistoryList(userId, limit, offset, requestData).then((messagesList) => {

        return datetimeObj.changeDatabaseTimestampFormat(messagesList, "created_at", databaseConstants.TIMESTAMP_FORMAT).then((messageData) => {
            return MessageModelObj.getCountDistinct(whereCondition, countColumn).then((count) => {
                response.code = HttpStatus.OK;
                response.data.totalMessages = count[0];

                return responseHelper.encryptobjectAllKeys(messageData, databaseConstants.CONTACT_KEY).then((message) => {
                    response.data.messages = message;
                    // 
                    return response;
                }).catch((error) => {
                   
                    throw Boom.badImplementation(localeService.translate(error));
                });
            }).catch((error) => {
         
                throw Boom.badImplementation(localeService.translate(error));
            });
        }).catch((error) => {
            
            throw Boom.badImplementation(localeService.translate(error));
        });
    }).catch((error) => {
       
        throw Boom.badImplementation(localeService.translate(error));
    });
};

/**
 * Get user message-detail.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const getMessageDetail = function (request) {
    const requestData = request.query,
        userId = CommonFnObj.getUserId(request),
        contactId = Security.decryptId(requestData.msg_contact_id),
        page = requestData.page_no,
        countColumn = ["msg_contact_id"],
        columns = [
            "msg_text",
            "created_at"
        ],
        whereCondition = {
            "msg_user_id": userId,
            "msg_contact_id": contactId
        },
        limit = databaseConstants.LIMIT,
        response = { "data": {} };
    const offset = CommonFnObj.getPageOffset(page, limit);


    return MessageModelObj.getMessageHistoryDetail(whereCondition, columns, limit, offset).then((messagesList) => {
        return datetimeObj.changeDatabaseTimestampFormat(messagesList, "created_at", databaseConstants.TIMESTAMP_FORMAT).then((messageData) => {
            return MessageModelObj.getCount(whereCondition, countColumn).then((count) => {

                response.code = HttpStatus.OK;
                response.data.totalMessages = count[0];
                // response.data.messages = messageData;
                response.data.messages = messageData.reverse();

                return response;
            }).catch((error) => {
                throw Boom.badImplementation(localeService.translate(error));
            });
        }).catch((error) => {
            throw Boom.badImplementation(localeService.translate(error));
        });
    }).catch((error) => {
        throw Boom.badImplementation(localeService.translate(error));
    });

};



/**
 * Check limit user message.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const checkMessageLimit = function (request, next) {
    const requestData = request.body ;
    
    return MessageModelObj.getUserMessageMonthList(requestData.user_id).then((userData) => {
       if(userData >= 10){
        return  next(Boom.badRequest(localeService.translate("Your monthly message limit exceeded"))) ;
       }
        next();
         
    }).catch((error) => {
        throw Boom.badImplementation(localeService.translate(error));
    }); 
};
