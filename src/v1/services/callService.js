import Boom from "boom";
import HttpStatus from "http-status-codes";
import logger from "~/utils/logger";
import CallModel from "~/v2/models/CallModel";
import statusConstants from "~/constants/statusConstants";
import databaseConstants from "~/constants/databaseConstants";
import DateTimeUtil from "~/utils/DateTimeUtil";
import { DateTime } from "~/libraries/DateTime";
import responseHelper from "~/utils/responseHelper";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
import { Security } from "~/libraries/Security";
import CommonFunction from "~/utils/CommonFunctions";

const CallModelObj = new CallModel(),
    datetimeObj = new DateTime(),
    CommonFnObj = new CommonFunction(),
    localeService = new LocaleService( i18n );

/**
 * Send user message.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const call = function( request ) {
    
    const requestData = request.body,
        callColumns = [ "call_id" ],
        callData = {
            "call_user_id": requestData.user_id,
            "call_contact_id": request.contactId
        },
        response = { "data": { } };

    callData.call_status = statusConstants.CALL_STATUS.DONE;
    callData.created_at = DateTimeUtil.getCurrentTimeObjForDB();

    return CallModelObj.createObj( callData, callColumns ).then( ( ) => {
        response.code = HttpStatus.OK;

        return response;
    } ).catch( ( error ) => {
        throw Boom.badImplementation( localeService.translate( error ) );
    } );     
};

/**
 * Make user call.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const getUserCalls = function( request ) {

    const requestData = request.query,
        userIdByAuthToken = CommonFnObj.getUserId( request ),
        page = requestData.page_no,
        countColumn = [ "call_contact_id" ],
        whereCondition = { "calls.call_user_id": userIdByAuthToken },
        limit = databaseConstants.LIMIT,
        response = { "data": { } };

    const offset = CommonFnObj.getPageOffset( page, limit );
    
    return CallModelObj.getUserCallList( userIdByAuthToken, limit, offset, requestData ).then( ( callsList ) => {
        return datetimeObj.changeDatabaseTimestampFormat( callsList, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( ( callData ) => {
            return CallModelObj.getCountDistinct( whereCondition, countColumn ).then( ( count ) => {
                response.code = HttpStatus.OK;
                response.data.totalCalls = count[ 0 ];

                return responseHelper.encryptobjectAllKeys( callData, databaseConstants.CONTACT_KEY ).then( ( calls ) => {
                    response.data.calls = calls;
                
                    return response;
                } ).catch( ( error ) => {
                    logger.error( error );
                    throw Boom.badImplementation( localeService.translate( error ) );
                } );
            } ).catch( ( error ) => {
                throw Boom.badImplementation( localeService.translate( error ) );
            } ); 
        } ).catch( ( error ) => {
            throw Boom.badImplementation( localeService.translate( error ) );
        } );
    } ).catch( ( error ) => {
        throw Boom.badImplementation( localeService.translate( error ) );
    } ); 
};

/**
 * Get user call details.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const getCallDetails = function( request ) {

    const requestData = request.query,
        userIdByAuthToken = CommonFnObj.getUserId( request ),
        page = requestData.page_no,
        countColumn = [ "calls.call_contact_id" ],
        contactId = Security.decryptId( requestData.contact_id ),
        columns = [ 
            "contacts.contact_country_code", 
            "contacts.contact_number", 
            "calls.created_at"
        ],
        whereCondition = { "calls.call_user_id": userIdByAuthToken, "contacts.contact_id": contactId },
        limit = databaseConstants.LIMIT,
        response = { "data": { } };
    const offset = CommonFnObj.getPageOffset( page, limit );

    return CallModelObj.getUserCallDetails( whereCondition, columns, limit, offset ).then( ( callsList ) => {
        response.code = HttpStatus.OK;

        return datetimeObj.changeDatabaseTimestampFormat( callsList, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( ( callData ) => {
            return CallModelObj.getCountCall( whereCondition, countColumn ).then( ( count ) => {
                response.data.totalCalls = count[ 0 ];
                response.data.calls = callData;
                    
                return response;
            } ).catch( ( error ) => {
                logger.error( error );
                throw Boom.badImplementation( localeService.translate( error ) );
            } );
        } ).catch( ( error ) => {
            logger.error( error );
            throw Boom.badImplementation( localeService.translate( error ) );
        } );
    } ).catch( ( error ) => {
        throw Boom.badImplementation( localeService.translate( error ) );
    } ); 
           
};
