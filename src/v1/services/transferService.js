import Boom from "boom";
import HttpStatus from "http-status-codes";
import logger from "~/utils/logger";
import TransferModel from "~/v2/models/TransferModel";
import WalletModel from "~/v2/models/WalletModel";
import WalletTransactionModel from "~/v2/models/WalletTransactionModel";
import AuthModel from "~/v2/models/AuthModel";
import NotificationModel from "~/v2/models/NotificationModel";
import UserModel from "~/v2/models/UserModel";
import envConstants from "~/constants/envConstants";


import statusConstants from "~/constants/statusConstants";
import databaseConstants from "~/constants/databaseConstants";
import DateTimeUtil from "~/utils/DateTimeUtil";
import { DateTime } from "~/libraries/DateTime";
import responseHelper from "~/utils/responseHelper";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
import { Security } from "~/libraries/Security";
import CommonFunction from "~/utils/CommonFunctions";
import { FirebaseLib } from "~/libraries/FirebaseLib";
import SettingModel from "~/v2/models/SettingModel";
import SMSLib from "~/libraries/SMSLib";
import Email from "~/libraries/Email";
import dataConstants from "../../constants/dataConstants";


const basePath = envConstants.ASSETS_BASE_URL,
    transferObj = new TransferModel(),
    WalletObj     = new WalletModel(),
    WalletTransactionObj = new WalletTransactionModel(),
    AuthModelObj = new AuthModel(),
    NotificationModelObj = new NotificationModel(),
    UserModelObj            = new UserModel(),
    datetimeObj = new DateTime(),
    CommonFnObj = new CommonFunction(),
    firebaseLibObj = new FirebaseLib(),
    SettingModelObj = new SettingModel(),
    SMSLibObj = new SMSLib(),
    emailObj     = new Email(),
    localeService = new LocaleService( i18n );

/**
 * Send user message.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const transferByContactNo = async function( request ) {
   
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    // let redirectUrl = `${basePath}/recharge`;
    let redirectUrl = `${basePath}/public/assets/website/img/qvaring_img.png`;
    const requestData = request.body,
        currentYear = DateTimeUtil.getCurrentYear(),
        nextYear = DateTimeUtil.getNextYear(),
        userType = request.headers['x-user-type'],
        walletWhere ={
           'wallet_user_id':requestData.transfer_to_user_id 
        },
        byUserWalletWhere ={
           'wallet_user_id':requestData.user_id 
        },
        updateWallet ={
            "updated_at":currentDateTime
        },
        columns = [ "transfer_id","transfer_to_user_id","transfer_by_user_id","transfer_amount" ],
        transferData = {
            "transfer_to_user_id": requestData.transfer_to_user_id,
            "transfer_by_user_id": requestData.user_id,
            "transfer_amount": requestData.transfer_amount,
            "transfer_type":1,
            "transfer_status":2,
            "created_at":currentDateTime,
            "updated_at":currentDateTime
        },
        createWalletTrs = {
            "wt_amount": requestData.transfer_amount,
            "user_id": requestData.user_id,
            "created_at":currentDateTime,
            "updated_at":currentDateTime
        },
        //Create notification by @hemant_solanki
        fetchWhere = { "auth_user_id": requestData.transfer_to_user_id },
        notificationColumn = {
            "notification_by": requestData.user_id,
            "notification_for": requestData.transfer_to_user_id,
            "notification_type": 2,
            "read_status": 2,
            "notification_status": 2,
            "delete_status": 2,
            "created_at": currentDateTime,
        },
        selectNotificationData = [
            "notification_by",
            "notification_for",
            "notification_type" 
        ],
        notificationType = 'Transfer amount',
        byUserId = requestData.user_id,
        response = { "data": { } };

    /*code for transfer limit*/

    var endDate = datetimeObj.currentDate();
    var startDate = datetimeObj.currentMonthStartData();
    var transferInfo = await transferObj.getUserTransferInfo(requestData.user_id, endDate, endDate);
    const total_transfer_amount = Number(transferInfo[0] ? transferInfo[0].transfer_sum : 0) + Number(requestData.transfer_amount);
    const total_transfer_number = Number(transferInfo[0] ? transferInfo[0].transfer_count : 0);
    const limitData = await SettingModelObj.fetchObjWithSingleRecord('', ["user_transfer_amount","reseller_transfer_amount","user_transfer_limit","reseller_transfer_limit"]);

    var byUserData =  await WalletObj.fetchFirstObj( byUserWalletWhere );
    
    var limit_nm = 3;
    var limit_amount = 2000;
    if( await limitData ){
         if( userType ==1 ){
              limit_nm =limitData.user_transfer_limit;
              limit_amount = limitData.user_transfer_amount;
         }else{
               limit_nm =limitData.reseller_transfer_limit;
               limit_amount = limitData.reseller_transfer_amount;
         }
    }

    if( Number(requestData.transfer_amount) < 1 ){
        throw Boom.badRequest( localeService.translate( "ENVALID_INPUT_AMOUNT" ) );
    }

    if(Number(byUserData.wallet_amount) < Number(requestData.transfer_amount) ){
        throw Boom.badRequest( localeService.translate( "ADD_MONEY" ) );
    }

    if( total_transfer_number >= limit_nm ){
        throw Boom.badRequest( localeService.translate( "TRANSACTION_DAILY_LIMIT" ) );
    }

    if( total_transfer_amount > limit_amount ) {
        throw Boom.badRequest( localeService.translate( "TRANSACTION_AMOUNT_LIMIT" ) );
    }


    /*end*/    
    // var byUserData =  await WalletObj.fetchFirstObj( byUserWalletWhere );
    var sentName =  await UserModelObj.fetchFirstObj( {'user_id':requestData.user_id} );
    var toUserName = await UserModelObj.fetchFirstObj( {'user_id':requestData.transfer_to_user_id} );
    const senderUserName = sentName.name;
    const receiverUserName = toUserName.name;

    //check number greater than 0
    // if( Number(requestData.transfer_amount) < 1 ){
    //     throw Boom.badRequest( localeService.translate( "ENVALID_INPUT_AMOUNT" ) );
    // }

    // if(Number(byUserData.wallet_amount) < Number(requestData.transfer_amount) ){
    //     throw Boom.badRequest( localeService.translate( "ADD_MONEY" ) );
    // }

    return transferObj.createObj( transferData, columns ).then( async ( transferStatus ) => {
      //  to use data
       var walletData =  await WalletObj.fetchFirstObj( walletWhere );
       createWalletTrs.wallet_id = walletData.wallet_id;
       createWalletTrs.wt_type = 1;
       createWalletTrs.wt_reference_id = transferStatus.transfer_id;
       createWalletTrs.wt_reference_type = 2;
       createWalletTrs.wt_status = 1;

       createWalletTrs.wt_pre_amount = walletData.wallet_amount;
       createWalletTrs.wt_balance = Number(walletData.wallet_amount) + Number(requestData.transfer_amount);

       await WalletTransactionObj.createObj(createWalletTrs,["wt_id"]);
       updateWallet.wallet_amount = Number(walletData.wallet_amount) + Number(requestData.transfer_amount); 
       await WalletObj.updateObj(updateWallet, walletWhere, ["wallet_id"]);
      // end
      // by user data
       updateWallet.wallet_amount = Number(byUserData.wallet_amount) - Number(requestData.transfer_amount); 
       createWalletTrs.wallet_id = byUserData.wallet_id;
       createWalletTrs.wt_type = 2;

       createWalletTrs.wt_pre_amount = byUserData.wallet_amount;
       createWalletTrs.wt_balance = Number(byUserData.wallet_amount) - Number(requestData.transfer_amount);

       await WalletTransactionObj.createObj(createWalletTrs,["wt_id"]);
       await WalletObj.updateObj(updateWallet, byUserWalletWhere, ["wallet_id"]);
      // end 
        /*code for otp*/
        const receiverInfo = await UserModelObj.fetchObjWithSingleRecord({'user_type':2},["country_code","contact_number"]);
       
        const sendMsg = senderUserName+''+'has transfered '+'$'+requestData.transfer_amount+' '+'USD'+' '+'to'+' '+receiverUserName+', visit admin from more info ';
        
        if( requestData.transfer_amount>200 ){
        await SMSLibObj.sendSms( receiverInfo.country_code+receiverInfo.contact_number, sendMsg );
        }
        /*end*/
      return AuthModelObj.fetchObj( fetchWhere )
        .then( ( userDetail ) => {
           
            var deviceToken = userDetail[0].auth_firebase_token;
            var referenceId = transferStatus.transfer_id;
            var notification = {
                transfer_id: referenceId,
                by_user_id: byUserId,
                notification_type: notificationType,
                click_action: "MainActivity",
                // icon: redirectUrl,
                // click_action: redirectUrl,
                // web_click_action: redirectUrl,
                title: 'Transfer amount',
                body: senderUserName+' '+'sent you'+' '+`$ ${requestData.transfer_amount}`
            }
            
            var objSavePayload = JSON.stringify(notification);
            firebaseLibObj.sendNotification(deviceToken, notification);
                notificationColumn.notification_msg = senderUserName+' '+'sent you'+' '+`$ ${requestData.transfer_amount}`;
                notificationColumn.notification_payload = objSavePayload;

              return NotificationModelObj.createObj( notificationColumn, selectNotificationData )
                  .then( ( ) => {  
                    return UserModelObj.getTransferUserDetail(requestData).then( ( transferUserInfo ) => {
                        return UserModelObj.fetchFirstObj({'user_type': dataConstants.USER_TYPE.USER_ADMIN }).then( ( adminInfo ) => {
                            
                            const mailDetails = {
                                "template": "transfer-to-contact",
                                "to": adminInfo.email,
                                "subject": "Qvaring - Transfer amount",
                                "dynamicFields": {
                                    "title": "Qvaring",
                                    "cubaring_icon": `${basePath}/public/email_icon/email_logo.png`,
                                    "latter_icon": `${basePath}/public/email_icon/letter.png`,
                                    "transfer_by": `${transferUserInfo[0].by_user}`,
                                    "transfer_to":`${transferUserInfo[0].to_user}`,
                                    "amount": `${requestData.transfer_amount}`,
                                    "nextYear": `${nextYear}`,
                                    "year": `${currentYear}`
                                }
                            };
                            // Sent Reset Password mail
                            return emailObj.sendEmail( mailDetails ).then( () => {
                                response.code = HttpStatus.OK;
                                return response;
                            } );
                        } ).catch( ( error ) => {
                            throw Boom.badImplementation( localeService.translate( error ) );
                        } );
                    } ).catch( ( error ) => {
                        throw Boom.badImplementation( localeService.translate( error ) );
                    } );
                } ).catch( ( error ) => {
                    throw Boom.badImplementation( localeService.translate( error ) );
                } );
            }).catch( ( updateErr ) => {
                throw Boom.badImplementation( localeService.translate( updateErr ) );
            } );

       
    } ).catch( ( error ) => { 
        throw Boom.badImplementation( localeService.translate( error ) );
    } );  
   
};

export const transferByAddress = async function( request ) {
    
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();

    const requestData = request.body,
        byUserWalletWhere ={
           'wallet_user_id':requestData.user_id 
        },
        updateWallet ={
            "updated_at":currentDateTime
        },
        columns = [ "transfer_id","transfer_to_user_id","transfer_by_user_id","transfer_amount" ],
        transferData = {
            "transfer_by_user_id": requestData.user_id,
            "recipient_name": requestData.recipient_name,
            "transfer_amount": requestData.transfer_amount,
            "recipient_contact_no":requestData.recipient_contact_no,
            "address":requestData.address,
            "latitude":requestData.latitude,
            "longitude":requestData.longitude,
            "transfer_type":2,
            "transfer_status":1,
            "created_at":currentDateTime,
            "updated_at":currentDateTime
        },
        createWalletTrs = {
            "wt_amount": requestData.transfer_amount,
            "user_id": requestData.user_id,
            "created_at":currentDateTime,
            "updated_at":currentDateTime
           
        },
        response = { "data": { } };
       
    var byUserData =  await WalletObj.fetchFirstObj( byUserWalletWhere );
 
    //check number greater than 0
    if (!requestData.address.match(/cuba/i)) { 
         throw Boom.badRequest( localeService.translate( "CORRECT_ADDRESS" ) );
     }
    if( Number(requestData.transfer_amount) < 1 ){
        throw Boom.badRequest( localeService.translate( "INVALID_INPUT_AMOUNT" ) );
    }

    if(Number(byUserData.wallet_amount) < Number(requestData.transfer_amount)){
        throw Boom.badRequest( localeService.translate( "ADD_MONEY" ) );
    }
    
    return transferObj.createObj( transferData, columns ).then( async ( transferStatus ) => {
    
       createWalletTrs.wt_reference_id = transferStatus.transfer_id;
        createWalletTrs.wt_reference_type = 2;
        createWalletTrs.wt_status = 2;
       // by user data
       updateWallet.wallet_amount = Number(byUserData.wallet_amount) - Number(requestData.transfer_amount); 
       createWalletTrs.wallet_id = byUserData.wallet_id;
       createWalletTrs.wt_type = 2;
       createWalletTrs.wt_pre_amount = byUserData.wallet_amount;
       createWalletTrs.wt_balance = Number(byUserData.wallet_amount) - Number(requestData.transfer_amount);
       await WalletTransactionObj.createObj(createWalletTrs,["wt_id"]);
       await WalletObj.updateObj(updateWallet, byUserWalletWhere, ["wallet_id"]);
      // end 
        response.code = HttpStatus.OK;

        return response;
    } ).catch( ( error ) => { 
        throw Boom.badImplementation( localeService.translate( error ) );
    } );  
   
};

export const transferHisttory =  async function( request ) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
          response = { "data": { } };
    const totalCount = await transferObj.getTransferHistoryCount( requestData );

    return transferObj.getTransferHistory( requestData ).then( ( historyList ) => {
      response.data.historyList = historyList;
      response.recordsTotal = totalCount[0].count;
      response.recordsFiltered  = totalCount[0].count;
      
      return response;
    }) 
};

export const transferList =  async function( request ) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
          response = { "data": { } };
    const totalCount = await transferObj.transferListCount( requestData );
    return transferObj.transferList( requestData ).then( ( historyList ) => {
        return datetimeObj.changeDatabaseTimestampFormat( historyList, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( ( transferList ) => {
            response.code = HttpStatus.OK;
            response.data.historyList = transferList;
            response.page = requestData.start;
            response.recordsTotal = totalCount[0].count;
            response.recordsFiltered = totalCount[0].count;
            return response;
        } ).catch( ( error ) => {
            logger.error( error );
            throw Boom.notImplemented( localeService.translate( error ) );
        } );
    }) 
};

/**
 * Update transfer status by Admin created by @hemant_solanki
 * 
 * @param {String} id
 */
export const updateTransferStatus = function( req ) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = req.body,
        token = req.cookies.admin_profile,
        profileData = JSON.parse(token),
        adminId = Security.decryptId( profileData.user.user_id),
        // id = Security.decryptId( requestData.transfer_id ), 
        id = requestData.transfer_id, 
        fetchColumns = [
            "transfer_id",
            "transfer_status",
            "updated_at"
        ],
        Updatecolumn = { "transfer_status": requestData.transfer_status, "updated_at": currentDateTime },
        updateWhere = { "transfer_id": id },
        fetchWalletColumns = [
            "wallet_id",
            "wallet_user_id",
            "wallet_amount"
        ],
        UpdateWalletcolumn = { "updated_at": currentDateTime },
        updateWalletWhere = { "wallet_id": requestData.wallet_id },
        fetchWhere = { "auth_user_id": requestData.transfer_by_user_id },
        column = {
            "wallet_id": requestData.wallet_id,
            "wt_type": 1,
            "wt_amount": parseFloat(requestData.transfer_amount),
            "wt_reference_type": 5,
            "wt_status": 1,
            "wt_currency": "USD",
            "user_id": adminId,
            "wt_payment_status": 6,
            "created_at":currentDateTime,
            "updated_at":currentDateTime
        },
        selectData = [ 
            "wt_id", 
            "wt_type",
        ],
        notificationColumn = {
            "notification_by": adminId,
            "notification_for": requestData.transfer_by_user_id,
            "notification_type": 2,
            "read_status": 2,
            "notification_status": 2,
            "delete_status": 2,
            "created_at": currentDateTime,
        },
        selectNotificationData = [
            "notification_by",
            "notification_for",
            "notification_type" 
        ],
        notificationType = 'Transfer amount',
        senderUserName = profileData.user.name,
        byUserId = parseInt(adminId),
        response = { "data": { } };
        
        return AuthModelObj.fetchObj( fetchWhere )
            .then( ( userDetail ) => {
                var deviceToken = '' ;
                if(userDetail.length > 0){
                     deviceToken = userDetail[0].auth_firebase_token;
                }
               
                var referenceId = parseInt(id);
                
                if( requestData.transfer_status == 2 ) {
                    return transferObj.updateObj( Updatecolumn, updateWhere, fetchColumns )
                        .then( ( updateTransferData ) => {
                            var notification = {
                                transfer_id: referenceId,
                                by_user_id: byUserId,
                                notification_type: notificationType,
                                click_action: "MainActivity",
                                title: 'Transfer amount',
                                body: 'Amount has been transfered successfully to your requested address.'
                            }
                            var objSavePayload = JSON.stringify(notification);

                            firebaseLibObj.sendNotification(deviceToken, notification);
                            notificationColumn.notification_msg = "Amount has been transfered successfully to your requested address.";
                            notificationColumn.notification_payload = objSavePayload;

                            return NotificationModelObj.createObj( notificationColumn, selectNotificationData )
                                .then( ( ) => {
                                    response.code = HttpStatus.OK;
                                    response.data.transfer = updateTransferData[0];
                                    response.message = localeService.translate( "UPDATE_TRANSFER_STATUS" );
                                    
                                    return response;
                            } ).catch( ( error ) => {
                                throw Boom.badImplementation( localeService.translate( error ) );
                            } );
                        }).catch( ( updateErr ) => {
                            throw Boom.badImplementation( localeService.translate( updateErr ) );
                        } );
                }

                if( requestData.transfer_status == 3 ) {
                    return WalletObj.fetchFirstObj(  updateWalletWhere )
                    .then( ( walletData ) => {
                        let totalAmount = Number(walletData.wallet_amount) + Number(requestData.transfer_amount);
                        UpdateWalletcolumn.wallet_amount = totalAmount;
                    
                        return WalletObj.updateObj( UpdateWalletcolumn, updateWalletWhere, fetchWalletColumns )
                        .then( ( updateUserWallet ) => {
                            
                            return WalletTransactionObj.createObj( column, selectData )
                                .then( ( ) => {
                                return transferObj.updateObj( Updatecolumn, updateWhere, fetchColumns )
                                    .then( ( updateTransferData ) => {

                                        var notification = {
                                            transfer_id: referenceId,
                                            by_user_id: byUserId,
                                            notification_type: notificationType,
                                            click_action: "MainActivity",
                                            title: 'Transfer amount',
                                            body: 'Transfer request on given address has been denied, Refund is being processed.'
                                        }
                                        
                                        var objSavePayload = JSON.stringify(notification);
                                        firebaseLibObj.sendNotification(deviceToken, notification);
                                        notificationColumn.notification_msg = 'Transfer request on given address has been denied, Refund is being processed.';
                                        notificationColumn.notification_payload = objSavePayload;
            
                                        return NotificationModelObj.createObj( notificationColumn, selectNotificationData )
                                            .then( ( ) => {
                                        
                                                response.code = HttpStatus.OK;
                                                response.data.transfer = updateTransferData[0];
                                                response.message = localeService.translate( "UPDATE_TRANSFER_STATUS" );
                                                
                                                return response;
                                            } ).catch( ( error ) => {
                                                throw Boom.badImplementation( localeService.translate( error ) );
                                            } );
                                    }).catch( ( updateErr ) => {
                                        throw Boom.badImplementation( localeService.translate( updateErr ) );
                                    } );
                                } ).catch( ( error ) => {
                                    throw Boom.badImplementation( localeService.translate( error ) );
                                } );
                            }).catch( ( updateErr ) => {
                                throw Boom.badImplementation( localeService.translate( updateErr ) );
                            } );
                        }).catch( ( updateErr ) => {
                            throw Boom.badImplementation( localeService.translate( updateErr ) );
                        } );
                }
            }).catch( ( updateErr ) => {

                throw Boom.badImplementation( localeService.translate( updateErr ) );
            } );
}

export const adminTransferHisttory = async function( request ) {
    
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
          walletWhere ={
          },
          response = { "data": { } };
    const totalCount = await transferObj.getadminTransferHisttoryCount( requestData );
    response.code = HttpStatus.OK;
    return transferObj.getadminTransferHisttory( requestData ).then( ( history ) => {
      response.data.transferHistory = history;
      response.recordsTotal = totalCount[0].count;
      response.recordsFiltered = totalCount[0].count;
      return response;
    }) 
};


export const getTransferHisttory = async function( request ) {
    
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
            Page = requestData.start,
            limit = 20,//databaseConstants.LIMIT,
    
          response = { "data": { } };
          let offset = databaseConstants.OFFSET;
            if(Page !== undefined) {
                offset = Page;
            }
    const totalCount = await transferObj.getTransferHisttoryCount( requestData );
    response.code = HttpStatus.OK;
    return transferObj.getTransferHisttory( requestData, offset, limit ).then( ( history ) => {
        return datetimeObj.changeDatabaseTimestampFormat( history, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( ( transferHistory ) => {

            // response.data.transferHistory = history;
            response.data.transferHistory = transferHistory;
            response.recordsTotal = totalCount[0].count;
            response.recordsFiltered = totalCount[0].count;
            response.page = offset;

            return response;
        } ).catch( ( error ) => {
            logger.error( error );
            throw Boom.notImplemented( localeService.translate( error ) );
        } );
    }) 
};


export const getTransferListByAdmin = async function( request ) {
    
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
            Page = requestData.start,
            limit = 20,//databaseConstants.LIMIT,
    
          response = { "data": { } };
          let offset = databaseConstants.OFFSET;
            if(Page !== undefined) {
                offset = Page;
            }
    const totalCount = await transferObj.getTransferListByAdminCount( requestData );
    response.code = HttpStatus.OK;
    return transferObj.getTransferListByAdmin( requestData, offset, limit ).then( ( history ) => {
        return datetimeObj.changeDatabaseTimestampFormat( history, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( ( transferList ) => {
            response.data.transferHistory = transferList;
            response.recordsTotal = totalCount[0].count;
            response.recordsFiltered = totalCount[0].count;
            response.page = offset;

            return response;
        } ).catch( ( error ) => {
            logger.error( error );
            throw Boom.notImplemented( localeService.translate( error ) );
        } );
    }) 
};