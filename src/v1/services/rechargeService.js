import Boom from "boom";
import HttpStatus from "http-status-codes";
import databaseConstants from "~/constants/databaseConstants";
import statusConstants from "~/constants/statusConstants";
import { DateTime } from "~/libraries/DateTime";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
import responseHelper from "~/utils/responseHelper";
import CommonFunction from "~/utils/CommonFunctions";
import { AirtimeRechargeLib } from "~/libraries/AirtimeRechargeLib";
import { DingRechargeLib } from "~/libraries/DingRechargeLib";
import { StripeLib } from "~/libraries/StripeLib";
import { Security } from "~/libraries/Security";
import { convertData } from "~/utils/convertData";
import UserModel from "~/v2/models/UserModel";
import RechargeModel from "~/v2/models/RechargeModel";
import RechargePaymentModel from "~/v2/models/RechargePaymentModel";
import RechargeSettingModel from "~/v2/models/RechargeSettingModel";
import VerifyModel from "~/v2/models/VerifyModel";
import WalletModel from "~/v2/models/WalletModel";
import WalletTransactionModel from "~/v2/models/WalletTransactionModel";
import AuthModel from "~/v2/models/AuthModel";
import NotificationModel from "~/v2/models/NotificationModel";
import RechargeLimitModal from "~/v2/models/RechargeLimitModel";
import StripeFeeTaxModel from "~/v2/models/StripeFeeTaxModel";
import OperatorModel from "~/v2/models/OperatorModel";
import ProductModel from "~/v2/models/ProductModel";
import envConstants from "~/constants/envConstants";

const moment = require("moment-timezone");

import SettingModel from "~/v2/models/SettingModel";
import CommissionOperatorModel from "~/v2/models/CommissionOperatorModel";
import UserCommissionModel from "~/v2/models/UserCommissionModel";

import dataConstants from "~/constants/dataConstants";
import commonConstants from "~/constants/comman";

import DateTimeUtil from "~/utils/DateTimeUtil";
import logger from "~/utils/logger";
import { FirebaseLib } from "~/libraries/FirebaseLib";

const airtimeRechargeLibObj = new AirtimeRechargeLib(),
    dingRechargeLibObj = new DingRechargeLib(),
    datetimeObj = new DateTime(),
    CommonFnObj = new CommonFunction(),
    convertDataObj = new convertData(),
    UserModelObj = new UserModel(),
    StripeLibObj = new StripeLib(),
    RechargeModelObj = new RechargeModel(),
    RechargePaymentModelObj = new RechargePaymentModel(),
    RechargeSettingModelObj = new RechargeSettingModel(),
    VerifyModelObj = new VerifyModel(),
    WalletModelObj = new WalletModel(),
    WalletTransactionObj = new WalletTransactionModel(),
    SettingModelObj = new SettingModel(),
    CommissionOperatorModelObj = new CommissionOperatorModel(),
    UserCommissionModelObj = new UserCommissionModel(),
    AuthModelObj = new AuthModel(),
    NotificationModelObj = new NotificationModel(),
    StripeFeeTaxModelObj = new StripeFeeTaxModel(),
    OperatorModelObj = new OperatorModel(),
    ProductModelObj = new ProductModel(),


    firebaseLibObj = new FirebaseLib(),
    localeService = new LocaleService(i18n),
    RechargeLimitModalObj = new RechargeLimitModal(),
    
    userRechargeLimit = commonConstants.RECHARGE_LIMIT,
    userRechargeAmountLimit = commonConstants.RECHARGE_AMOUNT_LIMIT;


/**
 * Get MSISDN Info
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const getMsisdnInfo = function (request) {
    const requestData = request.body,
        mobileNumber = requestData.recharge_mobile_number,
        response = { "data": {} };

    return airtimeRechargeLibObj.getMsisdnInfo(mobileNumber).then((responseData) => {

        let error_code = responseData.TransferTo.error_code._text;
        if (error_code != 0) {
            if (error_code == 101) {
                throw Boom.badRequest(localeService.translate("VALID_MOBILE_NUMBER"));
            }
            throw Boom.badRequest(responseData.TransferTo.error_txt._text);
        }
        response.data = responseData;
        return response;
    }).catch((error) => {
        throw Boom.badRequest(error);
    });
};

/**
 * Get list of operators
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const getOperators = function (request) {

    const requestData = request.body,
        country_id = requestData.recharge_country_id,
        response = { "data": {} };
    return airtimeRechargeLibObj.getOperators(country_id).then(async (responseData) => {

        let error_code = responseData.TransferTo.error_code._text;
        if (error_code != 0) {
            throw Boom.badRequest(responseData.TransferTo.error_txt._text);
        }
        let idArray = responseData.TransferTo.operatorid._text;
        let valueArray = responseData.TransferTo.operator._text;
        let operatorData = await convertDataObj.list2json(idArray, valueArray);

        response.data = operatorData;
        return response;
    }).catch((error) => {
        throw Boom.badRequest(error);
    });
};

/**
 * Get list of operators
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const getProducts = async function (request) {
    //var rid = await airtimeRechargeLibObj.getReserveId();
    const requestData = request.body,
        operator_id = requestData.recharge_operator_id,
        response = { "data": {} };
    return airtimeRechargeLibObj.getOperatorProducts(operator_id).then(async (responseData) => {
        let error_code = responseData.TransferTo.error_code._text;
        if (error_code != 0) {
            throw Boom.badRequest(responseData.TransferTo.error_txt._text);
        }
        let productArray = responseData.TransferTo.product_list._text;
        let currency = responseData.TransferTo.destination_currency._text;
        let productData = await convertDataObj.list2productArray(productArray, currency);
        response.data = productData;

        return response;
    }).catch((error) => {
        throw Boom.badRequest(error);
    });
};

const getStripeCustomerID = async function (user_id) {
    let searchQuery = {
        "user_id": user_id
    };

    let user = await UserModelObj.fetchObj(searchQuery);
    var stripe_customer_id = user[0]['stripe_customer_id'];
    var user_email = user[0]['email'];
    if (stripe_customer_id == null || stripe_customer_id == "") {
        if (user_email != null || user_email != "") {
            stripe_customer_id = await StripeLibObj.createCustomer(user_email);
            let updateQuery = {
                "stripe_customer_id": stripe_customer_id
            };
            UserModelObj.updateObj(updateQuery, { "user_id": user_id });
        } else {

        }
    }

    return stripe_customer_id;
}

const refundRechargeAmount = async function (rechargeRefundObj, ChargeID) {
    rechargeRefundObj['rp_payment_status'] = statusConstants.RECHARGE_PAYMENT_STATUS.REFUND_INITIATED;
    var recharge_refund_insert_obj = await RechargePaymentModelObj.createObj(rechargeRefundObj, ["rp_id"]);
    var rechargeRefundStatusObj = {
        "recharge_payment_status": statusConstants.RECHARGE_PAYMENT_STATUS.REFUND_INITIATED
    }
    await RechargeModelObj.updateObj(rechargeRefundStatusObj, { "recharge_id": rechargeRefundObj.recharge_id }, ["recharge_id"]);

    var refundResponse = await StripeLibObj.refundByChargeID(ChargeID);
    if (refundResponse.status) {
        var rechargeRefundUpdateObj = {
            "recharge_payment_status": statusConstants.RECHARGE_PAYMENT_STATUS.REFUND_COMPLETED
        }

        var rechargeRefundPayUpdateObj = {
            "rp_transaction_id": refundResponse.data.balance_transaction,
            "rp_payment_status": statusConstants.RECHARGE_PAYMENT_STATUS.REFUND_COMPLETED,
            "rp_response": refundResponse.data
        }

    } else {
        var rechargeRefundUpdateObj = {
            "recharge_payment_status": statusConstants.RECHARGE_PAYMENT_STATUS.REFUND_FAILED
        }

        var rechargeRefundPayUpdateObj = {
            "rp_payment_status": statusConstants.RECHARGE_PAYMENT_STATUS.REFUND_FAILED,
            "rp_response": refundResponse.data
        }
    }
    await RechargeModelObj.updateObj(rechargeRefundUpdateObj, { "recharge_id": rechargeRefundObj.recharge_id }, ["recharge_id"]);
    await RechargePaymentModelObj.updateObj(rechargeRefundPayUpdateObj, { "rp_id": recharge_refund_insert_obj.rp_id }, ["rp_id"]);
    return true;
}


/**
 * This service is responsible to complete recharge process with payment
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const makeRecharge = async function (request) {
    //var rid = await airtimeRechargeLibObj.getReserveId();
    const requestData = request.body,
        response = { "data": {} },
        cardToken = requestData.recharge_payment_token_id,
        mobile = requestData.recharge_mobile_number,
        operator = requestData.recharge_operator_id,
        price = requestData.recharge_product_id, // This is the product price
        currency = requestData.recharge_currency,
        description = "Payment for recharge",
        user_id = requestData.user_id;

    var rechargeObj = {
        "recharge_operator": operator,
        "recharge_mobile_number": mobile,
        "recharge_currency": currency,
        "recharge_product_price": price,
        "user_id": user_id
    }

    var rechargePayObj = {
        "recharge_id": 0,
        "rp_currency": currency,
        "rp_product_price": price,
        "user_id": user_id
    }

    var selectRechargeData = ["recharge_id"];
    var selectRechargePayData = ["rp_id"];

    var recharge_insert_obj = await RechargeModelObj.createObj(rechargeObj, selectRechargeData);

    rechargePayObj.recharge_id = recharge_insert_obj.recharge_id;
    var recharge_pay_insert_obj = await RechargePaymentModelObj.createObj(rechargePayObj, selectRechargePayData);

    var metaData = {
        recharge_id: recharge_insert_obj.recharge_id,
        rp_id: recharge_pay_insert_obj.rp_id
    }
    var paymentResponse = await StripeLibObj.makePayment(
        cardToken,
        price,
        currency,
        description,
        metaData
    );
    if (paymentResponse.status) {
        let rechargeUpdateObj = {
            "recharge_payment_status": statusConstants.RECHARGE_PAYMENT_STATUS.COMPLETED
        }

        let rechargePayUpdateObj = {
            "rp_charge_id": paymentResponse.data.id,
            "rp_transaction_id": paymentResponse.data.balance_transaction,
            "rp_payment_status": statusConstants.RECHARGE_PAYMENT_STATUS.COMPLETED,
            "rp_response": paymentResponse.data
        }

        await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge_insert_obj.recharge_id }, selectRechargeData);
        await RechargePaymentModelObj.updateObj(rechargePayUpdateObj, { "rp_id": recharge_pay_insert_obj.rp_id }, selectRechargePayData);

        return airtimeRechargeLibObj.makeRecharge(mobile, price).then(async (responseData) => {
            let error_code = responseData.TransferTo.error_code._text;
            let recharge_status = error_code != 0 ? statusConstants.RECHARGE_STATUS.FAILED : statusConstants.RECHARGE_STATUS.COMPLETED;
            let rechargeUpdateObj = {
                "recharge_status": recharge_status,
                "recharge_response": responseData
            }
            await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge_insert_obj.recharge_id }, selectRechargeData);
            if (error_code != 0) {
                throw Boom.badRequest(responseData.TransferTo.error_txt._text);
            }

            return response;
        }).catch(async (error) => {
            await refundRechargeAmount(rechargePayObj, paymentResponse.data.id);
            throw Boom.badRequest(localeService.translate("RECHARGE_ERROR"));
        });

    } else {
        let rechargeUpdateObj = {
            "recharge_payment_status": statusConstants.RECHARGE_PAYMENT_STATUS.FAILED
        }

        let rechargePayUpdateObj = {
            "rp_payment_status": statusConstants.RECHARGE_PAYMENT_STATUS.FAILED,
            "rp_response": paymentResponse.data
        }

        await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge_insert_obj.recharge_id }, selectRechargeData);
        await RechargePaymentModelObj.updateObj(rechargePayUpdateObj, { "rp_id": recharge_pay_insert_obj.rp_id }, selectRechargePayData);

        throw Boom.badRequest(paymentResponse.data.message);
    }
};

export const makeRechargeV2 = async function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();

    var endDate = datetimeObj.currentDate();
    var startDate = datetimeObj.currentMonthStartData();
    const requestData = request.body,
        response = { "data": {} },
        cardToken = requestData.recharge_payment_token_id,
        description = "Payment for recharge",
        user_id = requestData.user_id;
    var recharge_data = JSON.parse(requestData.recharge_data);
    var total_recharge = 0;
    var total_recharge_amount = 0;
    var verifyUser = await VerifyModelObj.fetchFirstObj({ 'verify_user_id': user_id });
    var rechargeInfo = await RechargeModelObj.getUserRechargeInfo(user_id, startDate, endDate);
    var regCount = recharge_data.length;
    var regAmount = await CommonFnObj.getArrySum(recharge_data, 'recharge_product_id');
    var wallet_amount = await WalletModelObj.fetchFirstObj({ 'wallet_user_id': user_id });
    total_recharge_amount = Number(rechargeInfo[0].recharge_sum) + Number(regAmount);
    total_recharge = Number(rechargeInfo[0].recharge_count) + Number(regCount);

    var newArray = [];
    var newArray1 = [];

    recharge_data.map(async function (reg, index) {
        let jsonObj = {
            recharge_operator: reg.recharge_operator_id,
            recharge_mobile_number: reg.recharge_mobile_number,
            recharge_currency: reg.recharge_currency,
            recharge_product_price: reg.recharge_product_id,
            user_id: user_id,
            recharge_payment_status: statusConstants.RECHARGE_PAYMENT_STATUS.COMPLETED,
            created_at: currentDateTime,
            updated_at: currentDateTime
        }
        newArray.push(jsonObj);
    })



    var selectRechargeData = ["recharge_id", "recharge_operator", "recharge_mobile_number", "recharge_currency", "recharge_product_price"];
    var selectRechargePayData = ["rp_id"];

    var recharge_insert_obj = await RechargeModelObj.batchinsertObjWithIds(newArray, selectRechargeData);

    let rechargeUpdateObj = {
        //"recharge_payment_status":statusConstants.RECHARGE_PAYMENT_STATUS.COMPLETED
    }
    // it is check for login use verify or not
    if (!verifyUser && (total_recharge_amount > 150 || total_recharge > 10)) {

        return response;

    } else {
        let promises = recharge_insert_obj.map((recharge, index) => {
            return new Promise((resolve) => {
                return airtimeRechargeLibObj.makeRecharge(recharge.recharge_mobile_number, recharge.recharge_product_price).then(async (responseData) => {
                    let error_code = responseData.TransferTo.error_code._text;
                    let recharge_status = error_code != 0 ? statusConstants.RECHARGE_STATUS.FAILED : statusConstants.RECHARGE_STATUS.COMPLETED;
                    let rechargeUpdateObj = {
                        "recharge_status": recharge_status,
                        "recharge_response": responseData
                    }
                    await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge.recharge_id }, selectRechargeData);
                    // if(error_code != 0){
                    //       throw Boom.badRequest( responseData.TransferTo.error_txt._text );
                    // }
                    return resolve(responseData);

                }).catch(async (error) => {

                    // throw Boom.badRequest( localeService.translate( "RECHARGE_ERROR" ) );
                });

                // return resolve;
            });
        });
        return Promise.all(promises).then((newArray1) => {
            return response;
        });
    }


};

export const addAccountLink = async function (request) {
    //var rid = await airtimeRechargeLibObj.getReserveId();
    const requestData = request.body,
        response = { "data": {} },
        account_id = requestData.account_id,
        description = "Payment for recharge",
        user_id = requestData.user_id;

    var paymentResponse = await StripeLibObj.createAccountLink(account_id);


};

/**
 * This service is responsible to complete recharge process with payment
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const makeRechargev3 = async function (request) {
    //var rid = await airtimeRechargeLibObj.getReserveId();
    const requestData = request.body,
        response = { "data": {} },
        cardToken = null,//requestData.recharge_payment_token_id,
        mobile = requestData.recharge_mobile_number,
        operator = requestData.recharge_operator_id,
        price = requestData.recharge_product_id, // This is the product price
        currency = requestData.recharge_currency,
        description = "Payment for recharge",
        user_id = '';
    //requestData.user_id;

    var rechargeObj = {
        "recharge_operator": operator,
        "recharge_mobile_number": mobile,
        "recharge_currency": currency,
        "recharge_product_price": price,
        "user_id": user_id
    }

    var rechargePayObj = {
        "recharge_id": 0,
        "rp_currency": currency,
        "rp_product_price": price,
        "user_id": user_id
    }

    var selectColumns = ["user_id", "name"],
        whereData = {
            "user_type": dataConstants.USER_TYPE.USER_ADMIN
        };
    var selectRechargeData = ["recharge_id"];
    var selectRechargePayData = ["rp_id"];

    var userObj = await UserModelObj.fetchObjWithSingleRecord(whereData, selectColumns)

    if (userObj != undefined) {

        rechargePayObj.user_id = userObj.user_id;
        rechargeObj.user_id = userObj.user_id;
        var recharge_insert_obj = await RechargeModelObj.createObj(rechargeObj, selectRechargeData);

        rechargePayObj.recharge_id = recharge_insert_obj.recharge_id;
        var recharge_pay_insert_obj = await RechargePaymentModelObj.createObj(rechargePayObj, selectRechargePayData);

        var metaData = {
            recharge_id: recharge_insert_obj.recharge_id,
            rp_id: recharge_pay_insert_obj.rp_id
        }

        let rechargeUpdateObj = {
            "recharge_payment_status": statusConstants.RECHARGE_PAYMENT_STATUS.COMPLETED
        }

        let rechargePayUpdateObj = {
            "rp_charge_id": null,//paymentResponse.data.id,
            "rp_transaction_id": null,//paymentResponse.data.balance_transaction,
            "rp_payment_status": statusConstants.RECHARGE_PAYMENT_STATUS.COMPLETED,
            "rp_response": null,//paymentResponse.data
        }

        await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge_insert_obj.recharge_id }, selectRechargeData);
        await RechargePaymentModelObj.updateObj(rechargePayUpdateObj, { "rp_id": recharge_pay_insert_obj.rp_id }, selectRechargePayData);

        return airtimeRechargeLibObj.makeRecharge(mobile, price).then(async (responseData) => {

            let error_code = responseData.TransferTo.error_code._text;
            let recharge_status = error_code != 0 ? statusConstants.RECHARGE_STATUS.FAILED : statusConstants.RECHARGE_STATUS.COMPLETED;
            let rechargeUpdateObj = {
                "recharge_status": recharge_status,
                "recharge_response": responseData
            }
            await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge_insert_obj.recharge_id }, selectRechargeData);
            if (error_code != 0) {
                response.error = responseData;//responseData.TransferTo.error_txt._text;
                return response;

            }
            response.code = HttpStatus.OK;
            response.data.message = 'Recharge successfully done.';
            return response;
        }).catch(async (error) => {

            throw Boom.badRequest(localeService.translate("RECHARGE_ERROR"));
        });
    }

};

export const makeRechargeBulkOld = async function (request) {

    const requestData = request.body,
        response = { "data": {} };

    var recharge_data = JSON.parse(requestData.recharge_data);
    var rechData = recharge_data.recharge_data;

    var newArray1 = [];

    let promises = rechData.map((recharge, index) => {

        return new Promise((resolve) => {
            return airtimeRechargeLibObj.makeRecharge(recharge.recharge_mobile_number, recharge.recharge_product_id).then(async (responseData) => {

                let error_code = responseData.TransferTo.error_code._text;
                let recharge_status = error_code != 0 ? statusConstants.RECHARGE_STATUS.FAILED : statusConstants.RECHARGE_STATUS.COMPLETED;

                newArray1.push(responseData)
                return resolve(responseData);

            }).catch(async (error) => {

                // throw Boom.badRequest( localeService.translate( "RECHARGE_ERROR" ) );
            });
        });
    });
    return Promise.all(promises).then((newArray1) => {
        response.data = newArray1;
        return response;
    });
};

export const makeRechargeBulk = async function (request) {
    const requestData = request.body,
        response = { "data": {} };

    var recharge_data = JSON.parse(requestData.recharge_data);
    var rechData = recharge_data.recharge_data;

    var newArray1 = [];
    for (const recharge of rechData) {
        const responseData = await airtimeRechargeLibObj.makeRecharge(recharge.recharge_mobile_number, recharge.recharge_product_id);
        if (responseData) {
            let error_code = responseData.TransferTo.error_code._text;
            let recharge_status = error_code != 0 ? statusConstants.RECHARGE_STATUS.FAILED : statusConstants.RECHARGE_STATUS.COMPLETED;

            newArray1.push(responseData)
            return resolve(responseData);
        }
    }
};

export const GetErrorCodeDescriptions = function (request) {
    const requestData = request.body,
        response = { "data": {} };
    return dingRechargeLibObj.GetErrorCodeDescriptions().then(async (responseData) => {

        response.data = responseData;
        return response;
    }).catch((error) => {
        throw Boom.badRequest(error);
    })

};

export const GetCountries = function (request) {
    const requestData = request.body,
        response = { "data": {} };
    return dingRechargeLibObj.GetCountries().then(async (responseData) => {
        response.data = responseData;
        return response;
    }).catch((error) => {
        throw Boom.badRequest(error);
    })

};

export const GetProviders = function (request) {
    const requestData = request.body,
        countryIsos = requestData.country_isos,
        RegionCodes = requestData.region_codes,
        contact_number = requestData.contact_number,
        response = { "data": {} };
    return dingRechargeLibObj.GetProviders(countryIsos, RegionCodes, contact_number).then(async (responseData) => {
        response.data = responseData;
        return response;
    }).catch((error) => {
        throw Boom.badRequest(error);
    })

};

export const getDingProdust = function (request) {
    const requestData = request.body,
        countryIsos = requestData.country_isos,
        RegionCodes = requestData.region_codes,
        contact_number = requestData.contact_number,
        provider_codes = requestData.provider_codes,
        response = { "data": {} };
    return dingRechargeLibObj.GetProducts(countryIsos, RegionCodes, provider_codes, contact_number).then(async (responseData) => {
        response.data = responseData;
        return response;
    }).catch((error) => {
        throw Boom.badRequest(error);
    })

};

export const SendTransfer = function (request) {
    const requestData = request.body,
        sku_code = requestData.sku_code,
        send_value = requestData.send_value,
        contact_number = requestData.contact_number,
        send_currency_iso = requestData.send_currency_iso,
        response = { "data": {} };
    return dingRechargeLibObj.SendTransfer(sku_code, send_value, send_currency_iso, contact_number).then(async (responseData) => {
        response.data = responseData;
        return response;
    }).catch((error) => {
        throw Boom.badRequest(error);
    })
};
/*code for multiple provider and operator*/

export const getProviderInfo = async function (request) {

    const requestData = request.body,
        mobileNumber = requestData.recharge_mobile_number,
        methodWhere = {
            is_default: 1
        },
        column = ["method"],
        response = { "data": {} };
    const getMethod = await RechargeSettingModelObj.fetchObjWithSingleRecord(methodWhere, column);
    response.code = HttpStatus.OK;
    if (getMethod.method == 'dtone') {
        const responseData = await airtimeRechargeLibObj.getMsisdnInfo(mobileNumber);

        let error_code = responseData.TransferTo.error_code._text;
        let idArray = responseData.TransferTo;

        if (error_code != 0) {
            if (error_code == 101) {
                throw Boom.badRequest(localeService.translate("VALID_MOBILE_NUMBER"));
            }
            throw Boom.badRequest(responseData.TransferTo.error_txt._text);
        }

        let operatorInfo = await convertDataObj.dtonInfojson(idArray);
        response.rechargeMethod = 1; // dtone
        response.data = operatorInfo;
    } else {

        const responseDing = await dingRechargeLibObj.GetAccountLookup(mobileNumber);
         if (responseDing.Items) {

            let operatorInfo = await convertDataObj.dingInfojson(responseDing);
            response.rechargeMethod = 2; // ding
            response.data = operatorInfo;
        } else {
            // throw Boom.badRequest("Invalid number" );
            throw Boom.badRequest(localeService.translate(responseDing.response.data.ErrorCodes[0].Code));
        }
    }

    return response;

};

export const getOperatorList = async function (request) {

    const requestData = request.body,
        country_id = requestData.recharge_country_id,
        RegionCodes = requestData.region_codes,
        methodWhere = {
            is_default: 1
        },
        column = ["method"],
        response = { "data": {} };
    response.code = HttpStatus.OK;

    const getMethod = await RechargeSettingModelObj.fetchObjWithSingleRecord(methodWhere, column);

    if (getMethod.method == 'dtone') {
        const responseData = await airtimeRechargeLibObj.getOperators(country_id);

        let error_code = responseData.TransferTo.error_code._text;
        if (error_code != 0) {
            throw Boom.badRequest(responseData.TransferTo.error_txt._text);
        }
        let idArray = responseData.TransferTo.operatorid._text;
        let valueArray = responseData.TransferTo.operator._text;
        let operatorData = await convertDataObj.list2json(idArray, valueArray);
        response.rechargeMethod = 1; // dtone
        response.data = operatorData;
        // response.old = responseData;

    } else {
        const responseData = await dingRechargeLibObj.GetProviders(country_id, RegionCodes);

        if (responseData.Items) {

            let operatorData = await convertDataObj.dingOperatorjson(responseData);
            response.data = operatorData;
        }
        response.rechargeMethod = 2; // ding
    }
    return response;
}

/**
 * Get product list operator wise
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const getProductList = async function (request) {
    //var rid = await airtimeRechargeLibObj.getReserveId();
    const requestData = request.body,
        operator_id = requestData.recharge_operator_id,
        methodWhere = {
            is_default: 1
        },
        column = ["method"],
        response = { "data": {} };
    response.code = HttpStatus.OK;

    const getMethod = await RechargeSettingModelObj.fetchObjWithSingleRecord(methodWhere, column);

    if (getMethod.method == 'dtone') {
        const responseData = await airtimeRechargeLibObj.getOperatorProducts(operator_id);
        let error_code = responseData.TransferTo.error_code._text;
        if (error_code != 0) {
            throw Boom.badRequest(responseData.TransferTo.error_txt._text);
        }
        let productArray = responseData.TransferTo.product_list._text;
        let currency = responseData.TransferTo.destination_currency._text;
        let productData = await convertDataObj.list2productArray(productArray, currency);
        response.rechargeMethod = 1; // dtone
        response.data = productData;

    } else {
        const responseData = await dingRechargeLibObj.GetProducts('', '', operator_id, '');

        if (responseData.Items) {
            let productData = await convertDataObj.dingProductsjson(responseData);
            response.data = productData;
        }
        response.rechargeMethod = 2; // ding

    }
    return response;

};

export const rechargeProcessOld = async function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();

    var endDate = datetimeObj.currentDate();
    var startDate = datetimeObj.currentMonthStartData();
    const requestData = request.body,
        methodWhere = {
            is_default: 1
        },
        column = ["method"],
        createWalletTrs = {
            "user_id": requestData.user_id,
            "created_at": currentDateTime,
            "updated_at": currentDateTime

        },
        updateWallet = {
            "updated_at": currentDateTime
        },

        response = { "data": {} },
        rechargeType = requestData.recharge_type,
        user_id = requestData.user_id,
        devWhere = {
            'commission_user_id': user_id,
            'commission_user_type': request.headers['x-user-type']
        };
    var recharge_data = JSON.parse(requestData.recharge_data);
    var total_recharge = 0;
    var total_recharge_amount = 0;
    var verifyUser = await VerifyModelObj.fetchFirstObj({ 'verify_user_id': user_id });
    var rechargeInfo = await RechargeModelObj.getUserRechargeInfo(user_id, startDate, endDate);
    var regCount = recharge_data.length;
    var regAmount = await CommonFnObj.getArrySum(recharge_data, 'recharge_product_id');
    var wallet_amount = await WalletModelObj.fetchFirstObj({ 'wallet_user_id': user_id });

    total_recharge_amount = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_sum : 0) + Number(regAmount);
    total_recharge = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_count : 0) + Number(regCount);
    var newArray = [];
    var newArray1 = [];
    var comArr = [];
    const getMethod = await RechargeSettingModelObj.fetchObjWithSingleRecord(methodWhere, column);
    if (getMethod.method == 'dtone' && rechargeType == 1) {
        var rgType = rechargeType;
    } else if (getMethod.method == 'ding' && rechargeType == 2) {
        var rgType = rechargeType;

    } else {
        response.code = HttpStatus.OK;
        response.message = 'Something went wrong,please fill all required field again. '
        return response;
    }
    //  code for commission  
    const globalSetting = await SettingModelObj.fetchObjWithSingleRecord('', ["res_dev_commission"]);
    const devCommission = await UserCommissionModelObj.fetchObjWithSingleRecord(devWhere, ["commission_id", "commission"]);
    const promises = recharge_data.map(async function (reg, index) {

        const whereOpr = {
            'operator_id': reg.provider_code,
            'cos_user_id': user_id,
            'cos_user_type': request.headers['x-user-type']
        };
        const opratorCom = await CommissionOperatorModelObj.fetchObjWithSingleRecord(whereOpr, ['cos_commission', 'cos_id']);
        var apply_commission = globalSetting.res_dev_commission;
        var commission_type = 1;
        if (opratorCom.cos_commission) {
            apply_commission = opratorCom.cos_commission;
            commission_type = 3;
        }
        if (devCommission && (opratorCom.cos_commission == null || opratorCom.cos_commission == 'undefined')) {
            apply_commission = devCommission.commission;
            commission_type = 2;

        }
        const commissionValue = (reg.recharge_product_id * (apply_commission / 100));// calculate commission



        let jsonObj = {
            recharge_operator: reg.recharge_operator_id,
            recharge_country_code: reg.recharge_country_code,
            recharge_country: reg.recharge_country,
            recharge_mobile_number: reg.recharge_mobile_number,
            recharge_currency: reg.recharge_currency,
            recharge_product_price: reg.recharge_product_id,
            user_id: user_id,
            recharge_type: rgType,
            recharge_payment_status: statusConstants.RECHARGE_PAYMENT_STATUS.COMPLETED,
            commission_value: apply_commission,
            commission_type: commission_type,
            created_at: currentDateTime,
            updated_at: currentDateTime
        };
        let comObj = {
            commission_value: commissionValue,

        }



        newArray.push(jsonObj);
        comArr.push(comObj);

        return newArray;

    })
    const results = await Promise.all(promises)
    const total_commission = await CommonFnObj.getArrySum(comArr, 'commission_value');
    const addDieCom = Number(wallet_amount.admin_commission_amount) + Number(total_commission);
    const chkRegAmount = (Number(regAmount) + Number(addDieCom)).toFixed(3); // it  is old cond
    /*code for deduct wallet amount*/

    if (regAmount > Number(wallet_amount.wallet_amount)) {
        response.code = HttpStatus.OK;
        response.message = 'insufficient credit, please recharge your account.';
        return response;
    }

    updateWallet.wallet_amount = Number(wallet_amount.wallet_amount) - (Number(regAmount) - Number(total_commission));
    updateWallet.admin_commission_amount = addDieCom;
    createWalletTrs.wallet_id = wallet_amount.wallet_id;
    createWalletTrs.wt_amount = regAmount;
    createWalletTrs.wt_reference_type = 1;
    createWalletTrs.wt_payment_status = 3;
    createWalletTrs.wt_status = 1;
    createWalletTrs.wt_type = 2;
    await WalletTransactionObj.createObj(createWalletTrs, ["wt_id"]);
    await WalletModelObj.updateObj(updateWallet, { 'wallet_user_id': user_id }, ["wallet_id"]);

    /*end*/
    var selectRechargeData = ["recharge_id", "recharge_operator", "recharge_country_code", "recharge_mobile_number", "recharge_currency", "recharge_product_price"];
    var selectRechargePayData = ["rp_id"];

    var recharge_insert_obj = await RechargeModelObj.batchinsertObjWithIds(newArray, selectRechargeData);

    const rechargeUpdateObj = {
    }
    // it is check for login use verify or not
    if (!verifyUser && (request.headers['x-user-type'] == 1) && (total_recharge_amount > 150 || total_recharge > 10)) {
        response.code = HttpStatus.OK;
        // response.message = 'Recharge process is complete Please view status.';
        response.message = 'you payment has been done successfully will let you know status for recharge soon'
        return response;

    } else {
        if (getMethod.method == 'dtone') {
            for (const recharge of recharge_insert_obj) {
                var contactNo = recharge.recharge_country_code + recharge.recharge_mobile_number;
                const responseData = await airtimeRechargeLibObj.makeRecharge(contactNo, recharge.recharge_product_price);
                if (responseData) {
                    let error_code = responseData.TransferTo.error_code._text;
                    let recharge_status = error_code != 0 ? statusConstants.RECHARGE_STATUS.FAILED : statusConstants.RECHARGE_STATUS.COMPLETED;
                    let rechargeUpdateObj = {
                        "recharge_status": recharge_status,
                        "recharge_response": responseData
                    }
                    await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge.recharge_id }, selectRechargeData);

                    newArray1.push(responseData)
                    //return resolve(responseData);
                }
            }
            response.message = 'you payment has been done successfully will let you know status for recharge soon';
            response.code = HttpStatus.OK;
            return response;

        } else {

            for (const recharge of recharge_insert_obj) {

                var contactNo = recharge.recharge_country_code + recharge.recharge_mobile_number;
                var contactNumber = contactNo.replace("+", "");
                const responseData = await dingRechargeLibObj.SendTransfer(
                    recharge.recharge_operator,
                    recharge.recharge_product_price,
                    recharge.recharge_currency,
                    contactNumber);

                if (responseData) {
                    if (responseData.TransferRecord) {
                        var error_code = statusConstants.RECHARGE_STATUS.COMPLETED;
                        rechargeUpdateObj.recharge_response = responseData
                    } else {
                        var error_code = statusConstants.RECHARGE_STATUS.FAILED;
                        rechargeUpdateObj.recharge_response = responseData.response.data;
                    }

                    rechargeUpdateObj.recharge_status = error_code;

                    await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge.recharge_id }, selectRechargeData);

                    // newArray1.push(responseData)
                }
            }
            response.code = HttpStatus.OK;
            response.message = 'you payment has been done successfully will let you know status for recharge soon';
            return response;
        }
    }


};

export const rechargeProcess = async function (request) {
   
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    var endDate = datetimeObj.currentDate();
    var startDate = datetimeObj.currentMonthStartData();
    const requestData = request.body,
        methodWhere = {
            is_default: 1
        },
        column = ["method"],
        createWalletTrs = {
            "user_id": requestData.user_id,
            "created_at": currentDateTime,
            "updated_at": currentDateTime

        },
        updateWallet = {
            "updated_at": currentDateTime
        },
        //add notification by @hemant_solanki
        fetchWhere = { "auth_user_id": requestData.user_id },

        // fetchAdminWhere = { "user_type": 2, "email": envConstants.ADMIN_EMAIL_ID },
        fetchAdminWhere = { "user_type": 2, "email": 'admin@cubaring.com' },
        notificationColumn = {
            "notification_for": requestData.user_id,
            "notification_type": 6,
            "read_status": 2,
            "notification_status": 2,
            "delete_status": 2,
            "created_at": currentDateTime,
        },
        selectNotificationData = [
            "notification_by",
            "notification_for",
            "notification_type"
        ],
        select = [
            "recharge_limit_id",
            "user_id",
            "recharge_limit_amount",
            "created_at"
        ],
        notificationType = 'Limited recharge',

        response = { "data": {} },
        rechargeType = requestData.recharge_type,
        user_id = requestData.user_id,
        devWhere = {
            'commission_user_id': user_id,
            'commission_user_type': request.headers['x-user-type']
        };
    var recharge_data = JSON.parse(requestData.recharge_data);
    var total_recharge = 0;
    var total_recharge_amount = 0;
    var verifyUser = await VerifyModelObj.fetchFirstObj({ 'verify_user_id': user_id, 'verify_status': 2 });
    var rechargeInfo = await RechargeModelObj.getUserRechargeInfo(user_id, startDate, endDate);
   
    var regCount = recharge_data.length;
    var regAmount = await CommonFnObj.getArrySum(recharge_data, 'recharge_product_id');
    var wallet_amount = await WalletModelObj.fetchFirstObj({ 'wallet_user_id': user_id });
    
    total_recharge_amount = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_sum : 0) + Number(regAmount);
    
    // var rechargeLimit = await RechargeLimitModalObj.fetchObjWithSingleRecord(whereColumn, select)
        
    // if( rechargeLimit != undefined ) {
    //     if( Number(rechargeLimit.recharge_limit_amount > Number(total_recharge_amount))){
    //         throw Boom.badRequest(localeService.translate('You have reached your maximum instant recharge amount limit of today.'));
    //     }
    // }
    
    total_recharge = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_count : 0) + Number(regCount);
    var newArray = [];
    var newArray1 = [];
    var comArr = [];
    const getMethod = await RechargeSettingModelObj.fetchObjWithSingleRecord(methodWhere, column);
    if (getMethod.method == 'dtone' && rechargeType == 1) {
        var rgType = rechargeType;
    } else if (getMethod.method == 'ding' && rechargeType == 2) {
        var rgType = rechargeType;

    } else {
        response.code = HttpStatus.OK;
        response.message = 'Something went wrong,please fill all required field again. '
        return response;
    }
    //  code for commission  
    const globalSetting = await SettingModelObj.fetchObjWithSingleRecord('', ["res_dev_commission", "user_commission"]);
    const devCommission = await UserCommissionModelObj.fetchObjWithSingleRecord(devWhere, ["commission_id", "commission"]);
    
    const promises = recharge_data.map(async function (reg, index) {
        
        const whereOpr = {
            'operator_id': reg.provider_code,
            'cos_user_id': user_id,
            'cos_user_type': request.headers['x-user-type']
        };
        const opratorCom = await CommissionOperatorModelObj.fetchObjWithSingleRecord(whereOpr, ['cos_commission', 'cos_id']);
        if (request.headers['x-user-type'] == 1) {
            var apply_commission = globalSetting.user_commission;
        } else {
            var apply_commission = globalSetting.res_dev_commission;
        }
        var commission_type = 1;
        if (opratorCom.cos_commission) {
            apply_commission = opratorCom.cos_commission;
            commission_type = 3;
        }
        if (devCommission && (opratorCom.cos_commission == null || opratorCom.cos_commission == 'undefined')) {
            apply_commission = devCommission.commission;
            commission_type = 2;

        }
        const commissionValueV = (reg.recharge_product_id * (apply_commission / 100));// calculate commission
       
        let jsonObj = {
            recharge_operator: reg.recharge_operator_id,
            provider_code: reg.provider_code,
            recharge_country_code: reg.recharge_country_code,
            recharge_country: reg.recharge_country,
            recharge_mobile_number: reg.recharge_mobile_number,
            recharge_currency: reg.recharge_currency,
            recharge_product_price: reg.recharge_product_id,
            user_id: user_id,
            recharge_type: rgType,
            recharge_payment_status: statusConstants.RECHARGE_PAYMENT_STATUS.COMPLETED,
            commission_value: apply_commission,
            commission_type: commission_type,
            // admin_commission_applied:commissionValueV, //@hemant
            created_at: currentDateTime,
            updated_at: currentDateTime
        };

        //for nauta recharge crated by @hemant
        if (reg.provider_code == "NUCU") {
            jsonObj.recharge_for = 2;
        }


        let comObj = {
            commission_value: commissionValueV,

        }
        newArray.push(jsonObj);
        comArr.push(comObj);



        return newArray;
    })
    const results = await Promise.all(promises)

    /*code for deduct wallet amount*/

    if (regAmount > Number(wallet_amount.wallet_amount)) {
        response.code = HttpStatus.OK;
        response.message = 'insufficient credit, please recharge your account.';
        return response;
    }


    /*end*/
    var selectRechargeData = ["recharge_id", "recharge_operator", "recharge_country_code", "recharge_mobile_number", "recharge_currency", "recharge_product_price", "commission_value", "commission_type", "admin_commission_applied", "recharge_for"];
    var selectRechargePayData = ["rp_id"];

    var recharge_insert_obj = await RechargeModelObj.batchinsertObjWithIds(newArray, selectRechargeData);

    const rechargeUpdateObj = {
    }
    // it is check for login use verify or not
    if (!verifyUser && (request.headers['x-user-type'] == 1) && (total_recharge_amount > 150 || total_recharge > 10)) {


        //code according to total recharge and total amount one by one

        var total_recharge_amount_usr = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_sum : 0);
        var total_recharge_usr = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_count : 0);
        var total_commission = 0;
        var dingCommission = wallet_amount.ding_commission_amount;
        var total_done_reg_amount = 0;

        for (const recharge of recharge_insert_obj) {

            total_recharge_amount_usr = total_recharge_amount_usr + Number(recharge.recharge_product_price);
            total_recharge_usr = total_recharge_usr + 1;
            //check amount and no. of recharge 
            if ((total_recharge_amount_usr <= 150 && total_recharge_usr <= 10)) {

                var contactNo = recharge.recharge_country_code + recharge.recharge_mobile_number;
                var contactNumber = contactNo.replace("+", "");
                const responseData = await dingRechargeLibObj.SendTransfer(
                    recharge.recharge_operator,
                    recharge.recharge_product_price,
                    recharge.recharge_currency,
                    contactNumber);



                if (responseData) {

                    if (responseData.ResultCode == 1) {
                        var error_code = statusConstants.RECHARGE_STATUS.COMPLETED;
                        rechargeUpdateObj.recharge_response = responseData;

                        dingCommission = Number(dingCommission) + Number(responseData.TransferRecord.CommissionApplied);

                        //code added by @hemant_Solanki
                        if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {

                            if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                                if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                    rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;

                                }
                            }
                            if (responseData.TransferRecord.CommissionApplied != null && responseData.TransferRecord.CommissionApplied != undefined) {

                                const rechargeCommissionValue = (recharge.recharge_product_price * (recharge.commission_value / 100));

                                rechargeUpdateObj.admin_commission_applied = rechargeCommissionValue;

                                rechargeUpdateObj.ding_commission_applied = responseData.TransferRecord.CommissionApplied;

                                rechargeUpdateObj.actual_commission = Number(responseData.TransferRecord.CommissionApplied) - Number(rechargeCommissionValue);
                            }

                        }

                        //code for ding commission value


                        //code added by @hemant_Solanki

                        //code for discuss flow

                        const commissionValue = (recharge.recharge_product_price * (recharge.commission_value / 100));// calculate commission
                        total_commission = (Number(total_commission) + Number(commissionValue));
                        total_done_reg_amount = (Number(total_done_reg_amount) + Number(recharge.recharge_product_price));
                        //end hear

                    } else {

                        var error_code = statusConstants.RECHARGE_STATUS.FAILED;
                        rechargeUpdateObj.recharge_response = responseData.response.data;
                        rechargeUpdateObj.recharge_payment_status = 1;



                        //code added by @hemant_Solanki
                        if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                            if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                                if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                    rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                                }
                            }
                        }


                        //code added by @hemant_Solanki
                    }

                    rechargeUpdateObj.recharge_status = error_code;

                    await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge.recharge_id }, selectRechargeData);

                    // newArray1.push(responseData)
                }
            } else {

                //code for discuss flow
                const commissionValue = (recharge.recharge_product_price * (recharge.commission_value / 100));// calculate commission
                total_commission = (Number(total_commission) + Number(commissionValue));
                total_done_reg_amount = (Number(total_done_reg_amount) + Number(recharge.recharge_product_price));
                //end hear
            }
        }
        const addDieCom = Number(wallet_amount.admin_commission_amount) + Number(total_commission);
        updateWallet.wallet_amount = Number(wallet_amount.wallet_amount) - (Number(total_done_reg_amount) - Number(total_commission));
        updateWallet.admin_commission_amount = addDieCom;
        updateWallet.ding_commission_amount = dingCommission;
        createWalletTrs.wallet_id = wallet_amount.wallet_id;
        createWalletTrs.wt_amount = (Number(total_done_reg_amount) - Number(total_commission));
        createWalletTrs.wt_reference_type = 1;
        createWalletTrs.wt_payment_status = 3;

        createWalletTrs.wt_pre_amount = wallet_amount.wallet_amount;
        createWalletTrs.wt_balance = Number(wallet_amount.wallet_amount) - (Number(total_done_reg_amount) - Number(total_commission));

        if (total_done_reg_amount > 0) {
            createWalletTrs.wt_status = 1;
        } else {
            createWalletTrs.wt_status = 2;
        }
        createWalletTrs.wt_type = 2;
        await WalletTransactionObj.createObj(createWalletTrs, ["wt_id"]);
        await WalletModelObj.updateObj(updateWallet, { 'wallet_user_id': user_id }, ["wallet_id"]);
        // end code according to total recharge and total amount one by one
        //Notification created by @hemant_solanki

        return UserModelObj.fetchObj(fetchAdminWhere)
            .then((adminDetail) => {
                
                return AuthModelObj.fetchObj(fetchWhere)
                    .then((userDetail) => {

                        var deviceToken = userDetail[0].auth_firebase_token;
                        var referenceId = '';
                        var byUserId = adminDetail[0].user_id;
                        var notification = {
                            recharge_id: referenceId,
                            by_user_id: byUserId,
                            notification_type: notificationType,
                            click_action: "MainActivity",
                            title: 'Limited recharge',
                            body: 'You have reached your maximum instant recharge limit of the month, Verify yourself for hassle free recharge experience'
                        }

                        var objSavePayload = JSON.stringify(notification);
                        firebaseLibObj.sendNotification(deviceToken, notification);
                        notificationColumn.notification_msg = "You have reached your maximum instant recharge limit of the month, Verify yourself for hassle free recharge experience";
                        notificationColumn.notification_payload = objSavePayload;
                        notificationColumn.notification_by = byUserId;

                        return NotificationModelObj.createObj(notificationColumn, selectNotificationData)
                            .then(() => {


                                if (responseData != undefined) {

                                    if (responseData.ResultCode != 1) {
                                        throw Boom.badRequest(localeService.translate(responseData.response.data.ErrorCodes[0].Code));
                                    } else {
                                        response.code = HttpStatus.OK;
                                        response.message = 'you payment has been done successfully will let you know status for recharge soon';
                                        return response;
                                    }
                                } else {

                                    response.code = HttpStatus.OK;
                                    response.message = 'you payment has been done successfully will let you know status for recharge soon';
                                    return response;
                                }
                            }).catch((error) => {
                                throw Boom.badImplementation(localeService.translate(error));
                            });
                    }).catch((updateErr) => {
                        throw Boom.badImplementation(localeService.translate(updateErr));
                    });
            }).catch((updateError) => {
                throw Boom.badImplementation(localeService.translate(updateError));
            });

    } else {

        if (getMethod.method == 'dtone') {
            for (const recharge of recharge_insert_obj) {
                var contactNo = recharge.recharge_country_code + recharge.recharge_mobile_number;
                const responseData = await airtimeRechargeLibObj.makeRecharge(contactNo, recharge.recharge_product_price);

                if (responseData) {
                    let error_code = responseData.TransferTo.error_code._text;
                    let recharge_status = error_code != 0 ? statusConstants.RECHARGE_STATUS.FAILED : statusConstants.RECHARGE_STATUS.COMPLETED;
                    let rechargeUpdateObj = {
                        "recharge_status": recharge_status,
                        "recharge_response": responseData
                    }

                    //code added by @hemant_Solanki
                    if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                        if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                            if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                            }
                        }
                    }


                    //code added by @hemant_Solanki
                    await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge.recharge_id }, selectRechargeData);

                    newArray1.push(responseData)

                }
            }
            response.message = 'you payment has been done successfully will let you know status for recharge soon';
            response.code = HttpStatus.OK;
            return response;

        } else {

            var total_commission = 0;
            var dingCommission = wallet_amount.ding_commission_amount;
            var total_done_reg_amount = 0;
            for (const recharge of recharge_insert_obj) {

                var contactNo = recharge.recharge_country_code + recharge.recharge_mobile_number;
                var contactNumber = contactNo.replace("+", "");


                var responseData = await dingRechargeLibObj.SendTransfer(
                    recharge.recharge_operator,
                    recharge.recharge_product_price,
                    recharge.recharge_currency,
                    contactNumber);

                if (responseData) {
                    //(responseData.TransferRecord)

                    if (responseData.ResultCode == 1) {
                        dingCommission = Number(dingCommission) + Number(responseData.TransferRecord.CommissionApplied);
                        var error_code = statusConstants.RECHARGE_STATUS.COMPLETED;
                        rechargeUpdateObj.recharge_response = responseData;

                        //code added by @hemant_Solanki
                        if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                            if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                                if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                    rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                                }
                            }

                            if (responseData.TransferRecord.CommissionApplied != null && responseData.TransferRecord.CommissionApplied != undefined) {

                                const rechargeCommissionValue = (recharge.recharge_product_price * (recharge.commission_value / 100));

                                rechargeUpdateObj.admin_commission_applied = rechargeCommissionValue;

                                rechargeUpdateObj.ding_commission_applied = responseData.TransferRecord.CommissionApplied;

                                rechargeUpdateObj.actual_commission = Number(responseData.TransferRecord.CommissionApplied) - Number(rechargeCommissionValue);
                            }
                        }


                        //code added by @hemant_Solanki
                        //code for discuss flow

                        const commissionValue = (recharge.recharge_product_price * (recharge.commission_value / 100));// calculate commission

                        total_commission = (Number(total_commission) + Number(commissionValue));
                        total_done_reg_amount = (Number(total_done_reg_amount) + Number(recharge.recharge_product_price));
                        //end hear


                    } else {

                        var error_code = statusConstants.RECHARGE_STATUS.FAILED;
                        rechargeUpdateObj.recharge_response = responseData.response.data;
                        rechargeUpdateObj.recharge_payment_status = 1;



                        //code added by @hemant_Solanki
                        if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                            if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                                if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                    rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                                }
                            }
                        }


                        //code added by @hemant_Solanki
                    }

                    rechargeUpdateObj.recharge_status = error_code;

                    await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge.recharge_id }, selectRechargeData);


                }
            }
            const addDieCom = Number(wallet_amount.admin_commission_amount) + Number(total_commission);
            updateWallet.wallet_amount = Number(wallet_amount.wallet_amount) - (Number(total_done_reg_amount) - Number(total_commission));
            updateWallet.admin_commission_amount = addDieCom;
            updateWallet.ding_commission_amount = dingCommission;
            createWalletTrs.wallet_id = wallet_amount.wallet_id;
            createWalletTrs.wt_amount = (Number(total_done_reg_amount) - Number(total_commission));
            createWalletTrs.wt_reference_type = 1;
            createWalletTrs.wt_payment_status = 3;

            createWalletTrs.wt_pre_amount = wallet_amount.wallet_amount;
            createWalletTrs.wt_balance = Number(wallet_amount.wallet_amount) - (Number(total_done_reg_amount) - Number(total_commission));


            if (total_done_reg_amount > 0) {
                createWalletTrs.wt_status = 1;

            } else {
                createWalletTrs.wt_status = 2;

            }
            createWalletTrs.wt_type = 2;
            await WalletTransactionObj.createObj(createWalletTrs, ["wt_id"]);
            await WalletModelObj.updateObj(updateWallet, { 'wallet_user_id': user_id }, ["wallet_id"]);

            response.code = HttpStatus.OK;

            if (responseData.ResultCode != 1) {
                throw Boom.badRequest(localeService.translate(responseData.response.data.ErrorCodes[0].Code));
            } else {
                response.message = 'you payment has been done successfully will let you know status for recharge soon';
                return response;
            }
        }
    }
};

export const rechargeCardProcess = async function (request) {

    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();

    var endDate = datetimeObj.currentDate();
    var startDate = datetimeObj.currentMonthStartData();
    const requestData = request.body,
        cardToken = requestData.recharge_payment_token_id,
        description = "Payment for recharge",
        userType = request.headers['x-user-type'],
        methodWhere = {
            is_default: 1
        },
        column = ["method"],
        createWalletTrs = {
            "user_id": requestData.user_id,
            "created_at": currentDateTime,
            "updated_at": currentDateTime

        },
        updateWallet = {
            "updated_at": currentDateTime
        },
        createData = {
            "wt_type": 1,
            "wt_reference_type": 3,
            "wt_currency": 'USD',
            "wt_gateway": 1,
            "wt_payment_method": 1,
            "user_id": requestData.user_id,
            "created_at": currentDateTime,
            "updated_at": currentDateTime
        },
        response = { "data": {} },
        rechargeType = requestData.recharge_type,
        user_id = requestData.user_id,
        devWhere = {
            'commission_user_id': user_id,
            'commission_user_type': request.headers['x-user-type']
        };
    var recharge_data = JSON.parse(requestData.recharge_data);
    var total_recharge = 0;
    var total_recharge_amount = 0;
    var verifyUser = await VerifyModelObj.fetchFirstObj({ 'verify_user_id': user_id, 'verify_status': 2 });
    var rechargeInfo = await RechargeModelObj.getUserRechargeInfo(user_id, startDate, endDate);
    var regCount = recharge_data.length;
    var regAmount = await CommonFnObj.getArrySum(recharge_data, 'recharge_product_id');
    var wallet_amount = await WalletModelObj.fetchFirstObj({ 'wallet_user_id': user_id });
    total_recharge_amount = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_sum : 0) + Number(regAmount);
    total_recharge = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_count : 0) + Number(regCount);
    var newArray = [];
    var newArray1 = [];
    var comArr = [];
    const getMethod = await RechargeSettingModelObj.fetchObjWithSingleRecord(methodWhere, column);

    if (getMethod.method == 'dtone' && rechargeType == 1) {
        var rgType = rechargeType;
    } else if (getMethod.method == 'ding' && rechargeType == 2) {
        var rgType = rechargeType;

    } else {
        response.code = HttpStatus.OK;
        response.message = 'Something went wrong,please fill all required field again. '
        return response;
    }
    //  code for commission  
    const globalSetting = await SettingModelObj.fetchObjWithSingleRecord('', ["res_dev_commission", "user_commission"]);

    const devCommission = await UserCommissionModelObj.fetchObjWithSingleRecord(devWhere, ["commission_id", "commission"]);


    const globalTaxFeeCommission = await StripeFeeTaxModelObj.fetchObj({ "user_type": userType });

    var totalTax = Number(globalTaxFeeCommission[0].stripe_fee) + Number(globalTaxFeeCommission[0].tax)
    var deductAmount = 0.0;
    var totalRegAmount = 0.0;
    const promises = recharge_data.map(async function (reg, index) {

        const whereOpr = {
            'operator_id': reg.provider_code,
            'cos_user_id': user_id,
            'cos_user_type': request.headers['x-user-type']
        };
        const opratorCom = await CommissionOperatorModelObj.fetchObjWithSingleRecord(whereOpr, ['cos_commission', 'cos_id']);
        if (request.headers['x-user-type'] == 1) {
            var apply_commission = globalSetting.user_commission;
        } else {
            var apply_commission = globalSetting.res_dev_commission;
        }

        var commission_type = 1;
        if (opratorCom.cos_commission) {
            apply_commission = opratorCom.cos_commission;
            commission_type = 3;
        }
        if (devCommission && (opratorCom.cos_commission == null || opratorCom.cos_commission == 'undefined')) {
            apply_commission = devCommission.commission;
            commission_type = 2;

        }
        const commissionValueV = (reg.recharge_product_id * (apply_commission / 100));// calculate commission

        let jsonObj = {
            recharge_operator: reg.recharge_operator_id,
            provider_code: reg.provider_code,
            recharge_country_code: reg.recharge_country_code,
            recharge_country: reg.recharge_country,
            recharge_mobile_number: reg.recharge_mobile_number,
            recharge_currency: reg.recharge_currency,
            recharge_product_price: reg.recharge_product_id,
            user_id: user_id,
            recharge_type: rgType,
            recharge_payment_status: statusConstants.RECHARGE_PAYMENT_STATUS.COMPLETED,
            commission_value: apply_commission,
            commission_type: commission_type,
            //admin_commission_applied:commissionValueV, //@hemant
            stripe_fee: globalTaxFeeCommission[0].stripe_fee,
            tax: globalTaxFeeCommission[0].tax,
            created_at: currentDateTime,
            updated_at: currentDateTime
        };

        //for nauta recharge crated by @hemant
        if (reg.provider_code == "NUCU") {
            jsonObj.recharge_for = 2;
        }

        deductAmount = Number(reg.recharge_product_id) - Number(commissionValueV.toString().match(/^-?\d+(?:\.\d{0,4})?/)[0]);


        totalRegAmount = Number(totalRegAmount) + Number(deductAmount);

        let comObj = {
            commission_value: commissionValueV.toString().match(/^-?\d+(?:\.\d{0,4})?/)[0]
        };

        newArray.push(jsonObj);
        comArr.push(comObj);
    })

    const results = await Promise.all(promises)



    /*code for card payment */
    createData.wallet_id = wallet_amount.wallet_id;

    var calculateAmount = (Number(totalRegAmount) - Number(wallet_amount.wallet_amount).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);

    var applyTax = ((calculateAmount) * (totalTax / 100));

    var totalAmounts = Number(applyTax) + Number(calculateAmount);

    var stripAmount = totalRegAmount.toString().match(/^-?\d+(?:\.\d{0,4})?/)[0];




    const cusId = await UserModelObj.fetchObjWithSingleRecord({ 'user_id': user_id }, ["stripe_customer_id"]);

    var paymentResponse = await StripeLibObj.cardPayment(
        cardToken,
        //  stripeWithFeeAmt.total, 
        stripAmount,
        'USD',
        description, cusId.stripe_customer_id
    );
    /*code for deduct wallet amount*/

    if (paymentResponse.status) {


        updateWallet.wallet_amount = Number(wallet_amount.wallet_amount) + Number(stripAmount)//Number(stripAmount); 
        await WalletModelObj.updateObj(updateWallet, { 'wallet_id': wallet_amount.wallet_id }, ["wallet_id"]);
        createData.wt_status = 1;
        createData.wt_payment_status = 3;
        createData.wt_charge_id = paymentResponse.data.id;
        createData.wt_transaction_id = paymentResponse.data.balance_transaction;
        createData.wt_response = paymentResponse.data;

        createData.wt_amount = Number(stripAmount)//Number(stripAmount);

        createData.wt_pre_amount = Number(wallet_amount.wallet_amount);
        createData.wt_balance = (Number(wallet_amount.wallet_amount) + Number(stripAmount));//Number(stripAmount));

        var insert_ob = await WalletTransactionObj.createObj(createData, ["wt_id"]);

        if (insert_ob) {
            var walletData = await WalletModelObj.fetchFirstObj({ 'wallet_user_id': user_id });




            var selectRechargeData = ["recharge_id", "recharge_operator", "recharge_country_code", "recharge_mobile_number", "recharge_currency", "recharge_product_price", "commission_value", "commission_type", "admin_commission_applied", "recharge_for"];
            var selectRechargePayData = ["rp_id"];
            var recharge_insert_obj = await RechargeModelObj.batchinsertObjWithIds(newArray, selectRechargeData);
            const rechargeUpdateObj = {
            };

            // it is check for login use verify or not
            if (!verifyUser && (request.headers['x-user-type'] == 1) && (total_recharge_amount > 150 || total_recharge > 10)) {


                //code according to total recharge and total amount one by one

                var total_recharge_amount_usr = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_sum : 0);
                var total_recharge_usr = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_count : 0);
                var total_commission = 0;
                var total_done_reg_amount = 0;
                var dingCommission = wallet_amount.ding_commission_amount;



                for (const recharge of recharge_insert_obj) {

                    total_recharge_amount_usr = total_recharge_amount_usr + Number(recharge.recharge_product_price);

                    total_recharge_usr = total_recharge_usr + 1;
                    //check amount and no. of recharge 
                    if ((total_recharge_amount_usr <= 150 && total_recharge_usr <= 10)) {

                        var contactNo = recharge.recharge_country_code + recharge.recharge_mobile_number;

                        var contactNumber = contactNo.replace("+", "");

                        var responseData = await dingRechargeLibObj.SendTransfer(
                            recharge.recharge_operator,
                            recharge.recharge_product_price,
                            recharge.recharge_currency,
                            contactNumber);

                        if (responseData) {

                            // if(responseData.TransferRecord){
                            if (responseData.ResultCode == 1) {     //Added by @hemant_Solanki

                                var error_code = statusConstants.RECHARGE_STATUS.COMPLETED;
                                rechargeUpdateObj.recharge_response = responseData;

                                dingCommission = Number(dingCommission) + Number(responseData.TransferRecord.CommissionApplied);
                                //code added by @hemant_Solanki


                                if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                                    if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                                        if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                            rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                                        }
                                    }

                                    if (responseData.TransferRecord.CommissionApplied != null && responseData.TransferRecord.CommissionApplied != undefined) {

                                        const rechargeCommissionValue = (recharge.recharge_product_price * (recharge.commission_value / 100));

                                        rechargeUpdateObj.admin_commission_applied = rechargeCommissionValue;

                                        rechargeUpdateObj.ding_commission_applied = responseData.TransferRecord.CommissionApplied;

                                        rechargeUpdateObj.actual_commission = Number(responseData.TransferRecord.CommissionApplied) - Number(rechargeCommissionValue);
                                    }
                                }


                                //code added by @hemant_Solanki
                                //code for discuss flow

                                const commissionValue = (recharge.recharge_product_price * (recharge.commission_value / 100));// calculate commission
                                total_commission = (Number(total_commission) + Number(commissionValue));
                                total_done_reg_amount = (Number(total_done_reg_amount) + Number(recharge.recharge_product_price));
                                //end hear


                            } else {

                                var error_code = statusConstants.RECHARGE_STATUS.FAILED;
                                rechargeUpdateObj.recharge_response = responseData.response.data;
                                rechargeUpdateObj.recharge_payment_status = 1;


                                //code added by @hemant_Solanki
                                if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                                    if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                                        if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                            rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                                        }
                                    }
                                }


                                //code added by @hemant_Solanki
                            }

                            rechargeUpdateObj.recharge_status = error_code;

                            await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge.recharge_id }, selectRechargeData);


                        }
                    } else {
                        //code for discuss flow
                        const commissionValue = (recharge.recharge_product_price * (recharge.commission_value / 100));// calculate commission
                        total_commission = (Number(total_commission) + Number(commissionValue));
                        total_done_reg_amount = (Number(total_done_reg_amount) + Number(recharge.recharge_product_price));
                        //end hear

                    }
                }

                if (responseData == undefined) {
                    const addDieCom = Number(wallet_amount.admin_commission_amount) + Number(total_commission);

                    updateWallet.wallet_amount = Number(walletData.wallet_amount) - Number(stripAmount);//Number(totalAmounts) 
                    updateWallet.admin_commission_amount = addDieCom;
                    updateWallet.ding_commission_amount = dingCommission;
                    createWalletTrs.wallet_id = wallet_amount.wallet_id;
                    createWalletTrs.wt_amount = Number(stripAmount);
                    createWalletTrs.wt_reference_type = 1;
                    createWalletTrs.wt_payment_status = 3;

                    createWalletTrs.wt_pre_amount = walletData.wallet_amount;

                    createWalletTrs.wt_balance = Number(walletData.wallet_amount) - Number(stripAmount);//Number(totalAmounts) 






                    if (total_done_reg_amount > 0) {
                        createWalletTrs.wt_status = 1;
                    } else {
                        createWalletTrs.wt_status = 2;
                    }
                    createWalletTrs.wt_type = 2;
                    await WalletTransactionObj.createObj(createWalletTrs, ["wt_id"]);
                    await WalletModelObj.updateObj(updateWallet, { 'wallet_user_id': user_id }, ["wallet_id"]);
                }
                // end code according to total recharge and total amount one by one

                //    response.code = HttpStatus.OK;
                //    response.message = 'you payment has been done successfully will let you know status for recharge soon';
                //    return response;
                if (responseData != undefined) {

                    if (responseData.ResultCode != 1) {

                        throw Boom.badRequest(localeService.translate(responseData.response.data.ErrorCodes[0].Code));
                    } else {

                        response.code = HttpStatus.OK;
                        response.message = 'you payment has been done successfully will let you know status for recharge soon';
                        return response;
                    }
                } else {

                    response.code = HttpStatus.OK;
                    response.message = 'you payment has been done successfully will let you know status for recharge soon';
                    return response;
                }
            } else {
                if (getMethod.method == 'dtone') {
                    for (const recharge of recharge_insert_obj) {
                        var contactNo = recharge.recharge_country_code + recharge.recharge_mobile_number;
                        const responseData = await airtimeRechargeLibObj.makeRecharge(contactNo, recharge.recharge_product_price); if (responseData) {
                            let error_code = responseData.TransferTo.error_code._text;
                            let recharge_status = error_code != 0 ? statusConstants.RECHARGE_STATUS.FAILED : statusConstants.RECHARGE_STATUS.COMPLETED;
                            let rechargeUpdateObj = {
                                "recharge_status": recharge_status,
                                "recharge_response": responseData
                            }
                            //code added by @hemant_Solanki

                            if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                                if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                                    if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                        rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                                    }
                                }
                            }
                            //code added by @hemant_Solanki

                            await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge.recharge_id }, selectRechargeData);

                            newArray1.push(responseData)
                            //return resolve(responseData);
                        }
                    }
                    response.message = 'you payment has been done successfully will let you know status for recharge soon';
                    response.code = HttpStatus.OK;
                    return response;

                } else {

                    var total_commission = 0;
                    var total_done_reg_amount = 0;
                    var dingCommission = wallet_amount.ding_commission_amount;
                    for (const recharge of recharge_insert_obj) {
                        var contactNo = recharge.recharge_country_code + recharge.recharge_mobile_number;
                        var contactNumber = contactNo.replace("+", "");
                        var responseData = await dingRechargeLibObj.SendTransfer(
                            recharge.recharge_operator,
                            recharge.recharge_product_price,
                            recharge.recharge_currency,
                            contactNumber);

                        if (responseData) {
                            //  if(responseData.TransferRecord){
                            if (responseData.ResultCode == 1) {     //Added by @hemant_Solanki

                                var error_code = statusConstants.RECHARGE_STATUS.COMPLETED;
                                rechargeUpdateObj.recharge_response = responseData;

                                dingCommission = Number(dingCommission) + Number(responseData.TransferRecord.CommissionApplied);
                                //code added by @hemant_Solanki

                                if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                                    if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                                        if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                            rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                                        }
                                    }

                                    if (responseData.TransferRecord.CommissionApplied != null && responseData.TransferRecord.CommissionApplied != undefined) {

                                        const rechargeCommissionValue = (recharge.recharge_product_price * (recharge.commission_value / 100));

                                        rechargeUpdateObj.admin_commission_applied = rechargeCommissionValue;

                                        rechargeUpdateObj.ding_commission_applied = responseData.TransferRecord.CommissionApplied;

                                        rechargeUpdateObj.actual_commission = Number(responseData.TransferRecord.CommissionApplied) - Number(rechargeCommissionValue);
                                    }
                                }


                                //code added by @hemant_Solanki
                                //code for discuss flow
                                const commissionValue = (recharge.recharge_product_price * (recharge.commission_value / 100));// calculate commission
                                total_commission = (Number(total_commission) + Number(commissionValue));
                                total_done_reg_amount = (Number(total_done_reg_amount) + Number(recharge.recharge_product_price));
                                //end hear

                            } else {
                                var error_code = statusConstants.RECHARGE_STATUS.FAILED;
                                rechargeUpdateObj.recharge_response = responseData.response.data;
                                rechargeUpdateObj.recharge_payment_status = 1;


                                //code added by @hemant_Solanki
                                if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                                    if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                                        if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                            rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                                        }
                                    }
                                }
                                //code added by @hemant_Solanki

                            }

                            rechargeUpdateObj.recharge_status = error_code;

                            await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge.recharge_id }, selectRechargeData);

                            // newArray1.push(responseData)
                        }
                    }

                    // if( responseData.ResultCode != 2 || responseData.ResultCode != 3 || responseData.ResultCode != 4 || responseData.ResultCode != 5 ) {
                    if (responseData.response == undefined) {

                        const addDieCom = Number(walletData.admin_commission_amount) + Number(total_commission);



                        var rechargeCommission = Number(total_done_reg_amount) - Number(total_commission);
                        applyTax = applyTax.toString().match(/^-?\d+(?:\.\d{0,4})?/)[0];


                        var totalCharge = (Number(walletData.wallet_amount) - Number(stripAmount));//(Number(rechargeCommission) + Number(applyTax)));

                        updateWallet.wallet_amount = totalCharge;

                        updateWallet.admin_commission_amount = addDieCom;
                        updateWallet.ding_commission_amount = dingCommission;
                        createWalletTrs.wallet_id = walletData.wallet_id;


                        createWalletTrs.wt_amount = stripAmount;

                        createWalletTrs.wt_reference_type = 1;
                        createWalletTrs.wt_payment_status = 3;

                        createWalletTrs.wt_pre_amount = walletData.wallet_amount;

                        createWalletTrs.wt_balance = totalCharge;

                        if (total_done_reg_amount > 0) {
                            createWalletTrs.wt_status = 1;

                        } else {
                            createWalletTrs.wt_status = 2;

                        }
                        createWalletTrs.wt_type = 2;
                        await WalletTransactionObj.createObj(createWalletTrs, ["wt_id"]);
                        await WalletModelObj.updateObj(updateWallet, { 'wallet_user_id': user_id }, ["wallet_id"]);
                    }

                    //  return response;
                    if (responseData.ResultCode != 1) {

                        throw Boom.badRequest(localeService.translate(responseData.response.data.ErrorCodes[0].Code));
                    } else {

                        response.code = HttpStatus.OK;
                        response.message = 'you payment has been done successfully will let you know status for recharge soon';
                        return response;
                    }
                }
            }

        } else {
            createData.wt_status = 2;
            createData.wt_payment_status = 2;
            createData.wt_response = paymentResponse.data;
            var insert_obj = await WalletTransactionObj.createObj(createData, ["wt_id"]);
            throw Boom.badRequest(paymentResponse.data.message);
        }
    } else {

        throw Boom.badRequest(localeService.translate("INSUFFICIENT_FUNDS"));
    }
    /*end*/
}

export const rechargeCardProcessOld = async function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();

    var endDate = datetimeObj.currentDate();
    var startDate = datetimeObj.currentMonthStartData();
    const requestData = request.body,
        cardToken = requestData.recharge_payment_token_id,
        description = "Payment for recharge",
        methodWhere = {
            is_default: 1
        },
        column = ["method"],
        createWalletTrs = {
            "user_id": requestData.user_id,
            "created_at": currentDateTime,
            "updated_at": currentDateTime

        },
        updateWallet = {
            "updated_at": currentDateTime
        },
        createData = {
            "wt_type": 1,
            "wt_reference_type": 3,
            "wt_currency": 'USD',
            "wt_gateway": 1,
            "wt_payment_method": 1,
            "user_id": requestData.user_id,
            "created_at": currentDateTime,
            "updated_at": currentDateTime
        },
        response = { "data": {} },
        rechargeType = requestData.recharge_type,
        user_id = requestData.user_id,
        devWhere = {
            'commission_user_id': user_id,
            'commission_user_type': request.headers['x-user-type']
        };
    var recharge_data = JSON.parse(requestData.recharge_data);
    var total_recharge = 0;
    var total_recharge_amount = 0;
    var verifyUser = await VerifyModelObj.fetchFirstObj({ 'verify_user_id': user_id, 'verify_status': 2 });
    var rechargeInfo = await RechargeModelObj.getUserRechargeInfo(user_id, startDate, endDate);
    var regCount = recharge_data.length;
    var regAmount = await CommonFnObj.getArrySum(recharge_data, 'recharge_product_id');
    var wallet_amount = await WalletModelObj.fetchFirstObj({ 'wallet_user_id': user_id });
    total_recharge_amount = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_sum : 0) + Number(regAmount);
    total_recharge = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_count : 0) + Number(regCount);
    var newArray = [];
    var newArray1 = [];
    var comArr = [];
    const getMethod = await RechargeSettingModelObj.fetchObjWithSingleRecord(methodWhere, column);
    if (getMethod.method == 'dtone' && rechargeType == 1) {
        var rgType = rechargeType;
    } else if (getMethod.method == 'ding' && rechargeType == 2) {
        var rgType = rechargeType;

    } else {
        response.code = HttpStatus.OK;
        response.message = 'Something went wrong,please fill all required field again. '
        return response;
    }
    //  code for commission  
    const globalSetting = await SettingModelObj.fetchObjWithSingleRecord('', ["res_dev_commission", "user_commission"]);
    const devCommission = await UserCommissionModelObj.fetchObjWithSingleRecord(devWhere, ["commission_id", "commission"]);

    const promises = recharge_data.map(async function (reg, index) {

        const whereOpr = {
            'operator_id': reg.provider_code,
            'cos_user_id': user_id,
            'cos_user_type': request.headers['x-user-type']
        };
        const opratorCom = await CommissionOperatorModelObj.fetchObjWithSingleRecord(whereOpr, ['cos_commission', 'cos_id']);
        if (request.headers['x-user-type'] == 1) {
            var apply_commission = globalSetting.user_commission;
        } else {
            var apply_commission = globalSetting.res_dev_commission;
        }

        var commission_type = 1;
        if (opratorCom.cos_commission) {
            apply_commission = opratorCom.cos_commission;
            commission_type = 3;
        }
        if (devCommission && (opratorCom.cos_commission == null || opratorCom.cos_commission == 'undefined')) {
            apply_commission = devCommission.commission;
            commission_type = 2;

        }
        const commissionValueV = (reg.recharge_product_id * (apply_commission / 100));// calculate commission


        let jsonObj = {
            recharge_operator: reg.recharge_operator_id,
            provider_code: reg.provider_code,
            recharge_country_code: reg.recharge_country_code,
            recharge_country: reg.recharge_country,
            recharge_mobile_number: reg.recharge_mobile_number,
            recharge_currency: reg.recharge_currency,
            recharge_product_price: reg.recharge_product_id,
            user_id: user_id,
            recharge_type: rgType,
            recharge_payment_status: statusConstants.RECHARGE_PAYMENT_STATUS.COMPLETED,
            commission_value: apply_commission,
            commission_type: commission_type,
            created_at: currentDateTime,
            updated_at: currentDateTime
        };

        let comObj = {
            commission_value: commissionValueV
        };

        newArray.push(jsonObj);
        comArr.push(comObj);
    })

    const results = await Promise.all(promises)


    /*code for card payment */
    createData.wallet_id = wallet_amount.wallet_id;
    // createData.wt_amount = parseFloat(regAmount);
    var stripAmount = (Number(regAmount) - Number(wallet_amount.wallet_amount)).toFixed(2);


    var stripeWithFeeAmt = await StripeLibObj.stripeFeeCalculator(stripAmount, 'USD');
    const cusId = await UserModelObj.fetchObjWithSingleRecord({ 'user_id': user_id }, ["stripe_customer_id"]);

    // return response;
    var paymentResponse = await StripeLibObj.cardPayment(
        cardToken,
        stripeWithFeeAmt.total,
        'USD',
        description, cusId.stripe_customer_id
    );
    /*code for deduct wallet amount*/
    //    EUR: { Percent: 2.4, Fixed: 0.24 },
    if (paymentResponse.status) {


        // var fee = Math.round(((price*0.029)+0.30),2);
        // var Pay =  price- fee;

        updateWallet.wallet_amount = Number(wallet_amount.wallet_amount) + Number(stripAmount);
        await WalletModelObj.updateObj(updateWallet, { 'wallet_id': wallet_amount.wallet_id }, ["wallet_id"]);
        createData.wt_status = 1;
        createData.wt_payment_status = 3;
        createData.wt_charge_id = paymentResponse.data.id;
        createData.wt_transaction_id = paymentResponse.data.balance_transaction;
        createData.wt_response = paymentResponse.data;

        createData.wt_amount = Number(stripAmount);

        createData.wt_pre_amount = Number(wallet_amount.wallet_amount);
        createData.wt_balance = (Number(wallet_amount.wallet_amount) + Number(stripAmount));

        var insert_ob = await WalletTransactionObj.createObj(createData, ["wt_id"]);

        if (insert_ob) {
            var walletData = await WalletModelObj.fetchFirstObj({ 'wallet_user_id': user_id });

            var selectRechargeData = ["recharge_id", "recharge_operator", "recharge_country_code", "recharge_mobile_number", "recharge_currency", "recharge_product_price", "commission_value", "commission_type"];
            var selectRechargePayData = ["rp_id"];
            var recharge_insert_obj = await RechargeModelObj.batchinsertObjWithIds(newArray, selectRechargeData);
            const rechargeUpdateObj = {
            };
            // it is check for login use verify or not
            if (!verifyUser && (request.headers['x-user-type'] == 1) && (total_recharge_amount > 150 || total_recharge > 10)) {

                var total_recharge_amount_usr = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_sum : 0);
                var total_recharge_usr = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_count : 0);
                var total_commission = 0;
                var total_done_reg_amount = 0;
                var dingCommission = wallet_amount.ding_commission_amount;
                for (const recharge of recharge_insert_obj) {

                    total_recharge_amount_usr = total_recharge_amount_usr + Number(recharge.recharge_product_price);
                    total_recharge_usr = total_recharge_usr + 1;
                    //check amount and no. of recharge 
                    if ((total_recharge_amount_usr <= 150 && total_recharge_usr <= 10)) {

                        var contactNo = recharge.recharge_country_code + recharge.recharge_mobile_number;
                        var contactNumber = contactNo.replace("+", "");
                        const responseData = await dingRechargeLibObj.SendTransfer(
                            recharge.recharge_operator,
                            recharge.recharge_product_price,
                            recharge.recharge_currency,
                            contactNumber);
                        if (responseData) {
                            // if(responseData.TransferRecord){
                            if (responseData.ResultCode == 1) {     //Added by @hemant_Solanki
                                var error_code = statusConstants.RECHARGE_STATUS.COMPLETED;
                                rechargeUpdateObj.recharge_response = responseData;

                                dingCommission = Number(dingCommission) + Number(responseData.TransferRecord.CommissionApplied);
                                //code added by @hemant_Solanki

                                if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                                    if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                                        if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                            rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                                        }
                                    }
                                }
                                //code added by @hemant_Solanki
                                //code for discuss flow

                                const commissionValue = (recharge.recharge_product_price * (recharge.commission_value / 100));// calculate commission
                                total_commission = (Number(total_commission) + Number(commissionValue));
                                total_done_reg_amount = (Number(total_done_reg_amount) + Number(recharge.recharge_product_price));
                                //end hear

                            } else {
                                var error_code = statusConstants.RECHARGE_STATUS.FAILED;
                                rechargeUpdateObj.recharge_response = responseData.response.data;
                                rechargeUpdateObj.recharge_payment_status = 1;


                                //code added by @hemant_Solanki
                                if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                                    if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                                        if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                            rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                                        }
                                    }
                                }
                                //code added by @hemant_Solanki
                            }

                            rechargeUpdateObj.recharge_status = error_code;

                            await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge.recharge_id }, selectRechargeData);

                            // newArray1.push(responseData)
                        }
                    } else {
                        //code for discuss flow
                        const commissionValue = (recharge.recharge_product_price * (recharge.commission_value / 100));// calculate commission
                        total_commission = (Number(total_commission) + Number(commissionValue));
                        total_done_reg_amount = (Number(total_done_reg_amount) + Number(recharge.recharge_product_price));
                        //end hear
                    }
                }
                const addDieCom = Number(wallet_amount.admin_commission_amount) + Number(total_commission);

                updateWallet.wallet_amount = Number(walletData.wallet_amount) - (Number(total_done_reg_amount) - Number(total_commission));
                updateWallet.admin_commission_amount = addDieCom;
                updateWallet.ding_commission_amount = dingCommission;
                createWalletTrs.wallet_id = wallet_amount.wallet_id;
                createWalletTrs.wt_amount = (Number(total_done_reg_amount) - Number(total_commission));
                createWalletTrs.wt_reference_type = 1;
                createWalletTrs.wt_payment_status = 3;

                createWalletTrs.wt_pre_amount = walletData.wallet_amount;
                createWalletTrs.wt_balance = Number(walletData.wallet_amount) - (Number(total_done_reg_amount) - Number(total_commission));

                if (total_done_reg_amount > 0) {
                    createWalletTrs.wt_status = 1;
                } else {
                    createWalletTrs.wt_status = 2;
                }
                createWalletTrs.wt_type = 2;
                await WalletTransactionObj.createObj(createWalletTrs, ["wt_id"]);
                await WalletModelObj.updateObj(updateWallet, { 'wallet_user_id': user_id }, ["wallet_id"]);
                // end code according to total recharge and total amount one by one
                response.code = HttpStatus.OK;
                response.message = 'you payment has been done successfully will let you know status for recharge soon';
                return response;

            } else {
                if (getMethod.method == 'dtone') {
                    for (const recharge of recharge_insert_obj) {
                        var contactNo = recharge.recharge_country_code + recharge.recharge_mobile_number;
                        const responseData = await airtimeRechargeLibObj.makeRecharge(contactNo, recharge.recharge_product_price); if (responseData) {
                            let error_code = responseData.TransferTo.error_code._text;
                            let recharge_status = error_code != 0 ? statusConstants.RECHARGE_STATUS.FAILED : statusConstants.RECHARGE_STATUS.COMPLETED;
                            let rechargeUpdateObj = {
                                "recharge_status": recharge_status,
                                "recharge_response": responseData
                            }
                            //code added by @hemant_Solanki

                            if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                                if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                                    if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                        rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                                    }
                                }
                            }
                            //code added by @hemant_Solanki

                            await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge.recharge_id }, selectRechargeData);

                            newArray1.push(responseData)
                            //return resolve(responseData);
                        }
                    }
                    response.message = 'you payment has been done successfully will let you know status for recharge soon';
                    response.code = HttpStatus.OK;
                    return response;

                } else {

                    var total_commission = 0;
                    var total_done_reg_amount = 0;
                    var dingCommission = wallet_amount.ding_commission_amount;
                    for (const recharge of recharge_insert_obj) {
                        var contactNo = recharge.recharge_country_code + recharge.recharge_mobile_number;
                        var contactNumber = contactNo.replace("+", "");
                        const responseData = await dingRechargeLibObj.SendTransfer(
                            recharge.recharge_operator,
                            recharge.recharge_product_price,
                            recharge.recharge_currency,
                            contactNumber);
                        if (responseData) {
                            //  if(responseData.TransferRecord){
                            if (responseData.ResultCode == 1) {     //Added by @hemant_Solanki
                                var error_code = statusConstants.RECHARGE_STATUS.COMPLETED;
                                rechargeUpdateObj.recharge_response = responseData;

                                dingCommission = Number(dingCommission) + Number(responseData.TransferRecord.CommissionApplied);
                                //code added by @hemant_Solanki

                                if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                                    if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                                        if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                            rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                                        }
                                    }
                                }
                                //code added by @hemant_Solanki
                                //code for discuss flow
                                const commissionValue = (recharge.recharge_product_price * (recharge.commission_value / 100));// calculate commission
                                total_commission = (Number(total_commission) + Number(commissionValue));
                                total_done_reg_amount = (Number(total_done_reg_amount) + Number(recharge.recharge_product_price));
                                //end hear
                            } else {
                                var error_code = statusConstants.RECHARGE_STATUS.FAILED;
                                rechargeUpdateObj.recharge_response = responseData.response.data;
                                rechargeUpdateObj.recharge_payment_status = 1;


                                //code added by @hemant_Solanki
                                if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                                    if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                                        if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                                            rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                                        }
                                    }
                                }
                                //code added by @hemant_Solanki

                            }

                            rechargeUpdateObj.recharge_status = error_code;

                            await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": recharge.recharge_id }, selectRechargeData);

                            // newArray1.push(responseData)
                        }
                    }


                    const addDieCom = Number(walletData.admin_commission_amount) + Number(total_commission);

                    updateWallet.wallet_amount = Number(walletData.wallet_amount) - (Number(total_done_reg_amount) - Number(total_commission));
                    updateWallet.admin_commission_amount = addDieCom;
                    updateWallet.ding_commission_amount = dingCommission;
                    createWalletTrs.wallet_id = walletData.wallet_id;
                    createWalletTrs.wt_amount = (Number(total_done_reg_amount) - Number(total_commission));
                    createWalletTrs.wt_reference_type = 1;
                    createWalletTrs.wt_payment_status = 3;

                    createWalletTrs.wt_pre_amount = walletData.wallet_amount;
                    createWalletTrs.wt_balance = Number(walletData.wallet_amount) - (Number(total_done_reg_amount) - Number(total_commission));

                    if (total_done_reg_amount > 0) {
                        createWalletTrs.wt_status = 1;

                    } else {
                        createWalletTrs.wt_status = 2;

                    }
                    createWalletTrs.wt_type = 2;
                    await WalletTransactionObj.createObj(createWalletTrs, ["wt_id"]);
                    await WalletModelObj.updateObj(updateWallet, { 'wallet_user_id': user_id }, ["wallet_id"]);
                    response.code = HttpStatus.OK;
                    response.message = 'you payment has been done successfully will let you know status for recharge soon';
                    return response;
                }
            }

        } else {
            createData.wt_status = 2;
            createData.wt_payment_status = 2;
            createData.wt_response = paymentResponse.data;
            var insert_obj = await WalletTransactionObj.createObj(createData, ["wt_id"]);
            throw Boom.badRequest(paymentResponse.data.message);
        }
    }
    /*end*/
};

export const rechargeHisttory = async function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    var rechargeLimit;
    const requestData = request.body,
        response = { "data": {} };
    response.code = HttpStatus.OK;
    response.lang = {
        REPEAT:localeService.translate( "REPEAT" ),
        success:localeService.translate( "success" ),
    }
    const totalCount = await RechargeModelObj.getRechargeHistoryCount(requestData);
    const totalRechargeAmount = await RechargeModelObj.getTotalRechargeAmount(requestData, currentDateTime);
    const totalDeveloperRechargeAmount = await RechargeModelObj.getTotalDeveloperRechargeAmount(requestData, currentDateTime);
    const totalRechargelimit = await RechargeModelObj.getTotalRechargeLimit(requestData);
    
    if (totalRechargeAmount[0].total_recharge_amount == null) {
        totalRechargeAmount[0].total_recharge_amount = 0.0;
    }
    if (totalDeveloperRechargeAmount[0].total_developer_recharge_amount == null) {
        totalDeveloperRechargeAmount[0].total_developer_recharge_amount = 0.0;
    }


    if (totalRechargelimit.length > 0) {
        rechargeLimit = totalRechargelimit[0].recharge_limit_amount;
    } else {
        rechargeLimit = null;
    }

    return RechargeModelObj.getRechargeHistory(requestData).then((historyList) => {
        return datetimeObj.changeDatabaseTimestampFormat(historyList, "created_at", databaseConstants.TIMESTAMP_FORMAT).then((rechargeList) => {

            // response.data.historyList = historyList;
            response.data.historyList = rechargeList;
            response.recordsTotal = totalCount[0].count;
            response.recordsFiltered = totalCount[0].count;
            response.totalRechargesAmount = Number(totalRechargeAmount[0].total_recharge_amount) + Number(totalDeveloperRechargeAmount[0].total_developer_recharge_amount)
            response.rechargeLimit = rechargeLimit;

            return response;
        }).catch((error) => {
            logger.error(error);
            throw Boom.notImplemented(localeService.translate(error));
        });
    })
};
export const rechargeDetails = async function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        where = {
            'recharge_mobile_number': requestData.recharge_mobile_number,
            'user_id': requestData.user_id
        },
        column = ["recharge_id", "recharge_operator", "provider_code", "recharge_mobile_number", "recharge_currency", "recharge_product_price", "recharge_status", "recharge_response", "recharge_for", "created_at"],
        response = { "data": {} };
    response.code = HttpStatus.OK;

    return RechargeModelObj.fetchObjWithSelectedFields(where, column, 'created_at', 'DESC').then((rechargeDetail) => {
        return datetimeObj.changeDatabaseTimestampFormat(rechargeDetail, "created_at", databaseConstants.TIMESTAMP_FORMAT).then((rechDetail) => {
            // response.data.rechargeDetail = rechargeDetail;
            response.data.rechargeDetail = rechDetail;
            return response;
        }).catch((error) => {
            logger.error(error);
            throw Boom.notImplemented(localeService.translate(error));
        });
    })
};

export const rechargeByAdmin = async function (request) {

    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();

    var endDate = datetimeObj.currentDate();
    var startDate = datetimeObj.currentMonthStartData();
    const requestData = request.body,
        methodWhere = {
            is_default: 1
        },
        column = ["method"],
        createWalletTrs = {
            "user_id": requestData.user_id,
            "created_at": currentDateTime,
            "updated_at": currentDateTime

        },
        updateWallet = {
            "updated_at": currentDateTime
        },

        response = { "data": {} },
        rechargeType = requestData.recharge_type,
        user_id = requestData.user_id,
        devWhere = {
            'commission_user_id': user_id,
            'commission_user_type': request.headers['x-user-type']
        };

    var rechargeInfo = await RechargeModelObj.getUserRechargeInfo(user_id, startDate, endDate);

    // return false;  
    const getMethod = await RechargeSettingModelObj.fetchObjWithSingleRecord(methodWhere, column);



    var selectRechargeData = ["recharge_id", "recharge_operator", "recharge_country_code", "recharge_mobile_number", "recharge_currency", "recharge_product_price"];

    let rechargeUpdateObj = {
    };

    if (getMethod.method == 'dtone') {

        var contactNo = requestData.recharge_mobile_number;
        const responseData = await airtimeRechargeLibObj.makeRecharge(contactNo, requestData.recharge_product_id);
        if (responseData) {
            let error_code = responseData.TransferTo.error_code._text;
            let recharge_status = error_code != 0 ? statusConstants.RECHARGE_STATUS.FAILED : statusConstants.RECHARGE_STATUS.COMPLETED;
            let rechargeUpdateObj = {
                "recharge_status": recharge_status,
                "recharge_response": responseData
            }

            //code added by @hemant_Solanki

            if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                    if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                        rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                    }
                }

            }

            //code added by @hemant_Solanki
            await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": requestData.recharge_id }, selectRechargeData);

            newArray1.push(responseData)
            //return resolve(responseData);
        }

        response.message = 'you payment has been done successfully will let you know status for recharge soon';
        response.code = HttpStatus.OK;
        return response;

    } else {

        var contactNo = requestData.recharge_mobile_number;
        var contactNumber = contactNo.replace("+", "");
        var responseData = await dingRechargeLibObj.SendTransfer(
            requestData.recharge_operator_id,
            requestData.recharge_product_id,
            requestData.recharge_currency,
            contactNumber);
        if (responseData) {

            //   if(responseData.TransferRecord){
            if (responseData.ResultCode == 1) {     //Added by @hemant_Solanki
                var error_code = statusConstants.RECHARGE_STATUS.COMPLETED;
                rechargeUpdateObj.recharge_response = responseData;
                //code added by @hemant_Solanki
                if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                    if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                        if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                            rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                        }
                    }

                    if (responseData.TransferRecord.CommissionApplied != null && responseData.TransferRecord.CommissionApplied != undefined) {

                        const rechargeCommissionValue = (requestData.recharge_product_id * (requestData.commission_value / 100));

                        rechargeUpdateObj.admin_commission_applied = rechargeCommissionValue;

                        rechargeUpdateObj.ding_commission_applied = responseData.TransferRecord.CommissionApplied;

                        rechargeUpdateObj.actual_commission = Number(responseData.TransferRecord.CommissionApplied) - Number(rechargeCommissionValue);
                    }
                }
                //code added by @hemant_Solanki
            } else {
                var error_code = statusConstants.RECHARGE_STATUS.FAILED;
                rechargeUpdateObj.recharge_response = responseData.response.data;

                //code added by @hemant_Solanki
                if (responseData.TransferRecord != null && responseData.TransferRecord != undefined) {
                    if (responseData.TransferRecord.TransferId != null && responseData.TransferRecord.TransferId != undefined) {
                        if (responseData.TransferRecord.TransferId.TransferRef != null && responseData.TransferRecord.TransferId.TransferRef != undefined) {
                            rechargeUpdateObj.transfer_ref = responseData.TransferRecord.TransferId.TransferRef;
                        }
                    }
                }
                //code added by @hemant_Solanki

            }
            let recharge_status = error_code;
            rechargeUpdateObj.recharge_status = recharge_status;


            await RechargeModelObj.updateObj(rechargeUpdateObj, { "recharge_id": requestData.recharge_id }, selectRechargeData);

            // newArray1.push(responseData)
        }


        if (responseData.ResultCode != 1) {
            throw Boom.badRequest(localeService.translate(responseData.response.data.ErrorCodes[0].Code));
        } else {
            response.code = HttpStatus.OK;
            response.message = 'you payment has been done successfully will let you know status for recharge soon';
            return response;
        }
    }
}
export const rechargeMonthStatus = async function (request) {

    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    var endDate = datetimeObj.currentDate();
    var startDate = datetimeObj.currentMonthStartData();
    const requestData = request.body,
        response = { "data": {} },
        user_id = requestData.user_id ? requestData.user_id : Security.decryptId(requestData.userId),
        whereColumn = {
           "user_id": user_id  
        },
        select = [
            "recharge_limit_id",
            "user_id",
            "recharge_limit_amount",
            "created_at"
        ];
    var total_recharge = 0;
    var total_recharge_amount = 0;
    //var verifyUser = await  VerifyModelObj.fetchFirstObj({'verify_user_id':user_id});
    var rechargeInfo = await RechargeModelObj.getUserRechargeInfo(user_id, startDate, endDate);
    var rechargeLimit = await RechargeLimitModalObj.fetchObjWithSingleRecord(whereColumn, select)
    var rechLimit = '';
    if( rechargeLimit != undefined ) {
        rechLimit= rechargeLimit.recharge_limit_amount
    }
    
    var wallet_amount = await WalletModelObj.fetchFirstObj({ 'wallet_user_id': user_id });

    total_recharge_amount = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_sum : 0);
    total_recharge = Number(rechargeInfo[0] ? rechargeInfo[0].recharge_count : 0);
    response.code = HttpStatus.OK;
    response.totalRecharge = total_recharge;
    response.totalAmount = total_recharge_amount;
    response.userRechargeAmountLimit = rechLimit;
    response.rechargeLimit = userRechargeLimit;
    response.rechargeAmountLimit = userRechargeAmountLimit;
    
    response.message =  localeService.translate("ACCOUNTS_STATUS") ? localeService.translate("ACCOUNTS_STATUS") :'Account status.';
    
    return response;
}
export const rechargeDeveloperHistory = async function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        response = { "data": {} };
    response.code = HttpStatus.OK;
    const totalCount = await RechargeModelObj.rechargeDeveloperHistoryCount(requestData);

    return RechargeModelObj.rechargeDeveloperHistory(requestData).then((historyList) => {
        response.data.historyList = historyList;
        response.recordsTotal = totalCount[0].count;
        response.recordsFiltered = totalCount[0].count;

        return response;
    })
};

/**
 * 
 * Developer recharge detail created by @hemant_solanki 
 */
export const rechargeDeveloperDetail = async function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.query,
        response = { "data": {} };

    return RechargeModelObj.rechargeDeveloperDetail(requestData).then((historyList) => {

        if (historyList[0].reg_response_data == null) {
            historyList[0].reg_response_data = 'No record found';
        }
        response.data.historyList = historyList;
        return response;
    })
};

/**
 * 
 * User recharge detail by admin created by @hemant_solanki 
 */
export const userRechargeDetails = async function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        page = requestData.start,
        countColumn = ["recharge_id"],
        userId = Security.decryptId(requestData.user_id),
        where = {
            'recharge_mobile_number': requestData.recharge_mobile_number,
            'user_id': userId
        },
        column = ["recharge_id", "recharge_operator", "provider_code", "recharge_mobile_number", "recharge_currency", "recharge_product_price", "recharge_status", "recharge_response", "created_at"],
        limit = databaseConstants.LIMIT,
        response = { "data": {} };

    let offset = databaseConstants.OFFSET;

    if (page !== undefined) {
        offset = page;
    }
    response.code = HttpStatus.OK;
    const totalCount = await RechargeModelObj.getCountResellerRechargeDetailData(where, countColumn, requestData);

    return RechargeModelObj.getResellerRechargeDetail(where, column, limit, offset, requestData).then((rechargeDetail) => {

        response.data.historyList = rechargeDetail;
        response.recordsTotal = totalCount[0];
        response.recordsFiltered = totalCount[0];

        return response;
    })

};

/**
 * Get Developer recharge list by admin created by @hemant_solenki
 * 
 */
export const developerRechargeHistory = async function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        userId = Security.decryptId(requestData.reseller_user_id),
        response = { "data": {} };
    response.code = HttpStatus.OK;
    const totalCount = await RechargeModelObj.getDeveloperrechargeHistoryCount(requestData, userId);

    return RechargeModelObj.getDeveloperRechargeHistory(requestData, userId).then((historyList) => {
        response.data.historyList = historyList;
        response.recordsTotal = totalCount[0].count;
        response.recordsFiltered = totalCount[0].count;

        return response;
    })
};

/**
 * Get recharge history by user id for download created by @hemant_solenki
 */
export const getRechargeHistoryByUserId = function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        response = { "data": {} };
    response.code = HttpStatus.OK;

    return RechargeModelObj.getRechargeHistoryByUserId(requestData).then((historyList) => {
        response.data.historyList = historyList;

        return response;
    })
};

/**
 * Get recharge list created by @hemant_solenki
 */
export const rechargeList = async function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    var rechargeLimit;
    const requestData = request.body,
        Page = requestData.start,
        limit = 20,//databaseConstants.LIMIT,
        response = { "data": {} };

    let offset = databaseConstants.OFFSET;
    if (Page !== undefined) {
        offset = Page;
    }
    response.code = HttpStatus.OK;
    const totalRechargeAmount = await RechargeModelObj.getTotalRechargeAmount(requestData, currentDateTime);

    const totalDeveloperRechargeAmount = await RechargeModelObj.getTotalDeveloperRechargeAmount(requestData, currentDateTime);

    const totalRechargelimit = await RechargeModelObj.getTotalRechargeLimit(requestData);

    if (totalRechargeAmount[0].total_recharge_amount == null) {
        totalRechargeAmount[0].total_recharge_amount = 0.0;
    }

    if (totalDeveloperRechargeAmount[0].total_developer_recharge_amount == null) {
        totalDeveloperRechargeAmount[0].total_developer_recharge_amount = 0.0;
    }

    if (totalRechargelimit.length > 0) {
        rechargeLimit = totalRechargelimit[0].recharge_limit_amount;
    } else {
        rechargeLimit = null;
    }

    return RechargeModelObj.getRechargeListCount(requestData).then((rechargeCount) => {

        return RechargeModelObj.getRechargeList(limit, offset, requestData).then((historyList) => {
            return datetimeObj.changeDatabaseTimestampFormat(historyList, "created_at", databaseConstants.TIMESTAMP_FORMAT).then((userData) => {
                // response.data.historyList = historyList;
                response.data.historyList = userData;
                response.recordsTotal = rechargeCount[0].count;
                response.recordsFiltered = rechargeCount[0].count;
                response.page = offset;
                response.totalRechargesAmount = Number(totalRechargeAmount[0].total_recharge_amount) + Number(totalDeveloperRechargeAmount[0].total_developer_recharge_amount)
            response.rechargeLimit = rechargeLimit;

                return response;
            }).catch((error) => {
                logger.error(error);
                throw Boom.notImplemented(localeService.translate(error));
            });
        })
    })
};

export const userRechargeDetail = function (request) {
    const requestData = request.body,
        where = {
            'recharge_id': requestData.recharge_id,
        },
        column = ["recharge_id", "recharge_operator", "provider_code", "recharge_mobile_number", "recharge_currency", "recharge_product_price", "recharge_status", "recharge_response", "created_at"],
        response = { "data": {} };
    response.code = HttpStatus.OK;

    return RechargeModelObj.fetchObjWithSelectedFields(where, column).then((rechargeDetail) => {

        response.data.rechargeDetail = rechargeDetail;
        return response;
    })
};

export const rechargeDeveloperApiHistory = function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        userId = Security.decryptId(requestData.user_id),
        Page = requestData.start,
        limit = 20,//databaseConstants.LIMIT,

        response = { "data": {} };

    let offset = databaseConstants.OFFSET;
    if (Page !== undefined) {
        offset = Page;
    }
    response.code = HttpStatus.OK;
    return RechargeModelObj.getrechargeDeveloperApiCount(requestData, userId).then((totalCount) => {
        return RechargeModelObj.getrechargeDeveloperApiHistory(limit, offset, requestData, userId).then((historyList) => {
            return datetimeObj.changeDatabaseTimestampFormat(historyList, "created_at", databaseConstants.TIMESTAMP_FORMAT).then((devRechargeList) => {
                response.data.historyList = devRechargeList;
                response.recordsTotal = totalCount[0].count;
                response.recordsFiltered = totalCount[0].count;
                response.page = offset;

                return response;
            }).catch((error) => {
                logger.error(error);
                throw Boom.notImplemented(localeService.translate(error));
            });
        })
    })
};

export const rechargeDeveloperApiDetail = function (request) {
    const requestData = request.body,
        response = { "data": {} };
    response.code = HttpStatus.OK;

    return RechargeModelObj.rechargeDeveloperApiDetail(requestData).then((rechargeDetail) => {

        response.data.rechargeDetail = rechargeDetail;
        return response;
    })
};

export const userRechargeHisttory = function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        Page = requestData.start,
        limit = 20,//databaseConstants.LIMIT,

        response = { "data": {} };
    let offset = databaseConstants.OFFSET;
    if (Page !== undefined) {
        offset = Page;
    }
    response.code = HttpStatus.OK;
    return RechargeModelObj.getUserRechargeHistoryCount(requestData).then((totalCount) => {

        return RechargeModelObj.getUserRechargeHistory(limit, offset, requestData).then((historyList) => {
            return datetimeObj.changeDatabaseTimestampFormat(historyList, "created_at", databaseConstants.TIMESTAMP_FORMAT).then((rechDetail) => {
                // response.data.historyList = historyList;
                response.data.historyList = rechDetail;
                response.recordsTotal = totalCount[0].count;
                response.recordsFiltered = totalCount[0].count;
                response.page = offset;

                return response;
            }).catch((error) => {
                logger.error(error);
                throw Boom.notImplemented(localeService.translate(error));
            });
        })
    });
};

export const gatUserRechargeDetailByAdmin = function (request) {
    const requestData = request.body,
        response = { "data": {} };
    response.code = HttpStatus.OK;

    return RechargeModelObj.gatUserRechargeDetailByAdmin(requestData).then((rechargeDetail) => {

        response.data.rechargeDetail = rechargeDetail;
        return response;
    })
};

export const getdeveloperRechargeHistoryByAdmin = function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        userId = Security.decryptId(requestData.reseller_user_id),
        Page = requestData.start,
        limit = 20,//databaseConstants.LIMIT,

        response = { "data": {} };

    let offset = databaseConstants.OFFSET;
    if (Page !== undefined) {
        offset = Page;
    }
    response.code = HttpStatus.OK;
    return RechargeModelObj.getdeveloperRechargeHistoryByAdminCount(requestData, userId).then((totalCount) => {
        return RechargeModelObj.getdeveloperRechargeHistoryByAdmin(limit, offset, requestData, userId).then((historyList) => {
            return datetimeObj.changeDatabaseTimestampFormat(historyList, "created_at", databaseConstants.TIMESTAMP_FORMAT).then((devRechargeList) => {
                response.data.historyList = devRechargeList;
                response.recordsTotal = totalCount[0].count;
                response.recordsFiltered = totalCount[0].count;
                response.page = offset;

                return response;
            }).catch((error) => {
                logger.error(error);
                throw Boom.notImplemented(localeService.translate(error));
            });
        })
    })
};

export const getdeveloperRechargeDetailByAdmin = function (request) {
    const requestData = request.body,
        response = { "data": {} };
    response.code = HttpStatus.OK;

    return RechargeModelObj.getdeveloperRechargeDetailByAdmin(requestData).then((rechargeDetail) => {

        response.data.rechargeDetail = rechargeDetail;
        return response;
    })
};

//get user recharge limit created by @hemant_solanki
export const getUserRechargesLimit = function (request) {

    let currentTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.query,
        whereColumn = {
            "user_id": Security.decryptId(requestData.user_id),
            // "created_at":currentTime
        },
        select = [
            "recharge_limit_id",
            "user_id",
            "recharge_limit_amount",
            "created_at"
        ],
        response = { "data": {} };
    response.code = HttpStatus.OK;

    return RechargeLimitModalObj.fetchObjWithSingleRecord(whereColumn, select).then((rechargeLimitDetail) => {

        response.code = HttpStatus.OK;
        response.rechargeLimitDetail = rechargeLimitDetail;
        return response;
    }).catch((err) => {

        throw Boom.badImplementation(localeService.translate("INTERNAL_SERVER_ERROR"));
    });

};

//save recharge limit created by @hemant_solanki
export const saveRechargeLimit = function (request) {

    let currentTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        whereColumn = {
            "user_id": Security.decryptId(requestData.user_id),
        },
        column = {
            "user_id": Security.decryptId(requestData.user_id),
            "recharge_limit_amount": requestData.recharge_limit,
            "created_at": currentTime,
            "updated_at": currentTime
        },
        update = {
            "recharge_limit_amount": requestData.recharge_limit,
            "updated_at": currentTime
        },
        updateWhereCondition = {
            "user_id": Security.decryptId(requestData.user_id),
        },
        select = [
            "recharge_limit_id",
            "user_id",
            "recharge_limit_amount",
            "created_at"
        ],
        response = { "data": {} };
    response.code = HttpStatus.OK;
    
    return RechargeLimitModalObj.fetchObjWithSingleRecord(whereColumn, select).then((rechargeLimitDetail) => {
        
        if (rechargeLimitDetail != undefined) {

            return RechargeLimitModalObj.updateObj(update, updateWhereCondition, select).then((updateRechargeLimit) => {
                response.code = HttpStatus.OK;
                response.rechargeLimitData = updateRechargeLimit[0];
                response.message = localeService.translate("Recharge limit updated successfullly.") ;
                return response;
            }).catch((err) => {

                throw Boom.badImplementation(localeService.translate("INTERNAL_SERVER_ERROR"));
            });
        } else {

            return RechargeLimitModalObj.createObj(column, select).then((rechargeLimitData) => {
                response.code = HttpStatus.OK;
                response.rechargeLimitData = rechargeLimitData;
                response.message = "Recharge limit added successfullly.";
                return response;
            }).catch((err) => {

                throw Boom.badImplementation(localeService.translate("INTERNAL_SERVER_ERROR"));
            });
        }
    }).catch((err) => {

        throw Boom.badImplementation(localeService.translate("INTERNAL_SERVER_ERROR"));
    });


};

//Delete recharge limit created by @hemant_solanki
export const deleteRechargeLimit = function (request) {

    let currentTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        column = {
            "user_id": Security.decryptId(requestData.user_id)
        },
        select = [
            "recharge_limit_id",
            "user_id",
            "recharge_limit_amount",
            "created_at"
        ],
        response = { "data": {} };
    response.code = HttpStatus.OK;

    return RechargeLimitModalObj.deleteObj(column, select).then((rechargeLimitData) => {
        response.code = HttpStatus.OK;
        response.rechargeLimitData = rechargeLimitData;
        response.message = "Recharge limit removed successfullly.";
        return response;
    }).catch((err) => {
        throw Boom.badImplementation(localeService.translate("INTERNAL_SERVER_ERROR"));
    });
};



//save recharge operator
export const saveOperator = async function (request) {
    let currentTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        country_id = requestData.recharge_country_id, //
        RegionCodes = requestData.region_codes,//
        whereData = {
            "operator_country": requestData.recharge_country_id,
        },
        selectOperator = [
            "recharge_operator_id",
            "operator_id"
        ],
        select = [
            "operator_id",
            "product_amount",
            "provider_code",
            "currency",
            "sku_code",
            "localization_key",
            "region_code"
        ],

        response = { "data": {} };

    var newArray = [];

    var operators = await OperatorModelObj.fetchObj(whereData);

    const responseData = await dingRechargeLibObj.GetProviders(country_id, RegionCodes);

    if (responseData.Items) {

        var operatorData = await convertDataObj.dingOperatorjsonToArray(responseData);

        var newOperator = await CommonFnObj.getDifference(operatorData, operators, 'operator_id')
        var updateOperator = await CommonFnObj.getMatchedData(operatorData, operators, 'operator_id')

        // response.data = newOperator;
    }

    //Save new operator and product
    if (newOperator.length > 0) {

        for (let i = 0; i < newOperator.length; i++) {
            let jsonObj = {
                operator_id: newOperator[i].operator_id,
                operator_value: newOperator[i].value,
                operator_region_codes: '',
                operator_country: newOperator[i].operatorCountry,
                operator_logo_url: newOperator[i].logoUrl,
                created_at: currentTime,
                updated_at: currentTime
            };

            var operatorInsertObj = await OperatorModelObj.createObj(jsonObj, selectOperator);

            //insert product
            const responseProductData = await dingRechargeLibObj.GetProducts('', '', operatorInsertObj.operator_id, '');

            if (responseProductData.Items) {
                var productData = await convertDataObj.dingProductsjson(responseProductData);
            }
            newArray = [];
            const promises = productData.map(async function (product, index) {
                let jsonObj = {
                    operator_id: operatorInsertObj.recharge_operator_id,
                    product_amount: product.product,
                    provider_code: product.providerCode,
                    currency: product.currency,
                    sku_code: product.skuCode,
                    localization_key: product.localizationKey,
                    region_code: product.regionCode,
                    created_at: currentTime,
                    updated_at: currentTime
                };

                newArray.push(jsonObj);

            });

            const results = await Promise.all(promises);

            var productInsertObj = await ProductModelObj.batchinsertObjWithIds(newArray, select);
        }
    }

    if (updateOperator.length > 0) {

        for (let i = 0; i < updateOperator.length; i++) {
            const Updatecolumn = {
                operator_id: updateOperator[i].operator_id,
                operator_value: updateOperator[i].value,
                operator_region_codes: '',
                operator_country: updateOperator[i].operatorCountry,
                operator_logo_url: updateOperator[i].logoUrl,
                updated_at: currentTime
            },
                fetchColumns = [
                    "recharge_operator_id",
                    "operator_id",
                ],
                updateWhereCondition = {
                    "operator_id": updateOperator[i].operator_id
                };

            //update operator
            var operatorUpdateObj = await OperatorModelObj.updateObj(Updatecolumn, updateWhereCondition, fetchColumns);

            const whereProductData = {
                "operator_id": operatorUpdateObj[0].recharge_operator_id,
            };

            //get products
            var products = await ProductModelObj.fetchObj(whereProductData);

            if (products.length > 0) {
                const whereDelete = {
                    "operator_id": operatorUpdateObj[0].recharge_operator_id,
                };

                //delete products
                var deleteProducts = await ProductModelObj.deleteObj(whereDelete);

                //insert products
                const responseNewProductData = await dingRechargeLibObj.GetProducts('', '', operatorUpdateObj[0].operator_id, '');

                if (responseNewProductData.Items) {
                    var newProductData = await convertDataObj.dingProductsjson(responseNewProductData);
                }
                newArray = [];
                const promises = newProductData.map(async function (newProduct, index) {
                    let jsonObj = {
                        operator_id: operatorUpdateObj[0].recharge_operator_id,
                        product_amount: newProduct.product,
                        provider_code: newProduct.providerCode,
                        currency: newProduct.currency,
                        sku_code: newProduct.skuCode,
                        localization_key: newProduct.localizationKey,
                        region_code: newProduct.regionCode,
                        created_at: currentTime,
                        updated_at: currentTime
                    };

                    newArray.push(jsonObj);

                });

                const results = await Promise.all(promises);

                var productInsertObj = await ProductModelObj.batchinsertObjWithIds(newArray, select);
            }
        }
    }

    response.code = HttpStatus.OK;
    response.message = localeService.translate( "Operator saved successfully." ) ;
    return response;
};

export const saveOperatorOld = async function (request) {

    let currentTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        country_id = requestData.recharge_country_id, //
        RegionCodes = requestData.region_codes,//
        whereData = {
            "operator_country": requestData.recharge_country_id,
        },
        selectOperator = [
            "recharge_operator_id",
            "operator_id"
        ],
        select = [
            "operator_id",
            "product_amount",
            "provider_code",
            "currency",
            "sku_code",
            "localization_key",
            "region_code"
        ],

        response = { "data": {} };

    var newArray = [];

    var operators = await OperatorModelObj.fetchObj(whereData);

    const responseData = await dingRechargeLibObj.GetProviders(country_id, RegionCodes);

    //new code
    if (responseData.Items) {

        var operatorData = await convertDataObj.dingOperatorjsonToArray(responseData);


        var newOperator = await CommonFnObj.getDifference(operatorData, operators, 'operator_id')
        response.data = newOperator;
    }




    if (operatorData.length > 0) {
        for (let i = 0; i < operatorData.length; i++) {
            newArray = [];
            let jsonObj = {
                operator_id: operatorData[i].operator_id,
                operator_value: operatorData[i].value,
                operator_region_codes: '',
                operator_country: operatorData[i].operatorCountry,
                operator_logo_url: operatorData[i].logoUrl,
                updated_at: currentTime
            };



            if (newOperator.length > 0) {

                jsonObj.created_at = currentTime;
                var operatorInsertObj = await OperatorModelObj.createObj(jsonObj, selectOperator);
                for (let j = 0; j < newOperator.length; j++) {

                    if (newOperator[j].operator_id == operatorData[i].operator_id) {



                        //insert product
                        const responseProductData = await dingRechargeLibObj.GetProducts('', '', operatorInsertObj.operator_id, '');

                        if (responseProductData.Items) {
                            var productData = await convertDataObj.dingProductsjson(responseProductData);
                        }
                        newArray = [];
                        const promises = productData.map(async function (product, index) {
                            let jsonObj = {
                                operator_id: operatorInsertObj.recharge_operator_id,
                                product_amount: product.product,
                                provider_code: product.providerCode,
                                currency: product.currency,
                                sku_code: product.skuCode,
                                localization_key: product.localizationKey,
                                region_code: product.regionCode,
                                created_at: currentTime,
                                updated_at: currentTime
                            };

                            newArray.push(jsonObj);

                        });

                        const results = await Promise.all(promises);

                        var productInsertObj = await ProductModelObj.batchinsertObjWithIds(newArray, select);
                    }
                }
            } else {

            }
        }
    }

    response.code = HttpStatus.OK;
    response.message = "Operator saved successfully.";
    return response;
}

//save recharge product 
export const saveProduct = async function (request) {

    let currentTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        recharge_operator_id = requestData.recharge_operator_id,
        operator_id = requestData.operator_id,
        select = [
            "operator_id",
            "product_amount",
            "provider_code",
            "currency",
            "sku_code",
            "localization_key",
            "region_code"
        ],
        response = { "data": {} };
    response.code = HttpStatus.OK;
    var newArray = [];

    const responseData = await dingRechargeLibObj.GetProducts('', '', operator_id, '');
    
    if (responseData.Items) {
        var productData = await convertDataObj.dingProductsjson(responseData);
    }

    const promises = productData.map(async function (product, index) {
        let jsonObj = {
            operator_id: recharge_operator_id,
            product_amount: product.product,
            provider_code: product.providerCode,
            currency: product.currency,
            sku_code: product.skuCode,
            localization_key: product.localizationKey,
            region_code: product.regionCode,
            default_text : product.DefaultDisplayText,
            created_at: currentTime,
            updated_at: currentTime
        };
        newArray.push(jsonObj);
    });

    const results = await Promise.all(promises);

    var productInsertObj = await ProductModelObj.batchinsertObjWithIds(newArray, select);

    response.code = HttpStatus.OK;
    response.message = "Product saved successfully..";
    return responseData;

};

//Get operator list
export const getOperatorsList = async function (request) {

    const requestData = request.body,
        whereData = {
            "operator_country": requestData.recharge_country_id,
        },
        response = { "data": {} };

    var operatorData = await OperatorModelObj.fetchObj(whereData);

    var newArray = [];
    var nautaOperator= [];
    var cubaOperator = [];

    const promises = operatorData.map(async function (operator, index) {
        let jsonObj = {
            operator_id: operator.recharge_operator_id,
            id: operator.operator_id,
            value: operator.operator_value,
            regionCodes: operator.operator_country,
            operator_logo_url: operator.operator_logo_url,
            created_at: operator.created_at,
            productData : await ProductModelObj.fetchObj({"provider_code":operator.operator_id})
        };
        newArray.push(jsonObj);
    });
    
    // var nautaProduct = await ProductModelObj.fetchObj({"operator_id":4});
    const results = await Promise.all(promises);
    cubaOperator = newArray.filter(el => el.id !== "NUCU");
    nautaOperator = newArray.filter(el => el.id == "NUCU");
   
    response.code = HttpStatus.OK;
    response.rechargeMethod = 2;
    response.data = cubaOperator;
    response.nauta = nautaOperator;
    
    return response;

}

//Get Product list
export const getProductsList = async function (request) {
    const requestData = request.body,
        whereData = {
            "provider_code": requestData.recharge_operator_id,
        },
        response = { "data": {} };

    var productData = await ProductModelObj.fetchObj(whereData);
   
    var newArray = [];

    const promises = productData.map(async function (product, index) {
        let jsonObj = {
            rechargeProductId: product.recharge_product_id,
            operatorId: product.operator_id,
            product: product.product_amount,
            providerCode: product.provider_code,
            currency: product.currency,
            skuCode: product.sku_code,
            localizationKey: product.localization_key,
            regionCode: product.region_code,
            created_at: product.created_at,
            default_text : product.default_text
        };
        newArray.push(jsonObj);
    });

    const results = await Promise.all(promises);
    response.code = HttpStatus.OK;
    response.rechargeMethod = 2;
    response.data = newArray;
   
    return response;

}
