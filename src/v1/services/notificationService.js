import Boom from "boom";
import bcrypt from "bcrypt";
import HttpStatus from "http-status-codes";
import NotificationModel from "~/v2/models/NotificationModel";

import { LocaleService } from "~/utils/localeService";
import logger from "~/utils/logger";
import i18n from "~/config/i18n.config";
import commonConstants from "~/constants/comman";
import envConstants from "~/constants/envConstants";
import responseHelper from "~/utils/responseHelper";
import { Security } from "~/libraries/Security";
import statusConstants from "~/constants/statusConstants";
import databaseConstants from "~/constants/databaseConstants";
import dataConstants from "~/constants/dataConstants";
import { DateTime } from "~/libraries/DateTime";
import CommonFunction from "~/utils/CommonFunctions";
import { response } from "express";
import DateTimeUtil from "~/utils/DateTimeUtil";
import util from "~/utils/util";

const basePath = envConstants.ASSETS_BASE_URL,
      staticPath = commonConstants.FILE_STATIC_PATH,
      fileUploadFolder = commonConstants.FILE_UPLOAD_FOLDER,
      profileImagePath = `${basePath}/${staticPath}/${fileUploadFolder}/`,
      defaultUserImage = commonConstants.DEFAULT_USER_IMAGE,
      defaultUserImagePath = `${basePath}/${defaultUserImage}`,
      NotificationModelObj = new NotificationModel(),
      localeService = new LocaleService( i18n ),
      datetimeObj = new DateTime();


/**
 * Get all notification.
 *
 * @param  {Object}   request 
 * @returns {Promise}
 */
export const getNotificationList = function( request ) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObj();
    const requestData = request.body,
        response = { "data": { } };

    return NotificationModelObj.getNotificationList( requestData, profileImagePath, defaultUserImagePath ).then( ( resdata ) => {
        return datetimeObj.changeDatabaseTimestampFormat( resdata, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( ( notificationData ) => {
            return NotificationModelObj.getNotificationCount( requestData ).then( ( count ) => {

                response.code = HttpStatus.OK;
                response.data.notificatiList = notificationData;
                response.recordsFiltered = count[0].totalnotification;
                response.recordsTotal = count[0].totalnotification;
                response.currentTime = currentDateTime;
                return response;
                
            } ).catch( ( error ) => {
                logger.error( error );
                throw Boom.badImplementation( localeService.translate( error ) );
            } );
        } ).catch( ( error ) => {
            logger.error( error );
            throw Boom.notImplemented( localeService.translate( error ) );
        } );
    } ).catch( ( err ) => {
        throw Boom.badImplementation( localeService.translate( err ) );
    } ); 
       
    
};