import Boom from "boom";
import bcrypt from "bcrypt";
import HttpStatus from "http-status-codes";
import UserModel from "~/v1/models/UserModel";
import RechargeModel from "~/v1/models/RechargeModel";
import RechargePaymentModel from "~/v1/models/RechargePaymentModel";
import WalletModel from "~/v1/models/WalletModel";
import CardModel from "~/v1/models/CardModel";
import RechargeSettingModel from "~/v1/models/RechargeSettingModel";
import DeveloperModel from "~/v1/models/DeveloperModel";
import DeveloperIPModel from "~/v1/models/DeveloperIPModel";

import SettingModel from "~/v1/models/SettingModel";
import CommissionOperatorModel from "~/v1/models/CommissionOperatorModel";
import UserCommissionModel from "~/v1/models/UserCommissionModel";
import DeveloperBalanceCommissionModel from "~/v1/models/DeveloperBalanceCommissionModel";
import DeveloperTransactionModel from "~/v1/models/DeveloperTransactionModel";
import CountryModel from "~/v1/models/CountryModel";
import ContactVerifyModel from "~/v1/models/ContactVerifyModel";

import { LocaleService } from "~/utils/localeService";
import logger from "~/utils/logger";
import FileUpload from "~/libraries/FileUpload";
import i18n from "~/config/i18n.config";
import { StripeLib } from "~/libraries/StripeLib";
import commonConstants from "~/constants/comman";
import envConstants from "~/constants/envConstants";
import responseHelper from "~/utils/responseHelper";
import { Security } from "~/libraries/Security";
import statusConstants from "~/constants/statusConstants";
import databaseConstants from "~/constants/databaseConstants";
import dataConstants from "~/constants/dataConstants";
import { DateTime } from "~/libraries/DateTime";
import CommonFunction from "~/utils/CommonFunctions";
import Page from "twilio/lib/base/Page";
import { response } from "express";
import password from "~/utils/password";
import DateTimeUtil from "~/utils/DateTimeUtil";
import Email from "~/libraries/Email";
import SMSLib from "~/libraries/SMSLib";
import util from "~/utils/util";

const basePath = envConstants.ASSETS_BASE_URL,
    staticPath = commonConstants.FILE_STATIC_PATH,
    fileUploadFolder = commonConstants.FILE_UPLOAD_FOLDER,
    verifyFileUploadFolder = commonConstants.VERIFY_FILE_UPLOAD_FOLDER,
    documentUploadFolder = commonConstants.DOCUMENT_UPLOAD_FOLDER,
    profileImagePath = `${basePath}/${staticPath}/${fileUploadFolder}/`,
    verifyImagePath = `${basePath}/${staticPath}/${verifyFileUploadFolder}/`,
    defaultUserImage = commonConstants.DEFAULT_USER_IMAGE,
    defaultUserImagePath = `${basePath}/${defaultUserImage}`,
    UserModelObj = new UserModel(),
    RechargeModelObj = new RechargeModel(),
    RechargePaymentModelObj = new RechargePaymentModel(),
    WalletModelObj = new WalletModel(),
    CardModelObj = new CardModel(),
    RechargeSettingModelObj = new RechargeSettingModel(),
    DeveloperModelObj = new DeveloperModel(),
    DeveloperIPModelObj = new DeveloperIPModel(),
    SettingModelObj = new SettingModel(),
    CommissionOperatorModelObj = new CommissionOperatorModel(),
    UserCommissionModelObj = new UserCommissionModel(),
    DeveloperBalanceObj = new DeveloperBalanceCommissionModel(),
    DeveloperTransactionObj = new DeveloperTransactionModel(),
    CountryModelObj = new CountryModel(),
    ContactVerifyModelObj = new ContactVerifyModel(),


    SMSLibObj = new SMSLib(),
    StripeLibObj = new StripeLib(),
    localeService = new LocaleService(i18n),
    fileUploadObj = new FileUpload(),
    CommonFnObj = new CommonFunction(),
    emailObj = new Email(),
    datetimeObj = new DateTime();


/**
 * Get a user.
 *
 * @param  {Number|String}  userId
 * @returns {Promise}
 */
export const getUser = function (userId) {
    return DeveloperModelObj.fetchObj({ "developer_id": userId }).then((user) => {
        if (user.length < 1) {
            throw Boom.notFound(localeService.translate("USER_NOT_FOUND"));
        }

        return user;
    });
};
/**
 * Check email for user update.
 *
 * @param  {String}  userEmail
 * @param  {String}  req
 * @returns {Promise}   
 */
export const checkEmail = function (userEmail, req) {
    const selectColumns = ["email"],
        whereData = {
            "email": userEmail,
            "user_type": req.headers['x-user-type']
        };
    return UserModelObj.fetchFirstObj(whereData, selectColumns).then((emailData) => {
        if (emailData) {
            throw Boom.forbidden(localeService.translate("EMAIL_DUPLICATE"));
        }

        return emailData;
    });
};
/**
 * Check email .
 *
 * @param  {String}  userEmail
 * @param  {String}  req
 * @returns {Promise}   
 */
export const checkEmailValid = function (userEmail, req) {
    const selectColumns = ["email"],
        whereData = {
            "email": userEmail
        };
    return DeveloperModelObj.fetchObjWithSingleRecord(whereData, selectColumns).then((emailData) => {
       
        if (!emailData) {
            throw Boom.forbidden(localeService.translate("EMAIL_NOT_EXIST"));
        }

        return emailData;
    });
};


/**
 * Check token .
 *
 * @param  {String}  userEmail
 * @param  {String}  req
 * @returns {Promise}   
 */
export const checkTokenValid = function (token, req) {
    const selectColumns = ["user_id", "api_token"],
        whereData = {
            "api_token": token
        };
    return UserModelObj.fetchObjWithSingleRecord(whereData, selectColumns).then((Data) => {
        if (!Data) {
            throw Boom.forbidden(localeService.translate("TOKEN_NOT_EXIST"));
        }
        req.body.developer_id = Data.user_id;
        return Data;
    });
};
/**
 * Check developer ip .
 *
 * @param  {String}  userEmail
 * @param  {String}  req
 * @returns {Promise}   
 */
export const checkDeveloperIp = async function (token, req) {
    var ip;
    if (req.headers['x-forwarded-for']) {
        ip = req.headers['x-forwarded-for'].split(",")[0];
    } else if (req.connection && req.connection.remoteAddress) {
        ip = req.connection.remoteAddress;
    } else {
        ip = req.ip;
    }
    const selectColumns = ["developer_id", "ip_id"],
        requestData = req.body,
        whereData = {
            "ip_address": ip
        };
    var devData = await UserModelObj.fetchObjWithSingleRecord({ "api_token": token }, ["user_id"]);

    if (devData) {
        whereData.developer_id = devData.user_id;
    }

    return DeveloperIPModelObj.fetchObjWithSingleRecord(whereData, selectColumns).then((Data) => {
        if (!Data) {
            throw Boom.forbidden(localeService.translate("IP_NOT_EXIST"));
        }

        return Data;
    });
};
export const developerList = async function (request) {
    const req = request.body,
        page = req.start,
        limit = databaseConstants.LIMIT,
        response = {};

    let offset = databaseConstants.OFFSET;

    if (Page !== undefined) {
        offset = page;
    }

    const totalCount = await DeveloperModelObj.getDeveloperListCount(req);

    return DeveloperModelObj.getDeveloperList(req, limit, offset).then((checkData) => {
        return responseHelper.encryptobjectAllKeys(checkData, databaseConstants.USER_KEY).then((message) => {
            response.status = "success";
            response.message = "OK";
            response.developerList = message;

            response.recordsTotal = totalCount[0].count;
            response.recordsFiltered = totalCount[0].count;
            return response;
        })
    }).catch(function (error) {

        throw Boom.serverUnavailable(localeService.translate("FAIL"));
    });


}

export const sendIpOtp = function (request) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body;
    const otp = util.generatOtp(commonConstants.INTEGER_RANDOME_NUMBER, commonConstants.RANDOME_NUMBER_LENGTH),
        countryCode = requestData.country_code,
        userType = request.headers['x-user-type'],
        contact = requestData.contact_number,
        id = Security.decryptId(requestData.developer_id),
        userContact = countryCode + contact,
        msg = localeService.translate("TWILIO_VERIFICATION") + otp,
        whereData = {
            "country_code": countryCode,
            "contact_number": contact,
        },
        whereNotData = {
            "user_id": id
        },
        selectData = [
            "country_code",
            "contact_number",
            "user_type",
            "is_profile_done"
        ],
        selectVerify = ["vn_id", "verify_otp", "verify_otp_expiry_at"],
        response = { "data": {} };

    return UserModelObj.fetchObj({ 'user_id': id }).then((userData) => {

        return SMSLibObj.sendSms(userContact, msg).then(async (twi) => {
            const checkData = await ContactVerifyModelObj.fetchObjWithSingleRecord(whereNotData, selectVerify);
            const otpExpiry = DateTimeUtil.getCurrentWithAddMinutes(commonConstants.OTP_EXP_AT);
            const column = {
                "user_id": id,
                "verify_otp": otp,
                "verify_otp_expiry_at": otpExpiry,
                "updated_at": currentDateTime
            };
            const updateWhereCondition = {
                "country_code": countryCode,
                "contact_number": contact,

            };
            if (!checkData) {
                column.country_code = countryCode;
                column.contact_number = contact;
                column.created_at = currentDateTime;
                //column.status = commonConstants.DEFAULT_USER_STATUS;
                return ContactVerifyModelObj.createObj(column, selectVerify).then(() => {
                    response.code = HttpStatus.OK;

                    return response;
                }).catch((err) => {
                    throw Boom.badImplementation(localeService.translate("INTERNAL_SERVER_ERROR"));
                });
            } else {
                const updateWhereCondition = {
                    "user_id": id,

                };

                return ContactVerifyModelObj.updateObj(column, updateWhereCondition, selectVerify).then(() => {
                    response.code = HttpStatus.OK;

                    return response;
                }).catch((err) => {
                    logger.error(err);
                    throw Boom.badImplementation(localeService.translate("INTERNAL_SERVER_ERROR"));
                });
            }

        }).catch((err) => {
            throw Boom.badRequest(localeService.translate("INVALID_TO_NUMBER"));
        });

    });

}

export const verifyDevIpotp = function (request) {

    // Variable Delaration
    const firebaseToken = request.headers['x-firebase-token'],
        deviceType = request.headers['x-device-type'],
        localTimeZone = request.headers['x-local-timezone'],
        deviceId = request.headers['x-device-id'],
        userType = request.headers['x-user-type'],
        remotAddress = request.connection.remoteAddress;

    const requestData = request.body,
        ip1 = requestData.ip_one,
        ip2 = requestData.ip_two,
        id = Security.decryptId(requestData.developer_id),
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        fetchColumns = [
            "vn_id",
            "user_id",
            "verify_otp",
            "verify_otp_expiry_at",
            "country_code",
            "contact_number",
            "created_at"
        ],
        updateColumns = ["is_mobile_verified"],
        updateProperties = {
            "is_mobile_verified": statusConstants.IS_MOBILE.VERIFIED,
            "status": statusConstants.USER_STATUS.ACTIVE
        },
        updateWhereCondition = {
            "country_code": requestData.user_countrycode,
            "contact_number": requestData.user_contact,
        },
        response = { "data": {} };
    var newArray = [];
    return ContactVerifyModelObj.fetchObjWithSingleRecord({ 'user_id': id }, fetchColumns).then(async (user) => {

        const loginOtpExpiry = user.verify_otp_expiry_at,
            loginOtp = user.verify_otp,
            expirytime = datetimeObj.changeFormat(loginOtpExpiry, databaseConstants.TIME_FORMAT);

        // Check login otp expiry.
        if (expirytime < currentTime) {
            throw Boom.badRequest(localeService.translate("EXPIRE_OTP"));
        }

        // Check OTP
        if (loginOtp !== requestData.user_loginotp) {
            throw Boom.badRequest(localeService.translate("INVALID_OTP"));
        }
        newArray.push({
            developer_id: user.user_id,
            ip_address: ip1,
            created_at: currentTime,
            updated_at: currentTime
        });
        if (ip2) {
            newArray.push({
                developer_id: user.user_id,
                ip_address: ip2,
                created_at: currentTime,
                updated_at: currentTime
            });
        }

        var checkIp = await DeveloperIPModelObj.fetchObj({ 'developer_id': user.user_id });
        if (checkIp.length > 0) {
            await DeveloperIPModelObj.deleteObj({ 'developer_id': user.user_id });
        }
        await DeveloperIPModelObj.batchinsertObjWithIds(newArray, ["ip_id"]);
        delete user.verify_otp;
        delete user.verify_otp_expiry_at;
        response.code = HttpStatus.OK;
        response.data.developer = user;
        return response;
    });
};
export const ipList = async function (request) {
    const req = request.body,
        response = {};
    return DeveloperIPModelObj.fetchObj({ 'developer_id': Security.decryptId(req.developer_id) }).then((checkData) => {
        response.status = "success";
        response.code = HttpStatus.OK;
        response.ipList = checkData;
        return response;
    }).catch(function (error) {

        throw Boom.serverUnavailable(localeService.translate("FAIL"));
    });


}
export const resetDeveloperPassword = function (request) {
    // Variable declaration
    const requestData = request.body,
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        currentYear = DateTimeUtil.getCurrentYear(),
        nextYear = DateTimeUtil.getNextYear(),
        randomPasswords = password.generateRandomPassword(),
        userType = request.headers['x-user-type'],
        randomstr = randomPasswords.hashPassword ,
        updatePassword = {
            "password_token": randomstr,
            "updated_at": currentTime
        },
        updateWhere = {
            "email": requestData.email,
            "user_type" : userType
        },
        fetchColumns = [
            "user_id"
        ],
        response = { "data": {} };
    return UserModelObj.fetchObj(updateWhere).then((user) => {
        // Check user email exist or not
      
        if (user.length < 1) {
            const data = { "error_code": "EMAIL_NOT_EXIST" };

            throw Boom.badRequest(localeService.translate("EMAIL_NOT_EXIST"), data);
        }
        return UserModelObj.updateObj(updatePassword, updateWhere, fetchColumns).then(() => {
            const mailDetails = {
                "template": "reset-password",
                "to": requestData.email,
                "subject": "Qvaring - Reset password",
                "dynamicFields": {
                    "title": "Qvaring",
                    "cubaring_icon": `${basePath}/public/email_icon/qvaring_logo.png`,
                    "name": `${user[0].name}`,
                    "randomstr": randomstr,
                    "basepath" : `${basePath}/reset-password?token=${randomstr}`,
                    "password": randomPasswords.password,
                    "nextYear": `${nextYear}`,
                    "year": `${currentYear}`
                }
            };
          
            
            // Sent Reset Password mail
            return emailObj.sendEmail(mailDetails).then(() => {

                // Set response
                response.code = HttpStatus.OK;
                response.message = localeService.translate("RESET_PASSWORD_MAIL");

                return response;
            });
        }).catch((error) => {
            logger.error(error);
            next(error);
        });
    }).catch((error) => {
        logger.error(error);
        next(error);
    });
}
/**
* Reset password
* @param {Object} request 
* @returns {Promise}
*/
export const changePassword = function (request) {
    // Variable declaration
    const requestData = request.body,
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        user_id = requestData.user_id,
        old_password = requestData.old_password,
        new_password = password.cryptPassword(requestData.new_password),
        userType = request.headers['x-user-type'],

        updatePassword = {
            "password": new_password,
            "updated_at": currentTime
        },
        updateWhere = {
            "user_id": user_id
        },
        fetchColumns = [
            "user_id"
        ],
        response = { "data": {} };
    return UserModelObj.fetchObj(updateWhere).then((user) => {
        const isMatch = bcrypt.compareSync(old_password, user[0].password);
        // Check user  exist or not
        if (!isMatch) {
            throw Boom.badRequest(localeService.translate("NOT_MATCH_OLD_PASS"));
        }
        return UserModelObj.updateObj(updatePassword, updateWhere, fetchColumns).then(() => {
            // Set response
            response.code = HttpStatus.OK;
            response.message = localeService.translate("SUCCESS_UPDATE_PASS");

            return response;

        })
    });

}

export const operatorCommisssionList = async function (request) {

    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        where = {
            userType: request.headers['x-user-type'],
            view_user_id: Security.decryptId(requestData.view_user_id)
        },
        response = { "data": {} };
    response.code = HttpStatus.OK;


    var id = Security.decryptId(requestData.view_user_id);
    const countryId = await UserModelObj.fetchFirstObj({ 'user_id': id }, ["user_country_id"]);
    where.country_id = countryId.user_country_id;


    const whereUser = {
        "commission_user_id": id,
        "commission_user_type": request.headers['x-user-type']
    };

    const checkUsr = await UserCommissionModelObj.fetchObj(whereUser);
    const totalCount = await CommissionOperatorModelObj.getCommissionOperatorCount(where);
    const country = await CountryModelObj.fetchObj();
    return CommissionOperatorModelObj.getCommissionOperator(where).then((operatorList) => {
        response.data.operatorList = operatorList;
        response.data.country = country;
        response.data.selectCountry = where.country_id;
        response.data.userCommission = checkUsr;
        response.recordsTotal = totalCount[0].count;
        response.recordsFiltered = totalCount[0].count;

        return response;
    })
};

export const checkBalance = async function (request) {

    const req = request.body,
        response = {};
    var userId = Security.decryptId(req.developer_id);

    const totalRecharge = await WalletModelObj.getDeveloperRechargeCount(userId);
    return WalletModelObj.fetchObj({ 'wallet_user_id': request.user }).then((checkData) => {

        checkData[0].wallet_amount = (checkData[0].wallet_amount).toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]

        response.status = "success";
        response.code = HttpStatus.OK;
        response.devBalance = checkData;
        response.rechargeCount = totalRecharge[0].count;
        return response;
    }).catch(function (error) {

        throw Boom.serverUnavailable(localeService.translate("FAIL"));
    });


}

/**
 * Update developer wallet amount by Admin created by @hemant_solanki
 * 
 * @param {String} id
 */
export const updateDeveloperWalletAmount = function (req) {

    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();

    const requestData = req.body,

        id = Security.decryptId(requestData.user_id),
        fetchColumns = [
            "wallet_id",
            "wallet_user_id",
            "wallet_amount"
        ],
        updateWhere = { "wallet_user_id": id },
        column = {
            "dt_type": 1,
            "amount": parseFloat(requestData.amount),
            "currency": 'USD',
            "reference_id": null,
            "reference_type": 2,
            "payment_status": 2,
            "created_at": currentDateTime,
            "updated_at": currentDateTime
        },
        selectData = [
            "dt_id",
            "amount",
        ],
        response = { "data": {} };

    return WalletModelObj.fetchFirstObj(updateWhere)
        .then((walletDetail) => {

            let totalAmount = parseFloat(walletDetail.wallet_amount) + parseFloat(requestData.amount);
            const Updatecolumn = { "wallet_amount": totalAmount };

            return WalletModelObj.updateObj(Updatecolumn, updateWhere, fetchColumns)
                .then((updateDeveloperWallet) => {

                    column.developer_id = updateDeveloperWallet[0].wallet_user_id;
                    return DeveloperTransactionObj.createObj(column, selectData)
                        .then(() => {
                            response.code = HttpStatus.OK;
                            response.data.developer = updateDeveloperWallet[0];
                            response.message = localeService.translate("UPDATE_WALLET_AMOUNT");

                            return response;
                        }).catch((error) => {
                            throw Boom.badImplementation(localeService.translate(error));
                        });
                }).catch((updateErr) => {
                    throw Boom.badImplementation(localeService.translate(updateErr));
                });
        }).catch((updateError) => {
            throw Boom.badImplementation(localeService.translate(updateError));
        });
}

/**
 * Deduct developer wallet amount by Admin created by @hemant_solanki
 * 
 * @param {String} id
 */
export const deductDeveloperWalletAmount = function (req) {

    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();

    const requestData = req.body,
        id = Security.decryptId(requestData.user_id),
        amount = parseFloat(req.body.amount),
        whereColumn = {
            'wallet_user_id': id
        },
        fetchColumns = [
            "wallet_id",
            "wallet_user_id",
            "wallet_amount"
        ],
        updateWhere = { "wallet_user_id": id },
        column = {
            "dt_type": 2,
            "amount": parseFloat(requestData.amount),
            "currency": 'USD',
            "reference_id": null,
            "reference_type": 2,
            "payment_status": 2,
            "created_at": currentDateTime,
            "updated_at": currentDateTime
        },
        selectData = [
            "dt_id",
            "amount",
        ],
        response = { "data": {} };

    return WalletModelObj.fetchFirstObj(whereColumn)
        .then((walletData) => {

            if (walletData.wallet_amount >= amount) {

                let totalAmount = parseFloat(walletData.wallet_amount) - parseFloat(requestData.amount),
                    Updatecolumn = {
                        "wallet_amount": totalAmount,
                        "updated_at": currentDateTime
                    };
                return WalletModelObj.updateObj(Updatecolumn, updateWhere, fetchColumns)
                    .then((updateDeveloperWallet) => {
                        column.developer_id = updateDeveloperWallet[0].wallet_user_id;

                        return DeveloperTransactionObj.createObj(column, selectData)
                            .then(() => {
                                response.code = HttpStatus.OK;
                                response.data.user = updateDeveloperWallet[0];
                                response.message = localeService.translate("UPDATE_WALLET_AMOUNT");

                                return response;
                            }).catch((error) => {
                                throw Boom.badImplementation(localeService.translate(error));
                            });
                    }).catch((updateErr) => {
                        throw Boom.badImplementation(localeService.translate(updateErr));
                    });
            } else {
                response.error = "Insufficient wallet amount";
                return response;
            }
        }).catch((updateErr) => {
            throw Boom.badImplementation(localeService.translate(updateErr));
        });
}

export const countryWiseOperator = async function (request) {
    const requestData = request.body,
        where = {
            userType: request.headers['x-user-type'],
            view_user_id: Security.decryptId(requestData.view_user_id),
            country_id: requestData.country_id
        },
        response = { "data": {} };
    response.code = HttpStatus.OK;
    const totalCount = await CommissionOperatorModelObj.getCommissionOperatorCount(where);
    return CommissionOperatorModelObj.getCommissionOperator(where).then((operatorList) => {
        response.data.operatorList = operatorList;
        response.recordsTotal = totalCount[0].count;
        response.recordsFiltered = totalCount[0].count;

        return response;
    })
};

