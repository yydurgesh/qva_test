import RequestModel from "~/v2/models/RequestModel";
const RequestModelObj = new RequestModel();

/**
 * Create new user.
 *
 * @param  {Object}  req
 * @param  {Object}  res
 * @returns {Promise}
 */
export const createRequest = function( req, res ) {
    return RequestModelObj.createObj(
        {
            "request_data": req.body,
            "request_response": res.body,
            "request_ip": req.connection.remoteAddress,
            "request_browser_agent": req.get( "User-Agent" ),
            "request_method": req.method,
            "request_headers": req.headers
        },
        [ "request_id" ]
    );
};
