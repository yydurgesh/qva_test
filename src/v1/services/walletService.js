import Boom from "boom";
import HttpStatus from "http-status-codes";
import logger from "~/utils/logger";
import WalletModel from "~/v2/models/WalletModel";
import WalletTransactionModel from "~/v2/models/WalletTransactionModel";
import UserModel from "~/v2/models/UserModel";
import statusConstants from "~/constants/statusConstants";
import databaseConstants from "~/constants/databaseConstants";
import DateTimeUtil from "~/utils/DateTimeUtil";
import { DateTime } from "~/libraries/DateTime";
import { StripeLib } from "~/libraries/StripeLib";
import responseHelper from "~/utils/responseHelper";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
import { Security } from "~/libraries/Security";
import CommonFunction from "~/utils/CommonFunctions";
import CardModel from "~/v2/models/CardModel";
import StripeFeeTaxModel from "~/v2/models/StripeFeeTaxModel";

const WalletModelObj = new WalletModel(),
     WalletTransactionObj = new WalletTransactionModel(),
     UserModelObj            = new UserModel(),
     datetimeObj = new DateTime(),
     StripeLibObj            = new StripeLib(),
     CommonFnObj = new CommonFunction(),
     CardModelObj = new CardModel(),
     StripeFeeTaxModelObj = new StripeFeeTaxModel(),
     localeService = new LocaleService( i18n );

/**
 * Send user message.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const addToWallet = function( request ) {
    
    const requestData = request.body,
        callColumns = [ "call_id" ],
        callData = {
            "call_user_id": requestData.user_id,
            "call_contact_id": request.contactId
        },
        response = { "data": { } };

    callData.call_status = statusConstants.CALL_STATUS.DONE;
    callData.created_at = DateTimeUtil.getCurrentTimeObjForDB();

    return WalletModelObj.createObj( callData, callColumns ).then( ( ) => {
        response.code = HttpStatus.OK;

        return response;
    } ).catch( ( error ) => {
        throw Boom.badImplementation( localeService.translate( error ) );
    } );     
};
/**
 * Send user message.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const walletTransactionHistory = async function( request ) {
    
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
         walletWhere ={
           'wallet_user_id':requestData.user_id 
          },
          response = { "data": { } };
           const wallet =  await WalletModelObj.fetchFirstObj( walletWhere );
           const totalCount = await WalletTransactionObj.getwalletTransactionHistoryCount( requestData );
          response.code = HttpStatus.OK;
    return WalletTransactionObj.getwalletTransactionHistory( requestData ).then( ( history ) => {
      
      response.data.walletList = history;
      response.wallet_amount = wallet.wallet_amount;
      response.recordsTotal = totalCount[0].count;
      response.recordsFiltered = totalCount[0].count;
      return response;
    }) 
};

/**
 * Update user wallet amount by Admin created by @hemant_solanki
 * 
 * @param {String} id
 */
export const updateWalletAmount = function( req ) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    
    const requestData = req.body,
        // token = req.cookies.admin_profile,
        // profileData = JSON.parse(token),
        // adminId = Security.decryptId( profileData.user.user_id),
        adminId = Security.decryptId( requestData.adminId),
        
        id = Security.decryptId( requestData.user_id ), 
        // totalAmount = parseFloat(requestData.pre_amount) + parseFloat(requestData.amount),
        fetchColumns = [
            "wallet_id",
            "wallet_user_id",
            "wallet_amount"
        ],
        // Updatecolumn = { "wallet_amount": totalAmount },
        updateWhere = { "wallet_user_id": id },
        column = {
            "wt_type": 1,
            "wt_reference_type": 4,
            "wt_status": 1,
            "wt_amount": parseFloat(requestData.amount),
            "user_id": adminId,
            "created_at":currentDateTime,
            "updated_at":currentDateTime
        },
        selectData = [ 
            "wt_id", 
            "wt_type",
        ],
        response = { "data": { } };
    
    //check number greater than 0
    if( Number(requestData.amount) < 1 ){
        throw Boom.badRequest( localeService.translate( "ENVALID_INPUT_AMOUNT" ) );
    }

    return WalletModelObj.fetchFirstObj( updateWhere )
        .then( ( walletDetail ) => {
         
            let totalAmount = parseFloat(walletDetail.wallet_amount) + parseFloat(requestData.amount);
            const Updatecolumn = { "wallet_amount": totalAmount };
            
            return WalletModelObj.updateObj( Updatecolumn, updateWhere, fetchColumns )
                .then( ( updateUserWallet ) => {
                   
                    column.wallet_id = updateUserWallet[0].wallet_id;
                    column.wt_pre_amount = walletDetail.wallet_amount;
                    column.wt_balance = updateUserWallet[0].wallet_amount;
                   
                    return WalletTransactionObj.createObj( column, selectData )
                        .then( ( walletTransaction ) => {
                            
                            response.code = HttpStatus.OK;
                            response.data.user = updateUserWallet[0];
                            response.message = localeService.translate( "UPDATE_WALLET_AMOUNT" );
                        
                            return response;
                        } ).catch( ( error ) => {
                           
                            throw Boom.badImplementation( localeService.translate( error ) );
                        } ); 
                }).catch( ( updateErr ) => {
                
                    throw Boom.badImplementation( localeService.translate( updateErr ) );
                } );
            }).catch( ( updateError ) => {
            
                throw Boom.badImplementation( localeService.translate( updateError ) );
            } );
}

/**
 * Deduct user wallet amount by Admin created by @hemant_solanki
 * 
 * @param {String} id
 */
export const deductWalletAmount = function( req ) {
  
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();

    const requestData = req.body,
        // token = req.cookies.admin_profile,
        // profileData = JSON.parse(token),
        // adminId = Security.decryptId( profileData.user.user_id),
        adminId = Security.decryptId( requestData.adminId),
        id = Security.decryptId( requestData.user_id ),
        amount = parseFloat(requestData.amount), 
        whereColumn = {
            'wallet_user_id': id
        },
        fetchColumns = [
            "wallet_id",
            "wallet_user_id",
            "wallet_amount"
        ],
        updateWhere = { "wallet_user_id": id },
        column = {
            "wt_type": 2,
            "wt_reference_type": 6,
            "wt_status": 1,
            "user_id": adminId,
            "created_at":currentDateTime,
            "updated_at":currentDateTime
        },
        selectData = [ 
            "wt_id", 
            "wt_type",
        ],
        response = { "data": { } };
        
        //check number greater than 0
    if( Number(amount) < 1 ){
        throw Boom.badRequest( localeService.translate( "ENVALID_INPUT_AMOUNT" ) );
    }

    return WalletModelObj.fetchFirstObj(  whereColumn )
        .then( ( walletData ) => { 
        
            if( walletData.wallet_amount >= amount ) {
                
                let totalAmount = parseFloat(walletData.wallet_amount) - parseFloat(requestData.amount),
                    Updatecolumn = { "wallet_amount": totalAmount };
               
                return WalletModelObj.updateObj( Updatecolumn, updateWhere, fetchColumns )
                    .then( ( updateUserWallet ) => {
                       
                        column.wallet_id = updateUserWallet[0].wallet_id,
                        column.wt_amount = amount;
                        column.wt_pre_amount =  walletData.wallet_amount;
                        column.wt_balance = updateUserWallet[0].wallet_amount;
                        
                        return WalletTransactionObj.createObj( column, selectData )
                            .then( ( walletTransaction ) => {
                                
                                response.code = HttpStatus.OK;
                                response.data.user = updateUserWallet[0];
                          
                                response.message = localeService.translate( "UPDATE_WALLET_AMOUNT" );
                            
                                return response;
                            } ).catch( ( error ) => {
                             
                                throw Boom.badImplementation( localeService.translate( error ) );
                            } ); 
                    }).catch( ( updateErr ) => {
                      
                        throw Boom.badImplementation( localeService.translate( updateErr ) );
                    } );
            } else {
                response.error = "Insufficient wallet amount";
                return response;
            }
        }).catch( ( updateErr ) => {
         
            throw Boom.badImplementation( localeService.translate( updateErr ) );
        } );
}

/**
 * Update Admin user wallet amount
 * 
 * @param {String} id
 */
export const addWalletAmount = async function( req ) {

    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = req.body,
        user_id = requestData.user_id,
        cardToken = requestData.stripe_card_id,
        price = requestData.amount,
        userType = req.headers['x-user-type'],
        description = "Payment for wallet",
        currency = "usd",
        fetchColumns = [
            "wallet_id",
            "wallet_user_id",
            "wallet_amount"
        ],
        walletWhere ={
           'wallet_user_id':requestData.user_id 
        },
        updateWallet ={
            "updated_at":currentDateTime
        },
        Updatecolumn = { "updated_at": currentDateTime },
        updateWhere = { "wallet_user_id": requestData.user_id },
        createData = {
            "wallet_id": requestData.wallet_id,
            "wt_type": 1,
            //"wt_amount": parseFloat(requestData.amount),
            "wt_reference_type": 3,
            //"wt_status":1,
            "wt_currency":'USD',
            "wt_gateway":1,
            "wt_payment_method":1,
            //"wt_payment_status":'',
            //"wt_response":'',
            "user_id":requestData.user_id,
            "created_at":currentDateTime,
            "updated_at":currentDateTime
        },
      
        response = { "data": { } };

        var cardStatus =  await CardModelObj.fetchFirstObj( {"card_user_id": user_id, "stripe_card_id": cardToken } );

        if( cardStatus.card_verify_status == 2 ) {
            throw Boom.badRequest(localeService.translate('CARD_VERIFY_FIRST'));
        }
         var byUserData =  await WalletModelObj.fetchFirstObj( walletWhere );
          var custId =  await UserModelObj.fetchObjWithSingleRecord( {"user_id":user_id},["stripe_customer_id"] );

          const globalTaxFeeCommission = await StripeFeeTaxModelObj.fetchObj( { "user_type": userType } );

          var totalTax = Number(globalTaxFeeCommission[0].stripe_fee) + Number( globalTaxFeeCommission[0].tax );

          var paymentResponse = await StripeLibObj.createCharge(custId.stripe_customer_id,
                                                            cardToken, 
                                                            Number(price), 
                                                            currency, 
                                                            description, 
                                                            );
    
        
        //  const stripe_fee = await StripeLibObj.stripeFeeCalculator( price, 'USD' );
        //  var Pay =  price- stripe_fee.fee;
        var applyTax = ((price)*(totalTax/100));
        var Pay =  price- applyTax; 
        
         createData.wt_amount = Pay;
         if(paymentResponse.status){
            // var fee = Math.round(((price*0.029)+0.30),2);
             
           // var p = artistPay*100;
        updateWallet.wallet_amount = Number(byUserData.wallet_amount) + Number(Pay); 
        await WalletModelObj.updateObj(updateWallet, walletWhere, ["wallet_id"]);
        createData.wt_status = 1;
        createData.wt_payment_status = 3;
        createData.wt_charge_id=paymentResponse.data.id;
        createData.wt_transaction_id=paymentResponse.data.balance_transaction;
        createData.wt_response = paymentResponse.data;
        createData.wt_pre_amount = byUserData.wallet_amount;
        createData.wt_balance = Number(byUserData.wallet_amount) + Number(Pay);
        createData.stripe_fee = Number(globalTaxFeeCommission[0].stripe_fee);
        createData.tax = Number( globalTaxFeeCommission[0].tax );

        var insert_obj =  await WalletTransactionObj.createObj(createData,["wt_id"]);
            response.code = HttpStatus.OK;
            response.message = localeService.translate( "Money added successfully." );
            return response;           

          }else{
                createData.wt_status = 2;
                createData.wt_payment_status = 2;
                createData.wt_response = paymentResponse.data;
                var insert_obj =  await WalletTransactionObj.createObj(createData,["wt_id"]); 
           throw Boom.badRequest(paymentResponse.data.message);
          }

}

/**
 * Get wallet transaction list.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const walletTransactionList = async function( request ) {
   
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        userId = Security.decryptId( requestData.user_id),
        Page = requestData.start,
        limit = 20,
        walletWhere ={
            'wallet_user_id':userId 
        },
        response = { "data": { } };
        let offset = databaseConstants.OFFSET;
        
        if(Page !== undefined) {
            offset = Page;
        }
       
        const totalRecharge =  await WalletModelObj.getRechargeCount( userId );
        const wallet =  await WalletModelObj.fetchFirstObj( walletWhere );
        const developerTotalRecharge = await UserModelObj.getDeveloperRechargeCount( userId );
        const totalCount = await WalletTransactionObj.getwalletTransactionListCount( requestData );
        
        response.code = HttpStatus.OK;
        return WalletTransactionObj.getwalletTransactionList( requestData, limit, offset ).then( ( history ) => {
            return datetimeObj.changeDatabaseTimestampFormat( history, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( ( transactionList ) => {
                // response.data.walletList = history;
                response.data.walletList = transactionList;
                response.wallet_amount = wallet.wallet_amount;
                response.recordsTotal = totalCount[0].count;
                response.recordsFiltered = totalCount[0].count;
                // response.totalRecharge = totalRecharge[0].count;
                response.totalRecharge = Number(totalRecharge[0].count) + Number(developerTotalRecharge[0].count);

                response.page = offset;

                return response;
            } ).catch( ( error ) => {
                logger.error( error );
                throw Boom.notImplemented( localeService.translate( error ) );
            } );
        }) 
};

/**
 * Get all wallet transaction list by admin.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const getTransactionListByAdmin = async function( request ) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        
        Page = requestData.start,
        limit = 20,
        
        response = { "data": { } };
        let offset = databaseConstants.OFFSET;
        
        if(Page !== undefined) {
            offset = Page;
        }
       
        const totalCount = await WalletTransactionObj.getTransactionListCountByAdmin( requestData );
        response.code = HttpStatus.OK;
        return WalletTransactionObj.getTransactionListByAdmin( requestData, limit, offset ).then( ( history ) => {
            return datetimeObj.changeDatabaseTimestampFormat( history, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( ( transactionList ) => {
               
                response.data.walletList = transactionList;
            
                response.recordsTotal = totalCount[0].count;
                response.recordsFiltered = totalCount[0].count;
                
                response.page = offset;
                return response;
            } ).catch( ( error ) => {
                logger.error( error );
                throw Boom.notImplemented( localeService.translate( error ) );
            } );
        }) 
};


/**
 * Get wallet transaction list.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const getUserTransactionListByAdmin = async function( request ) {
    let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    const requestData = request.body,
        userId = Security.decryptId( requestData.user_id),
        Page = requestData.start,
        limit = 20,
        
        response = { "data": { } };
        let offset = databaseConstants.OFFSET;
        
        if(Page !== undefined) {
            offset = Page;
        }
       
        const totalCount = await WalletTransactionObj.getwalletTransactionListCount( requestData );
       
        response.code = HttpStatus.OK;
        return WalletTransactionObj.getwalletTransactionList( requestData, limit, offset ).then( ( history ) => {
            return datetimeObj.changeDatabaseTimestampFormat( history, "created_at", databaseConstants.TIMESTAMP_FORMAT ).then( ( transactionList ) => {
                // response.data.walletList = history;
                response.data.walletList = transactionList;
                
                response.recordsTotal = totalCount[0].count;
                response.recordsFiltered = totalCount[0].count;
            
                response.page = offset;

                return response;
            } ).catch( ( error ) => {
                logger.error( error );
                throw Boom.notImplemented( localeService.translate( error ) );
            } );
        }) 
};