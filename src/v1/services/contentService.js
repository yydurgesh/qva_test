import Boom from "boom";
import bcrypt from "bcrypt";
import HttpStatus from "http-status-codes";
import ContentModel from "~/v2/models/ContentModel";


import { LocaleService } from "~/utils/localeService";
import logger from "~/utils/logger";
import i18n from "~/config/i18n.config";
import commonConstants from "~/constants/comman";
import envConstants from "~/constants/envConstants";
import responseHelper from "~/utils/responseHelper";
import { Security } from "~/libraries/Security";
import statusConstants from "~/constants/statusConstants";
import databaseConstants from "~/constants/databaseConstants";
import dataConstants from "~/constants/dataConstants";
import { DateTime } from "~/libraries/DateTime";
import CommonFunction from "~/utils/CommonFunctions";
import { response } from "express";
import DateTimeUtil from "~/utils/DateTimeUtil";
import util from "~/utils/util";



const ContentModelObj = new ContentModel(),
datetimeObj = new DateTime(),
localeService = new LocaleService( i18n );



/**
 *  Get content data.
 *
 * @param  {Object}  req 
 * @returns {Promise}
 */
export const getContent = function( req ) {

    var contentName;
    
    switch (req.query.type) {
        case "terms_condition":
            contentName = "Terms & Condition"
            break;
        case "privacy_policy":
            contentName = "Privacy Policy"
            break;
        default:
            contentName = ""
    }
   
    const fetchColumns = [
            "content_id",
            "content_name",
            "content_value",
            "created_at",
            "updated_at"
        ],
        whereCondition = {
            "content_name": contentName
        },
        response = { "data": { } };
       
    return ContentModelObj.fetchObjWithSingleRecord( whereCondition, fetchColumns ).then( ( content ) => {
        
        response.code = HttpStatus.OK;
        response.data = content;
        //responseHelper.encryptAllKeys( response.data, [ databaseConstants.USER_KEY ] );
        
        return response;
    } ).catch( ( error ) => {
        throw Boom.badImplementation( localeService.translate( error ) );
    } );     
};

/**
 *  Save content data.
 *
 * @param  {Object}  req 
 * @returns {Promise}
 */
export const saveContent = function( req ) {

    var contentName;
    
    switch (req.body.contentName) {
        case "Terms & Condition":
            contentName = "Terms & Condition"
            break;
        case "Privacy Policy":
            contentName = "Privacy Policy"
            break;
        default:
            contentName = ""
    }
   
    const requestData = req.body, 
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        fetchColumns = [
            "content_id",
            "content_name",
            "content_value",
            "created_at",
            "updated_at"
        ],
        whereCondition = {
            "content_name": contentName
        },
        updateContent = { 
            "content_value": requestData.contentValue,
            "updated_at": currentTime
        },
        updateWhere = { 
            "content_name": contentName,
        },
        column = {
            "content_name": contentName,
            "content_value": requestData.contentValue,
            "created_at": currentTime,
            "updated_at": currentTime
        },
        response = { "data": { } };

    return ContentModelObj.fetchObjWithSingleRecord( whereCondition, fetchColumns ).then( ( content ) => {
        if ( content != undefined ) {
            
            return ContentModelObj.updateObj( updateContent, updateWhere, fetchColumns ).then( () => {
                response.code = HttpStatus.OK;
                response.message = localeService.translate( "CONTENT_UPDATED" );
                response.contentName = contentName;
                return response;
            } ).catch( ( error ) => {
                logger.error( error );
                throw Boom.badImplementation( localeService.translate( "INTERNAL_SERVER_ERROR" ) );
            } );    
        } else {

            return ContentModelObj.createObj( column, fetchColumns ).then( () => {
                response.code = HttpStatus.OK;
                response.message = localeService.translate( "CONTENT_SAVED" );
                response.contentName = contentName;
                return response;
            } ).catch( ( err ) => {
                logger.error( err );
                throw Boom.badImplementation( localeService.translate( "INTERNAL_SERVER_ERROR" ) );
            } );
        }
        
        //responseHelper.encryptAllKeys( response.data, [ databaseConstants.USER_KEY ] );
        
        
    } ).catch( ( error ) => {
        throw Boom.badImplementation( localeService.translate( error ) );
    } );     
};


/**
 *  Get content data.
 *
 * @param  {Object}  req 
 * @returns {Promise}
 */
export const getFrontContent = function( req, contentName ) {
    const fetchColumns = [
            "content_id",
            "content_name",
            "content_value",
            "created_at",
            "updated_at"
        ],
        whereCondition = {
            "content_name": contentName
        },
        response = { "data": { } };
       
    return ContentModelObj.fetchObjWithSingleRecord( whereCondition, fetchColumns ).then( ( content ) => {
        
        response.code = HttpStatus.OK;
        response.data = content;
        //responseHelper.encryptAllKeys( response.data, [ databaseConstants.USER_KEY ] );
        
        return response;
    } ).catch( ( error ) => {
        throw Boom.badImplementation( localeService.translate( error ) );
    } );     
};