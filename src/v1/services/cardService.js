import Boom from "boom";
import bcrypt from "bcrypt";
import HttpStatus from "http-status-codes";
import UserModel from "~/v2/models/UserModel";
import RechargeModel from "~/v2/models/RechargeModel";
import RechargePaymentModel from "~/v2/models/RechargePaymentModel";
import WalletModel from "~/v2/models/WalletModel";
import CardModel from "~/v2/models/CardModel";
import { LocaleService } from "~/utils/localeService";
import logger from "~/utils/logger";
import FileUpload from "~/libraries/FileUpload";
import i18n from "~/config/i18n.config";
import { StripeLib } from "~/libraries/StripeLib";
import commonConstants from "~/constants/comman";
import envConstants from "~/constants/envConstants";
import responseHelper from "~/utils/responseHelper";
import { Security } from "~/libraries/Security";
import statusConstants from "~/constants/statusConstants";
import databaseConstants from "~/constants/databaseConstants";
import dataConstants from "~/constants/dataConstants";
import { DateTime } from "~/libraries/DateTime";
import CommonFunction from "~/utils/CommonFunctions";
import Page from "twilio/lib/base/Page";
import { response } from "express";
import password from "~/utils/password";
import DateTimeUtil from "~/utils/DateTimeUtil";
import Email from "~/libraries/Email";
import util from "~/utils/util";
const moment = require("moment-timezone");

const basePath = envConstants.ASSETS_BASE_URL,
    staticPath = commonConstants.FILE_STATIC_PATH,
    fileUploadFolder = commonConstants.FILE_UPLOAD_FOLDER,
    documentUploadFolder = commonConstants.DOCUMENT_UPLOAD_FOLDER,
    profileImagePath = `${basePath}/${staticPath}/${fileUploadFolder}/`,
    defaultUserImage = commonConstants.DEFAULT_USER_IMAGE,
    defaultUserImagePath = `${basePath}/${defaultUserImage}`,
    UserModelObj = new UserModel(),
    RechargeModelObj = new RechargeModel(),
    RechargePaymentModelObj = new RechargePaymentModel(),
    WalletModelObj = new WalletModel(),
    CardModelObj = new CardModel(),

    StripeLibObj = new StripeLib(),
    localeService = new LocaleService(i18n),
    fileUploadObj = new FileUpload(),
    CommonFnObj = new CommonFunction(),
    emailObj = new Email(),
    datetimeObj = new DateTime();


/**
 * add to card.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const addCard = async function (request) {
    let createdDate = moment(new Date()).utc().format();
    let expirationDate = moment(createdDate).add(7, 'd');
    const requestData = request.body,
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        user_id = requestData.user_id,
        userType = request.headers['x-user-type'],
        columns = ["card_id"],
        where = {
            "card_user_id": user_id
        },
        createData = {
            "card_user_id": user_id,
            "stripe_card_id": requestData.stripe_card_id,
            "card_holder_name": requestData.card_holder_name,
            "card_last_4_digits": requestData.card_last_4_digits,
            "card_expiry_month": requestData.card_expiry_month,
            "card_expiry_year": requestData.card_expiry_year,
            "card_barnd_type": requestData.card_barnd_type,
            "created_at": currentTime,
            "updated_at": currentTime,
            "card_expire": expirationDate

        },
        fetchColumns = ["card_id", "card_verify_status"],
        response = { "data": {} };

    if (userType == 3 || userType == 1) {

        //code for card verify
        const rmAmount = util.generateRandomSerise(0.5, 1);
        const cusId = await UserModelObj.fetchObjWithSingleRecord({ 'user_id': user_id }, ["stripe_customer_id"]);
        var paymentResponse = await StripeLibObj.cardPayment(
            requestData.stripe_card_id,
            rmAmount,
            'USD',
            'Card verify process',
            cusId.stripe_customer_id
        );

        createData.card_verify_amount = rmAmount;
        createData.card_verify_status = 2;
        //end
    } else {
        createData.card_verify_amount = rmAmount;
        createData.card_verify_status = 2;
    }


    return CardModelObj.createObj(createData, fetchColumns).then((record) => {
        response.code = HttpStatus.OK;
        response.data = record;
        response.message = localeService.translate("Card added successfully.");

        return response;
    })
};

/**
 * Card list.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const cardList = async function (request) {


    if (isNaN(request.body.user_id)) {

        request.body.user_id = Security.decryptId(request.body.user_id);

    }

    const requestData = request.body,
        user_id = requestData.user_id,
        columns = ["card_id", "card_user_id","stripe_card_id", "card_holder_name", "card_last_4_digits", "card_expiry_month", "card_expiry_year", "card_barnd_type", "is_default", "status", "card_verify_status","created_at", "card_expire","card_attempts"],
        where = {
            "card_user_id": user_id
        },
        response = { "data": {} };
    return CardModelObj.fetchObjWithSelectedFields(where,columns,'card_id','desc').then((card) => {

        var today = new Date();

        for (let i = 0; i < card.length; i++) {
            var daysDiff = moment.utc(moment(today, "DD/MM/YYYY HH:mm:ss").diff(moment(card[i].created_at, "DD/MM/YYYY HH:mm:ss"))).format("D")

            if (daysDiff > 7 && card[i].card_verify_status != 1) {
                card[i].card_verify_status = 3;
            }

            delete card[i].card_expiry_month;
            delete card[i].card_expiry_year;
            delete card[i].status;
            delete card[i].card_expire;
            delete card[i].card_attempts;
        }
        // Check user  exist or not
        if (card.length == 0) {
            throw Boom.badRequest(localeService.translate("No record found."));
        }

        response.code = HttpStatus.OK;
        response.data = card;

        return response;

    });

};
/**
 * Set default card.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const setDetaultCard = async function (request) {

    const requestData = request.body,
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        user_id = requestData.user_id,
        stripe_card_id = requestData.stripe_card_id,
        columns = ["card_id", "stripe_card_id", "status"],
        getWwhere = {
            "card_user_id": user_id,
            "is_default": 1
        },
        response = { "data": {} };
    return CardModelObj.fetchObjWithSingleRecord(getWwhere, columns).then(async (card) => {
        // update old card
        if(card){
        await CardModelObj.updateObj({ 'is_default': 2, "updated_at": currentTime }, { 'card_id': card.card_id }, columns);
        }
        // set default card
        const setCard = await CardModelObj.updateObj({ 'is_default': 1, "updated_at": currentTime }, { 'stripe_card_id': stripe_card_id, "card_user_id": user_id,"card_verify_status":1 }, columns);

        delete setCard[0].stripe_card_id;
        delete setCard[0].status;

        response.code = HttpStatus.OK;
        response.data = setCard;
        response.message = localeService.translate("Card set successfully.");

        return response;

    });

};
/**
 * deleteCard.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const deleteCard = async function (request) {

    const requestData = request.body,
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        user_id = requestData.user_id,
        stripe_card_id = requestData.stripe_card_id,
        customer_id = requestData.customer_id,
        columns = ["card_id"],
        where = {
            "card_user_id": user_id,
            "is_default": 2
        },
        deleteWhere = {
            "stripe_card_id": stripe_card_id
        },
        response = { "data": {} };
    // const cardCount = await CardModelObj.getCount( where, columns );
    const cardCount = await CardModelObj.fetchObjWithSingleRecord(where, columns);

    // if(cardCount[0] >1){
    if (cardCount) {
        var cardResponse = await StripeLibObj.deleteCard(customer_id, stripe_card_id);

        if (cardResponse.status) {
            return CardModelObj.deleteObj(deleteWhere)
                .then((status) => {
                    response.code = HttpStatus.OK;
                    response.message = localeService.translate("Card delete successfully.");

                    return response;
                })

        } else {
            throw Boom.badRequest(localeService.translate(cardResponse.data));

        }

    } else {
        throw Boom.badRequest(localeService.translate("Please add more card to delete this card."));
    }

};
/**
 * Set default card.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const createCard = async function (request) {

    const requestData = request.body,
        user_id = requestData.user_id,
        customer_id = requestData.customer_id,
        token = requestData.token,

        response = { "data": {} };
    var cardResponse = await StripeLibObj.createCard(customer_id, token);
    if (cardResponse.status) {
        response.code = HttpStatus.OK;
        return response;
    } else {
        throw Boom.badRequest(localeService.translate(cardResponse.data));
    }

};

/**
 * Verify card.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const verifyCard = async function (request) {

    const requestData = request.body,
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        cardId = requestData.card_id,
        verifyStatus = requestData.verify_status,
        amount = requestData.amount,
        userId = requestData.user_id,
        Where = {
            "card_id": cardId,
            "card_verify_status": verifyStatus
        },
        WhereCard = {
            "card_id": cardId,
        },
        Update = {
            "card_verify_status": 1,
            "updated_at": currentTime
        },
        Columns = [
            "card_id",
            "card_user_id",
            "stripe_card_id",
            "card_holder_name",
            "card_last_4_digits",
            "card_expiry_month",
            "card_expiry_year",
            "card_barnd_type",
            "is_default",
            "created_at",
            "card_verify_amount",
            "card_verify_status",
            "status"
        ],
        whereData = {
            "card_user_id": userId,
            "is_default": 1
        },
        columnsData = ["card_id"],
        response = { "data": {} };

    //If no card is default set default card start
    const cardCount = await CardModelObj.getCount(whereData, columnsData);

    if (cardCount[0] == 0) {
        Update.is_default = 1;
    }
    //end

    return CardModelObj.fetchObj(WhereCard).then((cardData) => {

        if (cardData[0].card_attempts >= 3) {
            throw Boom.badRequest(localeService.translate("YOUR_CARD_ATTEPMTS"));
        }

        if (Number(cardData[0].card_verify_amount) == Number(amount)) {

            Update.card_attempts = Number(cardData[0].card_attempts) + 1;

            return CardModelObj.updateObj(Update, WhereCard, Columns).then((cardDetail) => {
                
                delete cardDetail[0].card_id;
                delete cardDetail[0].stripe_card_id;
                delete cardDetail[0].card_holder_name;
                delete cardDetail[0].card_last_4_digits;
                delete cardDetail[0].card_expiry_month;
                delete cardDetail[0].card_expiry_year;
                delete cardDetail[0].card_barnd_type;
                delete cardDetail[0].is_default;
                delete cardDetail[0].card_verify_amount;
                delete cardDetail[0].card_verify_status;
                delete cardDetail[0].status;

                response.code = HttpStatus.OK;
                response.data = cardDetail;
                response.message = "Your card verified successfully";
                
                return response;
            });
        } else {
            let cardAttmpts = Number(cardData[0].card_attempts) + 1;
            const updateAttempts = {
                "card_attempts": cardAttmpts
            }

            return CardModelObj.updateObj(updateAttempts, WhereCard, Columns).then((cardData) => {
                throw Boom.badRequest(localeService.translate("Your card is not verify, Please enter correct amount."));
            });
        }
    });
};

/**
 * Verify card.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const verifyUserCardByAdmin = async function (request) {

    const requestData = request.body,
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        cardId = requestData.card_id,
        verifyStatus = requestData.verify_status,

        WhereCard = {
            "card_id": cardId,
        },
        Update = {
            "card_verify_status": verifyStatus,
            "updated_at": currentTime
        },
        Columns = [
            "card_id",
            "card_user_id",
            "stripe_card_id",
            "card_holder_name",
            "card_last_4_digits",
            "card_expiry_month",
            "card_expiry_year",
            "card_barnd_type",
            "is_default",
            "created_at",
            "card_verify_amount",
            "card_verify_status",
            "status"
        ],

        response = { "data": {} };

    return CardModelObj.updateObj(Update, WhereCard, Columns).then((cardDetail) => {
        response.code = HttpStatus.OK;
        response.message = "Card verified successfully";
       
        return response;
    });

};

/**
 * Get card list.
 *
 * @param  {Object}     request 
 * @returns {Promise}
 */
export const getCardListByadmin = async function (request) {
    
    const requestData = request.body,
        user_id = Security.decryptId(requestData.user_id),//requestData.user_id,
        page = requestData.start,
        countColumn = ["card_id"],
        columns = [
            "card_id", 
            "card_user_id", 
            "card_holder_name", 
            "card_last_4_digits",  
            "card_barnd_type", 
            "created_at", 
            "card_expire", 
            "card_verify_status"
        ],
        where = {
            "card_user_id": user_id
        },
        limit = databaseConstants.LIMIT,
        response = { "data": {} };

    let offset = databaseConstants.OFFSET;
    if (Page !== undefined) {
        offset = page;
    }

    return CardModelObj.getUserCardList(where, columns, limit, offset, requestData).then((card) => {

        var today = new Date();

        for (let i = 0; i < card.length; i++) {
            var daysDiff = moment.utc(moment(today, "DD/MM/YYYY HH:mm:ss").diff(moment(card[i].created_at, "DD/MM/YYYY HH:mm:ss"))).format("D")

            if (daysDiff > 7 && card[i].card_verify_status != 1) {
                card[i].card_verify_status = 3;
            }
        }

        response.code = HttpStatus.OK;
        response.data = card;
       
        return response;

    });

};