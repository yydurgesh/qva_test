import dataConstants from "~/constants/dataConstants";
import Boom from "boom";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
import ContactModel from "~/v2/models/ContactModel";
import DateTimeUtil from "~/utils/DateTimeUtil";

const ContactModelObj = new ContactModel(),
    localeService = new LocaleService( i18n );

/**
 * Check Reciever contact number.
 *
 * @param  {object}  requestData
 * @returns {Promise}
 */
export const checkRecieverContact = function( requestData ) {
    const recieverContact = requestData.to_number,
        contactArray = recieverContact.split( dataConstants.MOBILE_COUNTRYCODE_SAPERATOR ),
        countryCode = contactArray[ 0 ],
        mobileNumber = contactArray[ 1 ];

    if( contactArray.length !== 2 || countryCode.length < 2 || countryCode.includes( "+" ) === false || !mobileNumber || isNaN( mobileNumber ) ) {
        throw Boom.badRequest( localeService.translate( "INVALID_CONTACT" ) );
    }
    const contactColumns = [ "contact_id" ],
        whereOrInsert = {
            "contact_user_id": requestData.user_id,
            "contact_country_code": countryCode,
            "contact_number": mobileNumber
        };

    return ContactModelObj.fetchObjWithSingleRecord( whereOrInsert, contactColumns ).then( ( contactData ) => {
        if ( !contactData ) {
            whereOrInsert.created_at = DateTimeUtil.getCurrentTimeObjForDB();

            return ContactModelObj.createObj( whereOrInsert, contactColumns ).then( ( contact ) => {
                return contact.contact_id;
            } ).catch( ( error ) => {
                throw Boom.badImplementation( localeService.translate( error ) );
            } );
        }

        return contactData.contact_id;
    } ).catch( ( error ) => {
        throw Boom.badImplementation( localeService.translate( error ) );
    } );
};
