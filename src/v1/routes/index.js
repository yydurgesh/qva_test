import { Router } from "express";
//import { authRoutes } from "./api/authRoutes";
//import { rechargeRoutes } from "./api/rechargeRoutes";
//import { callRoutes } from "./api/callRoutes";
//import { msgRoutes } from "./api/msgRoutes";
//import { notificationRoutes } from "./api/notificationRoutes";
//import { walletRoutes,transferRoutes,userRoutes,verifyRoutes,cardRoutes } from "./api";

import {dingCronRoutes } from "./api";
import {authRoutes, rechargeRoutes, userRoutes} from "./web";

const webRoutes = new Router();

 const apiRoutes = new Router();
// console.log("here")
// apiRoutesv2.use( "/", authRoutes );
// apiRoutesv2.use( "/", callRoutes );
// apiRoutesv2.use( "/", msgRoutes );
// apiRoutesv2.use( "/", adminApiRoutes );
// apiRoutesv2.use( "/", adminCallRoutes );
// apiRoutesv2.use( "/", adminMsgRoutes );
webRoutes.use( "/", rechargeRoutes );
webRoutes.use( "/", userRoutes )
apiRoutes.use( "/", dingCronRoutes );
// apiRoutesv2.use( "/", walletRoutes );
// apiRoutesv2.use( "/", transferRoutes );
// apiRoutesv2.use( "/", userRoutes );
// apiRoutesv2.use( "/", verifyRoutes );
// apiRoutesv2.use( "/", cardRoutes );

// apiRoutesv2.use( "/", notificationRoutes );



export {  authRoutes, webRoutes, apiRoutes };
