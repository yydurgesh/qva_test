import { Router } from "express";
import { headerValidator,
        checkTokenValid,
        regionsValidator,
        providerValidator,
        productsValidator,
        transferValidator,
        porviderInfoValidator,
        checkDeveloperIp} from "~/validators/dingValidator";
import dingController from "~/v2/controllers/api/dingController";
import passportLb from "~/config/passport";
import { Security } from "~/libraries/Security";
const rateLimit = require("express-rate-limit");

// const createRechargeRequestLimiter = rateLimit({
//     windowMs: 1 * 60 * 1000, // 1 minute window
//     max: 1, // start blocking after 5 requests
//     message:
//       "Too many requests created from this IP, please try again after an minute."
// });

/**
 * Contains all API routes for the application.
 */
const dingDeveloperRoutes = new Router();

dingDeveloperRoutes.post( "/developer/developer-list", 
    passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    dingController.developerList );

/*code for other rechagrge provider */
dingDeveloperRoutes.post( "/developer/get-error-code-descriptions",
    headerValidator,
    checkTokenValid,
    checkDeveloperIp,
    dingController.GetErrorCodeDescriptions );

dingDeveloperRoutes.post( "/developer/get-countries", 
    headerValidator,
    checkTokenValid,
    checkDeveloperIp,
    dingController.GetCountries );
dingDeveloperRoutes.post( "/developer/get-regions", 
    headerValidator,
    regionsValidator,
    checkDeveloperIp,
    checkTokenValid,
    dingController.GetRegions );

dingDeveloperRoutes.post( "/developer/get-providers", 
    headerValidator,
    checkDeveloperIp,
    providerValidator,
    checkTokenValid,
    dingController.GetProviders );

dingDeveloperRoutes.post( "/developer/get-products", 
    headerValidator,
    checkDeveloperIp,
    productsValidator,
    checkTokenValid,
    dingController.getproducts );

dingDeveloperRoutes.post( "/developer/make-recharge", 
    // createRechargeRequestLimiter,
    // headerValidator,
    checkDeveloperIp,
    transferValidator,
    checkTokenValid,
    dingController.SendTransfer );

dingDeveloperRoutes.post( "/developer/get-provider-info", 
    headerValidator,
    checkDeveloperIp,
    porviderInfoValidator,
    checkTokenValid,
    dingController.getProfiderInfo );

    dingDeveloperRoutes.post( "/developer/addPromotion", 
    dingController.promotion );


export { dingDeveloperRoutes };
