import { Router } from "express";
import userController from "~/v2/controllers/api/userController";
import { checkEmail,
  userValidator,
  profileImageValidator,
  checkEmailValid,
  userProfileValidator,
  changePassworkValidator,
  customerValidator,
  changeNumberValidator,
  verifyOtpValidator,
  stripeFeeValidator,
  operatorComValidator
   } from "~/validators/userValidator";
import passportLb from "~/config/passport";
import { Security } from "~/libraries/Security";
import { setLanguage } from "~/middlewares/setLanguage";


/**
 * Contains all API routes for the application.
 */
const userRoutes = new Router();

userRoutes.post( "/user/get-my-profile", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
      Security.checkAuthUserId, userController.getMyProfile);

userRoutes.post( "/user/update-profile", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    userProfileValidator,
    checkEmail, 
    profileImageValidator, 
    userController.updateProfile );

userRoutes.post( "/user/change-password", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    changePassworkValidator,
    userController.changePassword );

userRoutes.post( "/user/create-customer", setLanguage, 
passportLb.authenticate( "jwt", { "session": false } ),
    customerValidator,
    Security.checkAuthUserId,
    userController.createCustomer );

userRoutes.post( "/user/change-number", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    changeNumberValidator,
    Security.checkAuthUserId,
    userController.changeNumber );
userRoutes.post( "/user/verify-number", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    verifyOtpValidator,
    Security.checkAuthUserId,
    userController.verifyNumber )

userRoutes.post( "/user/stripe-fee", setLanguage,
     passportLb.authenticate( "jwt", { "session": false } ),
     stripeFeeValidator,
     Security.checkAuthUserId,
    userController.stripeFee )
userRoutes.post( "/user/get-commission-operator", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    userController.getCommissionOperator );
userRoutes.post( "/user/get-country-wise-operator", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    userController.getCountryWiseOperator );
userRoutes.post( "/user/update-operator-commission", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    operatorComValidator,
    userController.updateCommissionOperator );
userRoutes.post( "/commission/get-user-commission-setting", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
           Security.checkAuthUserId, 
           userController.userCommissionSetting );

userRoutes.post( "/commission/update-user-commission", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
           Security.checkAuthUserId, 
           userController.updateUserCommission );
userRoutes.post( "/user/get-developerIp", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
           Security.checkAuthUserId, 
           userController.getDeveloperIp );


//authRoutes.post( "/user/user-list", passportLb.authenticate( "jwt", { "session": false } ),
//      Security.checkAuthUserId,
//    userController.userList );

userRoutes.post( "/user/sendNotification",  setLanguage,
           userController.sendNotification );

userRoutes.post( "/user/stripe-fee-tax", setLanguage, passportLb.authenticate( "jwt", { "session": false } ), Security.checkAuthUserId, userController.getStripeFeeTax )
           
export { userRoutes };
