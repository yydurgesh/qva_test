import { Router } from "express";
import { misdnInfoValidator,
		 getOperatorsValidator,
         getProductsValidator,
         webMisdnInfoValidator,
         getWebOperatorsValidator,
         rechargeHistValidator,
         rechargeListValidator,
         getWebProductsValidator,
         rechargeDetailValidator,
         rechargePayValidator,
         rechargeViewValidator,
         transferValidator,
         rechargeHistoryValidator,
         userRechargeDeatilValidator,
         userValidator } from "~/validators/rechargeValidator";

import rechargeController from "~/v2/controllers/api/rechargeController";
import passportLb from "~/config/passport";
import { Security } from "~/libraries/Security";
import userIsActive from "~/middlewares/userIsActive";
import { setLanguage } from "~/middlewares/setLanguage";


/**
 * Contains all API routes for the application.
 */
const rechargeRoutes = new Router();

rechargeRoutes.post( "/get-msisdn-info", setLanguage,
	passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    misdnInfoValidator, 
    rechargeController.getMsisdnInfo );


rechargeRoutes.post( "/get-operators", setLanguage,
	passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    getOperatorsValidator, 
    rechargeController.getOperators );

rechargeRoutes.post( "/get-products", setLanguage,
	passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    getProductsValidator, 
    rechargeController.getProducts );

rechargeRoutes.post( "/make-recharge",  setLanguage,
	passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    userIsActive.isActiveUser,
    rechargeController.makeRecharge );

rechargeRoutes.post( "/make-recharge-v2", setLanguage,
    passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    userIsActive.isActiveUser,
    rechargeController.makeRechargeV2 );

/**
 * Makerecharge api created by @hemant_solanki
 */
rechargeRoutes.post( "/make-recharge-v3", setLanguage, rechargeController.makeRechargev3 );

/**
 * Makerecharge api created by @hemant_solanki
 */
rechargeRoutes.post( "/make-recharge-bulk", setLanguage, rechargeController.makeRechargeBulk );

rechargeRoutes.post( "/web-get-msisdn-info", setLanguage, webMisdnInfoValidator, rechargeController.getMsisdnInfo );

rechargeRoutes.post( "/web-get-operators", setLanguage, getWebOperatorsValidator, rechargeController.getOperators );

rechargeRoutes.post( "/web-get-products", setLanguage, getWebProductsValidator, rechargeController.getProducts );

/*code for other rechagrge provider */
rechargeRoutes.post( "/recharge/get-error-code-descriptions", setLanguage,
    passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    rechargeController.GetErrorCodeDescriptions );

rechargeRoutes.post( "/recharge/get-countries", setLanguage,
    passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    rechargeController.GetCountries );

rechargeRoutes.post( "/recharge/get-providers", setLanguage,
     rechargeController.GetProviders );

rechargeRoutes.post( "/recharge/get-ding-produst", setLanguage,
    passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    rechargeController.getDingProdust );

rechargeRoutes.post( "/recharge/send-transfer", setLanguage,
    passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    rechargeController.SendTransfer );


/*development for multiple porvider*/

rechargeRoutes.post( "/recharge/get-porvider-info", setLanguage,
    webMisdnInfoValidator,   
    rechargeController.getProviderInfo );

rechargeRoutes.post( "/recharge/get-operator-list", setLanguage,
    getWebOperatorsValidator, 
    rechargeController.getOperatorList );

rechargeRoutes.post( "/recharge/get-products-list", setLanguage,
    getWebProductsValidator, 
    rechargeController.getProductList );

rechargeRoutes.post( "/recharge/make-recharge", setLanguage,
    passportLb.authenticate( "jwt", { "session": false } ),
    rechargePayValidator,
    Security.checkAuthUserId,
    userIsActive.isActiveUser,
    rechargeController.rechargeProcess );

rechargeRoutes.post( "/recharge/recharge-history", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    rechargeHistValidator,
    Security.checkAuthUserId,
    rechargeController.rechargeHisttory );

rechargeRoutes.post( "/recharge/recharge-details", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    rechargeDetailValidator,
    Security.checkAuthUserId,
    rechargeController.rechargeDetails );

// rechargeRoutes.post( "/recharge/user-recharge-history", passportLb.authenticate( "jwt", { "session": false } ),
//     rechargeViewValidator,
//     Security.checkAuthUserId,
//     rechargeController.userRechargeHisttory );

rechargeRoutes.post( "/recharge/user-recharge-history", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    // rechargeViewValidator,
    Security.checkAuthUserId,
    rechargeController.userRechargeHisttory );

rechargeRoutes.post( "/recharge/recharge-by-admin", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    // transferValidator,
    Security.checkAuthUserId,
    rechargeController.rechargeByAdmin );
rechargeRoutes.post( "/recharge/recharge-status", setLanguage, passportLb.authenticate( "jwt", { "session": false } ), 
    userValidator,
    Security.checkAuthUserId,
    rechargeController.rechargeMonthStatus );
rechargeRoutes.post( "/recharge/recharge-developer-history", setLanguage, passportLb.authenticate( "jwt", { "session": false } ), 
    userValidator,
    Security.checkAuthUserId,
    rechargeController.rechargeDeveloperHistory );

// rechargeRoutes.post( "/recharge/developer-recharge-detail", passportLb.authenticate( "jwt", { "session": false } ), 
//     Security.checkAuthUserId,
//     rechargeController.rechargeDeveloperDetail );

/**
 * Get recharge history by user id for download created by @hemant_solenki
 */
rechargeRoutes.post( "/recharge/my-recharge-history", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    rechargeHistoryValidator,
    Security.checkAuthUserId,
    rechargeController.getRechargeHistoryByUserId );

rechargeRoutes.post( "/recharge/recharge-list", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    // rechargeListValidator,
    Security.checkAuthUserId,
    rechargeController.rechargeList );
    

rechargeRoutes.post( "/recharge/user-recharge-detail", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    userRechargeDeatilValidator,
    Security.checkAuthUserId,
    rechargeController.userRechargeDetail );

rechargeRoutes.post( "/recharge/developer-recharge-history", setLanguage, passportLb.authenticate( "jwt", { "session": false } ), 
    // userValidator,
    // Security.checkAuthUserId,
    rechargeController.rechargeDeveloperApiHistory );

rechargeRoutes.post( "/recharge/developer-api-recharge-detail", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    // userRechargeDeatilValidator,
    // Security.checkAuthUserId,
    rechargeController.rechargeDeveloperApiDetail );

rechargeRoutes.post( "/recharge/add-operator", setLanguage,
    rechargeController.saveOperator );

rechargeRoutes.post( "/recharge/add-product", setLanguage,
    rechargeController.saveProduct );

export { rechargeRoutes };
