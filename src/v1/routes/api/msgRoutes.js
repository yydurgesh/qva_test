import { Router } from "express";
import messageController from "~/v2/controllers/api/messageController";
import { messageValidator, messageDetailValidator, messageListValidator } from "~/validators/messageValidator";
import { checkRecieverContact, checkMessageLimit } from "~/validators/contactValidator";
import passportLb from "~/config/passport";
import { Security } from "~/libraries/Security";
import { setLanguage } from "~/middlewares/setLanguage";

/**
 * Contains all API routes for the application.
 */
const msgRoutes = new Router();

msgRoutes.post( "/message", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    messageValidator,
    checkRecieverContact,
    checkMessageLimit,
    messageController.sendMessage );
msgRoutes.get( "/messages", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    messageController.getUserMessages );

//NEW by user id
// msgRoutes.get( "/messages", passportLb.authenticate( "jwt", { "session": false } ),
//     messageListValidator,
//     messageController.getUserMessages );

msgRoutes.get( "/message-detail", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    messageDetailValidator,
    messageController.getMessageDetail );

export { msgRoutes };
