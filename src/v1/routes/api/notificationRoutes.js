import { Router } from "express";
import notificationController from "~/v2/controllers/api/notificationController";
import passportLb from "~/config/passport";
import { Security } from "~/libraries/Security";
import { setLanguage } from "~/middlewares/setLanguage";

/**
 * Contains all API routes for the application.
 */
const notificationRoutes = new Router();

notificationRoutes.post( "/notification/get-notification-list", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
      Security.checkAuthUserId, notificationController.notificationList);

export { notificationRoutes };