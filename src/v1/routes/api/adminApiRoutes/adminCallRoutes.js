import { Router } from "express";
import adminCallController from "~/v2/controllers/api/admin/adminCallController";
import { adminCallValidator } from "~/validators/callValidator";
import { checkAdminUserTypeByAuthID } from "~/validators/userValidator";
import passportLb from "~/config/passport";

/**
 * Contains all API routes for the application.
 */
const adminCallRoutes = new Router();

adminCallRoutes.get( "/admin/calls", passportLb.authenticate( "jwt", { "session": false } ), 
    checkAdminUserTypeByAuthID,
    adminCallController.getUserCalls );
adminCallRoutes.get( "/admin/call", passportLb.authenticate( "jwt", { "session": false } ),
    checkAdminUserTypeByAuthID,
    adminCallValidator,
    adminCallController.getCallDetailByAdmin );

export { adminCallRoutes };
