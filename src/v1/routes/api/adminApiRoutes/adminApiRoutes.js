import { Router } from "express";
import adminUserController from "~/v2/controllers/api/admin/adminUserController";
import adminAuthController from "~/v2/controllers/api/admin/adminAuthController";
import { adminLoginValidator } from "~/validators/loginValidator";
import { adminUserValidator, checkAdminUserTypeByAuthID, methodValidator } from "~/validators/userValidator";
import passportLb from "~/config/passport";

/**
 * Contains all API routes for the application.
 */
const adminApiRoutes = new Router();

adminApiRoutes.post( "/admin/auth/login", adminLoginValidator, adminAuthController.adminLogin );
// adminApiRoutes.get( "/admin/users", passportLb.authenticate( "jwt", { "session": false } ),
//     adminUserController.getAllUsers );
adminApiRoutes.post( "/admin/consumer-users-list", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.getAllConsumerUser );
adminApiRoutes.post( "/admin/reseller-users-list", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.getAllResellerUser );
adminApiRoutes.get( "/admin/user", passportLb.authenticate( "jwt", { "session": false } ),
    checkAdminUserTypeByAuthID,
    adminUserValidator,
    adminUserController.getUserDetail );
adminApiRoutes.put( "/admin/updateUserStatus", passportLb.authenticate( "jwt", { "session": false } ),
    checkAdminUserTypeByAuthID,
    adminUserController.updateUserStatus );
adminApiRoutes.get( "/admin/recharges", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.getAllRecharges );

/**
 * Get recharge payment detail created by @hemant_solanki
 */
adminApiRoutes.get( "/admin/rechargePaymentDetail", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.getRechargesPaymentList );

/**
 * Refund amount created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/paymentRefund", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.refundRechargeAmount );

/**
 * Add wallet amount created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/addAmountToWallet", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.updateWalletAmount );

/**
 * Deduct wallet amount created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/deductAmountToWallet", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.deductWalletAmount );

/*this api is use for show recharge method list*/

adminApiRoutes.post( "/admin/get-recharege-method", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.getRechargeMethod );

/*this api is use for set default recharge method */

adminApiRoutes.post( "/admin/set-default-method", passportLb.authenticate( "jwt", { "session": false } ),
    methodValidator,
    adminUserController.setDefaultMethod );

/**
 * Add wallet amount created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/addDeveloperAmountToWallet", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.updateDeveloperWalletAmount );

/**
 * Deduct developer wallet amount created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/deductDeveloperAmountToWallet", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.deductDeveloperWalletAmount );

/**
 * Send apllication link created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/sendAppLink", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.sendAppLink );

/**
 * Send apllication link created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/recharge-status", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.rechargeMonthStatus );

/**
 * Recharge detail created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/recharge-detail", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.rechargeDetail );
/**
 * User Recharge detail created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/user-recharge-detail", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.userRechargeDetail );
 
/**
 * Developer Recharge history created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/developer-recharge-history", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.developerRechargeHistory );

adminApiRoutes.post( "/admin/admin-profile", passportLb.authenticate( "jwt", { "session": false } ),
    // checkAdminUserTypeByAuthID,
    // adminUserValidator,
    adminUserController.getAdminDetail )

/**
 * Developer Recharge history created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/reseller-developer-recharge-history", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.getdeveloperRechargeHistoryByAdmin );

/**
 * Developer Recharge history created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/reseller-developer-recharge-detail", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.getdeveloperRechargeDetailByAdmin );

/**
 * Transaction history created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/transaction-list", passportLb.authenticate( "jwt", { "session": false } ),
adminUserController.getTransactionListByAdmin );

/**
 * User Transaction history created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/user-transaction-list", passportLb.authenticate( "jwt", { "session": false } ),
adminUserController.getUserTransactionListByAdmin );

/**
 * Transfer history created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/transfer-list", passportLb.authenticate( "jwt", { "session": false } ),
adminUserController.getTransferListByAdmin );

/**
 * Get user data created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/user-list", passportLb.authenticate( "jwt", { "session": false } ),
adminUserController.getUserData );

/**
 * get user recharge limit data created by @hemant_solanki
 */
adminApiRoutes.get( "/admin/get-recharge-limit", passportLb.authenticate( "jwt", { "session": false } ),
    adminUserController.getUserRechargesLimit );

/**
 * Save recharge limit created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/recharge-limit", passportLb.authenticate( "jwt", { "session": false } ),
adminUserController.saveRechargeLimit );

/**
 * Delete user recharge limit by @hemant_solanki
 */
adminApiRoutes.post( "/admin/remove-recharge-limit", passportLb.authenticate( "jwt", { "session": false } ),
adminUserController.deleteRechargeLimit );

adminApiRoutes.post( "/admin/auth/admin-verify-otp", adminAuthController.verifyAdminOtp );

adminApiRoutes.post( "/admin/update-admin-profile", passportLb.authenticate( "jwt", { "session": false } ),
    // checkAdminUserTypeByAuthID,
    // adminUserValidator,
    adminUserController.updateAdminProfile );

    
/**
 * Delete user recharge limit by @hemant_solanki
 */
adminApiRoutes.post( "/admin/update-user-profile", passportLb.authenticate( "jwt", { "session": false } ),
adminUserController.updateUserProfileByAdmin );

/**
 * Send user approve otp to admin created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/send-user-approve-otp", passportLb.authenticate( "jwt", { "session": false } ),
adminUserController.sendUserApproveOtp );

/**
 * Send user approve otp to admin created by @hemant_solanki
 */
adminApiRoutes.post( "/admin/verify-user-approve-otp", passportLb.authenticate( "jwt", { "session": false } ),
adminUserController.verifyUserApproveOtp );

export { adminApiRoutes };
