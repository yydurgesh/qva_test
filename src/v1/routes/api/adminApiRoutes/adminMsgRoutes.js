import { Router } from "express";
import adminMessageController from "~/v2/controllers/api/admin/adminMessageController";
import { adminMessageDetailValidator } from "~/validators/messageValidator";
import { checkAdminUserTypeByAuthID } from "~/validators/userValidator";
import passportLb from "~/config/passport";

/**
 * Contains all API routes for the application.
 */
const adminMsgRoutes = new Router();

adminMsgRoutes.get( "/admin/messages", passportLb.authenticate( "jwt", { "session": false } ), 
    checkAdminUserTypeByAuthID,
    adminMessageController.getUserMessages );
adminMsgRoutes.get( "/admin/message", passportLb.authenticate( "jwt", { "session": false } ),
    checkAdminUserTypeByAuthID, 
    adminMessageDetailValidator,
    adminMessageController.getMessageDetailByAdmin );

export { adminMsgRoutes };
