import { Router } from "express";
import verifyController from "~/v2/controllers/api/verifyController";
import { verifyValidator,documentDeleteValidator,getDocumentValidator,profileImageValidator } from "~/validators/verifyValidator";

import passportLb from "~/config/passport";
import { Security } from "~/libraries/Security";
import { setLanguage } from "~/middlewares/setLanguage";


/**
 * Contains all API routes for the application.
 */
const verifyRoutes = new Router();

verifyRoutes.post( "/verify/add-document", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
     verifyValidator,
     Security.checkAuthUserId,
     profileImageValidator,
     verifyController.addDocument );

verifyRoutes.post( "/verify/get-document", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
     getDocumentValidator,
     Security.checkAuthUserId,
     verifyController.getDocument );

verifyRoutes.post( "/verify/delete-document", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
     documentDeleteValidator,
     Security.checkAuthUserId,
     verifyController.deleteDocument );
     
verifyRoutes.post( "/verify/updateDocumentStatus", setLanguage,
     passportLb.authenticate( "jwt", { "session": false } ),
     verifyController.updateDocumentStatus );

export { verifyRoutes };
