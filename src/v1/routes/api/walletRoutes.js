import { Router } from "express";
import walletController from "~/v2/controllers/api/walletController";
import { loginOtpValidator } from "~/validators/loginValidator";
import { otpValidator } from "~/validators/otpValidator";
import { checkEmail, userValidator, profileImageValidator,adminUserValidator } from "~/validators/userValidator";
import { walletHistoryValidator,walletAmountValidator } from "~/validators/walletValidator";

import passportLb from "~/config/passport";
import { Security } from "~/libraries/Security";
import userIsActive from "~/middlewares/userIsActive";
import { setLanguage } from "~/middlewares/setLanguage";


/**
 * Contains all API routes for the application.
 */
const walletRoutes = new Router();

walletRoutes.post( "/wallet/add-to-wallet", setLanguage, adminUserValidator, walletController.addToWallet );
walletRoutes.post( "/wallet/wallet-transaction-history", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    walletHistoryValidator,
    Security.checkAuthUserId,
    walletController.walletTransactionHistory );
walletRoutes.post( "/wallet/add-wallet-amount", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    walletAmountValidator,
    Security.checkAuthUserId,
    userIsActive.isActiveUser,
    walletController.addWalletAmount );

walletRoutes.post( "/wallet/wallet-transaction-list", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    // walletHistoryValidator,
    // Security.checkAuthUserId,
    walletController.walletTransactionList );

export { walletRoutes };