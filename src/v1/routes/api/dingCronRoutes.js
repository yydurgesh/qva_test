import { Router } from "express";

import dingController from "~/v1/controllers/api/dingController";


const dingCronRoutes = new Router();

dingCronRoutes.post( "/cron/addPromotion", 
    dingController.addPromotion );

dingCronRoutes.post( "/cron/addProviders", 
    dingController.addProviders );

dingCronRoutes.post( "/cron/addContinentPromotion", 
    dingController.addContinentPromotion );

dingCronRoutes.post( "/cron/addProducts", 
    dingController.addProducts );

dingCronRoutes.post( "/cron/addCountries", 
    dingController.addCountries );

export { dingCronRoutes };
