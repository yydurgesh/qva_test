import { Router } from "express";
import { checkRecieverContact } from "~/validators/contactValidator";
import callController from "~/v2/controllers/api/callController";
import { callValidator } from "~/validators/callValidator";
import passportLb from "~/config/passport";
import { Security } from "~/libraries/Security";
import { setLanguage } from "~/middlewares/setLanguage";

/**
 * Contains all API routes for the application.
 */
const callRoutes = new Router();

callRoutes.post( "/call", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    Security.checkAuthUserId,
    callValidator,
    checkRecieverContact,
    callController.call );
callRoutes.get( "/calls", setLanguage, passportLb.authenticate( "jwt", { "session": false } ), 
    callController.getUserCalls );
callRoutes.get( "/call-details", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    callController.getCallDetails );

export { callRoutes };
