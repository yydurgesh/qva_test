import { Router } from "express";
import transferController from "~/v2/controllers/api/transferController";
import { transferValidator,transferByAddressValidator,transferHistoryValidator, transferStatusValidator,adminTransferHistoryValidator } from "~/validators/transferValidator";
import passportLb from "~/config/passport";
import { Security } from "~/libraries/Security";
import userIsActive from "~/middlewares/userIsActive";
import { setLanguage } from "~/middlewares/setLanguage";


/**
 * Contains all API routes for the application.
 */
const transferRoutes = new Router();

transferRoutes.post( "/transfer/transfer-amount",setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    transferValidator,
    Security.checkAuthUserId,
    userIsActive.isActiveUser,
    transferController.transferAmount );

transferRoutes.post( "/transfer/transfer-by-address", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    transferByAddressValidator,
    Security.checkAuthUserId,
    userIsActive.isActiveUser,
    transferController.transferByAddress );

transferRoutes.post( "/transfer/transfer-history", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    transferHistoryValidator,
    Security.checkAuthUserId,
    transferController.transferHisttory );

transferRoutes.post( "/transfer/transfer-list", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    transferController.transferList );

transferRoutes.post( "/transfer/updateTranferStatus", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    transferStatusValidator,    
    transferController.updateTransferStatus );

transferRoutes.post( "/transfer/admin-transfer-history", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    adminTransferHistoryValidator,
    Security.checkAuthUserId,
    transferController.adminTransferHisttory );

// transferRoutes.post( "/user/user-list", passportLb.authenticate( "jwt", { "session": false } ),
//     transferHistoryValidator,
//     Security.checkAuthUserId,
//     transferController.transferHisttory );

transferRoutes.post( "/transfer/get-transfer-history", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    // adminTransferHistoryValidator,
    Security.checkAuthUserId,
    transferController.getTransferHisttory );

export { transferRoutes };
