import { Router } from "express";
import cardController from "~/v2/controllers/api/cardController";
import passportLb from "~/config/passport";
import { cardValidator,cardListValidator,defaultCardValidator,deleteCardValidator,createCardListValidator, cardVerifyValidator } from "~/validators/cardValidator";
import { Security } from "~/libraries/Security";
import { setLanguage } from "~/middlewares/setLanguage";


/**
 * Contains all API routes for the application.
 */
const cardRoutes = new Router();


cardRoutes.post( "/card/add-card",setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    cardValidator,
    Security.checkAuthUserId,
    cardController.addCard );

cardRoutes.post( "/card/card-list", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    cardListValidator,
    Security.checkAuthUserId,
    cardController.cardList );

cardRoutes.post( "/card/set-default-card", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    defaultCardValidator,
    Security.checkAuthUserId,
    cardController.setDetaultCard );


cardRoutes.post( "/card/delete-card", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    deleteCardValidator,
    Security.checkAuthUserId,
    cardController.deleteCard );

cardRoutes.post( "/card/create-card", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    createCardListValidator,
    Security.checkAuthUserId,
    cardController.createCard );

/**
 * Card verify created by @hemant_solanki
 */
cardRoutes.post( "/card/card-verify", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    cardVerifyValidator,
    Security.checkAuthUserId,
    cardController.cardVerify );

/**
 * Get user card list by admin
 */
cardRoutes.post( "/card/user-card-list", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    // cardListValidator,
    // Security.checkAuthUserId,
    cardController.getCardListByadmin );

/**
 * Card verify created by @hemant_solanki
 */
cardRoutes.post( "/card/user-card-verify", setLanguage, passportLb.authenticate( "jwt", { "session": false } ),
    // cardVerifyValidator,
    // Security.checkAuthUserId,
    cardController.userCardVerify );


export { cardRoutes };
