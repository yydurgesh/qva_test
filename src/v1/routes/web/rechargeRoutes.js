import { Router } from "express";
import websiteController from "~/v1/controllers/web/websiteController";
import rechargeControllerWeb from "~/v1/controllers/web/rechargeControllerWeb";
import verifyWebToken from "~/middlewares/verifyUserToken";



/**
 * Contains all API routes for the application.
 */
const rechargeRoutes = new Router();

// rechargeRoutes.post( "/get-msisdn-info", setLanguage,
// 	passportLb.authenticate( "jwt", { "session": false } ),
//     Security.checkAuthUserId,
//     misdnInfoValidator, 
//     rechargeController.getMsisdnInfo );


rechargeRoutes.get("/recharge", verifyWebToken,  ( req, res ) => {
    
    websiteController.recharge(req, res);
} );

rechargeRoutes.get("/promotions", verifyWebToken,  ( req, res ) => {
    
    rechargeControllerWeb.continentPromotions(req, res);
} );

rechargeRoutes.post("/recharge/promotions-description", verifyWebToken,  ( req, res ) => {
    
    rechargeControllerWeb.continentPromotionsDescription(req, res);
} );

export { rechargeRoutes };
