import { Router } from "express";
import userController from "~/v1/controllers/web/userController";

import i18n from "~/config/i18n.config";
import csrf from 'csurf' ;
import verifyWebToken from "~/middlewares/verifyUserToken";
import { checkEmail,
    
    profileImageValidator,
   
    userProfileValidator
   
 } from "~/validators/userValidator";


import passport from '~/config/passport' ;
const csrfProtection = csrf();



/** 
 * Contains all API routes for the application.
 */
const userRoutes = new Router();

userRoutes.get( "/profile", csrfProtection, verifyWebToken,  ( req, res ) => {
   
    userController.userProfile(req, res);
} );

userRoutes.post( "/update-user", csrfProtection, verifyWebToken, userProfileValidator,checkEmail,profileImageValidator,
   
    userController.updateProfile
 );


export { userRoutes };
