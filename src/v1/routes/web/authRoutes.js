import { Router } from "express";
import websiteController from "~/v1/controllers/web/websiteController";
import isUsersAuthenticate from "~/middlewares/isUsersAuthenticate";
//import {checkPassword} from "~/validators/userValidator";
import {contactValidator, otpValidator, resendotpValidator } from "~/validators/otpValidator";
import {userValidator, checkPassword, checkEmailValid  } from "~/validators/userValidator";
import authController from "~/v1/controllers/web/authControllerWeb";
//import setWebLanguage from '~/middlewares/setWebLanguage';

import { loginOtpValidator,loginValidator,adminLoginValidator,developerValidator } from "~/validators/loginValidator";
import i18n from "~/config/i18n.config";
import csrf from 'csurf' ;
import verifyWebToken from "~/middlewares/verifyUserToken";


import passport from '~/config/passport' ;
const csrfProtection = csrf();



/** 
 * Contains all API routes for the application.
 */
const authRoutes = new Router();

authRoutes.get( "/", csrfProtection, isUsersAuthenticate,  ( req, res ) => {
   
    websiteController.home(req, res);
} );
//authController.sendOtp
authRoutes.post( "/auth/send-otp", csrfProtection,   contactValidator , authController.sendOtp);
authRoutes.post( "/auth/verify-otp",   otpValidator , authController.verifyOtp);
authRoutes.post( "/auth/user-signup",  userValidator,  checkPassword, authController.userSignup);
authRoutes.post( "/auth/resend-otp",   resendotpValidator, authController.resendOtp );

authRoutes.post( "/auth/login", 
csrfProtection,
loginValidator,
authController.login );

//verify otp for ip address
authRoutes.post( "/auth/verify-user-otp",  otpValidator, authController.verifyUserOtp );

//verify otp for ip address
authRoutes.post( "/auth/resetPassword", csrfProtection,  checkEmailValid, authController.resetPassword );

authRoutes.get( "/reset-password", csrfProtection, isUsersAuthenticate, ( req, res ) => {
    authController.CheckPasswordToken(req, res);
} );


authRoutes.post( "/update-Password", csrfProtection,  checkPassword,
authController.updatePassword
 );

 authRoutes.post( "/auth/change_language",  ( req, res ) => {
   
    websiteController.change_language(req, res);
} );

// logout
authRoutes.get( "/logout", 
   
    verifyWebToken,
    authController.logout 
);

export { authRoutes };
