import { Router } from "express";
import verifytoken from "~/middlewares/verifyDeveloperToken.middleware";
import isUserAuthenticate from '~/middlewares/isDeveloperAuthenticate.middleware';
import developerController from "~/v2/controllers/web/developerController";

/**
 * Contains all API routes for the application.
 */
const developerRoutes = new Router();

// developerRoutes.get( "/", ( req, res ) => {
//     developerController.home(req, res);
// } );

developerRoutes.get( "/developer-signin", isUserAuthenticate, ( req, res ) => {
    developerController.developerSignin(req, res);
} );
developerRoutes.get( "/developer-signup", isUserAuthenticate, ( req, res ) => {
    developerController.developerSignUp(req, res);
} );

developerRoutes.get( "/developer-Profile", verifytoken, ( req, res ) => {
    developerController.developerProfile(req, res);
} );

// developerRoutes.get( "/transferMoney", verifytoken, ( req, res ) => {
//     developerController.transferMoney(req, res);
// } );

developerRoutes.get( "/cubaRecharge", ( req, res ) => {
    developerController.cubaRecharge(req, res);
} );

developerRoutes.get( "/developer-logout", ( req, res ) => {
    developerController.logout(req, res);
} );

developerRoutes.get( "/developer-recharge-detail", ( req, res ) => {
    developerController.developerRechargeDetail(req, res);
} );

export { developerRoutes };
