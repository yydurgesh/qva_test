import { Router } from "express";
import adminController from "~/v2/controllers/web/adminController";
import isAdminAuthenticate from '~/middlewares/isAdminAuthenticate.middleware';
import verifytoken from "~/middlewares/verifyAdminToken.middleware";
import { findUser, userValidator } from "~/validators/userValidator";
import * as adminValidators from "~/validators/adminValidator";
/**
 * Contains all API routes for the application.
 */
const adminRoutes = new Router();

/**
 * admin login created by @hemant_solanki
 */
adminRoutes.get( "/admin", isAdminAuthenticate, (req, res) => { 
    adminController.login(req, res); 
});

/**
 * Dashboard detail created by @hemant_solanki
 */
adminRoutes.get( "/admin/dashboard", verifytoken, (req, res) => { 
    adminController.dashboard(req, res); 
});

/**
 * User list created by @hemant_solanki
 */
adminRoutes.get( "/admin/consumer", verifytoken, (req, res) => { 
    adminController.consumerList(req, res); 
});

/**
 * Reseller user list created by @hemant_solanki
 */
adminRoutes.get( "/admin/reseller", verifytoken, (req, res) => { 
    adminController.resellerList(req, res); 
});

/**
 * User list created by @hemant_solanki
 */
adminRoutes.get( "/admin/userProfile", verifytoken, (req, res) => { 
    adminController.userProfile(req, res); 
});

/**
 * Recharge list created by @hemant_solanki
 */
adminRoutes.get( "/admin/recharges", verifytoken, (req, res) => { 
    adminController.rechargeList(req, res); 
});

/**
 * Tranfer by number list created by @hemant_solanki
 */
adminRoutes.get( "/admin/tranferByNumber", verifytoken, (req, res) => { 
    adminController.transferByNumberList(req, res); 
});

/**
 * Tranfer by address list created by @hemant_solanki
 */
adminRoutes.get( "/admin/tranferByAddress", verifytoken, (req, res) => { 
    adminController.transferByAddressList(req, res); 
});

/**
 * Developer list created by @hemant_solanki
 */
adminRoutes.get( "/admin/developers", verifytoken, (req, res) => { 
    adminController.developerList(req, res); 
});

/**
 * Logout created by @hemant_solanki
 */
adminRoutes.get( "/admin/logout", (req, res) => { 
    adminController.logout(req, res); 
});

adminRoutes.get( "/admin/developerProfile", verifytoken, (req, res) => { 
    adminController.developerProfile(req, res); 
});

/**
 * Content created by @hemant_solanki
 */
adminRoutes.get( "/admin/content", verifytoken, (req, res) => { 
    adminController.getContent(req, res); 
});
adminRoutes.post( "/admin/content", verifytoken, (req, res) => { 
    adminController.saveContent(req, res); 
});

/**
 * Content created by @hemant_solanki
 */
adminRoutes.get( "/admin/invitation", verifytoken, (req, res) => { 
    adminController.userInvitation(req, res); 
});

/**
 * User recharge detail created by @hemant_solanki
 */
adminRoutes.get( "/admin/user-recharge-detail", verifytoken, (req, res) => { 
    adminController.getUserRechargeDetail(req, res); 
});

/**
 * User recharge detail created by @hemant_solanki
 */
adminRoutes.get( "/admin/developer-recharge-detail", verifytoken, (req, res) => { 
    adminController.getDeveloperRechargeDetail(req, res); 
});

/**
 * Admin profile detail created by @hemant_solanki
 */
adminRoutes.get( "/admin/profile-detail", verifytoken, (req, res) => { 
    adminController.getAdminProfileDetail(req, res); 
});

/**
 * All transaction data created by @hemant_solanki
 */
adminRoutes.get( "/admin/transactions", verifytoken, (req, res) => { 
    adminController.getTransactionList(req, res); 
});

/**
 * All transfer list created by @hemant_solanki
 */
adminRoutes.get( "/admin/transfers", verifytoken, (req, res) => { 
    adminController.getTransferList(req, res); 
});

/**
 * Verify otp created by @hemant_solanki
 */
adminRoutes.get( "/admin/verify-otp", ( req, res ) => {
    adminController.verifyOtp(req, res);
} ); 

/**
 * Get user stripe fee and tax created by @hemant_solanki
 */
adminRoutes.get( "/admin/user-stripe-fee-tax", ( req, res ) => {
    adminController.getUserStripeTaxDetail(req, res);
} ); 

/**
 * Get reseller developer stripe fee and tax created by @hemant_solanki
 */
adminRoutes.get( "/admin/reseller-developer-stripe-fee-tax", ( req, res ) => {
    adminController.getResellerDeveloperStripeTaxDetail(req, res);
} ); 


/**
 * Get privacy policy created by @hemant_solanki
 */
adminRoutes.get( "/content/privacyPolicy", (req, res) => { 
    adminController.getPrivacyPolicy(req, res); 
});

/**
 * Get terms and condition created by @hemant_solanki
 */
adminRoutes.get( "/content/termsCondition", (req, res) => { 
    adminController.getTermsCondition(req, res); 
});

/**
 * Get user transfer limit created by @Vihag_kumar
 */
adminRoutes.get( "/admin/user-transfer-limit", verifytoken, ( req, res ) => {
    adminController.getCustomerTransferLimit( req, res );
} );

/**
 * Get reseller transfer limit created by @Vihag_kumar
 */
adminRoutes.get( "/admin/reseller-transfer-limit", verifytoken, ( req, res ) => {
    adminController.getResellerTransferLimit( req, res );
} );

/**
 * Update user & reseller transfer limit created by @Vihag_kumar
 */
adminRoutes.post( "/admin/update-transfer-limit", verifytoken,
    adminValidators.transferLimitValidator,
    adminController.updateTransferLimit
);


export { adminRoutes };
