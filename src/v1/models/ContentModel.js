import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import Util from "~/utils/util";
const knex = knexJs( knexConfig );

/**
 * Class representing a content model.
 * @class
 */
export default class ContentModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     * @returns {Promise}
     */
    constructor( opts ) {
        super( opts );
        this.table = "contents";
        this._hasTimestamps = true;
    }
}