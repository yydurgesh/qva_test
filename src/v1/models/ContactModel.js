import BaseModel from "./BaseModel";

/**
 * Class representing a Contact model.
 * @class
 */
export default class ContactModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     */
    constructor( opts ) {
        super( opts );
        this.table = "contacts";
        this._hasTimestamps = true;
    }

}
