import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import { Security } from "~/libraries/Security";

const knex = knexJs( knexConfig );

/**
 * Class representing a message model.
 * @class
 */
export default class CountryModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     */
    constructor( opts ) {
        super( opts );
        this.table = "ding_country";
        this._hasTimestamps = true;
    }


   
}
