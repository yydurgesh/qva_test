import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import Util from "~/utils/util";
const knex = knexJs( knexConfig );

/**
 * Class representing a user model.
 * @class
 */
export default class VerifyModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     * @returns {Promise}
     */
    constructor( opts ) {
        super( opts );
        this.table = "verify";
        this._hasTimestamps = true;
    }

    getRechargeCount( userId ) {
        var bindingParam = [userId];
        
        var q = ' SELECT count(dav_recharge_id )as count'
              + ' FROM developer_recharge'
              + ' WHERE developer_id = ? AND dev_recharge_status = 2'; 
    
        return  knex.raw(q, bindingParam).then(( res ) => {
            var result = res.rows;
            return result;
        })
    }

}
