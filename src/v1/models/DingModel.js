import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import Util from "~/utils/util";
const knex = knexJs( knexConfig );

/**
 * Class representing a user model.
 * @class
 */
export default class DingModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     * @returns {Promise}
     */
    constructor( opts ) {
        super( opts );
        this.table = "qva_promotion_desc";
        this._hasTimestamps = true;
    }

    /**
     * Inserts a new model into the database then returns an instantiation of the model.
     *
     * @param {Object} properties The Model properties.
     * 
     * @returns {*} An instantiation of the model.
     */
    batchPromotionInsertObj( properties ) {
        
   
        return knex.batchInsert( 'qva_promotion_desc', properties, 1000 ).then( ( res ) => {
            //console.log("res is",res[0].rowCount)
            return res[0].rowCount;
        } );
        
    }


    /**
     * Inserts a new model into the database then returns an instantiation of the model.
     *
     * @param {Object} properties The Model properties.
     * 
     * @returns {*} An instantiation of the model.
     */
    batchProvidersInsertObj( properties ) {
        
        return knex.batchInsert( 'qva_providers', properties, 1000 ).then( ( res ) => {
            //console.log("res is",res[0].rowCount)
            return res[0].rowCount;
        } );
        
    }


 /**
     * Inserts a new model into the database then returns an instantiation of the model.
     *
     * @param {Object} properties The Model properties.
     * 
     * @returns {id} An instantiation of the model.
     */
    deleteDingDataObj(tablname, query = {} ) {
        return knex( tablname )
              .where( query )
              .del()
              .then(function(ids) {
                 return ids;
              })
    }

  /**
     * Inserts a new model into the database then returns an instantiation of the model.
     *
     * @param {Object} properties The Model properties.
     * 
     * @returns {id} An instantiation of the model.
     */
    GetAllContinentPromotions(query = {} ) {
  
      let querys =  "select distinct(qva_providers.countries_iso) as countries_iso ,qva_continent.name, qva_continent.id as continent_id, qva_providers.countries_iso as countries_iso,dis.provider_code,dis.currency_iso, minimum_send_amount, dis.localization_key,headline, terms_conditions	,bonus_validity	,promotion_type,language_code,start_utc	,end_utc from qva_continent inner JOIN"
          +" qva_providers" 
          + " ON  qva_providers.countries_iso = any( string_to_array( qva_continent.countries_iso, ',' ) )"
          + " inner JOIN qva_promotion_desc as dis ON dis.provider_code = qva_providers.provider_code" 
           return  knex.raw(querys).then(( res ) => {
             
            var result = res.rows;
            return result;
           });
            
    }



    /**
     * Inserts a new model into the database then returns an instantiation of the model.
     *
     * @param {Object} properties The Model properties.
     *  @param {string} table name The Model properties.
     * @returns {*} An instantiation of the model.
     */
    batchDataInsertObj( properties, table ) {
        
        return knex.batchInsert( table, properties, 1000 ).then( ( res ) => {
            //console.log("res is",res[0].rowCount)
            return res[0].rowCount;
        } );
        
    }


     /**
     * Get promotions list according to continent id.
     *
     * @param {int} continent id The Model properties.
     * 
     * @returns {id} An instantiation of the model.
     */
    fetchPromotionsData(continent_id) {
  
        let querys =  "select country.id as country_id, headline, promo.bonus_validity, promotion_type, providers.logo_url as providers_logo,provider_name, country.name as country,country.logo_url as country_logo	from qva_continent_promotion as promo inner JOIN"
            +" qva_providers as providers" 
            + " ON  providers.provider_code = promo.provider_code	"
            + " inner JOIN qva_countries as country ON country.countries_iso = promo.countries_iso"
            + ` where promo.continent_id =  ${continent_id}`
            + 'ORDER BY country.id ASC';
           // querys = querys.where( 'promo.continent_id' , 1 )     
          
             return  knex.raw(querys).then(( res ) => {
               
              var result = res.rows;
              
              return result;
             });
              
      }
  
}
