import BaseModel from "./BaseModel";

/**
 * define RequestModel
 */
export default class RequestModel extends BaseModel {
    /**
     * @param {number} opts The first number.
     */
    constructor( opts ) {
        super( opts );
        this.table = "requests_log";
        this.hasTimestamps = true;
    }
}
