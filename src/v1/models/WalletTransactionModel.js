import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import Util from "~/utils/util";
const knex = knexJs(knexConfig);

/**
 * Class representing a user model.
 * @class
 */
export default class WalletTransactionModel extends BaseModel {
      /**
       * Constructor.
       *
       * @param  {Object}  opts
       * @returns {Promise}
       */
      constructor(opts) {
            super(opts);
            this.table = "wallet_transaction";
            this._hasTimestamps = true;
      }
      getwalletTransactionHistory(req) {
            var user_id = req.user_id;
            if (req.page) {
                  var page = Number(req.page) * Number(req.limit);
            } else {
                  var page = 0;
            }
            if (req.limit) {
                  var limit = Number(req.limit);
            } else {
                  var limit = 10;
            }

            var bindingParam = [req.wallet_id];
            var q = ' SELECT wt_id, wallet_id, wt_type, wt_amount, wt_currency, wt_reference_id, wt_reference_type,wt_user_type,'
                  + ' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT name FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN recipient_name END as transfer_to FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2),'
                  + ' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT country_code FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN \'+53\' END as to_country_code FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2 ),'
                  + ' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT contact_number FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN recipient_contact_no END as to_contact_no FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2 ),'
                  + ' (SELECT name  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from,'
                  + ' (SELECT country_code  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from_country_code,'
                  + ' (SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from_contact_no,'
                  + '  wt_status,wallet_transaction.created_at '
                  + ' FROM wallet_transaction'
                  + ' WHERE wallet_transaction.wallet_id = ? AND wt_status =1 ORDER BY wt_id DESC';

            if (q) {
                  q += ' OFFSET ? LIMIT ? ';
                  bindingParam.push(page);
                  bindingParam.push(limit);
            }

            return knex.raw(q, bindingParam).then((res) => {
                  var result = res.rows;

                  return result;
            })
      }
      getwalletTransactionHistoryOld(req) {

            var user_id = req.user_id;
            if (req.page) {
                  var page = Number(req.page) * Number(req.limit);
            } else {
                  var page = 0;
            }
            if (req.limit) {
                  var limit = Number(req.limit);
            } else {
                  var limit = 10;
            }

            var bindingParam = [req.wallet_id];
            var q = ' SELECT wt_id, wallet_id, wt_type, wt_amount, wt_currency, wt_reference_id, wt_reference_type,'
                  + ' (SELECT name  FROM users WHERE user_id = (SELECT transfer_to_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=2 AND wt_reference_type =2)) as transfer_to,'
                  + ' (SELECT country_code  FROM users WHERE user_id = (SELECT transfer_to_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=2 AND wt_reference_type =2)) as to_country_code,'
                  + ' (SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_to_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=2 AND wt_reference_type =2)) as to_contact_no,'
                  + ' (SELECT name  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from,'
                  + ' (SELECT country_code  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from_country_code,'
                  + ' (SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from_contact_no,'
                  + '  wt_status,wallet_transaction.created_at '
                  + ' FROM wallet_transaction'
                  + ' WHERE wallet_transaction.wallet_id = ?';

            if (q) {
                  q += ' OFFSET ? LIMIT ? ';
                  bindingParam.push(page);
                  bindingParam.push(limit);
            }


            return knex.raw(q, bindingParam).then((res) => {
                  var result = res.rows;

                  return result;
            })
      }
      getwalletTransactionHistoryCount(req) {
            var user_id = req.user_id;

            var bindingParam = [req.wallet_id];
            var q = ' SELECT count(wt_id)'
                  + ' FROM wallet_transaction'
                  + ' WHERE wallet_transaction.wallet_id = ? AND wt_status =1';



            return knex.raw(q, bindingParam).then((res) => {
                  var result = res.rows;

                  return result;
            })
      }

      getwalletTransactionList(req, limit, offset) {
            // var user_id      = req.user_id;
            let searchData = '';
            let orderData = 'desc';
            let orderBy = 'wallet_transaction.created_at';

            var bindingParam = [req.wallet_id];
            var q = ' SELECT wt_id, wallet_id, wt_type, wt_amount, wt_currency, wt_reference_id, wt_reference_type,wt_user_type,wt_pre_amount,wt_balance,'
                  + ' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT name FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN recipient_name END as transfer_to FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2),'
                  + ' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT country_code FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN \'+53\' END as to_country_code FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2 ),'
                  + ' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT contact_number FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN recipient_contact_no END as to_contact_no FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2 ),'
                  + ' (SELECT name  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from,'
                  + ' (SELECT country_code  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from_country_code,'
                  + ' (SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from_contact_no,'
                  + '  wt_status,wallet_transaction.created_at '
                  + ' FROM wallet_transaction'
                  + ' WHERE wallet_transaction.wallet_id = ? AND wt_status =1';

            if (req["search[value]"] != undefined && req["search[value]"] != "") {
                  searchData = req["search[value]"].toLowerCase();
            }

            if (req.transactionStatus != undefined && req.transactionStatus != '') {
                  q += " AND wallet_transaction.wt_reference_type = ? "
                  bindingParam.push(req.transactionStatus);
            }

            if ((req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
                  q += " AND ( DATE(wallet_transaction.created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/New_York') BETWEEN ? and ? )"
                  bindingParam.push(req.searchByFromdate);
                  bindingParam.push(req.searchByTodate);
            }

            if (searchData) {
                  // q += " AND ((SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) LIKE ? OR (SELECT contact_number FROM users WHERE user_id = ( SELECT transfer_by_user_id FROM transfers WHERE transfer_id = wt_reference_id AND wt_type = 2 AND wt_reference_type = 2)) LIKE ?)";
                  q += " AND (SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) LIKE ?";
                  bindingParam.push("%" + searchData + "%");
                  // bindingParam.push("%" + searchData + "%");    
            }

            if (q) {
                  q += " ORDER BY wt_id DESC OFFSET ? LIMIT ? ";
                  //  bindingParam.push(orderBy); 
                  //  bindingParam.push(orderData); 
                  bindingParam.push(offset);
                  bindingParam.push(limit);
            }

            return knex.raw(q, bindingParam).then((res) => {
                  var result = res.rows;

                  return result;
            })
      }

      getwalletTransactionListCount(req) {
            // var user_id      = req.user_id;
            let searchData = '';

            var bindingParam = [req.wallet_id];
            var q = ' SELECT count(wt_id)'
                  + ' FROM wallet_transaction'
                  + ' WHERE wallet_transaction.wallet_id = ? AND wt_status =1';

            if (req["search[value]"] != undefined && req["search[value]"] != "") {
                  searchData = req["search[value]"].toLowerCase();
            }

            if (req.transactionStatus != undefined && req.transactionStatus != '') {
                  q += " AND wallet_transaction.wt_reference_type = ? "
                  bindingParam.push(req.transactionStatus);
            }

            if ((req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
                  q += " AND ( DATE(wallet_transaction.created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/New_York') BETWEEN ? and ? )"
                  bindingParam.push(req.searchByFromdate);
                  bindingParam.push(req.searchByTodate);
            }

            if (searchData) {
                  // q += " AND ((SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) LIKE ? OR (SELECT contact_number FROM users WHERE user_id = ( SELECT transfer_by_user_id FROM transfers WHERE transfer_id = wt_reference_id AND wt_type = 2 AND wt_reference_type = 2)) LIKE ?)";
                  q += " AND (SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) LIKE ?";
                  bindingParam.push("%" + searchData + "%");
                  // bindingParam.push("%" + searchData + "%");          
            }


            return knex.raw(q, bindingParam).then((res) => {
                  var result = res.rows;
                  return result;
            })
      }

      getTransactionListByAdmin(req, limit, offset) {
            // var user_id      = req.user_id;

            let searchData = '';
            let orderData = 'desc';
            let orderBy = 'wallet_transaction.created_at';
            var walletId = req.wallet_id;

            var bindingParam = [];
            var q = ' SELECT wt_id, wallet_id, wt_type, wt_amount, wt_currency, wt_reference_id, wt_reference_type,wt_user_type,wt_pre_amount,wt_balance,'
                  + ' ( Select name AS admin_added_money FROM wallet_transaction AS wallet_trs JOIN wallets ON wallet_trs.wallet_id = wallets.wallet_id JOIN users ON wallets.wallet_user_id = users.user_id WHERE wallet_trs.wt_type = 1 AND wallet_trs.wt_reference_type = 4 AND wallet_trs.wt_id = wallet_transaction.wt_id ),'
                  + ' ( Select name AS admin_deducted_money FROM wallet_transaction AS wallet_trs JOIN wallets ON wallet_trs.wallet_id = wallets.wallet_id JOIN users ON wallets.wallet_user_id = users.user_id WHERE wallet_trs.wt_type = 2 AND wallet_trs.wt_reference_type = 6 AND wallet_trs.wt_id = wallet_transaction.wt_id ),'
                  + ' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT name FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN recipient_name END as transfer_to FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2),'
                  + ' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT name FROM users WHERE user_id = transfer_by_user_id ) WHEN transfer_type = 2 THEN recipient_name END as transfer_by FROM transfers WHERE transfer_id = wt_reference_id AND wt_type = 2 AND wt_reference_type = 2 ),'
                  + ' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT country_code FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN \'+53\' END as to_country_code FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2 ),'
                  + ' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT contact_number FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN recipient_contact_no END as to_contact_no FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2 ),'
                  + ' (SELECT name  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from,'
                  + ' ( SELECT name FROM users WHERE user_id = ( SELECT transfer_to_user_id FROM transfers WHERE transfer_id = wt_reference_id AND wt_type = 1 AND wt_reference_type = 2 ) ) as receiver,'
                  + ' (SELECT country_code  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from_country_code,'
                  + ' (SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from_contact_no,'
                  + '  wt_status,wallet_transaction.created_at '
                  + ' FROM wallet_transaction';
            //      + ' WHERE wt_status =1'; 

            if (walletId != undefined) {
                  q += ' WHERE wallet_transaction.wallet_id = ? AND wt_status =1'
                  bindingParam.push(walletId);
            } else {
                  q += ' WHERE wt_status =1';
            }

            if (req.userType != undefined) {
                  q += ' AND wallet_id IN (select wallet_id from wallets inner join users on users.user_id = wallets.wallet_user_id and user_type = ? )'
                  bindingParam.push(req.userType);
            }

            if (req["search[value]"] != undefined && req["search[value]"] != "") {
                  searchData = req["search[value]"].toLowerCase();
            }

            if (req.transactionStatus != undefined && req.transactionStatus != '') {
                  q += " AND wallet_transaction.wt_reference_type = ? "
                  bindingParam.push(req.transactionStatus);
            }

            if ((req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
                  q += " AND ( DATE(wallet_transaction.created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/New_York' ) BETWEEN ? and ? )"
                  bindingParam.push(req.searchByFromdate);
                  bindingParam.push(req.searchByTodate);
            }

            if (searchData) {
                  // q += " AND ((SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) LIKE ? OR (SELECT contact_number FROM users WHERE user_id = ( SELECT transfer_by_user_id FROM transfers WHERE transfer_id = wt_reference_id AND wt_type = 2 AND wt_reference_type = 2)) LIKE ?)";
                  q += " AND (SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) LIKE ?";
                  bindingParam.push("%" + searchData + "%");
                  // bindingParam.push("%" + searchData + "%");    
            }

            if (q) {
                  q += " ORDER BY wt_id DESC OFFSET ? LIMIT ? ";
                  //  bindingParam.push(orderBy); 
                  //  bindingParam.push(orderData); 
                  bindingParam.push(offset);
                  bindingParam.push(limit);
            }
     
            return knex.raw(q, bindingParam).then((res) => {
                  var result = res.rows;

                  return result;
            })
      }

      getTransactionListCountByAdmin(req) {
            // var user_id      = req.user_id;
            let searchData = '';
            var bindingParam = [];
            var walletId = req.wallet_id;

            var q = ' SELECT count(wt_id)'
                  + ' FROM wallet_transaction';
            // + ' WHERE wt_status =1'; 

            if (walletId != undefined) {
                  q += ' WHERE wallet_transaction.wallet_id = ? AND wt_status =1'
                  bindingParam.push(walletId);
            } else {
                  q += ' WHERE wt_status =1';
            }

            if (req.userType != undefined) {
                  q += ' AND wallet_id IN (select wallet_id from wallets inner join users on users.user_id = wallets.wallet_user_id and user_type = ? )'
                  bindingParam.push(req.userType);
            }

            if (req["search[value]"] != undefined && req["search[value]"] != "") {
                  searchData = req["search[value]"].toLowerCase();
            }

            if (req.transactionStatus != undefined && req.transactionStatus != '') {
                  q += " AND wallet_transaction.wt_reference_type = ? "
                  bindingParam.push(req.transactionStatus);
            }

            if ((req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
                  q += " AND ( DATE(wallet_transaction.created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/New_York' ) BETWEEN ? and ? )"
                  bindingParam.push(req.searchByFromdate);
                  bindingParam.push(req.searchByTodate);
            }

            if (searchData) {
                  // q += " AND ((SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) LIKE ? OR (SELECT contact_number FROM users WHERE user_id = ( SELECT transfer_by_user_id FROM transfers WHERE transfer_id = wt_reference_id AND wt_type = 2 AND wt_reference_type = 2)) LIKE ?)";
                  q += " AND (SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) LIKE ?";
                  bindingParam.push("%" + searchData + "%");
                  // bindingParam.push("%" + searchData + "%");          
            }


            return knex.raw(q, bindingParam).then((res) => {
                  var result = res.rows;
                  return result;
            })
      }
}
