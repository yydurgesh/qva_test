import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import Util from "~/utils/util";
const knex = knexJs( knexConfig );

/**
 * Class representing a user model.
 * @class
 */
export default class InvalidAttemptedModel extends BaseModel {
     /**
     * Constructor.
     *
     * @param  {Object}  opts
     * @returns {Promise}
     */
    constructor( opts ) {
        super( opts );
        this.table = "qva_invalid_otp_attempted";
        this._hasTimestamps = true;
    }

}