import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
const knex = knexJs( knexConfig );

/**
 * Class representing a message model.
 * @class
 */
export default class CallModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     */
    constructor( opts ) {
        super( opts );
        this.table = "calls";
        this._hasTimestamps = true;
    }

    /**
    * Get call list with reciever detail by sender Id.
    *
    * @param {Object} userId The query to match against.
    * @param {string} limit The query to match against.
    * @param {string} offset The query to match against.
    * @param {string} requestData The query to match LIKE.
    * @returns {Promise} Will return promise.
    */
    getUserCallList( userId, limit, offset, requestData ) {
        const searchData = requestData.search,
            rawSelect = "t1.*",
            queryString1 = "(SELECT DISTINCT ON(cl.call_contact_id) contact_id, (SELECT count(call_contact_id) FROM calls Where call_user_id = ",
            queryString2 = " and call_contact_id = contact_id) as callcount, ct.contact_country_code, ct.contact_number, cl.created_at FROM calls as cl INNER JOIN contacts as ct on ct.contact_id = cl.call_contact_id WHERE cl.call_user_id = ",
            queryString3 = " order by cl.call_contact_id desc, cl.created_at DESC ) as t1",
            mainQuery = queryString1 + userId + queryString2 + userId + queryString3;

        let prepareQuery = knex( knex.raw( mainQuery ) )
            .select( knex.raw( rawSelect ) )
            .limit( limit )
            .offset( offset )
            .orderByRaw( "t1.created_at DESC" );

        if( searchData ) {
            prepareQuery = prepareQuery.andWhereRaw( `t1.contact_number LIKE '%${searchData}%'` );
        }
        prepareQuery = prepareQuery.map( ( res ) => {
            return res;
        } );
        
        return prepareQuery;
    }

    /**
        * Get call details with reciever detail by sender Id.
        *
        * @param {Object} where The query to match against.
        * @param {Object} opts The query to match against.
        * @param {string} limit The query to match against.
        * @param {string} offset The query to match against.
        * @param {string} orderby
        * @param {string} order
        * @returns {Promise} Will return promise.
        */
    getUserCallDetails( where = {}, opts = {}, limit, offset, orderby, order ) {
        let prepareQuery = knex( this.table )
            .select( opts )
            .innerJoin( "contacts", "contacts.contact_id", "calls.call_contact_id" )
            .limit( limit )
            .offset( offset )
            .where( where );
            
        if ( orderby !== undefined && order !== undefined ) {
            prepareQuery = prepareQuery.orderBy( orderby, order );
        }else{
            prepareQuery = prepareQuery.orderBy( [ { "column": "calls.call_contact_id" }, { "column": "calls.created_at", "order": "desc" } ] );
        }
        prepareQuery = prepareQuery.map( ( res ) => {
            return res;
        } );
        
        return prepareQuery;
    }

    /**
     * Return count call details of perticular user by user_id.
     *
     * @param {Object} query The properties to condition.
     * @param {Object} column Where clause for count.
     * @returns {Array} A collection of the count column models.
     */
    getCountCall( query = {}, column ) {
        return knex( this.table )
            .count( knex.raw( column ) )
            .innerJoin( "contacts", "contacts.contact_id", "calls.call_contact_id" )
            .where( query )
            .map( ( res ) => {
                return res.count;
            } );
    }

}
