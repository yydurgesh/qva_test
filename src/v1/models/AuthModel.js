import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
const knex = knexJs( knexConfig );

/**
 * Class representing a message model.
 * @class
 */
export default class AuthModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     */
    constructor( opts ) {
        super( opts );
        this.table = "auth";
        this._hasTimestamps = true;
    }
    getSum(table,coulmn){
       
              var bindingParam = [];
              var q = ' SELECT SUM(admin_commission_amount) as profit_sum'
              + ' FROM '+table;
       
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
            return result;
            })
    }
}