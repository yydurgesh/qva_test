import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import Util from "~/utils/util";
const knex = knexJs( knexConfig );

/**
 * Class representing a message model.
 * @class
 */
export default class OTPModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     */
    constructor( opts ) {
        super( opts );
        this.table = "qva_otp";
        this._hasTimestamps = true;
    }


    /**
     * Get a collection of models matching a given query.
     *
     * @param {Object} query The query to match against.
     * @param {Object} opts Options.
     * @param {Object} orderby
     * @param {Object} order
     * @returns {Array} An array holding resultant models.
     */
     /**
     * Get a collection of models matching a given query.
     *
     * @param {Object} query The query to match against.
     * @param {Object} opts Options.
     * @returns {Array} An array holding resultant models.
     */
    fetchOtpObjWithSelectedFields( query = {}, opts = {} ) {
        return knex( this.table )
            .select( opts )
            .orderBy( 'created_at', 'desc' )
            .where( Util.toSnake( query ) )
            .first()
            .then( ( row ) => {
                return row;
            } );


           
       
    }
   
}