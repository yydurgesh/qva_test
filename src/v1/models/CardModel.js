import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import Util from "~/utils/util";
const knex = knexJs( knexConfig );

/**
 * Class representing a user model.
 * @class
 */
export default class CardModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     * @returns {Promise}
     */
    constructor( opts ) {
        super( opts );
        this.table = "cards";
        this._hasTimestamps = true;
    }

    
/**
     * Get a all rows from consumer user table.
     *
     * @param {Object} where The query to match against.
     * @param {Object} opts Options.
     * @param {string} limit The query to page limit.
     * @param {string} offset The query to set page offset.
     * @param {string} requestData The query to match LIKE.
     */
    getUserCardList( where = {}, opts = {}, limit, offset, requestData ) {
        let searchData = '';
       
        // if( requestData[ "search[value]" ] != undefined && requestData[ "search[value]" ] != "" ) {
        //    searchData = requestData[ "search[value]" ].toLowerCase();
        // }
       
        let prepareQuery = knex( this.table )
            .select( opts )
            .where( where )
            .limit( limit )
            .offset( offset );


        prepareQuery = prepareQuery.map( ( res ) => {
            return res;
        } );
        
        return prepareQuery;
    }
}
