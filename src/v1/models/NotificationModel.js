import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import Util from "~/utils/util";
const knex = knexJs( knexConfig );

/**
 * Class representing a user model.
 * @class
 */
export default class NotificationModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     * @returns {Promise}
     */
    constructor( opts ) {
        super( opts );
        this.table = "notifications";
        this._hasTimestamps = true;
    }

    /**
     * Get a all rows from table.
     * @param {string} requestData The body to match.
     * @param {string} profileImagePath The profile image path.
     * @param {string} defaultUserImagePath The default user image path.
     */
    getNotificationList( requestData, profileImagePath, defaultUserImagePath ) {
        
        if (requestData.page) {
            var page = Number(requestData.page)*Number(requestData.limit);
        } else {
            var page=0;
        }
        if (requestData.limit) {
            var limit = Number(requestData.limit);
        } else {
            var limit= 5;
        }
       
        var bindingParam = [requestData.user_id];
        var q = "SELECT notifications.notification_id,notifications.notification_by,notifications.notification_for,notifications.notification_type,notifications.notification_msg,"
                +"notifications.read_status,notifications.notification_status,notifications.delete_status,notifications.created_at,notifications.notification_payload,sender.name AS sender_user_name,receiver.name AS receiver_user_name,"
                +"( CASE WHEN ( sender.profile_image = null AND sender.profile_image != '' ) THEN CONCAT( '" + profileImagePath + "', sender.profile_image ) ELSE '" + defaultUserImagePath + "' END   ) AS sender_user_profile_image"
                +" FROM notifications"
                +" JOIN users AS sender ON notifications.notification_by = sender.user_id"
                +" JOIN users AS receiver ON notifications.notification_for = receiver.user_id"
                +" WHERE notifications.notification_for = ?";
           
        if( q ) {
            q += ' ORDER BY notifications.created_at desc OFFSET ? LIMIT ? ';
            bindingParam.push(page); 
            
            bindingParam.push(limit);
        } 
        return  knex.raw(q, bindingParam).then(( res ) => {
        var result = res.rows;
        return result;
        });

    }

    getNotificationCount( requestData ) {
        var bindingParam = [requestData.user_id];
        var q = "SELECT COUNT(notifications.notification_id) AS totalNotification"
                +" FROM notifications"        
                +" WHERE notifications.notification_for = ?";

        return  knex.raw(q, bindingParam).then(( res ) => {
            var result = res.rows;
            return result;
        });
    }
}