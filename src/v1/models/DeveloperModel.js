import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import Util from "~/utils/util";
const knex = knexJs( knexConfig );

/**
 * Class representing a user model.
 * @class
 */
export default class DeveloperModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     * @returns {Promise}
     */
    constructor( opts ) {
        super( opts );
        this.table = "developer";
        this._hasTimestamps = true;
    }

   
    getDeveloperList( where, limit, offset ) {
        
         let orderData = 'DESC';
         let orderBy = 'created_at';
         
        // if (where.page) {
        //    var page = Number(where.page)*Number(where.limit);
        // } else {
        //     var page=0;
        // }
        // if (where.limit) {
        //     var limit = Number(where.limit);
        // } else {
        //     var limit=1;
        // }
       var bindingParam = [];
       var q = 'SELECT users.user_id,name,email,is_approval,dev_bal.wallet_amount,admin_commission_amount,status,users.created_at'
              +' FROM users'
              + ' LEFT JOIN wallets dev_bal on dev_bal.wallet_user_id = users.user_id WHERE user_type = 4 ';
            

            if( where[ "search[value]" ] != undefined && where[ "search[value]" ] != "" ){
                
                q += 
                    ' AND  name LIKE ? '
                  +' OR email LIKE ? ';
                                          
                    bindingParam.push("%" + where[ "search[value]" ] + "%");
                    bindingParam.push("%" + where[ "search[value]" ] + "%");
                    
            }

            if(( where[ "order[0][dir]" ] === "asc" || where[ "order[0][dir]" ] === "desc") && where[ "order[0][column]" ] == "1" ) {
              
                orderData = where[ "order[0][dir]" ];
                orderBy = 'name';
                
            } else if(( where[ "order[0][dir]" ] === "asc" || where[ "order[0][dir]" ] === "desc") && where[ "order[0][column]" ] == "2" ) {
    
                orderData = where[ "order[0][dir]" ];
                orderBy = 'email';                
    
            } else if(( where[ "order[0][dir]" ] === "asc" || where[ "order[0][dir]" ] === "desc") && where[ "order[0][column]" ] == "3" ) {
    
                orderData = where[ "order[0][dir]" ];
                orderBy = 'wallet_amount';
            }

            if( q ) {
                q += ` ORDER BY ${orderBy} ${orderData} `;
                q += ' OFFSET ? LIMIT ? ';
                 
                 bindingParam.push(offset); 
                 bindingParam.push(limit);
            } 
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
             return result;
            });

    }
    getDeveloperListCount( where ){

       var bindingParam = [];
       var q = 'SELECT count(users.user_id)'
              +' FROM users'
              + ' LEFT JOIN wallets dev_bal on dev_bal.wallet_user_id = users.user_id WHERE user_type =4';
            
             if(where[ "search[value]" ] != undefined && where[ "search[value]" ] != "" ){
                q += 
                    ' AND  name LIKE ? '
                  +' OR email LIKE ? ';
                                          
                    bindingParam.push("%" + where[ "search[value]" ] + "%");
                    bindingParam.push("%" + where[ "search[value]" ] + "%");
                    
            }
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
             return result;
            });

    }
    developerDetails( where ){
         var bindingParam = [where.developer_id];
       var q = 'SELECT users.user_id,name,email,country_code,contact_number,dev_bal.wallet_amount,admin_commission_amount,ding_commission_amount,status,api_token,users.created_at'
              +' FROM users'
              + ' LEFT JOIN wallets dev_bal on dev_bal.wallet_user_id = users.user_id WHERE users.user_id = ?';
            
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows[0];
             return result;
            });

    } 
}
