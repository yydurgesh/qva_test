import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import Util from "~/utils/util";
const knex = knexJs(knexConfig);

/**
 * Class representing a user model.
 * @class
 */
export default class WalletModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     * @returns {Promise}
     */
    constructor(opts) {
        super(opts);
        this.table = "wallets";
        this._hasTimestamps = true;
    }
    getTransferHistory(req) {

        var user_id = req.user_id;
        if (req.page) {
            var page = Number(req.page) * Number(req.limit);
        } else {
            var page = 0;
        }
        if (req.limit) {
            var limit = Number(req.limit);
        } else {
            var limit = 10;
        }


        var bindingParam = [req.transfer_type, user_id];
        var q = ' SELECT transfer_id,transfer_to_user_id,name,country_code,contact_number,transfer_by_user_id,transfer_amount,recipient_name,'
            + ' recipient_contact_no,address,latitude,longitude,transfer_status,transfers.created_at'
            + ' FROM transfers'
            + ' LEFT JOIN users usr on usr.user_id = transfer_to_user_id'
            + ' WHERE transfers.transfer_type = ? AND transfer_by_user_id = ?';

        if (q) {
            q += ' OFFSET ? LIMIT ? ';
            bindingParam.push(page);
            bindingParam.push(limit);
        }


        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    getdeveloperRechargeDetailByAdmin(req) {

        var dev_recharge_id = req.recharge_id;

        var bindingParam = [dev_recharge_id];
        var q = ' SELECT * FROM developer_recharge'
            + ' WHERE dav_recharge_id = ? ';



        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    getRechargeCount(userId) {
        var bindingParam = [userId];
        var q = ' SELECT count(recharge_id ) as count'
            + ' FROM recharges'
            + ' WHERE user_id = ? AND recharge_status = 3';


        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;
            return result;
        })
    }

    getDeveloperRechargeCount(userId) {
        var bindingParam = [userId];
        var q = ' SELECT count(dav_recharge_id )as count'
            + ' FROM developer_recharge'
            + ' WHERE developer_id = ? AND dev_recharge_status = 2';


        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;
            return result;
        })
    }
}
