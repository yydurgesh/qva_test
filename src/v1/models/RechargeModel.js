import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import Util from "~/utils/util";
import { Security } from "~/libraries/Security";

const knex = knexJs(knexConfig);

/**
 * Class representing a user model.
 * @class
 */
export default class RechargeModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     * @returns {Promise}
     */
    constructor(opts) {
        super(opts);
        this.table = "recharges";
        this._hasTimestamps = true;
    }

    /**
     * Get a all rows from table.
     *
     * @param {Object} opts Options.
     * @param {string} limit The query to page limit.
     * @param {string} offset The query to set page offset.
     * @param {string} requestData The query to match LIKE.
     */
    getAllRecharges(opts = {}, limit, offset, requestData) {

        let searchData = '';
        let orderData = 'DESC';
        let orderBy = 'created_at';

        if (requestData.search.value != undefined && requestData.search.value != "") {
            searchData = requestData.search.value.toLowerCase();
        }

        if (requestData.order != undefined && requestData.order != "") {

            if ((requestData.order[0].dir == "asc" || requestData.order[0].dir == "desc") && requestData.order[0].column == "0") {

                orderData = requestData.order[0].dir;
                orderBy = 'users.name';

            } else if ((requestData.order[0].dir == "asc" || requestData.order[0].dir == "desc") && requestData.order[0].column == "1") {

                orderData = requestData.order[0].dir;
                orderBy = 'recharges.recharge_mobile_number';

            }

        }

        let prepareQuery = knex(this.table)
            .select(opts)
            .innerJoin("users", "recharges.user_id", "users.user_id")
            .orderBy(orderBy, orderData)
            .where({ "users.user_type": 1 })
            .limit(limit)
            .offset(offset);

        if (searchData) {
            prepareQuery = prepareQuery.whereRaw(`recharges.recharge_mobile_number LIKE '%${searchData}%'`)
                .orWhereRaw(`LOWER(users.name) LIKE '%${searchData}%'`);
        }


        prepareQuery = prepareQuery.map((res) => {

            return res;
        });

        return prepareQuery;
    }

    /**
     * Return count call details of perticular recharge by recharge_id.
     *
     * @param {Object} column Where clause for count.
     * @returns {Array} A collection of the count column models.
     */
    getCountRechargesData(column, requestData) {
        let searchData = '';
        if (requestData.search.value != undefined && requestData.search.value != "") {
            searchData = requestData.search.value.toLowerCase();
        }
        let prepareQuery = knex(this.table)
            .count(knex.raw(column))
            .innerJoin("users", "recharges.user_id", "users.user_id")
            .where({ "users.user_type": 1 });

        if (searchData) {
            prepareQuery = prepareQuery.whereRaw(`recharges.recharge_mobile_number LIKE '%${searchData}%'`)
                .orWhereRaw(`LOWER(users.name) LIKE '%${searchData}%'`);
        }

        prepareQuery = prepareQuery.map((res) => {
            return res.count;
        });

        return prepareQuery;
    }
    /**
     * Return user recharge count and amount sum info to given date range
     *
     * @param {Object} column Where clause for count.
     * @returns {Array} A collection of the count column models.
     */
    getUserRechargeInfo(user_id, startDate, endDate) {
        var bindingParam = [user_id, startDate, endDate];
        var q = ' SELECT count(USER_ID)as recharge_count,sum(recharge_product_price) as recharge_sum'
            + ' FROM recharges'
            + ' WHERE user_id = ? AND  date(created_at) BETWEEN ? AND ? '
            + ' GROUP BY user_id';


        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;
            return result;
        })

    }
    getRechargeHistoryCount(req) {
        var bindingParam = [req.user_id];
        var q = ' SELECT count( DISTINCT recharge_mobile_number)as count'
            + ' FROM recharges'
            + ' WHERE user_id = ?';

        if (req.search) {
            q += ' AND recharge_mobile_number LIKE ? ';
            bindingParam.push("%" + req.search + "%");

        }

        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;
            return result;
        })

    }
    getRechargeHistory(req) {

        var user_id = req.user_id;
        if (req.page) {
            var page = Number(req.page) * Number(req.limit);
        } else {
            var page = 0;
        }
        if (req.limit) {
            var limit = Number(req.limit);
        } else {
            var limit = 10;
        }


        var bindingParam = [user_id, user_id];
        var q = ' SELECT * FROM ( SELECT DISTINCT ON(recharge_mobile_number) recharge_id,provider_code,recharge_country_code,recharge_country,recharge_operator,name,recharge_mobile_number,recharge_currency,recharge_product_price,recharge_status,recharge_payment_status, recharge_for, ding_commission_applied, admin_commission_applied, actual_commission,'
            + ' recharge_type,recharges.created_at,'
            + ' (select count(*) from recharges as rc  where rc.recharge_mobile_number=recharges.recharge_mobile_number AND rc .user_id =? ) as rechagre_count'
            + ' FROM recharges'
            + ' LEFT JOIN users usr on usr.user_id = recharges.user_id'
            + ' WHERE recharges.user_id = ?';

        if (req.search) {
            q += ' AND recharge_mobile_number LIKE ? ';
            bindingParam.push("%" + req.search + "%");

        }
        if (q) {
            q += ' order by recharge_mobile_number, created_at desc ) AS TBL ORDER BY recharge_id DESC OFFSET ? LIMIT ? ';
            bindingParam.push(page);
            bindingParam.push(limit);
        }


        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    rechargeDeveloperHistoryCount(req) {
        var bindingParam = [req.user_id];
        var q = ' SELECT count(dav_recharge_id )as count'
            + ' FROM developer_recharge'
            + ' WHERE developer_id = ?';

        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;
            return result;
        })

    }
    rechargeDeveloperHistory(req) {

        var user_id = req.user_id;
        if (req.page) {
            var page = Number(req.page) * Number(req.limit);
        } else {
            var page = 0;
        }
        if (req.limit) {
            var limit = Number(req.limit);
        } else {
            var limit = 10;
        }


        var bindingParam = [user_id];
        var q = ' SELECT * FROM developer_recharge'
            + ' WHERE developer_id = ?  ORDER BY dav_recharge_id DESC OFFSET 0 LIMIT 50';

        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    rechargeDeveloperDetail(req) {

        var dev_recharge_id = req.id;
        if (req.page) {
            var page = Number(req.page) * Number(req.limit);
        } else {
            var page = 0;
        }
        if (req.limit) {
            var limit = Number(req.limit);
        } else {
            var limit = 10;
        }


        var bindingParam = [dev_recharge_id];
        var q = ' SELECT * FROM developer_recharge'
            + ' WHERE dav_recharge_id = ?  ORDER BY dav_recharge_id DESC OFFSET 0 LIMIT 50';

        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    /**
     * Get a all rows from reseller user table.
     *
     * @param {Object} where The query to match against.
     * @param {Object} opts Options.
     * @param {string} limit The query to page limit.
     * @param {string} offset The query to set page offset.
     * @param {string} requestData The query to match LIKE.
     */
    getResellerRechargeDetail(where = {}, opts = {}, limit, offset, requestData) {
        let searchData = '';
        let orderData = 'DESC';
        let orderBy = 'created_at';

        if (requestData["search[value]"] != undefined && requestData["search[value]"] != "") {
            searchData = requestData["search[value]"].toLowerCase();
        }

        if ((requestData["order[0][dir]"] === "asc" || requestData["order[0][dir]"] === "desc") && requestData["order[0][column]"] == "2") {

            orderData = requestData["order[0][dir]"];
            orderBy = 'recharge_mobile_number';

        } else if ((requestData["order[0][dir]"] === "asc" || requestData["order[0][dir]"] === "desc") && requestData["order[0][column]"] == "3") {

            orderData = requestData["order[0][dir]"];
            orderBy = 'recharge_operator';

        } else if ((requestData["order[0][dir]"] === "asc" || requestData["order[0][dir]"] === "desc") && requestData["order[0][column]"] == "4") {

            orderData = requestData["order[0][dir]"];
            orderBy = 'provider_code';
        }


        let prepareQuery = knex(this.table)
            .select(opts)
            .orderBy(orderBy, orderData)
            .where(where)
            .limit(limit)
            .offset(offset);

        if (searchData) {

            prepareQuery = prepareQuery.whereRaw(`(recharge_mobile_number LIKE '%${searchData}%' )`)
        }


        prepareQuery = prepareQuery.map((res) => {
            return res;
        });

        return prepareQuery;
    }

    /**
     * Return count reseller user by user_id.
     *
     * @param {Object} where The query to match against.
     * @param {Object} column Where clause for count.
     * @returns {Array} A collection of the count column models.
     */
    getCountResellerRechargeDetailData(where = {}, column, requestData) {
        let searchData = '';

        if (requestData["search[value]"] != undefined && requestData["search[value]"] != "") {
            searchData = requestData["search[value]"].toLowerCase();
        }
        let prepareQuery = knex(this.table)
            .count(knex.raw(column))
            .where(where);

        if (searchData) {
            prepareQuery = prepareQuery.whereRaw(`(recharge_mobile_number LIKE '%${searchData}%' )`)
        }

        prepareQuery = prepareQuery.map((res) => {
            return res.count;
        });

        return prepareQuery;
    }

    /**
     * Return count developer recharge by admin created by @hemant_solanki
     * @param {*} req 
     */
    getDeveloperrechargeHistoryCount(req, userId) {

        var bindingParam = [userId];
        var q = ' SELECT count(dav_recharge_id )as count'
            + ' FROM developer_recharge'
            + ' WHERE developer_id = ?';

        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;
            return result;
        })

    }

    /**
     * Return developer recharge history by admin created by @hemant_solanki
     * @param {*} req 
     */
    getDeveloperRechargeHistory(req, userId) {


        if (req.page) {
            var page = Number(req.page) * Number(req.limit);
        } else {
            var page = 0;
        }
        if (req.limit) {
            var limit = Number(req.limit);
        } else {
            var limit = 10;
        }


        var bindingParam = [userId];
        var q = ' SELECT * FROM developer_recharge'
            + ' WHERE developer_id = ?  ORDER BY dav_recharge_id DESC OFFSET 0 LIMIT 50';

        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    getRechargeHistoryByUserId(req) {

        var user_id = req.user_id;

        var bindingParam = [user_id];
        var q = ' SELECT provider_code,recharge_country_code,recharge_country,recharge_operator,recharge_mobile_number,recharge_currency,recharge_product_price,recharge_status,recharge_payment_status,'
            + ' recharge_type,recharges.created_at'
            + ' FROM recharges'
            + ' WHERE recharges.user_id = ?';

        if (q) {
            q += ' order by recharge_mobile_number, created_at DESC';
        }


        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    /**
     * Get a all rows from table.
     *
     * @param {string} limit The query to page limit.
     * @param {string} offset The query to set page offset.
     * @param {string} requestData The query to match LIKE.
     */
    getRechargeList(limit, offset, requestData) {

        var user_id = requestData.user_id;
        let searchData = '';
        let orderData = 'desc';
        let orderBy = 'recharges.created_at';

        var bindingParam = [user_id];
        var q = " SELECT recharge_id,provider_code,recharge_country_code,recharge_country,recharge_operator,name,recharge_mobile_number,recharge_currency,recharge_product_price,recharge_status,recharge_payment_status,transfer_ref, commission_value, recharge_for,"
            + " recharge_type,recharges.created_at"
            + " FROM recharges"
            + " LEFT JOIN users usr on usr.user_id = recharges.user_id"
            + " WHERE recharges.user_id = ?";

        if (requestData["search[value]"] != undefined && requestData["search[value]"] != "") {
            searchData = requestData["search[value]"].toLowerCase();
        }


        if (requestData.rechargeStatus != undefined && requestData.rechargeStatus != '') {
            q += " AND recharges.recharge_status = ? "
            bindingParam.push(requestData.rechargeStatus);
        }

        if ((requestData.searchByFromdate != undefined && requestData.searchByFromdate != '') && (requestData.searchByTodate != undefined && requestData.searchByTodate != '')) {
            q += " AND ( DATE(recharges.created_at) BETWEEN ? and ? )"
            bindingParam.push(requestData.searchByFromdate);
            bindingParam.push(requestData.searchByTodate);
        }

        if (searchData) {
            q += " AND recharge_mobile_number LIKE ? OR transfer_ref LIKE ? "
            bindingParam.push("%" + searchData + "%");
            bindingParam.push("%" + searchData + "%");
        }

        if (q) {
            q += " ORDER BY recharges.created_at desc OFFSET ? LIMIT ? ";
            bindingParam.push(offset);
            bindingParam.push(limit);
        }


        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    getRechargeListCount(req) {
        let searchData = '';
        var bindingParam = [req.user_id];

        var q = ' SELECT count( recharge_mobile_number )as count'
            + ' FROM recharges'
            + ' WHERE user_id = ?';

        if (req["search[value]"] != undefined && req["search[value]"] != "") {
            searchData = req["search[value]"].toLowerCase();
        }

        if (req.rechargeStatus != undefined && req.rechargeStatus != '') {
            q += " AND recharges.recharge_status = ? "
            bindingParam.push(req.rechargeStatus);
        }

        if ((req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
            q += ' AND ( DATE(recharges.created_at) BETWEEN ? and ? )'
            bindingParam.push(req.searchByFromdate);
            bindingParam.push(req.searchByTodate);
        }

        if (searchData) {
            q += ' AND recharge_mobile_number LIKE ? OR transfer_ref LIKE ?';
            bindingParam.push("%" + searchData + "%");
            bindingParam.push("%" + searchData + "%");
        }
           return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;
            return result;
        })
    }

    getrechargeDeveloperApiCount(req, userId) {
        let searchData = '';
        var bindingParam = [userId];
        var q = ' SELECT count(dav_recharge_id )as count'
            + ' FROM developer_recharge'
            + ' WHERE developer_id = ?';

        if (req["search[value]"] != undefined && req["search[value]"] != "") {
            searchData = req["search[value]"].toLowerCase();
        }

        if (req.rechargeStatus != undefined && req.rechargeStatus != '') {
            q += " AND dev_recharge_status = ? "
            bindingParam.push(req.rechargeStatus);
        }

        if ((req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
            q += ' AND ( DATE( created_at ) BETWEEN ? and ? )'
            bindingParam.push(req.searchByFromdate);
            bindingParam.push(req.searchByTodate);
        }

        if (searchData) {
            q += ' AND account_number LIKE ? OR transfer_ref LIKE ? ';
            bindingParam.push("%" + searchData + "%");
            bindingParam.push("%" + searchData + "%");
        }


        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;
            return result;
        })
    }

    getrechargeDeveloperApiHistory(limit, offset, requestData, userId) {
        let searchData = '';
        let orderData = 'desc';
        let orderBy = 'recharges.created_at';
        var bindingParam = [userId];

        var q = " SELECT * FROM developer_recharge"
            + " WHERE developer_id = ?";

        if (requestData["search[value]"] != undefined && requestData["search[value]"] != "") {
            searchData = requestData["search[value]"].toLowerCase();
        }

        if (requestData.rechargeStatus != undefined && requestData.rechargeStatus != '') {
            q += " AND dev_recharge_status = ? "
            bindingParam.push(requestData.rechargeStatus);
        }

        if ((requestData.searchByFromdate != undefined && requestData.searchByFromdate != '') && (requestData.searchByTodate != undefined && requestData.searchByTodate != '')) {
            q += " AND ( DATE( created_at ) BETWEEN ? and ? )"
            bindingParam.push(requestData.searchByFromdate);
            bindingParam.push(requestData.searchByTodate);
        }

        if (searchData) {
            q += " AND account_number LIKE ? OR transfer_ref LIKE ? "
            bindingParam.push("%" + searchData + "%");
            bindingParam.push("%" + searchData + "%");
        }

        if (q) {
            q += " ORDER BY created_at desc OFFSET ? LIMIT ? ";
            bindingParam.push(offset);
            bindingParam.push(limit);
        }


        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    rechargeDeveloperApiDetail(req) {

        var dev_recharge_id = req.recharge_id;

        var bindingParam = [dev_recharge_id];
        var q = ' SELECT * FROM developer_recharge'
            + ' WHERE dav_recharge_id = ? ';



        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    getUserRechargeHistoryCount(req) {
        const id = Security.decryptId(req.view_user_id);
        let searchData = '';
        var bindingParam = [id];
        var q = ' SELECT count( recharge_mobile_number )as count'
            + ' FROM recharges'
            + ' WHERE user_id = ?';


        if (req["search[value]"] != undefined && req["search[value]"] != "") {
            searchData = req["search[value]"].toLowerCase();
        }

        if (req.rechargeStatus != undefined && req.rechargeStatus != '') {
            q += " AND recharges.recharge_status = ? "
            bindingParam.push(req.rechargeStatus);
        }

        if ((req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
            q += " AND ( DATE(recharges.created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/New_York') BETWEEN ? and ? )"
            bindingParam.push(req.searchByFromdate);
            bindingParam.push(req.searchByTodate);
        }

        if (searchData) {
            q += ' AND recharge_mobile_number LIKE ? OR transfer_ref LIKE ?';
            bindingParam.push("%" + searchData + "%");
            bindingParam.push("%" + searchData + "%");
        }



        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;
            return result;
        })
    }

    getUserRechargeHistory(limit, offset, req) {

        let searchData = '';
        let orderData = 'desc';
        let orderBy = 'recharges.created_at';
        const id = Security.decryptId(req.view_user_id);

        var bindingParam = [id];
        var q = " SELECT recharge_id,recharge_operator,name,provider_code,recharge_mobile_number,recharge_country_code,recharge_currency,recharge_product_price,recharge_status,recharge_payment_status, transfer_ref,"
            + " recharges.created_at"
            + " FROM recharges"
            + " LEFT JOIN users usr on usr.user_id = recharges.user_id"
            + " WHERE recharges.user_id = ?";

        if (req["search[value]"] != undefined && req["search[value]"] != "") {
            searchData = req["search[value]"].toLowerCase();
        }

        if (req.rechargeStatus != undefined && req.rechargeStatus != '') {
            q += " AND recharges.recharge_status = ? "
            bindingParam.push(req.rechargeStatus);
        }

        if ((req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
            q += " AND ( DATE(recharges.created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/New_York') BETWEEN ? and ? )"
            bindingParam.push(req.searchByFromdate);
            bindingParam.push(req.searchByTodate);
        }

        if (searchData) {
            q += " AND recharge_mobile_number LIKE ? OR transfer_ref LIKE ? "
            bindingParam.push("%" + searchData + "%");
            bindingParam.push("%" + searchData + "%");
        }

        if (q) {
            q += " ORDER BY recharges.created_at desc OFFSET ? LIMIT ? ";
            bindingParam.push(offset);
            bindingParam.push(limit);
        }



        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    gatUserRechargeDetailByAdmin(req) {

        var recharge_id = req.recharge_id;

        var bindingParam = [recharge_id];
        var q = ' SELECT * FROM recharges'
            + ' WHERE recharge_id = ? ';



        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    getdeveloperRechargeHistoryByAdminCount(req, userId) {
        let searchData = '';
        var bindingParam = [userId];
        var q = ' SELECT count(dav_recharge_id )as count'
            + ' FROM developer_recharge'
            + ' WHERE developer_id = ?';

        if (req["search[value]"] != undefined && req["search[value]"] != "") {
            searchData = req["search[value]"].toLowerCase();
        }

        if (req.rechargeStatus != undefined && req.rechargeStatus != '') {
            q += " AND dev_recharge_status = ? "
            bindingParam.push(req.rechargeStatus);
        }

        if ((req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
            q += " AND ( DATE( created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/New_York') BETWEEN ? and ? )"
            bindingParam.push(req.searchByFromdate);
            bindingParam.push(req.searchByTodate);
        }

        if (searchData) {
            q += ' AND account_number LIKE ? OR transfer_ref LIKE ? ';
            bindingParam.push("%" + searchData + "%");
            bindingParam.push("%" + searchData + "%");
        }



        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;
            return result;
        })
    }

    getdeveloperRechargeHistoryByAdmin(limit, offset, requestData, userId) {
        let searchData = '';
        let orderData = 'desc';
        let orderBy = 'recharges.created_at';
        var bindingParam = [userId];

        var q = " SELECT * FROM developer_recharge"
            + " WHERE developer_id = ?";

        if (requestData["search[value]"] != undefined && requestData["search[value]"] != "") {
            searchData = requestData["search[value]"].toLowerCase();
        }

        if (requestData.rechargeStatus != undefined && requestData.rechargeStatus != '') {
            q += " AND dev_recharge_status = ? "
            bindingParam.push(requestData.rechargeStatus);
        }

        if ((requestData.searchByFromdate != undefined && requestData.searchByFromdate != '') && (requestData.searchByTodate != undefined && requestData.searchByTodate != '')) {
            q += " AND ( DATE( created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/New_York' ) BETWEEN ? and ? )"
            bindingParam.push(requestData.searchByFromdate);
            bindingParam.push(requestData.searchByTodate);
        }

        if (searchData) {
            q += " AND account_number LIKE ? OR transfer_ref LIKE ? "
            bindingParam.push("%" + searchData + "%");
            bindingParam.push("%" + searchData + "%");
        }

        if (q) {
            q += " ORDER BY created_at desc OFFSET ? LIMIT ? ";
            bindingParam.push(offset);
            bindingParam.push(limit);
        }


        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    getTotalRechargeAmount(requestData, currentDateTime) {
        var bindingParam = [requestData.user_id, currentDateTime];
        var q = " SELECT sum(recharge_product_price) AS total_recharge_amount FROM recharges"
            + " WHERE user_id = ? AND DATE(created_at) = ?";



        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    getTotalDeveloperRechargeAmount(requestData, currentDateTime) {
        var bindingParam = [requestData.user_id, currentDateTime];
        var q = " SELECT sum(send_value) AS total_developer_recharge_amount FROM developer_recharge"
            + " WHERE developer_id = ? AND DATE(created_at) = ?";



        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    getTotalRechargeLimit(requestData) {
        var bindingParam = [requestData.user_id];
        var q = " SELECT recharge_limit_amount FROM recharge_limit"
            + " WHERE user_id = ? ";



        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    getDeveloperRechargeAmount(requestData, currentDateTime) {
        var bindingParam = [requestData.developer_id, currentDateTime];
        var q = " SELECT sum(send_value) AS total_developer_recharge_amount FROM developer_recharge"
            + " WHERE developer_id = ? AND DATE(created_at) = ?";



        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    getDeveloperRechargeLimit(requestData) {
        var bindingParam = [requestData.developer_id];
        var q = " SELECT recharge_limit_amount FROM recharge_limit"
            + " WHERE user_id = ? ";

        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }

    getdeveloperRechargeDetailByAdmin(req) {

        var recharge_id = req.recharge_id;

        var bindingParam = [recharge_id];
        var q = ' SELECT * FROM developer_recharge'
            + ' WHERE dav_recharge_id = ? ';



        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;

            return result;
        })
    }


     /**
     * Get a collection of models matching a given query.
     *
     * @param {Object} query The query to match against.
     * @returns {Array} An array holding resultant models.
     */
    fetchPramotionsContinet( query = {} ) {
        return knex( "qva_continent" )
            .select()
            .where( Util.toSnake( query ) )
            .map( ( res ) => {
                return res;
            } );
    }
}

