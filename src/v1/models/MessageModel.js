import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";

const knex = knexJs( knexConfig );

/**
 * Class representing a message model.
 * @class
 */
export default class MessageModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     */
    constructor( opts ) {
        super( opts );
        this.table = "messages";
        this._hasTimestamps = true;
    }

    /**
     * Get message list with reciever detail by sender Id.
     *
     * @param {Object} userId The query to match WhereId.
     * @param {string} limit The query to match against.
     * @param {string} offset The query to match against.
     * @param {string} requestData The query to match LIKE.
     * @returns {Promise} Will return promise.
     */
    getUserMessageHistoryList( userId, limit, offset, requestData ) {
        const searchData = requestData.search,
            rawSelect = "t1.*",
            queryString1 = "(SELECT DISTINCT ON(ms.msg_contact_id) contact_id, ct.contact_country_code, ct.contact_number, ms.msg_text, ms.created_at FROM messages as ms INNER JOIN contacts as ct on ct.contact_id = ms.msg_contact_id WHERE ms.msg_user_id = ",
            queryString2 = " order by ms.msg_contact_id desc, ms.created_at DESC ) as t1",
            mainQuery = queryString1 + userId + queryString2;

        let prepareQuery = knex( knex.raw( mainQuery ) )
            .select( knex.raw( rawSelect ) )
            .limit( limit )
            .offset( offset )
            .orderByRaw( "t1.created_at DESC" );

        if( searchData ) {
            prepareQuery = prepareQuery.andWhereRaw( `t1.contact_number LIKE '%${searchData}%'` );
        }


        prepareQuery = prepareQuery.map( ( res ) => {
            return res;
        } );
    
        return prepareQuery;
    }

    /**
     * Get message list with reciever detail by sender Id.
     *
     * @param {Object} where The query to match against.
     * @param {Object} opts The query to match against.
     * @param {string} limit The query to match against.
     * @param {string} offset The query to match against.
     * @returns {Promise} Will return promise.
     */
    getMessageHistoryDetail( where = {}, opts = {}, limit, offset ) {
        let prepareQuery = knex( this.table )
            .select( opts )
            .limit( limit )
            .offset( offset )
            .where( where )
            .orderBy( "created_at", "DESC" );

        prepareQuery = prepareQuery.map( ( res ) => {
            return res;
        } );
    
        return prepareQuery;
    }



    /**
     * Get number of message per month for a user.
     *
     * @param {Object} userId The query to match WhereId.
     * 
     * @returns {Promise} Will return promise.
     */
    getUserMessageMonthList( userId ) {
        var bindingParam = [userId];
        var q = 'SELECT msg_user_id'
        +' FROM messages'
        +' WHERE  msg_user_id  = ? And  extract(month from created_at) = extract(month from NOW()) And  extract(year from created_at) = extract(year from NOW()) ';
          
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
             
             return result.length;
            });
    }

}
