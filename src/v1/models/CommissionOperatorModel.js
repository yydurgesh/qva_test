import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import { Security } from "~/libraries/Security";

const knex = knexJs( knexConfig );

/**
 * Class representing a message model.
 * @class
 */
export default class CommissionOperatorModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     */
    constructor( opts ) {
        super( opts );
        this.table = "commission_operator_setting";
        this._hasTimestamps = true;
    }

     getCommissionOperatorCount( req ){
       // if(req.userType ==4){
            var id = req.view_user_id

        // }else{
        //   var id = Security.decryptId( req.view_user_id );
        // }
       
       var bindingParam = [id,req.userType,req.country_id];
       var q = ' SELECT count( DISTINCT cos_id)as count'
             + ' FROM commission_operator_setting'
             + ' WHERE cos_user_id = ? AND cos_user_type = ? AND country_id = ?'; 

            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
            return result;
            })

    }
    getCommissionOperator( req ) {
        //if(req.userType ==4){
            var id = req.view_user_id

        // }else{
        //   var id = Security.decryptId( req.view_user_id );
        // }
       
       var bindingParam =  [id,req.userType,req.country_id];

        if (req.page) {
               var page = Number(req.page)*Number(req.limit);
        } else {
              var page=0;
        }
        if (req.limit) {
              var limit = Number(req.limit);
        } else {
                var limit=10;
        }

 
       var q = ' SELECT cos_id,cos_commission,operator_id,cos_user_type,operator_name'
             + ' FROM commission_operator_setting'
             + ' WHERE cos_user_id = ? AND cos_user_type = ?  AND country_id = ?';

            // if(req.search){
            //     q += ' AND recharge_mobile_number LIKE ? ';
            //        bindingParam.push("%" + req.search + "%");
                   
            // }
            if( q ) {
                 q +=  'ORDER BY cos_id OFFSET ? LIMIT ? ';
                 bindingParam.push(page); 
                 bindingParam.push(limit);
            } 
            
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
            
             return result;
            })
    }
}
