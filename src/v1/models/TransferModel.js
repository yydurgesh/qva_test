import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
const knex = knexJs( knexConfig );

/**
 * Class representing a message model.
 * @class
 */
export default class TransferModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     */
    constructor( opts ) {
        super( opts );
        this.table = "transfers";
        this._hasTimestamps = true;
    }

    /**
    * Get call list with reciever detail by sender Id.
    *
    * @param {Object} userId The query to match against.
    * @param {string} limit The query to match against.
    * @param {string} offset The query to match against.
    * @param {string} requestData The query to match LIKE.
    * @returns {Promise} Will return promise.
    */
    getTransferHistory( req ) {
       
    var user_id      = req.user_id;
    if (req.page) {
           var page = Number(req.page)*Number(req.limit);
    } else {
          var page=0;
    }
    if (req.limit) {
          var limit = Number(req.limit);
    } else {
            var limit=10;
    }

   
       var bindingParam = [req.transfer_type,user_id];
       var q = ' SELECT transfer_id,transfer_to_user_id,name,country_code,contact_number,transfer_by_user_id,transfer_amount,recipient_name,'
             + ' recipient_contact_no,address,transfer_status,transfers.created_at'
             + ' FROM transfers'
             + ' LEFT JOIN users usr on usr.user_id = transfer_to_user_id' 
             + ' WHERE transfers.transfer_type = ? AND transfer_by_user_id = ?';

            if(req.search && req.transfer_type ==1){
                q += 
                    ' AND (usr.name LIKE ? '
                   +' OR usr.contact_number LIKE ?) ';
                                          
                    bindingParam.push("%" + req.search + "%");
                    bindingParam.push("%" + req.search + "%");
                    
            }
            if(req.search && req.transfer_type ==2){
                q += 
                    ' AND (recipient_name LIKE ? '
                   +' OR recipient_contact_no LIKE ?) ';
                                          
                    bindingParam.push("%" + req.search + "%");
                    bindingParam.push("%" + req.search + "%");
                    
            } 
            
            if( q ) {
                 q += ' ORDER BY transfers.created_at DESC OFFSET ? LIMIT ? ';
                 bindingParam.push(page); 
                 bindingParam.push(limit);
            } 
           
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
            
             return result;
            })
    }
    getTransferHistoryCount( req ){
        var user_id      = req.user_id;
        var bindingParam = [req.transfer_type,user_id];
       var q = ' SELECT count(transfer_id)'
             + ' FROM transfers'
             + ' LEFT JOIN users usr on usr.user_id = transfer_to_user_id' 
             + ' WHERE transfers.transfer_type = ? AND transfer_by_user_id = ?';

            if(req.search && req.transfer_type ==1){
                q += 
                    ' AND (usr.name LIKE ? '
                   +' OR usr.contact_number LIKE ?) ';
                                          
                    bindingParam.push("%" + req.search + "%");
                    bindingParam.push("%" + req.search + "%");
                    
            }
            if(req.search && req.transfer_type ==2){
                q += 
                    ' AND (recipient_name LIKE ? '
                   +' OR recipient_contact_no LIKE ?) ';
                                          
                    bindingParam.push("%" + req.search + "%");
                    bindingParam.push("%" + req.search + "%");
                    
            } 
      
          
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
            
             return result;
            })
    }
    transferList( req ){
    
    let searchData = '';
    let orderData = 'DESC';
    let orderBy = 'created_at';
    
    var page = req.start;
    // if (req.page) {
    //        var page = Number(req.page)*Number(req.limit);
    // } else {
    //       var page=0;
    // }
    if (req.limit) {
          var limit = Number(req.limit);
    } else {
            var limit=10;
    }
    var bindingParam = [req.transfer_type];
    var q = ' SELECT transfer_id,transfer_to_user_id,name,country_code,contact_number,transfer_by_user_id,transfer_amount,recipient_name,'
             + ' recipient_contact_no,address,transfer_status,transfers.created_at, wallet_id'
             + ' FROM transfers'
             + ' LEFT JOIN users usr on usr.user_id = transfer_by_user_id' 
             + ' LEFT JOIN wallets wallet on wallet.wallet_user_id = transfer_by_user_id'
             + ' WHERE transfers.transfer_type = ?';

            if( req.tranferStatus != undefined ) {
                q += ' AND transfer_status = ?';
                    bindingParam.push(req.tranferStatus);
            }

            if( (req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
                q += " AND ( DATE(transfers.created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/New_York' ) BETWEEN ? and ? )"
                bindingParam.push(req.searchByFromdate);
                bindingParam.push(req.searchByTodate);
            }

            if( req[ "search[value]" ] != undefined && req[ "search[value]" ] != "" && req.transfer_type == 1 ) {
                searchData = req[ "search[value]" ].toLowerCase();
                q += 
                    ' AND (usr.name LIKE ? '
                   +' OR usr.contact_number LIKE ?) ';
                                          
                    bindingParam.push("%" + searchData + "%");
                    bindingParam.push("%" + searchData + "%");
            }

            if( req[ "search[value]" ] != undefined && req[ "search[value]" ] != "" && req.transfer_type == 2 ) {
                searchData = req[ "search[value]" ].toLowerCase();
                q += 
                    ' AND (usr.name LIKE ? '
                   +' OR usr.contact_number LIKE ?) ';
                                          
                    bindingParam.push("%" + searchData + "%");
                    bindingParam.push("%" + searchData + "%");
            }

            
            // if(req.search && req.transfer_type ==1){
            //     q += 
            //         ' AND (usr.name LIKE ? '
            //        +' OR usr.contact_number LIKE ?) ';
                                          
            //         bindingParam.push("%" + req.search + "%");
            //         bindingParam.push("%" + req.search + "%");
                    
            // }
            // if(req.search && req.transfer_type == 2){
            //     q += 
            //         ' AND (recipient_name LIKE ? '
            //        +' OR recipient_contact_no LIKE ?) ';
                                          
            //         bindingParam.push("%" + req.search + "%");
            //         bindingParam.push("%" + req.search + "%");
                    
            // }
            if(req.transfer_type == 1) { 
                if( ( req[ "order[0][dir]" ] === "asc" || req[ "order[0][dir]" ] === "desc" ) && req[ "order[0][column]" ] === "1" && req.transfer_type == 1) {
                    if( req[ "search[value]" ] !== undefined ) {
                        searchData = req[ "search[value]" ];
                        
                    } 
                    orderData = req[ "order[0][dir]" ];
                    orderBy = "usr.name";
                    q += ` ORDER BY ${orderBy} ${orderData}`;
        
                } else if( ( req[ "order[0][dir]" ] === "asc" || req[ "order[0][dir]" ] === "desc" ) && req[ "order[0][column]" ] === "2" && req.transfer_type == 1) {
                    if( req[ "search[value]" ] !== undefined ) {
                        searchData = req[ "search[value]" ];
                        
                    }
                    orderData = req[ "order[0][dir]" ];
                    orderBy = "usr.contact_number";
                    q += ` ORDER BY ${orderBy} ${orderData}`;

                } else if( ( req[ "order[0][dir]" ] === "asc" || req[ "order[0][dir]" ] === "desc" ) && req[ "order[0][column]" ] === "3" && req.transfer_type == 1) {
                    if( req[ "search[value]" ] !== undefined ) {
                        searchData = req[ "search[value]" ];
                        
                    }
                    orderData = req[ "order[0][dir]" ];
                    orderBy = "transfers.transfer_amount";
                    q += ` ORDER BY ${orderBy} ${orderData}`;

                } else if( ( req[ "order[0][dir]" ] === "asc" || req[ "order[0][dir]" ] === "desc" ) && req[ "order[0][column]" ] === "4" && req.transfer_type == 1) {
                    if( req[ "search[value]" ] !== undefined ) {
                        searchData = req[ "search[value]" ];
                        
                    }
                    orderData = req[ "order[0][dir]" ];
                    orderBy = "transfers.transfer_status";
                    q += ` ORDER BY ${orderBy} ${orderData}`;

                } else {
                    q += ` ORDER BY ${orderBy} ${orderData}`;
                }
            }
            
            if( req.transfer_type == 2 ) {
                if( ( req[ "order[0][dir]" ] === "asc" || req[ "order[0][dir]" ] === "desc" ) && req[ "order[0][column]" ] === "1" && req.transfer_type == 2) {
                    if( req[ "search[value]" ] !== undefined ) {
                        searchData = req[ "search[value]" ];
                        
                    } 
                    orderData = req[ "order[0][dir]" ];
                    orderBy = "usr.name";
                    q += ` ORDER BY ${orderBy} ${orderData}`;
        
                } else if( ( req[ "order[0][dir]" ] === "asc" || req[ "order[0][dir]" ] === "desc" ) && req[ "order[0][column]" ] === "2" && req.transfer_type == 2) {
                    if( req[ "search[value]" ] !== undefined ) {
                        searchData = req[ "search[value]" ];
                        
                    }
                    orderData = req[ "order[0][dir]" ];
                    orderBy = "transfers.recipient_name";
                    q += ` ORDER BY ${orderBy} ${orderData}`;

                } else if( ( req[ "order[0][dir]" ] === "asc" || req[ "order[0][dir]" ] === "desc" ) && req[ "order[0][column]" ] === "3" && req.transfer_type == 2) {
                    if( req[ "search[value]" ] !== undefined ) {
                        searchData = req[ "search[value]" ];
                        
                    }
                    orderData = req[ "order[0][dir]" ];
                    orderBy = "usr.contact_number";
                    q += ` ORDER BY ${orderBy} ${orderData}`;

                } else if( ( req[ "order[0][dir]" ] === "asc" || req[ "order[0][dir]" ] === "desc" ) && req[ "order[0][column]" ] === "4" && req.transfer_type == 2) {
                    if( req[ "search[value]" ] !== undefined ) {
                        searchData = req[ "search[value]" ];
                        
                    }
                    orderData = req[ "order[0][dir]" ];
                    orderBy = "transfers.recipient_contact_no";
                    q += ` ORDER BY ${orderBy} ${orderData}`;

                } else if( ( req[ "order[0][dir]" ] === "asc" || req[ "order[0][dir]" ] === "desc" ) && req[ "order[0][column]" ] === "5" && req.transfer_type == 2) {
                    if( req[ "search[value]" ] !== undefined ) {
                        searchData = req[ "search[value]" ];
                        
                    }
                    orderData = req[ "order[0][dir]" ];
                    orderBy = "transfers.transfer_amount";
                    q += ` ORDER BY ${orderBy} ${orderData}`;

                } else if( ( req[ "order[0][dir]" ] === "asc" || req[ "order[0][dir]" ] === "desc" ) && req[ "order[0][column]" ] === "6" && req.transfer_type == 2) {
                    if( req[ "search[value]" ] !== undefined ) {
                        searchData = req[ "search[value]" ];
                        
                    }
                    orderData = req[ "order[0][dir]" ];
                    orderBy = "transfers.address";
                    q += ` ORDER BY ${orderBy} ${orderData}`;

                } else if( ( req[ "order[0][dir]" ] === "asc" || req[ "order[0][dir]" ] === "desc" ) && req[ "order[0][column]" ] === "7" && req.transfer_type == 2) {
                    if( req[ "search[value]" ] !== undefined ) {
                        searchData = req[ "search[value]" ];
                        
                    }
                    orderData = req[ "order[0][dir]" ];
                    orderBy = "transfers.transfer_status";
                    q += ` ORDER BY ${orderBy} ${orderData}`;

                } else {
                    q += ` ORDER BY ${orderBy} ${orderData}`;
                }
            }

            if( q ) {
                 q += ' OFFSET ? LIMIT ? ';
                 bindingParam.push(page); 
                 bindingParam.push(limit);
            } 
         
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
            
             return result;
            })

    }

    transferListCount( req ){

    let searchData = '';
   
    var bindingParam = [req.transfer_type];
    var q = ' SELECT count(transfer_id)'
             + ' FROM transfers'
             + ' LEFT JOIN users usr on usr.user_id = transfer_to_user_id'
             + ' WHERE transfers.transfer_type = ?';

            if( req.tranferStatus != undefined ) {
                q += ' AND transfer_status = ?';
                    bindingParam.push(req.tranferStatus);
            } 

            if( (req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
                q += " AND ( DATE(transfers.created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/New_York' ) BETWEEN ? and ? )"
                bindingParam.push(req.searchByFromdate);
                bindingParam.push(req.searchByTodate);
            }
            
            if( req[ "search[value]" ] != undefined && req[ "search[value]" ] != "" && req.transfer_type ==1) {
               
                searchData = req[ "search[value]" ].toLowerCase();
                q += 
                    ' AND (usr.name LIKE ? '
                   +' OR usr.contact_number LIKE ?) ';
                                          
                    bindingParam.push("%" + searchData + "%");
                    bindingParam.push("%" + searchData + "%");
            }
            if( req[ "search[value]" ] != undefined && req[ "search[value]" ] != "" && req.transfer_type == 2 ) {
                searchData = req[ "search[value]" ].toLowerCase();
                q += 
                    ' AND (usr.name LIKE ? '
                   +' OR usr.contact_number LIKE ?) ';
                                          
                    bindingParam.push("%" + searchData + "%");
                    bindingParam.push("%" + searchData + "%");
            }
            // if(req.search && req.transfer_type ==1){
            //     q += 
            //         ' AND (usr.name LIKE ? '
            //        +' OR usr.contact_number LIKE ?) ';
                                          
            //         bindingParam.push("%" + req.search + "%");
            //         bindingParam.push("%" + req.search + "%");
                    
            // }
            // if(req.search && req.transfer_type ==2){
            //     q += 
            //         ' AND (recipient_name LIKE ? '
            //        +' OR recipient_contact_no LIKE ?) ';
                                          
            //         bindingParam.push("%" + req.search + "%");
            //         bindingParam.push("%" + req.search + "%");
                    
            // } 
           
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
            
             return result;
            })

    }

    getadminTransferHisttory( req ) {
        if (req.page) {
               var page = Number(req.page)*Number(req.limit);
        } else {
              var page=0;
        }
        if (req.limit) {
              var limit = Number(req.limit);
        } else {
                var limit=10;
        }
        
       var bindingParam = [req.wallet_id,req.transfer_type];
       var q = ' SELECT wt_id, wallet_id, wt_type, wt_amount, wt_currency, wt_reference_id, wt_reference_type,address,latitude,longitude,transfer_status,'
             +' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT name FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN recipient_name END as transfer_to FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2),'
             +' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT country_code FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN \'+53\' END as to_country_code FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2 ),'
             +' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT contact_number FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN recipient_contact_no END as to_contact_no FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2 ),'
             +' ( SELECT transfer_to_user_id FROM transfers WHERE transfer_id = wt_reference_id AND wt_reference_type = 2),'
             + ' (SELECT name  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from,'
             + ' (SELECT country_code  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from_country_code,'
             + ' (SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from_contact_no,'
             + '  wt_status,wallet_transaction.created_at '
             + ' FROM wallet_transaction'
             + ' INNER JOIN transfers trs on trs.transfer_id =wallet_transaction.wt_reference_id';
             if(req.transfer_type == 1){
                if( req.search ){
                   q +=
                       ' INNER JOIN users usr on usr.user_id = trs.transfer_by_user_id'
                     + ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2 AND transfer_type = ?'
                     +' AND usr.contact_number LIKE ? ';
                     bindingParam.push("%" + req.search + "%");
                }else {
                  q +=
                      ' INNER JOIN users usr on usr.user_id = trs.transfer_by_user_id'
                    + ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2 AND transfer_type = ?';
                }
             }
             if(req.transfer_type ==2){
                if( req.search ){
                   q +=
                      ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2 AND transfer_type = ?'
                     +' AND trs.recipient_contact_no LIKE ? ';
                     bindingParam.push("%" + req.search + "%");
                }else {
                  q +=
                     ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2 AND transfer_type = ?';
                }
            }
            // + ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2 AND transfer_type = ?'; 
            
            if( q ) {
                 q += ' ORDER BY wt_id DESC OFFSET ? LIMIT ? ';
                 bindingParam.push(page); 
                 bindingParam.push(limit);
            } 
          
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
            
             return result;
            })
    }

    getadminTransferHisttoryCount( req ){

       var bindingParam = [req.wallet_id,req.transfer_type];
       var q = ' SELECT count(wt_id)'
             + ' FROM wallet_transaction'
             + ' INNER JOIN transfers trs on trs.transfer_id =wallet_transaction.wt_reference_id';
             if(req.transfer_type == 1){
                if( req.search ){
                   q +=
                       ' INNER JOIN users usr on usr.user_id = trs.transfer_by_user_id'
                     + ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2 AND transfer_type = ?'
                     +' AND usr.contact_number LIKE ? ';
                     bindingParam.push("%" + req.search + "%");
                }else {
                  q +=
                      ' INNER JOIN users usr on usr.user_id = trs.transfer_by_user_id'
                    + ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2 AND transfer_type = ?';
                }
             }
             if(req.transfer_type ==2){
                if( req.search ){
                   q +=
                      ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2 AND transfer_type = ?'
                     +' AND trs.recipient_contact_no LIKE ? ';
                     bindingParam.push("%" + req.search + "%");
                }else {
                  q +=
                     ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2 AND transfer_type = ?';
                }
            }
             //+ ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2 AND transfer_type = ?'; 
                 
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
            
             return result;
            })
    }

    
    getTransferHisttoryCount( req ){
        let searchData = '';

        var bindingParam = [req.wallet_id];
        var q = ' SELECT count(wt_id)'
              + ' FROM wallet_transaction'
              + ' INNER JOIN transfers trs on trs.transfer_id =wallet_transaction.wt_reference_id';
              
                if( req[ "search[value]" ] != undefined && req[ "search[value]" ] != "" ) {
                    searchData = req[ "search[value]" ].toLowerCase();
                }

                if( req.status == 1 ) {
                    q +=
                        ' INNER JOIN users usr on usr.user_id = trs.transfer_by_user_id'
                    + ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2 AND transfer_type = ?';
                    bindingParam.push(req.status);
                  } else if( req.status == 2 ) {
                    q +=
                        ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2 AND transfer_type = ?';
                        bindingParam.push(req.status); 
                  } else {
                    q += ' INNER JOIN users usr on usr.user_id = trs.transfer_by_user_id'
                        + ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2';
                  }

                if( (req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
                    q += " AND ( DATE(wallet_transaction.created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/New_York') BETWEEN ? and ? )"
                    bindingParam.push(req.searchByFromdate);
                    bindingParam.push(req.searchByTodate);
                }

                if( searchData ){
                    if( req.status == 1 ) {
                       q += ' AND usr.contact_number LIKE ? ';
                        bindingParam.push("%" + searchData + "%");
                    } else if( req.status == 2 ) {
                      q += ' AND trs.recipient_contact_no LIKE ? ';
                      bindingParam.push("%" + searchData + "%");
                    } else {
                      q += ' AND usr.contact_number LIKE ? AND trs.recipient_contact_no LIKE ?';
                      bindingParam.push("%" + searchData + "%");
                      bindingParam.push("%" + searchData + "%");
                    }
                }
            
             return  knex.raw(q, bindingParam).then(( res ) => {
              var result = res.rows;
             
              return result;
             })
    }
    
    getTransferHisttory( req, offset, limit ) {
        let searchData = '';
        let orderData = 'desc';
        let orderBy = 'wallet_transaction.created_at';
       var bindingParam = [req.wallet_id];
       var q = ' SELECT wt_id, wallet_id, wt_type, wt_amount, wt_currency, wt_reference_id, wt_reference_type,address,latitude,longitude,transfer_status,'
             +' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT name FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN recipient_name END as transfer_to FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2),'
             +' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT country_code FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN \'+53\' END as to_country_code FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2 ),'
             +' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT contact_number FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN recipient_contact_no END as to_contact_no FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2 ),'
             +' ( SELECT transfer_to_user_id FROM transfers WHERE transfer_id = wt_reference_id AND wt_reference_type = 2),'
             + ' (SELECT name  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from,'
             + ' (SELECT country_code  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from_country_code,'
             + ' (SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from_contact_no,'
             + '  wt_status,wallet_transaction.created_at '
             + ' FROM wallet_transaction'
             + ' INNER JOIN transfers trs on trs.transfer_id =wallet_transaction.wt_reference_id';
            
            if( req[ "search[value]" ] != undefined && req[ "search[value]" ] != "" ) {
                searchData = req[ "search[value]" ].toLowerCase();
            }

            if( req.status == 1 ) {
                q +=
                    ' INNER JOIN users usr on usr.user_id = trs.transfer_by_user_id'
                    + ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2 AND transfer_type = ?';
                    bindingParam.push(req.status);
                  
            } else if( req.status == 2 ) {
                q +=
                ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2 AND transfer_type = ?';
                bindingParam.push(req.status); 
            
            } else {
                q += ' INNER JOIN users usr on usr.user_id = trs.transfer_by_user_id'
                    + ' WHERE wallet_transaction.wallet_id = ? AND wt_reference_type = 2';
            }

            if( (req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
                q += " AND ( DATE(wallet_transaction.created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/New_York') BETWEEN ? and ? )"
                bindingParam.push(req.searchByFromdate);
                bindingParam.push(req.searchByTodate);
            }

            if( searchData ){
                if( req.status == 1 ) {
                   q += ' AND usr.contact_number LIKE ? ';
                    bindingParam.push("%" + searchData + "%");
                } else if( req.status == 2 ) {
                  q += ' AND trs.recipient_contact_no LIKE ? ';
                  bindingParam.push("%" + searchData + "%");
                } else {
                  q += ' AND usr.contact_number LIKE ? AND trs.recipient_contact_no LIKE ?';
                  bindingParam.push("%" + searchData + "%");
                  bindingParam.push("%" + searchData + "%");
                }
            }

            if( q ) {
                q +=  " ORDER BY wallet_transaction.created_at desc OFFSET ? LIMIT ? ";
               //  bindingParam.push(orderBy); 
               //  bindingParam.push(orderData); 
                bindingParam.push(offset); 
                bindingParam.push(limit);
            }
                        
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
            
             return result;
            })
    }

    
    getTransferListByAdminCount( req ){
        let searchData = '';
        var walletId = req.wallet_id;
        var bindingParam = [];
        // var bindingParam = [req.wallet_id];
        var q = ' SELECT count(wt_id)'
              + ' FROM wallet_transaction'
              + ' INNER JOIN transfers trs on trs.transfer_id =wallet_transaction.wt_reference_id';
              
                if( req[ "search[value]" ] != undefined && req[ "search[value]" ] != "" ) {
                    searchData = req[ "search[value]" ].toLowerCase();
                }

                if( req.status == 1 ) {
                    q +=
                        ' INNER JOIN users usr on usr.user_id = trs.transfer_by_user_id'
                        + ' WHERE wt_reference_type = 2 AND transfer_type = ?';
                        bindingParam.push(req.status);
                } else if( req.status == 2 ) {
                    q +=
                    ' WHERE wt_reference_type = 2 AND transfer_type = ?';
                    bindingParam.push(req.status); 
                } else {
                    q += ' INNER JOIN users usr on usr.user_id = trs.transfer_by_user_id'
                        + ' WHERE wt_reference_type = 2';
                }

                if( walletId != undefined ) {
                    q +=  ' AND wallet_transaction.wallet_id = ?'
                    bindingParam.push( walletId );
                } 

                if( req.userType != undefined ) {
                    q += ' AND wallet_id IN (select wallet_id from wallets inner join users on users.user_id = wallets.wallet_user_id and user_type = ? )'
                    bindingParam.push( req.userType );
                }
                
                if( (req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
                    q += " AND ( DATE(wallet_transaction.created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/New_York') BETWEEN ? and ? )"
                    bindingParam.push(req.searchByFromdate);
                    bindingParam.push(req.searchByTodate);
                }

                if( searchData ){
                    if( req.status == 1 ) {
                       q += ' AND usr.contact_number LIKE ? ';
                        bindingParam.push("%" + searchData + "%");
                    } else if( req.status == 2 ) {
                      q += ' AND trs.recipient_contact_no LIKE ? ';
                      bindingParam.push("%" + searchData + "%");
                    } else {
                      q += ' AND usr.contact_number LIKE ? AND trs.recipient_contact_no LIKE ?';
                      bindingParam.push("%" + searchData + "%");
                      bindingParam.push("%" + searchData + "%");
                    }
                }
          
             return  knex.raw(q, bindingParam).then(( res ) => {
              var result = res.rows;
             
              return result;
             })
    }

    getTransferListByAdmin( req, offset, limit ) {
        let searchData = '';
        let orderData = 'desc';
        let orderBy = 'wallet_transaction.created_at';
        var walletId = req.wallet_id;
    //    var bindingParam = [req.wallet_id];
        var bindingParam = [];
       var q = ' SELECT wt_id, wallet_id, wt_type, wt_amount, wt_currency, wt_reference_id, wt_reference_type,address,latitude,longitude,transfer_status,'
             +' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT name FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN recipient_name END as transfer_to FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2),'
             +' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT name FROM users WHERE user_id = transfer_by_user_id ) WHEN transfer_type = 2 THEN recipient_name END as transfer_by FROM transfers WHERE transfer_id = wt_reference_id AND wt_type = 2 AND wt_reference_type = 2),'
             +' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT country_code FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN \'+53\' END as to_country_code FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2 ),'
             +' ( SELECT CASE WHEN transfer_type = 1 THEN ( SELECT contact_number FROM users WHERE user_id = transfer_to_user_id ) WHEN transfer_type = 2 THEN recipient_contact_no END as to_contact_no FROM transfers WHERE transfer_id = wt_reference_id  AND wt_type = 2 AND wt_reference_type = 2 ),'
             +' ( SELECT transfer_to_user_id FROM transfers WHERE transfer_id = wt_reference_id AND wt_reference_type = 2),'
             +' ( SELECT name FROM users WHERE user_id = ( SELECT transfer_to_user_id FROM transfers WHERE transfer_id = wt_reference_id AND wt_type = 1 AND wt_reference_type = 2 ) ) as receiver,'
             + ' (SELECT name  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from,'
             + ' (SELECT country_code  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from_country_code,'
             + ' (SELECT contact_number  FROM users WHERE user_id = (SELECT transfer_by_user_id FROM transfers WHERE transfer_id =wt_reference_id AND wt_type=1 AND wt_reference_type =2)) as received_from_contact_no,'
             + '  wt_status,wallet_transaction.created_at '
             + ' FROM wallet_transaction'
             + ' INNER JOIN transfers trs on trs.transfer_id =wallet_transaction.wt_reference_id';
            
            if( req[ "search[value]" ] != undefined && req[ "search[value]" ] != "" ) {
                searchData = req[ "search[value]" ].toLowerCase();
            }

            if( req.status == 1 ) {
                q +=
                    ' INNER JOIN users usr on usr.user_id = trs.transfer_by_user_id'
                    + ' WHERE wt_reference_type = 2 AND transfer_type = ?';
                    bindingParam.push(req.status);
                  
            } else if( req.status == 2 ) {
                q +=
                ' WHERE wt_reference_type = 2 AND transfer_type = ?';
                bindingParam.push(req.status); 
            
            } else {
                q += ' INNER JOIN users usr on usr.user_id = trs.transfer_by_user_id'
                    + ' WHERE wt_reference_type = 2';
            }

            if( walletId != undefined ) {
                q +=  ' AND wallet_transaction.wallet_id = ?'
                bindingParam.push( walletId );
            } 

            if( req.userType != undefined ) {
                q += ' AND wallet_id IN (select wallet_id from wallets inner join users on users.user_id = wallets.wallet_user_id and user_type = ? )'
                bindingParam.push( req.userType );
            }

            if( (req.searchByFromdate != undefined && req.searchByFromdate != '') && (req.searchByTodate != undefined && req.searchByTodate != '')) {
                q += " AND ( DATE(wallet_transaction.created_at AT TIME ZONE 'UTC' AT TIME ZONE 'America/New_York') BETWEEN ? and ? )"
                bindingParam.push(req.searchByFromdate);
                bindingParam.push(req.searchByTodate);
            }

            if( searchData ){
                if( req.status == 1 ) {
                   q += ' AND usr.contact_number LIKE ? ';
                    bindingParam.push("%" + searchData + "%");
                } else if( req.status == 2 ) {
                  q += ' AND trs.recipient_contact_no LIKE ? ';
                  bindingParam.push("%" + searchData + "%");
                } else {
                  q += ' AND usr.contact_number LIKE ? AND trs.recipient_contact_no LIKE ?';
                  bindingParam.push("%" + searchData + "%");
                  bindingParam.push("%" + searchData + "%");
                }
            }

            if( q ) {
                q +=  " ORDER BY wallet_transaction.created_at desc OFFSET ? LIMIT ? ";
               //  bindingParam.push(orderBy); 
               //  bindingParam.push(orderData); 
                bindingParam.push(offset); 
                bindingParam.push(limit);
            }
            
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
            
             return result;
            })
    }
    getUserTransferInfo(user_id, startDate, endDate) {
        var bindingParam = [user_id, startDate, endDate];
        var q = ' SELECT count(transfer_by_user_id)as transfer_count,sum(transfer_amount) as transfer_sum'
            + ' FROM transfers'
            + ' WHERE transfer_type =1 AND transfer_by_user_id = ? AND  date(created_at) BETWEEN ? AND ? '
            + ' GROUP BY transfer_by_user_id';

        return knex.raw(q, bindingParam).then((res) => {
            var result = res.rows;
            return result;
        })
    }
}
