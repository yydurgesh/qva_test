import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
const knex = knexJs( knexConfig );

/**
 * Class representing a firebase auth model.
 * @class
 */
export default class FirebaseAuthModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     */
    constructor( opts ) {
        super( opts );
        this.table = "qva_device_auth";
        this._hasTimestamps = true;
    }


    deviceToken( userId, deviceId ) {
  
      //console.log(userId , deviceId)
        var bindingParam = [userId, deviceId];
        var q = ' SELECT *'
              + ' FROM firebase_auth'
              + ' WHERE firebase_auth_user_id = ? AND device_id = ?';
      
        return  knex.raw(q, bindingParam).then(( res ) => {
            var result = res.rows;
          //  console.log("query",q)
            return result;
        })
    }


    createObj1( properties, opts = {} ) {
      let propertie = properties;
   
      if ( this._hasTimestamps ) {
          propertie = this._timestampAppender( propertie, "insert" );
      }

      let c = knex( this.table )
          .insert( Util.toSnake( propertie ), opts );
          console.log(c.toString())
          // .spread( ( res ) => {
          //     return res;
          // } );
  }
    
}