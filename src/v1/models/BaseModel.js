import knexJs from "knex";
import knexConfig from "~/knexfile";
import Util from "~/utils/util";
import DateTimeUtil from "~/utils/DateTimeUtil";
import databaseConstants from "~/constants/databaseConstants";

const knex = knexJs( knexConfig );

/**
 * define base model
 */
class BaseModel {

    /**
    * Define base model constructor.
    */
    constructor() {
        this._hasTimestamps = false;
    }

    /**
     * Get the table used for this model.
     *
     * @returns {string} The database table used.
     */
    static get table() {
        return this._table;
    }

    /**
     * Set the table used for this model.
     *
     * @param {string} t The database table to be used.
     */
    static set table( t ) {
        this._table = t;
    }

    /**
     * Get the hasTimestamps used for this model.
     *
     * @returns {string} The hasTimestamps setting.
     */
    static get hasTimestamps() {
        return this._hasTimestamps;
    }

    /**
     * Set the hasTimestamps used for this model.
     *
     * @param {string} t The hasTimestamps setting.
     */
    static set hasTimestamps( t ) {
        this._hasTimestamps = t;
    }

    /**
     * Get the primary or compound keys used to uniquely identify a model.
     *
     * @returns {array} The columns used to uniquely identify a model.
     */
    static get keys() {
        if ( !this._keys ) {
            this._keys = [];
        }

        return this._keys;
    }

    /**
     * Set the primary or compound keys used to uniquely identify a model.
     *
     * @param {array} k The columns used to uniquely identify a model.
     */
    static set keys( k ) {
        this._keys = k;
    }

    /**
     * Get a all rows from table.
     *
     * @param {Object} query The query to match against.
     */
    fetchAll( query = {} ) {
        return knex( this.table )
            .select()
            .where( Util.toSnake( query ) )
            .map( ( res ) => {
                return res;
            } );
    }

    /**
     * Get a collection of models matching a given query.
     *
     * @param {Object} query The query to match against.
     * @returns {Array} An array holding resultant models.
     */
    fetchObj( query = {} ) {
        return knex( this.table )
            .select()
            .where( Util.toSnake( query ) )
            .map( ( res ) => {
                return res;
            } );
    }

    /**
     * Get a collection of models matching a given query.
     *
     * @param {Object} query The query to match against.
     * @returns {Array} An array holding resultant models.
     */
    fetchFirstObj( query = {} ) {
        return knex( this.table )
            .select()
            .where( Util.toSnake( query ) )
            .first();
    }

    /**
     * Get a collection of models matching a given query.
     *
     * @param {Object} query The query to match against.
     * @param {Object} opts Options.
     * @returns {Array} An array holding resultant models.
     */
    fetchObjWithSingleRecord( query = {}, opts = {} ) {
        return knex( this.table )
            .select( opts )
            .where( Util.toSnake( query ) )
            .first()
            .then( ( row ) => {
                return row;
            } );
       
    }

    /**
     * Get a collection of models matching a given query.
     *
     * @param {Object} query The query to match against.
     * @param {Object} opts Options.
     * @param {Object} orderby
     * @param {Object} order
     * @returns {Array} An array holding resultant models.
     */
    fetchObjWithSelectedFields( query = {}, opts = {}, orderby, order ) {
        let prepareQuery = knex( this.table )
            .select( opts )
            .where( Util.toSnake( query ) );

        if ( orderby !== undefined && order !== undefined ) {
            prepareQuery = prepareQuery.orderBy( orderby, order );
        }
        
        prepareQuery = prepareQuery.map( ( res ) => {
            return res;
        } );

        return prepareQuery;
    }

    /**
     * Get a collection of models matching a given query.
     *
     * @param {Object} query The query to match against.
     * @param {Object} opts Options.
     * @returns {Array} An array holding resultant models.
     */
    fetchObjWithSelectedFieldsOrderBy( query = {}, opts = {} ) {
        return knex( this.table )
            .select( opts )
            .where( Util.toSnake( query ) )
            .orderBy( "package_type", "asc" )
            .map( ( res ) => {
                return res;
            } );
    }

    /**
     * Inserts a new model into the database then returns an instantiation of the model.
     *
     * @param {Object} properties The Model properties.
     * @param {Object} opts Options.
     * @returns {*} An instantiation of the model.
     */
    createObj( properties, opts = {} ) {
        let propertie = properties;
     
        if ( this._hasTimestamps ) {
            propertie = this._timestampAppender( propertie, "insert" );
        }

        return knex( this.table )
            .insert( Util.toSnake( propertie ), opts )
            .spread( ( res ) => {
                return res;
            } );
    }

    /**
     * Saves the properties currently set on the model.
     *
     * @param {Object} properties The properties to update.
     * @param {Object} query Where clause for updating.
     * @param {Object} opts Options for saving.
     * @returns {Array} A collection of the updated models.
     */
    updateObj( properties, query = {}, opts = {} ) {
        let propertie = properties;

        if ( this._hasTimestamps ) {
            propertie = this._timestampAppender( propertie, "update" );
        }

        return knex( this.table )
            .update( Util.toSnake( propertie ), opts )
            .where( Util.toSnake( query ) )
            .map( ( res ) => {
                return res;
            } );
    }

    /**
     * Soft delete the object.
     *
     * @param {Object} properties The properties to update.
     * @param {Object} query Where clause for updating.
     * @param {Object} opts Options for saving.
     * @returns {Array} A collection of the updated models.
     */
    softDeleteObj( properties, query = {}, opts = {} ) {
        const isDELETED = databaseConstants.IS_DELETED;

        let propertie = properties;

        if ( propertie.deleted_by === undefined ) {
            propertie = { "deleted_by": "0", "is_deleted": isDELETED };
        } else {
            propertie = { "deleted_by": propertie.deleted_by, "is_deleted": isDELETED };
        }

        if ( this._hasTimestamps ) {
            propertie = this._timestampAppender( propertie, "delete" );
        }

        return knex( this.table )
            .update( Util.toSnake( propertie ), opts )
            .where( Util.toSnake( query ) )
            .map( ( res ) => {
                return res;
            } );
    }

    /**
     * Saves the properties currently set on the model.
     *
     * @param {Object} query The properties to update.
     * @param {Object} column Where clause for updating.
     * @returns {Array} A collection of the updated models.
     */
    getCount( query = {}, column ) {
        return knex( this.table )
            .count( column )
            .where( Util.toSnake( query ) )
            .map( ( res ) => {
                return res.count;
            } );
      

    }

    /**
     * Add created_at and updated_at timestamp in object.
     *
     * @param {Object} properties The Model properties.
     * @param {String} method Options.
     * @returns {*} An instantiation of the model.
     */
    _timestampAppender( properties, method = "insert" ) {
        const ct = DateTimeUtil.getCurrentTimeObjForDB();

        if ( method === "insert" ) {
            properties.created_at = ct;
        } else if ( method === "delete" ) {
            properties.deleted_at = ct;
        } else {
            properties.updated_at = ct;
        }

        return properties;
    }

    /**
     * Inserts a new model into the database then returns an instantiation of the model.
     *
     * @param {Object} properties The Model properties.
     * @returns {*} An instantiation of the model.
     */
    batchinsertObj( properties ) {
        let propertie = properties;

        if ( this._hasTimestamps ) {
            propertie = this._timestampAppender( propertie, "insert" );
        }

        return knex.batchInsert( this.table, propertie, 1000 ).map( ( res ) => {
            return res.rowCount;
        } );
    }

    /**
     * Inserts a new model into the database then returns an instantiation of the model.
     *
     * @param {Object} properties The Model properties.
     * @param {array} returningId Options.
     * @returns {*} An instantiation of the model.
     */
    batchinsertObjWithIds( properties, returningId ) {
        let propertie = properties;

        if ( this._hasTimestamps ) {
            propertie = this._timestampAppender( propertie, "insert" );
        }


        return knex.batchInsert( this.table, properties, 1000 )
        .returning(returningId)
        .then(function(ids) {
        return ids;
      })
    }

    /**
     * Return count distinct of any column from table.
     *
     * @param {Object} query The properties to update.
     * @param {Object} column Where clause for updating.
     * @returns {Array} A collection of the updated models.
     */
    getCountDistinct( query = {}, column ) {
        return knex( this.table )
            .countDistinct( knex.raw( column ) )
            .where( query )
            .map( ( res ) => {
                return res.count;
            } );
    }
    deleteObj( query = {} ) {
        return knex( this.table )
              .where( query )
              .del()
              .then(function(ids) {
                 return ids;
              })
    }

     /**
     * Get a all rows from table.
     *
     * @param {Object} query The query to match against.
     */
    fetchResults(table, query = {}, orderby, order ) {
        let prepareQuery =  knex( table )
            .select()
            .where( Util.toSnake( query ) );
            console.log("orderby", orderby, order)
            if ( orderby !== undefined && order !== undefined ) {
                prepareQuery = prepareQuery.orderBy( orderby, order );
            }
           
    
            return prepareQuery;

          
    }

 
}
export default BaseModel;
