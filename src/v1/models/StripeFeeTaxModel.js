import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
const knex = knexJs( knexConfig );

/**
 * Class representing a message model.
 * @class
 */
export default class StripeFeeTaxModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     */
    constructor( opts ) {
        super( opts );
        this.table = "stripefee_tax";
        this._hasTimestamps = true;
    }
}