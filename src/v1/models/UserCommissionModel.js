import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import { Security } from "~/libraries/Security";

const knex = knexJs( knexConfig );

/**
 * Class representing a message model.
 * @class
 */
export default class UserCommissionModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     */
    constructor( opts ) {
        super( opts );
        this.table = "commission_setting";
        this._hasTimestamps = true;
    }


   
}
