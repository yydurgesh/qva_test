import BaseModel from "./BaseModel";
import knexJs from "knex";
import knexConfig from "~/knexfile";
import Util from "~/utils/util";
const knex = knexJs( knexConfig );

/**
 * Class representing a user model.
 * @class
 */
export default class UserModel extends BaseModel {
    /**
     * Constructor.
     *
     * @param  {Object}  opts
     * @returns {Promise}
     */
    constructor( opts ) {
        super( opts );
        this.table = "qva_users";
        this._hasTimestamps = true;
    }

    /**
     * Varify contact number in database for otp.
     *
     * @param {Object} query The query to match against.
     * @param {Object} column The query to match against.
     * @returns {Promise} Will return promise.
     */
    checkContactNumber( query = {}, column = {} ) {
        return knex( this.table )
            .select( column )
            .where( Util.toSnake( query ) )
            .map( ( res ) => {
                return res;
            } );
    }

    /**
     * Get a collection of models matching a given query.
     *
     * @param {Object} where The query to match against.
     * @param {Object} query The query to match against.
     * @param {Object} opts Options.
     * @returns {Array} An array holding resultant models.
     */
    checkEmailForUpdateProfile( where = {}, query = {}, opts = {} ) {
        return knex( this.table )
            .select( opts )
            .where( where )
            .whereNot( Util.toSnake( query ) )
            .map( ( res ) => {
                return res;
            } );
    }

    /**
     * Get a all rows from table.
     *
     * @param {Object} where The query to match against.
     * @param {Object} opts Options.
     * @param {string} limit The query to page limit.
     * @param {string} offset The query to set page offset.
     * @param {string} requestData The query to match LIKE.
     */
    getAllUsers( where = {}, opts = {}, limit, offset, requestData ) {
        let searchData = '';
        let orderData = 'DESC';
        let orderBy = 'created_at';
       
        if(requestData.search.value != undefined && requestData.search.value != "") {
           searchData = requestData.search.value.toLowerCase();
        }

        if(requestData.order != undefined && requestData.order != "") {

            if(( requestData.order[0].dir == "asc" || requestData.order[0].dir == "desc") && requestData.order[0].column == "1" ) {

                orderData = requestData.order[0].dir;
                orderBy = 'name';
                
            } else if(( requestData.order[0].dir == "asc" || requestData.order[0].dir == "desc") && requestData.order[0].column == "2" ) {

                orderData = requestData.order[0].dir;
                orderBy = 'contact_number';                

            } else if(( requestData.order[0].dir == "asc" || requestData.order[0].dir == "desc") && requestData.order[0].column == "3" ) {

                orderData = requestData.order[0].dir;
                orderBy = 'email';
            }
        }

        let prepareQuery = knex( this.table )
            .select( opts )
            .innerJoin("wallets", "users.user_id", "wallets.wallet_user_id")
            .orderBy( orderBy, orderData )
            .where( where )
            .limit( limit )
            .offset( offset );

        if( searchData ) {
            prepareQuery = prepareQuery.whereRaw( `LOWER(name) LIKE '%${searchData}%'` )
                .orWhereRaw( `LOWER(email) LIKE '%${searchData}%'` )
                .orWhereRaw( `contact_number LIKE '%${searchData}%'` );
        }

        prepareQuery = prepareQuery.map( ( res ) => {
            return res;
        } );
        
        return prepareQuery;
    }

    /**
     * Return count call details of perticular user by user_id.
     *
     * @param {Object} column Where clause for count.
     * @returns {Array} A collection of the count column models.
     */
    getCountUsers( column ) {
        return knex( this.table )
            .count( knex.raw( column ) )
            .map( ( res ) => {
                return res.count;
            } );
    }

    /**
     * Return count call details of perticular user by user_id.
     *
     * @param {Object} where The query to match against.
     * @param {Object} column Where clause for count.
     * @returns {Array} A collection of the count column models.
     */
    getCountUsersData( where = {}, column, requestData ) {
        let searchData = '';
        
        if(requestData.search.value != undefined && requestData.search.value != "") {
           searchData = requestData.search.value.toLowerCase();
        }
        let prepareQuery = knex( this.table )
            .count( knex.raw( column ) )
            .innerJoin("wallets", "users.user_id", "wallets.wallet_user_id")
            .where( where );
            
            if( searchData ) {
                prepareQuery = prepareQuery.whereRaw( `LOWER(name) LIKE '%${searchData}%'` )
                    .orWhereRaw( `LOWER(email) LIKE '%${searchData}%'` )
                    .orWhereRaw( `contact_number LIKE '%${searchData}%'` );
                    
            }
          
            prepareQuery = prepareQuery.map( ( res ) => {
                return res.count;
            } );

            return prepareQuery;
    }

    /**
     * Varify user id in database by auth token for admin.
     *
     * @param {Object} query The query to match against.
     * @param {Object} column The query to match against.
     * @returns {Promise} Will return promise.
     */
    getUserIdByAuthToken( query = {}, column = {} ) {
        return knex( this.table )
            .select( column )
            .where( Util.toSnake( query ) )
            .map( ( res ) => {
                return res;
            } );
    }
    /**
     * get use info.
     *
     * @param {Object} query The query to match against.
     * @param {Object} column The query to match against.
     * @returns {Promise} Will return promise.
     */
    getUserInfo( query = {}, column = {} ) {
        return knex( this.table )
            .select( column )
           // .leftJoin("wallets", "qva_users.id", "wallets.wallet_user_id")
           // .innerJoin("auth", "qva_users.user_id", "auth.auth_user_id")
            .where( query )
            .first()
            .then( ( row ) => {
                return row;
            } );
    }
    /**
     * get use list.
     *
     * @param {Object} query The query to match against.
     * @param {Object} column The query to match against.
     * @returns {Promise} Will return promise.
     */
    getUserList( where, userType ) {
        if (where.page) {
           var page = Number(where.page)*Number(where.limit);
        } else {
            var page=0;
        }
        if (where.limit) {
            var limit = Number(where.limit);
        } else {
            var limit=10;
        }
       var bindingParam = [ userType, where.user_id ];
       var q = 'SELECT user_id,name,country_code,contact_number,user_type'
              +' FROM users'
              +' WHERE NOT user_type  = 2 AND NOT user_type  = ? AND is_profile_done = 1 AND NOT user_id = ?';
            

             if(where.search){
                q += 
                    '  AND (name LIKE ? '
                  +' OR contact_number LIKE ?) ';
                                          
                    bindingParam.push("%" + where.search + "%");
                    bindingParam.push("%" + where.search + "%");
                    
            }
            if( q ) {
                 q += ' OFFSET ? LIMIT ? ';
                 bindingParam.push(page); 
                 bindingParam.push(limit);
            } 
           

            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
             return result;
            });

    }
    getUserListCount( where, userType ){
       var bindingParam = [userType, where.user_id];
       var q = 'SELECT count(user_id)'
              +' FROM users'
              +' WHERE NOT user_type  = 2 AND NOT user_type  = ? AND is_profile_done = 1 AND NOT user_id = ?';
            
             if(where.search){
                q += 
                    '  AND (name LIKE ? '
                  +' OR contact_number LIKE ?) ';
                                          
                    bindingParam.push("%" + where.search + "%");
                    bindingParam.push("%" + where.search + "%");
                    
            }
          
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
             return result;
            });

    }
    getMyProfile(query = {}, column = {}){
         return knex( this.table )
            .select( column )
            .innerJoin("wallets", "users.user_id", "wallets.wallet_user_id")
            .leftJoin("verify", "users.user_id", "verify.verify_user_id")
            .innerJoin("auth", "users.user_id", "auth.auth_user_id")
            .where( query )
            .first()
            .then( ( row ) => {
                return row;
        } );
    }

    /**
     * Get a all rows from consumer user table.
     *
     * @param {Object} where The query to match against.
     * @param {Object} opts Options.
     * @param {string} limit The query to page limit.
     * @param {string} offset The query to set page offset.
     * @param {string} requestData The query to match LIKE.
     */
    getAllConsumerUser( where = {}, opts = {}, limit, offset, requestData ) {
        let searchData = '';
        let orderData = 'DESC';
        let orderBy = 'created_at';
       
        if( requestData[ "search[value]" ] != undefined && requestData[ "search[value]" ] != "" ) {
           searchData = requestData[ "search[value]" ].toLowerCase();
        }
        
        if(( requestData[ "order[0][dir]" ] === "asc" || requestData[ "order[0][dir]" ] === "desc") && requestData[ "order[0][column]" ] == "1" ) {

            orderData = requestData[ "order[0][dir]" ];
            orderBy = 'users.name';
            
        } else if(( requestData[ "order[0][dir]" ] === "asc" || requestData[ "order[0][dir]" ] === "desc") && requestData[ "order[0][column]" ] == "2" ) {

            orderData = requestData[ "order[0][dir]" ];
            orderBy = 'users.contact_number';                

        } else if(( requestData[ "order[0][dir]" ] === "asc" || requestData[ "order[0][dir]" ] === "desc") && requestData[ "order[0][column]" ] == "3" ) {

            orderData = requestData[ "order[0][dir]" ];
            orderBy = 'users.email';
        }
        

        let prepareQuery = knex( this.table )
            .select( opts )
            .distinctOn('users.created_at')
            .leftJoin("wallets", "users.user_id", "wallets.wallet_user_id")
            .leftJoin("verify", "users.user_id", "verify.verify_user_id")
            .orderBy( orderBy, orderData )
            .where( where )
            .limit( limit )
            .offset( offset );

        if( searchData ) {
            // prepareQuery = prepareQuery.whereRaw( `LOWER(name) LIKE '%${searchData}%'` )
            //     .orWhereRaw( `LOWER(email) LIKE '%${searchData}%'` )
            //     .orWhereRaw( `contact_number LIKE '%${searchData}%'` );
            prepareQuery = prepareQuery.whereRaw( `(LOWER(name) LIKE '%${searchData}%' or LOWER(email) LIKE '%${searchData}%' or contact_number LIKE '%${searchData}%')` )                
        }

        prepareQuery = prepareQuery.map( ( res ) => {
            return res;
        } );
        
        return prepareQuery;
    }

    /**
     * Return count consumer user by user_id.
     *
     * @param {Object} where The query to match against.
     * @param {Object} column Where clause for count.
     * @returns {Array} A collection of the count column models.
     */
    getCountConsumerUserData( where = {}, column, requestData ) {
        let searchData = '';
        
        if( requestData[ "search[value]" ] != undefined && requestData[ "search[value]" ] != "" ) {
           searchData = requestData[ "search[value]" ].toLowerCase();
        }
        let prepareQuery = knex( this.table )
            .count( knex.raw( column ) )
            .leftJoin("wallets", "users.user_id", "wallets.wallet_user_id")
            .leftJoin("verify", "users.user_id", "verify.verify_user_id")
            .where( where );
            
            if( searchData ) {
                // prepareQuery = prepareQuery.whereRaw( `LOWER(name) LIKE '%${searchData}%'` )
                //     .orWhereRaw( `LOWER(email) LIKE '%${searchData}%'` )
                //     .orWhereRaw( `contact_number LIKE '%${searchData}%'` );
                prepareQuery = prepareQuery.whereRaw( `(LOWER(name) LIKE '%${searchData}%' or LOWER(email) LIKE '%${searchData}%' or contact_number LIKE '%${searchData}%')` )
            }
            prepareQuery = prepareQuery.map( ( res ) => {
                return res.count;
            } );

            return prepareQuery;
    }

    /**
     * Get a all rows from reseller user table.
     *
     * @param {Object} where The query to match against.
     * @param {Object} opts Options.
     * @param {string} limit The query to page limit.
     * @param {string} offset The query to set page offset.
     * @param {string} requestData The query to match LIKE.
     */
    getAllResellerUser( where = {}, opts = {}, limit, offset, requestData ) {
        let searchData = '';
        let orderData = 'DESC';
        let orderBy = 'created_at';
       
        if( requestData[ "search[value]" ] != undefined && requestData[ "search[value]" ] != "" ) {
           searchData = requestData[ "search[value]" ].toLowerCase();
        }
        
        if(( requestData[ "order[0][dir]" ] === "asc" || requestData[ "order[0][dir]" ] === "desc") && requestData[ "order[0][column]" ] == "1" ) {

                orderData = requestData[ "order[0][dir]" ];
                orderBy = 'users.name';
                
            } else if(( requestData[ "order[0][dir]" ] === "asc" || requestData[ "order[0][dir]" ] === "desc") && requestData[ "order[0][column]" ] == "2" ) {

                orderData = requestData[ "order[0][dir]" ];
                orderBy = 'users.contact_number';                

            } else if(( requestData[ "order[0][dir]" ] === "asc" || requestData[ "order[0][dir]" ] === "desc") && requestData[ "order[0][column]" ] == "3" ) {

                orderData = requestData[ "order[0][dir]" ];
                orderBy = 'users.email';
            }
        

        let prepareQuery = knex( this.table )
            .select( opts )
            .innerJoin("wallets", "users.user_id", "wallets.wallet_user_id")
            .orderBy( orderBy, orderData )
            .where( where )
            .limit( limit )
            .offset( offset );

        if( searchData ) {
            // prepareQuery = prepareQuery.whereRaw( `LOWER(name) LIKE '%${searchData}%'` )
            //     .orWhereRaw( `LOWER(email) LIKE '%${searchData}%'` )
            //     .orWhereRaw( `contact_number LIKE '%${searchData}%'` );
            prepareQuery = prepareQuery.whereRaw( `(LOWER(name) LIKE '%${searchData}%' or LOWER(email) LIKE '%${searchData}%' or contact_number LIKE '%${searchData}%')` )                
        }

        prepareQuery = prepareQuery.map( ( res ) => {
            return res;
        } );
        
        return prepareQuery;
    }

    /**
     * Return count reseller user by user_id.
     *
     * @param {Object} where The query to match against.
     * @param {Object} column Where clause for count.
     * @returns {Array} A collection of the count column models.
     */
    getCountResellerUserData( where = {}, column, requestData ) {
        let searchData = '';
        
        if( requestData[ "search[value]" ] != undefined && requestData[ "search[value]" ] != "" ) {
           searchData = requestData[ "search[value]" ].toLowerCase();
        }
        let prepareQuery = knex( this.table )
            .count( knex.raw( column ) )
            .innerJoin("wallets", "users.user_id", "wallets.wallet_user_id")
            .where( where );
            
            if( searchData ) {
                // prepareQuery = prepareQuery.whereRaw( `LOWER(name) LIKE '%${searchData}%'` )
                //     .orWhereRaw( `LOWER(email) LIKE '%${searchData}%'` )
                //     .orWhereRaw( `contact_number LIKE '%${searchData}%'` );
                prepareQuery = prepareQuery.whereRaw( `(LOWER(name) LIKE '%${searchData}%' or LOWER(email) LIKE '%${searchData}%' or contact_number LIKE '%${searchData}%')` )
            }
            prepareQuery = prepareQuery.map( ( res ) => {
                return res.count;
            } );

            return prepareQuery;
    }

    getRechargeCount( userId ) {
        var bindingParam = [userId];
        var q = ' SELECT count(recharge_id ) as count'
              + ' FROM recharges'
              + ' WHERE user_id = ? AND recharge_status = 3';
       
        return  knex.raw(q, bindingParam).then(( res ) => {
            var result = res.rows;
            return result;
        })
    }

    getDeveloperRechargeCount( userId ) {
        var bindingParam = [userId];
        var q = ' SELECT count(dav_recharge_id )as count'
              + ' FROM developer_recharge'
              + ' WHERE developer_id = ? AND dev_recharge_status = 2';
       
        return  knex.raw(q, bindingParam).then(( res ) => {
            var result = res.rows;
            return result;
        })
    }

    /**
     * get use list.
     *
     * @param {Object} query The query to match against.
     * @param {Object} column The query to match against.
     * @returns {Promise} Will return promise.
     */
    getUserData( where ) {
       
        if (where.page) {
           var page = Number(where.page)*Number(where.limit);
        } else {
            var page=0;
        }
        if (where.limit) {
            var limit = Number(where.limit);
        } else {
            var limit=10;
        }
       var bindingParam = [];
       var q = 'SELECT user_id,name,user_type,contact_number,wallet_user_id,wallet_id'
              +' FROM users'
              + ' INNER JOIN wallets on wallets.wallet_user_id = users.user_id';
              +' WHERE is_profile_done = 1 ';
            
            if( where.searchUserType != undefined ) {
                q += 
                    '  AND user_type = ?';
                    bindingParam.push(where.searchUserType);
            }

            if(where.search){
                q += 
                    '  AND name ILIKE ? ';
                    // bindingParam.push("%" + where.search + "%");
                    bindingParam.push( where.search + "%");
            } else {
                q += 
                    '  AND name ILIKE ? ';
                        bindingParam.push( where.search + "%");
            }
            if( q ) {
                 q += ' OFFSET ? LIMIT ? ';
                 bindingParam.push(page); 
                 bindingParam.push(limit);
            } 
         
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
             return result;
            });
    }

    
    /**
     * get use info.
     *
     * @param {Object} query The query to match against.
     * @param {Object} column The query to match against.
     * @returns {Promise} Will return promise.
     */
    getAdminInfo( query = {}, column = {} ) {
        return knex( this.table )
            .select( column )
            .leftJoin("auth", "users.user_id", "auth.auth_user_id")
            .where( query )
            .first()
            .then( ( row ) => {
                return row;
            } );
    }

    /**
     * get use list.
     *
     * @param {Object} query The query to match against.
     * @param {Object} column The query to match against.
     * @returns {Promise} Will return promise.
     */
    getNumberSearchList( where, userType ) {
        if (where.page) {
           var page = Number(where.page)*Number(where.limit);
        } else {
            var page=0;
        }
        if (where.limit) {
            var limit = Number(where.limit);
        } else {
            var limit=10;
        }
    //    var bindingParam = [ userType, where.user_id ];
       var bindingParam = [ where.user_id ];
       var q = 'SELECT user_id,name,country_code,contact_number,user_type'
              +' FROM users'
              //+' WHERE NOT user_type  = 2 AND NOT user_type  = ? AND is_profile_done = 1 AND NOT user_id = ?';
              +' WHERE NOT user_type  = 2 AND is_profile_done = 1 AND NOT user_id = ?';
            
            q += 
                '  AND (name LIKE ? '
                +' OR contact_number LIKE ?) ';
                                        
                bindingParam.push("%" + where.search + "%");
                bindingParam.push("%" + where.search + "%");
                
            
            if( q ) {
                 q += ' OFFSET ? LIMIT ? ';
                 bindingParam.push(page); 
                 bindingParam.push(limit);
            } 
           
            return  knex.raw(q, bindingParam).then(( res ) => {
             var result = res.rows;
             return result;
            });

    }

    getNumberSearchCount( where, userType ){
        // var bindingParam = [userType, where.user_id];
        var bindingParam = [ where.user_id];
        var q = 'SELECT count(user_id)'
               +' FROM users'
               //+' WHERE NOT user_type  = 2 AND NOT user_type  = ? AND is_profile_done = 1 AND NOT user_id = ?';
               +' WHERE NOT user_type  = 2 AND is_profile_done = 1 AND NOT user_id = ?';
             
              
                 q += 
                     '  AND (name LIKE ? '
                   +' OR contact_number LIKE ?) ';
                                           
                     bindingParam.push("%" + where.search + "%");
                     bindingParam.push("%" + where.search + "%");
                     
      
             return  knex.raw(q, bindingParam).then(( res ) => {
              var result = res.rows;
              return result;
             });
    }

    
    checkEmailIsExist( userData ) {
      
        var bindingParam = [userData.user_id, userData.user_type, userData.email];
        var q = ' SELECT *'
              + ' FROM users'
              + ' WHERE user_id != ? AND user_type = ? AND email = ?';
      
        return  knex.raw(q, bindingParam).then(( res ) => {
            var result = res.rows;
            return result;
        })
    }

    getTransferUserDetail( userData ) {
      
        var bindingParam = [userData.user_id, userData.transfer_to_user_id];
        var q = ' SELECT name as to_user, (select name from users where user_id = ?) as by_user'
              + ' FROM users'
              + ' WHERE user_id = ?';
      
        return  knex.raw(q, bindingParam).then(( res ) => {
            var result = res.rows;
            return result;
        })
    }
    
}
