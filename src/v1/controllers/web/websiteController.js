import jwt_decode from 'jwt-decode';
import envConstants from "~/constants/envConstants";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
import CommonFunctionUtil from "~/utils/CommonFunctions";
import * as userService from "~/v1/services/userService";
const moment = require( "moment-timezone" );

var jwt = require('jsonwebtoken');
import commonConstants from "~/constants/comman";

const stripePublicKey = envConstants.STRIPE_PUBLIC_KEY;

const localeService = new LocaleService(i18n),
//const commonUtils = new commonUtils();
CommonFnObj = new CommonFunctionUtil();


let userDatas = {};
userDatas = {
    Home: localeService.translate("Home"),
    Transfer: localeService.translate("Transfer"),
    Recharge: localeService.translate("Recharge"),
    Messaging: localeService.translate("Messaging"),
    Documentation: localeService.translate("Documentation"),
    Login: localeService.translate("Login")

};
exports.home = async (req, res, next) => {
    //console.log("req.csrfToken()", )
    if (typeof req.session.language == 'undefined' || req.session.language == "") {
    var browser_lang = req.headers["accept-language"].split(',')
        if (browser_lang[0] == 'en' || browser_lang[0] == 'es') {
           
            i18n.setLocale(browser_lang[0]);

        }

    }

    //var userDatas = {};
    var userDatas = CommonFnObj.get_banner_data();
   
    var homeData = {
        Reseller: localeService.translate("Reseller"),
        Be_a: localeService.translate("Be a"),
        Use_for: localeService.translate("Use for"), Developer: localeService.translate("Developer"),
        MainText: localeService.translate('Recharge from anywhere in the world anytime can stay'),
        Quickly_Recharge: localeService.translate("Quickly Recharge"),
        number: localeService.translate("Enter Mobile number"),
        Select_Operator: localeService.translate("Select Operator"),
        Select_Amount: localeService.translate("Select Amount"),
        Proceed_To_Recharge: localeService.translate('Proceed To Recharge'),
        Why_Choose: localeService.translate('Why Choose'),
        Qvaring: localeService.translate('Qvaring'),
        choose_text: localeService.translate('choose_text'),
        Low_Cost: localeService.translate('Low Cost'),
        Super_Fast: localeService.translate('Super Fast'),
        Trusted: localeService.translate('Trusted'),
        Simple: localeService.translate('Simple'),
        Work: localeService.translate('Works'),
        How: localeService.translate('How'),
        Recharge_Nauta: localeService.translate('Recharge_Nauta'),
        Cubacel_Recharge: localeService.translate('Cubacel_Recharge'),
        We_work_so: localeService.translate('We_work_so'),
        Register_for_free: localeService.translate('Register_for_free'),
        Register_for_free_on_our_site: localeService.translate('Register_for_free_on_our_site'),
        Send_a_Recharge: localeService.translate('Send_a_Recharge'),
        Send_a_Recharge_to_all: localeService.translate('Send_a_Recharge_to_all'),
        Payments: localeService.translate('Payments'),
        make_Payments: localeService.translate('make_Payments'),

       
        Our: localeService.translate('Our'),
        Send_recharges_Text_messages: localeService.translate('Send_recharges_Text_messages'),
        Recharge: localeService.translate('Recharge'),
        Send_refills_to_any_destination: localeService.translate('Send_refills_to_any_destination'),
        Messaging: localeService.translate('Messaging'),
        Send_messages_and_contact: localeService.translate('Send_messages_and_contact'),
        Wallet: localeService.translate('Wallet'),
        Top_up_your_wallet_and_make: localeService.translate('Top_up_your_wallet_and_make'),
        You_can_find_all: localeService.translate('You_can_find_all'),
        In_addition_to_all: localeService.translate('In_addition_to_all'),
        Download_on: localeService.translate('Download_on'),
        App_Store: localeService.translate('App_Store'),
        Google_Play: localeService.translate('Google_Play'),
        Select_Operator: localeService.translate('Select Operator'),
        Select_Amount: localeService.translate('Select Amount'),
        mobile_required: localeService.translate('The mobile number field is mandatory'),
        Operator_required: localeService.translate('Operator is required.'),
        recharge_plan_required: localeService.translate('Recharge plan is required.'),
        Enter_Email: localeService.translate('Enter Email'),
        please_enter_valid_mob: localeService.translate('Please enter a valid number.'),
        Recharge_Nauta: localeService.translate("Recharge Nauta"),
        Cubacel_Recharge: localeService.translate("Cubacel Recharge"),
        Continue: localeService.translate("Continue"),

        // new data
        Features: localeService.translate('Features'),
        Enter_the_receiver_mobile: localeService.translate('Enter the receiver mobile number'),
        Enter_the_receiver_email: localeService.translate('Enter the receiver email'),

    }
    console.log("req.csrfToken()", req.csrfToken())
    
    return res.render("website/home", { headerData: userDatas, homeData: homeData,  "csrfToken": req.csrfToken() });
}



exports.recharge = (req, res, next) => {
    var userDatas = CommonFnObj.get_banner_data();
   
    var rechargeData = {
        Recharge: localeService.translate("Recharge"),
        Search_Mobile_Number: localeService.translate("Search Mobile Number"),
        Total_Amount: localeService.translate("Total Amount"),
        Accepting_this_you_accept: localeService.translate("Accepting_this_you_accept"),
        Proceed_to_Recharge: localeService.translate("Proceed to Recharge"),
        Recharge_History: localeService.translate("Recharge History"),

        Select_Operator: localeService.translate("Select Operator"),
        Select_Amount: localeService.translate("Select Amount"),
        Enter_Mobile_Number: localeService.translate("Enter Mobile Number"),
        Enter_Email: localeService.translate("Enter Email"),
        Recharge_Nauta: localeService.translate("Recharge Nauta"),
        Cubacel_Recharge: localeService.translate("Cubacel Recharge"),
        Enter_Email: localeService.translate("Enter Email")


    };

    return res.render("website/recharge", {  headerData: userDatas, rechargeData: rechargeData });
}


/*Change language
* Function call when user change language from banner dropdown
*/
exports.change_language = (req, res) => {
    var language = req.body.lang;
    console.log("req.session.userAuthToken", req.session.userAuthToken)
    if (typeof req.session.userAuthToken != 'undefined') {
      
        const token = req.session.userAuthToken;
        if (token) {
            //res.cookie('user_data', language)
            jwt.verify(token, commonConstants.JWT_SECRET, function (err, decoded) {

                var userId = decoded.id;

                //  update user language in db

                return userService
                    .updateLanguage(req, userId)
                    .then((data) => {

                    })
                    .catch((err) => next(err));
            });
        }
        

    }

    i18n.setLocale(language);
        // res.clearCookie('lang ')
        req.session.language = language;
        console.log("lang", i18n.getLocale())
   
    res.status(200).json({
        code: 200,
        status: 'success',
        message: 'language changed successfully'

    })
}
