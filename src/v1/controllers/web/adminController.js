import HttpStatus from "http-status-codes";
import { sendResponse } from "~/middlewares/responseHandler";
import * as contentService from "~/v2/services/contentService";
import * as userService from "~/v2/services/userService";
import * as rechargeService from "~/v2/services/rechargeService";

/**
 * admin login created by @hemant_solanki
 */
exports.login = (req, res, next) => {

    return res.render("admin/login");
};

/**
 * Dashboard detail created by @hemant_solanki
 */
exports.dashboard = (req, res, next) => {
    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }

    return userService
        .getDashboardData(req)
        .then((data) => {
            return res.render("admin/dashboard", { adminData: adminData, dashData: data.data });
        })
        .catch((err) => next(err));
};

/**
 * Consumer user list created by @hemant_solanki
 */
exports.consumerList = (req, res, next) => {
    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    return res.render("admin/consumerUserList", { adminData: adminData, authToken: authentication });
};

/**
 * Reseller user list created by @hemant_solanki
 */
exports.resellerList = (req, res, next) => {
    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    return res.render("admin/resellerUserList", { adminData: adminData, authToken: authentication });
};

/**
 * User detail created by @hemant_solanki
 */
exports.userProfile = (req, res, next) => {
    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var userId = req.query.user_id;
    var walletId = req.query.wallet_id;
    var userType = req.query.user_type;
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }

    if (userType == 1) {
        return res.render("admin/consumerUserDetail", { adminData: adminData, user_id: userId, wallet_id: walletId, user_type: userType, authToken: authentication });
    }

    if (userType == 3) {
        return res.render("admin/resellerUserDetail", { adminData: adminData, user_id: userId, wallet_id: walletId, user_type: userType, authToken: authentication });
    }
};

/**
 * User list created by @hemant_solanki
 */
exports.rechargeList = (req, res, next) => {

    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    return res.render("admin/recharge", { adminData: adminData, authToken: authentication });
};

/**
 * Tranfer by number list created by @hemant_solanki
 */
exports.transferByNumberList = (req, res, next) => {
    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    return res.render("admin/transferByNumberList", { adminData: adminData, authToken: authentication });
};

/**
 * Tranfer by address list created by @hemant_solanki
 */
exports.transferByAddressList = (req, res, next) => {
    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    return res.render("admin/transferByAddressList", { adminData: adminData, authToken: authentication });
};

/**
 * Developer list created by @hemant_solanki
 */
exports.developerList = (req, res, next) => {
    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }

    return res.render("admin/developerList", { adminData: adminData, authToken: authentication });
};

/**
 * Logout created by @hemant_solanki
 */
exports.logout = (req, res, next) => {
    var token = req.cookies.admin_token;//req.headers['x-access-token'];

    if (!token) {
        return res.redirect('/admin');
    } else {

        res.cookie('admin_token', "");
        res.cookie('admin_profile', "");
        res.clearCookie('admin_token');
        res.clearCookie('admin_profile');

        res.cookie('admin_token', 'tobi', { path: '/admin' });
        res.clearCookie('admin_token', { path: '/admin' });
        res.cookie('admin_profile', 'tobi', { path: '/admin' });
        res.clearCookie('admin_profile', { path: '/admin' });
        return res.redirect('/admin');
    }
}

exports.developerProfile = (req, res, next) => {
    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var userId = req.query.developer_id;
    var userType = req.query.user_type;
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }

    if (userType == 4) {
        return res.render("admin/developerDetail", { adminData: adminData, developer_id: userId, user_type: userType, authToken: authentication });
    }
};

exports.getContent = (req, res, next) => {

    var contentName;
    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    switch (req.query.type) {
        case "terms_condition":
            contentName = "Terms & Condition"
            break;
        case "privacy_policy":

            contentName = "Privacy Policy"
            break;
        default:
            contentName = ""
    }

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    contentService
        .getContent(req)
        .then((data) => {

            res.render("admin/content", { adminData: adminData, authToken: authentication, content: data.data, contentName: contentName, });
        })
        .catch((err) => next(err));
    // return res.render( "admin/content", { adminData: adminData, authToken: authentication } );
};

/**
 * Update user.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
exports.saveContent = (req, res, next) => {

    return contentService
        .saveContent(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

/**
 * User invitation created by @hemant_solanki
 */
exports.userInvitation = (req, res, next) => {

    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    return res.render("admin/userInvitation", { adminData: adminData, authToken: authentication });
};

/**
 * Recharge detail list created by @hemant_solanki
 */
exports.getUserRechargeDetail = (req, res, next) => {
    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    return res.render("admin/userRechargeDetail", { adminData: adminData, authToken: authentication, code: req.query.code, mobileNumber: req.query.number, userId: req.query.id });
};

/**
 * Developer recharge detail created by @hemant_solanki
 */
exports.getDeveloperRechargeDetail = (req, res, next) => {


    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    return rechargeService
        .rechargeDeveloperDetail(req)
        .then((data) => {
            return res.render("admin/developerRechargeDetail", { adminData: adminData, authToken: authentication, rechargeData: data.data.historyList });
        })
        .catch((err) => next(err));
};

/**
 * Admin profile detail page created by @hemant_solanki
 */
exports.getAdminProfileDetail = (req, res, next) => {
    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    return res.render("admin/adminProfileDetail", { adminData: adminData, authToken: authentication, code: req.query.code, mobileNumber: req.query.number, userId: req.query.id });
};

/**
 * Transaction list created by @hemant_solanki
 */
exports.getTransactionList = (req, res, next) => {
    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }

    return res.render("admin/transactionList", { adminData: adminData, authToken: authentication });
};

/**
 * Transfer list created by @hemant_solanki
 */
exports.getTransferList = (req, res, next) => {
    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }

    return res.render("admin/transferList", { adminData: adminData, authToken: authentication });
};

/**
 * Verify otp created by @hemant_solanki
 */
exports.verifyOtp = (req, res, next) => {
    var number = req.query.number;
    var country = req.query.code;

    return res.render("admin/verifyOtp", { contactNumber: number, countryCode: country });
};

/**
 * Get user stripe fee tax detail created by @hemant_solanki
 */
exports.getUserStripeTaxDetail = (req, res, next) => {

    var adminData;
    var userType = 1;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    return userService
        .getStripeFeeTax(req, userType)
        .then((data) => {

            return res.render("admin/userStripeFeeTax", { adminData: adminData, authToken: authentication, StripeFeetaxDetail: data.stripetaxDetail });
        })
        .catch((err) => next(err));

};

/**
 * Get reseller developer stripe fee tax detail created by @hemant_solanki
 */
exports.getResellerDeveloperStripeTaxDetail = (req, res, next) => {

    var adminData;
    var userType = 3;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse(token);
    var authentication = req.cookies.admin_token;

    if (profileData.user != undefined) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    return userService
        .getStripeFeeTax(req, userType)
        .then((data) => {
            return res.render("admin/resellerDeveloperStripeFeeTax", { adminData: adminData, authToken: authentication, StripeFeetaxDetail: data.stripetaxDetail });
        })
        .catch((err) => next(err));
};


/**
 * Get terms condition
 */
exports.getTermsCondition = (req, res, next) => {

    var contentName = 'Terms & Condition';

    contentService
        .getFrontContent(req, contentName)
        .then((data) => {

            res.render("admin/frontContent", { data: data, contentName: contentName });
        })
        .catch((err) => next(err));
};

/**
 * Get privacy policy
 */
exports.getPrivacyPolicy = (req, res, next) => {

    var contentName = 'Privacy Policy';

    contentService
        .getFrontContent(req, contentName)
        .then((data) => {

            res.render("admin/frontContent", { data: data, contentName: contentName });
        })
        .catch((err) => next(err));
};


/**
 * Get customer transfer limit created by @Vihag_kumar Dubey.
 */
exports.getCustomerTransferLimit = ( req, res, next ) => {

    let adminData;
    const userType = 1,
        token = req.cookies.admin_profile,
        profileData = JSON.parse( token ),
        authentication = req.cookies.admin_token;

    if ( profileData.user != undefined ) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    return userService
        .getTransferLimits( req, userType )
        .then( ( data ) => {
            return res.render( "admin/customerTransferLimit", { 
                adminData: adminData, 
                authToken: authentication, 
                transferLimitsDetail: data.stripetaxDetail 
            } );
        } )
        .catch( ( err ) => next( err ) );
};

/**
 * Get reseller transfer limit created by @Vihag_kumar Dubey.
 */
exports.getResellerTransferLimit = ( req, res, next ) => {

    var adminData;
    var userType = 3;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse( token );
    var authentication = req.cookies.admin_token;

    if ( profileData.user != undefined ) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    return userService
        .getTransferLimits( req, userType )
        .then( ( data ) => {
            return res.render( "admin/resellerTransferLimit", { 
                adminData: adminData, 
                authToken: authentication, 
                transferLimitsDetail: data.stripetaxDetail 
            } );
        } )
        .catch( ( err ) => next( err ) );
};

/**
 * Get reseller transfer limit created by @Vihag_kumar Dubey.
 */
exports.updateTransferLimit = ( req, res, next ) => {

    var adminData;
    var token = req.cookies.admin_profile;
    var profileData = JSON.parse( token );
    var authentication = req.cookies.admin_token;

    if ( profileData.user != undefined ) {
        adminData = profileData.user;
    } else {
        adminData = profileData.data;
    }
    return userService
        .updateTransferLimit( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) ); 
};

