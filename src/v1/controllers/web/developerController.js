import jwt_decode from 'jwt-decode';
import * as rechargeService from "~/v2/services/rechargeService";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";


const localeService = new LocaleService( i18n );
let userDatas = {};
userDatas = {
    Home:localeService.translate( "Home" ),
    Transfer:localeService.translate( "Transfer" ),
    Recharge:localeService.translate( "Recharge" ),
    Messaging:localeService.translate( "Messaging" ),
    Documentation:localeService.translate( "Documentation" ),
    Login:localeService.translate( "Login" ),

};



// exports.home = ( req, res, next ) => {
   
//     var userData = '';
//     var token = req.cookies.user_profile;
//     if(token) {
//         var profileData = JSON.parse(token);
     
//         if( profileData.user != undefined) {
//             userData = profileData.user;
//         } else {
//             userData = profileData.data;
//         }
//         return res.render( "website/home", { userData: userData } );
//     }
//     return res.render( "website/home" );
// };

exports.developerSignin = ( req, res, next ) => {

    var userDatas = get_banner_data();
    var developerData = {
        
        Login_Your_Account:localeService.translate("Login Your Account"),
        Please_fill_your_bellow:localeService.translate("Please fill your below information to login your account."),
        Login:localeService.translate("Login"),
        Don_have_account:localeService.translate("Don't have an account"),
        Sign_Up:localeService.translate("Sign Up"),
        Forgot_Password:localeService.translate("Forgot Password"),
        lease_fill_your_below_reset:localeService.translate("Please enter your registered email address below to reset your password."),
        Email_Address:localeService.translate("Email Address"),
        Submit:localeService.translate("Submit"),
        Enter_Email:localeService.translate("Enter Email"),
        Enter_Password:localeService.translate("Enter Password"),
        Submit:localeService.translate("Submit"),



    }
    return res.render( "website/developerLogin" ,{headerData :userDatas, developerData:developerData} );
};

exports.developerSignUp = ( req, res, next ) => {
    var userDatas = get_banner_data();
    var developerData = {
        
        Create_Your_Account:localeService.translate("Create_Your_Account"),
        Please_enter_your_valid_mobile:localeService.translate("Please_enter_your_valid_mobile"),
        Continue:localeService.translate("Continue"),
        Verify_Your_Number:localeService.translate("Verify Your Number"),
        Please_enter_the_verification_code:localeService.translate("Please_enter_the_verification_code"),
        Resend_Code:localeService.translate("Resend_Code"),
        Confirm:localeService.translate("Confirm"),
        Create_Your_Account:localeService.translate("Create_Your_Account"),
        to_create_your_account:localeService.translate("Please fill your below information to create your account."),
        Done : localeService.translate("Done"),
        Enter_OTP_Number : localeService.translate("Enter OTP Number"),
        Enter_your_Name : localeService.translate("Enter your Name"),
        Enter_Your_Email : localeService.translate("Enter Your Email"),
        Enter_Password : localeService.translate("Enter Password"),
        Enter_Mobile_Number : localeService.translate("Enter Mobile Number")

    }
    return res.render( "website/developerSignUp" ,{headerData :userDatas, developerData : developerData}  );
};



exports.developerProfile = ( req, res, next ) => {
    var developerData;
    var token = req.cookies.developer_profile;
    var profileData = JSON.parse(token);


    if( profileData.developer != undefined) {
        developerData = profileData.developer;
    } else {
        developerData = profileData.data;
    }
    var profileData = {
        
        Recharge_History:localeService.translate("Recharge History"),
        Balance:localeService.translate("Balance"),
        Total_Recharge:localeService.translate("Total Recharge"),
        API_TOKEN:localeService.translate("API-TOKEN"),
        Api_link:localeService.translate("Api_link"),
        Active_Ip_Address:localeService.translate("Active Ip Address"),
        Confirm:localeService.translate("Confirm"),
        Continue:localeService.translate("Continue"),
        Verify_Your_Number:localeService.translate("Verify Your Number"),
        Please_enter_the_verification_code : localeService.translate("Please_enter_the_verification_code"),
        Resend_Code : localeService.translate("Resend_Code"),
        Add_New_Card : localeService.translate("Add New Card"),
        By_Number : localeService.translate("By Number"),
        By_Address : localeService.translate("By Address"),
        Search_Mobile_Number : localeService.translate("Search Mobile Number"),
        We_need_to_verify_your_ID : localeService.translate("We need to verify your ID."),
        You_can_upload_your_password : localeService.translate("You_can_upload_your_password"),
        Upload_Image : localeService.translate("Upload Image"),
        Please_verification_process_under_review : localeService.translate("Please wait, verification process under review."),
        Cancel : localeService.translate("Cancel"),
        Update : localeService.translate("Update"),
        Change_Number : localeService.translate("Change Number"),
        Do_you_need_to_update_your_phone_number : localeService.translate("Do you need to update your phone number"),
        Cancel : localeService.translate("Cancel"),
        Operator_Profit : localeService.translate("Operator Profit"),
        Select_Status : localeService.translate("Select Status"),
        Pending : localeService.translate("Pending"),
        Cancel : localeService.translate("Cancel"),
        Success : localeService.translate("Success"),
        Start_date : localeService.translate("Start date"),
        End_date : localeService.translate("End date"),
        Search : localeService.translate("Search"),
        Reset : localeService.translate("Reset"),
        SNo : localeService.translate("SNo"),
        Recharge_Number : localeService.translate("Recharge Number"),
        SKU_Code : localeService.translate("SKU Code"),
        Transfer_Reference : localeService.translate("Transfer Reference"),
        Status : localeService.translate("Status"),
        Date_Time : localeService.translate("Date/Time"),
        Action : localeService.translate("Action"),
        Crop_Image : localeService.translate("Crop Image"),
        Cancel : localeService.translate("Cancel"),
        Save : localeService.translate("Save"),
        Alert : localeService.translate("Alert"),
        delete_card : localeService.translate("Are you sure you want to delete this card"),
        Cancel : localeService.translate("Cancel"),
        Ok : localeService.translate("Ok"),
        Please_Add_Card : localeService.translate("Please Add Card"),
        Cancel : localeService.translate("Cancel"),
        Credit_card : localeService.translate("Credit/Debit card"),
        Add_Card : localeService.translate("Add Card"),
        Transaction_Detail : localeService.translate("Transaction Detail"),
        Transaction_Receipt : localeService.translate("Transaction Receipt"),
        Qvaring_Receipt : localeService.translate("Qvaring Receipt"),
        Enter_First_IP : localeService.translate("Enter First IP"),
        Enter_Second_IP : localeService.translate("Enter Second IP"),
        Enter_OTP_Number : localeService.translate("Enter OTP Number"),
        Enter_Second_IP : localeService.translate("Enter Second IP"),
        My_Account : localeService.translate("My Account"),
        My_Ip_Address : localeService.translate("My_Ip_Address"),
        Profit : localeService.translate("Profit"),
        Enter_Mobile_number : localeService.translate("Enter Mobile number"),

       




    }
    var userDatas = get_banner_data();
    return res.render( "website/developerProfile", { developerData: developerData, headerData :userDatas, profileData : profileData} );
}


exports.cubaRecharge = ( req, res, next ) => {
    var userDatas = get_banner_data();
    return res.render( "website/cubaRecharge" ,{headerData :userDatas} );
}


exports.logout = ( req, res, next ) => {

    var token = req.cookies.developer_data;//req.headers['x-access-token'];
    if (!token) { 
        return res.redirect('/');
    } else {
        res.cookie('developer_data', "");
        res.cookie('developer_profile', "");
        return res.redirect('/');
    }
}

exports.developerRechargeDetail = ( req, res, next ) => {

    var developerData;
    var token = req.cookies.developer_profile;
    var profileData = JSON.parse(token);

    if( profileData.developer != undefined) {
        developerData = profileData.developer;
    } else {
        developerData = profileData.data;
    }
    return rechargeService
        .rechargeDeveloperDetail( req )
        .then( ( data ) => {
            return res.render( "website/developerRechargeHistoryDetail", { developerData: developerData, rechargeData: data.data.historyList} );
        } )
        .catch( ( err ) => next( err ) );
}

var get_banner_data = function() {
    return userDatas = {
         Home:localeService.translate( "Home" ),
         Transfer:localeService.translate( "Transfer" ),
         Recharge:localeService.translate( "Recharge" ),
         Messaging:localeService.translate( "Messaging" ),
         Documentation:localeService.translate( "Documentation" ),
         Login:localeService.translate( "Login" ),
         English:localeService.translate( "English" ),
         Spanish:localeService.translate( "Spanish" ),
        
         mobile_required: localeService.translate('The mobile number field is mandatory'),
         please_enter_valid_mob: localeService.translate('Please enter a valid number.'),
         The_password_field_is_mandatory: localeService.translate('The password field is mandatory'),
         Minimum_length_character: localeService.translate('Minimum length 8 character'),
         Password_message_should_be: localeService.translate('Password_message_should_be'),
         The_email_mandatory: localeService.translate('The email field is mandatory'),
         Please_valid_email_address: localeService.translate('Please enter a valid email address'),
         The_otp_is_mandatory: localeService.translate('The otp field is mandatory'),
         The_name_field_is_mandatory: localeService.translate('The user name field is mandatory'),
         The_name_two: localeService.translate('User name length must be at least 2 characters long'),
         The_name_max: localeService.translate('User name length must be less than or equal to 255 characters long'),
         All_rights_reserved: localeService.translate("All rights reserved"),
         OTP_verified_successfully: localeService.translate("OTP verified successfully"),
         OTP_sent_successfully: localeService.translate("OTP sent successfully"),
         No_record_found: localeService.translate("No record found"),
         Message_history_not_found: localeService.translate("Message history not found"),
         Enter_characters: localeService.translate("Enter your message 2-250 characters"),
         Reseller_Commission: localeService.translate("Reseller Commission"),
         My_Profile: localeService.translate("My Profile"),
         at_list: localeService.translate("Please enter at least 2 characters."),
         otp_field_is_mandatory: localeService.translate("The otp field is mandatory"),
         old_password: localeService.translate("The old password field is mandatory"),
         The_confirm_password: localeService.translate("The confirm password field is mandatory"),
         Enter_confirm_password: localeService.translate("Enter confirm password same as new password"),
         Please_enter_your_email: localeService.translate("Please enter your email."),
         Recharge_amount_is_greater: localeService.translate("Recharge_amount_is_greater"),
         Please_acknowledge_you: localeService.translate("Please_acknowledge_you"),
         Please_newpassword: localeService.translate("Please enter new password"),
         Language : i18n.getLocale()
     
     };
 }