import HttpStatus from "http-status-codes";
import * as authService from "~/v1/services/authService";
import * as userService from "~/v1/services/userService";
import { sendResponse } from "~/middlewares/responseHandler";
import DateTimeUtil from "~/utils/DateTimeUtil";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
const moment = require( "moment-timezone" );
const localeService = new LocaleService(i18n) ;

/**
 * Send Otp.
 *
 * @param  {object} req
 * @param  {object} res
 * @param  {Object} next Next request.
 */
const sendOtp = function( req, res, next ) {
    return authService
        .sendOtp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );  
}; 



/**
 * ReSend Otp.
 *
 * @param  {object} req
 * @param  {object} res
 * @param  {Object} next Next request.
 */
const resendOtp = function( req, res, next ) {
    return authService
        .resendOtp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );  
}; 

/**
 * Verify Otp check.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const verifyOtp = ( req, res, next ) => {
    return authService
        .verifyOtp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Verify Otp check.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const login = ( req, res, next ) => { 

    return authService
        .login( req )
        .then( ( response ) =>
        {
        
            if(response.isExpired == 1){
                
                const  data = response.data.web_user ,
             
                storeSessionData = {
                    "id": data.id,
                    "status": data.status,
                    "name": data.name,
                    "profile_image": data.profile_image,
                    "email": data.email,
                    "country_code": data.country_code,
                    "country_dial_code": data.country_dial_code,
                    "contact_number": data.contact_number,
                        "is_mail_verified": data.is_mail_verified,
                        "is_mobile_verified": data.is_mobile_verified,
                        "api_token": data.api_token,
                      
                        "device_id": data.device_id

                     
               };

            /* Set Auth session */
            if(data.language == '' || data.language == null){
                req.session.language = "en"
            }else{
                req.session.language = data.language;
            }
            i18n.setLocale( req.session.language);
            req.session.userAuthToken = response.token;
            //req.session.deviceId = response.data.deviceData.device_id;
            req.session.loginUserData = storeSessionData;

            

        }
        
         sendResponse( req, res, HttpStatus.OK, response )
         })
        .catch( ( err ) => next( err ) );
};

/**
 * Check user exist or not.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const verifyUser = ( req, res, next ) => { 
    return authService
        .verifyUser( req, next )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Verify Otp for ip address.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const verifyUserOtp = ( req, res, next ) => {
    return authService
        .verifyUserOtp( req )
        .then( ( response ) =>{

            const  data = response.user ,
             
                storeSessionData = {
                    "id": data.id,
                    "status": data.status,
                    "name": data.name,
                    "profile_image": data.profile_image,
                    "email": data.email,
                    "country_code": data.country_code,
                    "country_dial_code": data.country_dial_code,
                    "contact_number": data.contact_number,
                        "is_mail_verified": data.is_mail_verified,
                        "is_mobile_verified": data.is_mobile_verified,
                        "api_token": data.api_token,
                       
                        "device_id": data.device_id

                     
               };

            /* Set Auth session */
            if(data.language == '' || data.language == null){
                req.session.language = "en"
            }else{
                req.session.language = data.language;
            }
            i18n.setLocale( req.session.language);
            req.session.userAuthToken = response.token;
          
            //req.session.deviceId = response.data.deviceData.device_id;
            req.session.loginUserData = storeSessionData;
         sendResponse( req, res, HttpStatus.OK, response ) })
        .catch( ( err ) => next( err ) );
};






/**
 * Verify Otp check.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const logout = ( req, res, next ) => { 
    const token = req.session.userAuthToken,
        deviceId = req.session.loginUserData.device_id;

    
    
    req.headers[ "X-DEVICE-ID" ] = deviceId;

    if ( !token ) {
        return res.redirect( "/" );
    } else {

        return authService.logout( req, next ).then( ( ) => {
                
            /* Destroy session */
            req.session.destroy();

            return res.redirect( "/" );
            
        } ).catch( ( err ) => next( err ) );
   
    } 
};

/**
 * User signup
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const userSignup = ( req, res, next ) => { 
   
    return authService
        .userSignup( req, next )
        .then( ( response ) => { 
           // console.log("alldata", response.data.deviceData);
           
             const  data = response.data.userData ,
             
                storeSessionData = {
                    "id": data.id,
                    "status": data.status,
                    "name": data.name,
                    "profile_image": data.profile_image,
                    "email": data.email,
                    "country_code": data.country_code,
                    "country_dial_code": data.country_dial_code,
                    "contact_number": data.contact_number,
                        "is_mail_verified": data.is_mail_verified,
                        "is_mobile_verified": data.is_mobile_verified,
                        "api_token": data.api_token,
                        
                        "device_id": data.device_id

                     
               };

            /* Set Auth session */
            if(data.language == '' || data.language == null){
                req.session.language = "en"
            }else{
                req.session.language = data.language;
            }
            i18n.setLocale( req.session.language);
            
            req.session.userAuthToken = response.token;
            
            req.session.deviceId = response.data.deviceData.device_id;
            req.session.loginUserData = storeSessionData;
           // console.log("session", storeSessionData)
            sendResponse( req, res, HttpStatus.OK, response ) })
        .catch( ( err ) => next( err ) );
};


/**
* Reset password.
*
* @param {Object} req Request.
* @param {Object} res Response.
* @param {Object} next Next request.
*/
const resetPassword = ( req, res, next ) => {
    return userService
        .resetPassword( req, next )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
        }

// Reset use password
const CheckPasswordToken = (req, res, next) => {
    var token = req.query.token;
    
    var currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
    currentDateTime = new Date(currentDateTime).toISOString();
   
    var resData;
    var error = 'Sorry, this link is expired';
   
        var browser_lang = req.headers["accept-language"].split(',')
        if (browser_lang[0] == 'en' || browser_lang[0] == 'es') {
             res.cookie('language', browser_lang[0]);
            i18n.setLocale(browser_lang[0]);

        }
        

    if(token == "undefined" || token == ""){
        return res.render("website/resetPassword", {  error: error });
    }
    userService
        .updatePassword(req)
        .then((data) => {
         //   history.pushState(null, '/token/');
         

            if (data == 'undefined' || data == "null" || data == '') {
               
                return res.render("website/resetPassword", {  error: error });
            }
           
            var diff_ms = moment(currentDateTime).diff(moment(data[0].updated_at));
            
            var dur_obj = moment.duration(diff_ms);
           
            var hour = Math.round(dur_obj.asHours()); 
           
           if(hour > 12){
            
                return res.render("website/resetPassword", {  error: error });
           }
           
            if (data[0].password_token !== token || token == '' ||  token == null) {

                

                return res.render("website/resetPassword", {  error: error });
            }
            
            return res.render("website/resetPassword", { resData: data[0], "csrfToken": req.csrfToken() });

        })
        .catch((err) => next(err));

};


// update users password
const updatePassword = (req, res, next) => {

  userService
  .updateUserPassword(req)
  .then((data) => {
      if(data == "link_expired"){

        res.status(200).json({
            code: 200,
            status: 'fail',
            message: 'This link is expired '
    
        })
      }else{
                    
        res.status(200).json({
            code: 200,
            status: 'success',
            message: 'Password changed successfully'
    
        })
      }
    
  }).catch((err) => next(err));

};



const authController = {
    verifyOtp,
    sendOtp,
    resendOtp,
    login,
    verifyUser,
    userSignup,
    logout,
    verifyUserOtp,
    resetPassword,
    CheckPasswordToken,
    updatePassword
    
};

export default authController;
