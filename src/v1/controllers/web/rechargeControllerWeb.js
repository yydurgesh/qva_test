import jwt_decode from 'jwt-decode';
import envConstants from "~/constants/envConstants";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
import CommonFunctionUtil from "~/utils/CommonFunctions";
import * as userService from "~/v1/services/userService";
import * as dingService from "~/v1/services/dingService";
const moment = require( "moment-timezone" );
import { sendResponse } from "~/middlewares/responseHandler";
import HttpStatus from "http-status-codes";
import lodash from "lodash";
var jwt = require('jsonwebtoken');
import commonConstants from "~/constants/comman";

const stripePublicKey = envConstants.STRIPE_PUBLIC_KEY;

const localeService = new LocaleService(i18n),
//const commonUtils = new commonUtils();
CommonFnObj = new CommonFunctionUtil();


let userDatas = {};
/**
 * get all continent
 */
exports.continentPromotions = async (req, res, next) => {
 
    //var userDatas = {};
    var userDatas = CommonFnObj.get_banner_data();
    return dingService.getContinent( req, next ).then( (response ) => {
               // console.log("response.data", response.data)
        return res.render("website/continentPromotions", { headerData: userDatas, continent :response.data });
        
    } ).catch( ( err ) => next( err ) );
   
}


/**
 * get all continent promotions
 */
exports.continentPromotionsDescription = async (req, res, next) => {
 
    //var userDatas = {};
    var userDatas = CommonFnObj.get_banner_data();
   
    return dingService.continentPromotionsDescription( req, next ).then( ( data ) => {
       var c_id = lodash.uniq(lodash.map(data.data, 'country_id'));
      
       data.c_id = c_id ;
    sendResponse( req, res, HttpStatus.OK, data ) 
 })
    .catch( ( err ) => next( err ) );
   
}



