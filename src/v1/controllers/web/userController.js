import jwt_decode from 'jwt-decode';
import envConstants from "~/constants/envConstants";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
import * as userService from "~/v1/services/userService";
import CommonFunctionUtil from "~/utils/CommonFunctions";

const moment = require( "moment-timezone" );
import { sendResponse } from "~/middlewares/responseHandler";
import HttpStatus from "http-status-codes";

var jwt = require('jsonwebtoken');



const stripePublicKey = envConstants.STRIPE_PUBLIC_KEY;

const localeService = new LocaleService(i18n),
//const commonUtils = new commonUtils();
CommonFnObj = new CommonFunctionUtil();


let userDatas = {};
/**
 * get user profile
 */

 exports.userProfile = async (req, res, next) => {
    var userDatas = CommonFnObj.get_banner_data();
   
    var userData =  req.session.loginUserData ;
    
    var profileData = {
        Recharge: localeService.translate("Recharge"),
        Search_Mobile_Number: localeService.translate("Search Mobile Number"),
        Total_Amount: localeService.translate("Total Amount"),
        Accepting_this_you_accept: localeService.translate("Accepting_this_you_accept"),
        Proceed_to_Recharge: localeService.translate("Proceed to Recharge"),
        Recharge_History: localeService.translate("Recharge History"),

        Select_Operator: localeService.translate("Select Operator"),
        Select_Amount: localeService.translate("Select Amount"),
        Enter_Mobile_Number: localeService.translate("Enter Mobile Number"),
        Enter_Email: localeService.translate("Enter Email"),
        Recharge_Nauta: localeService.translate("Recharge Nauta"),
        Cubacel_Recharge: localeService.translate("Cubacel Recharge"),
        Enter_Email: localeService.translate("Enter Email")


    };
     
    return res.render("website/profile", {  headerData: userDatas, profileData: profileData, userData :userData,  "csrfToken": req.csrfToken() });
}

/**
 * get all continent
 */
exports.updateProfile = async (req, res, next) => {
 
    //var userDatas = {};
    var userDatas = CommonFnObj.get_banner_data();
    return userService.updateUser( req ,next)
    .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
    .catch( ( err ) => next( err ) ); 
   
}


