import HttpStatus from "http-status-codes";
import * as authService from "~/v1/services/authService";
import { sendResponse } from "~/middlewares/responseHandler";

/**
 * Send Otp.
 *
 * @param  {object} req
 * @param  {object} res
 * @param  {Object} next Next request.
 */
const sendOtp = function( req, res, next ) {
    return authService
        .sendOtp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );  
}; 

/**
 * Verify Otp check.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const verifyOtp = ( req, res, next ) => {
    return authService
        .verifyOtp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Verify Otp check.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const login = ( req, res, next ) => { 

    return authService
        .login( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Check user exist or not.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const verifyUser = ( req, res, next ) => { 
    return authService
        .verifyUser( req, next )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const sendSms = ( req, res, next ) => { 
    return authService
        .sendSms( req, next )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/* code for developer*/

const developerLogin = ( req, res, next ) => { 
    return authService
        .developerLogin( req, next )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
const developerSignUp = ( req, res, next ) => { 
    return authService
        .developerSign( req, next )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
const clientIp = ( req, res, next ) => { 
    return authService
        .clientIp( req, next )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
/**
 * Send Otp for developer.
 *
 * @param  {object} req
 * @param  {object} res
 * @param  {Object} next Next request.
 */
const sendDevOtp = function( req, res, next ) {
    return authService
        .sendDevOtp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );  
}; 
const verifyDevotp = function( req, res, next ) {
    return authService
        .verifyDevotp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );  
};
const setOperator = function( req, res, next ) {
    return authService
        .setOperator( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );  
};  

const developerDetails = function( req, res, next ) {
    return authService
        .developerDetails( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );  
}; 
const countryData = function( req, res, next ) {
    return authService
        .countryData( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );  
};  

/**
 * Send Otp for ip address.
 *
 * @param  {object} req
 * @param  {object} res
 * @param  {Object} next Next request.
 */
const sendOtpToIp = function( req, res, next ) {
    return authService
        .sendOtpToIp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );  
}; 

/**
 * Verify Otp for ip address.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const verifyUserOtp = ( req, res, next ) => {
    return authService
        .verifyUserOtp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
/**
 * Get user ip
 */
const getUserIp = ( req, res, next ) => { 
    return authService
        .getUserIp( req, next )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Verify Otp for ip address.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const verifyAdminOtp = ( req, res, next ) => {
    return authService
        .verifyAdminOtp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};


/**
 * Verify Otp check.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const logout = ( req, res, next ) => { 
    return authService
        .logout( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};


const authController = {
    verifyOtp,
    sendOtp,
    login,
    verifyUser,
    sendSms,
    developerLogin,
    developerSignUp,
    clientIp,
    sendDevOtp,
    verifyDevotp,
    setOperator,
    developerDetails,
    countryData,
    sendOtpToIp,
    verifyUserOtp,
    getUserIp,
    verifyAdminOtp,
    logout
};

export default authController;
