import HttpStatus from "http-status-codes";
import * as verifyService from "~/v2/services/verifyService";
import { sendResponse } from "~/middlewares/responseHandler";

/**
 *  add document.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const addDocument = ( req, res, next ) => {
    return verifyService
        .addDocument( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 *  get document.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getDocument = ( req, res, next ) => {
    return verifyService
        .getDocument( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 *  delete.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const deleteDocument = ( req, res, next ) => {
    return verifyService
        .deleteDocument( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * update document status created by @hemant_solanki
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const updateDocumentStatus = ( req, res, next ) => {
    
    return verifyService
        .updateDocumentStatus( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const verifyController = {
	addDocument,
	deleteDocument,
    getDocument,
    updateDocumentStatus
  
};

export default verifyController;
