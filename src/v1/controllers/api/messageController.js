import HttpStatus from "http-status-codes";
import * as msgService from "~/v2/services/messageService";
import { sendResponse } from "~/middlewares/responseHandler";

/**
 * Send Message.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const sendMessage = ( req, res, next ) => {
    return msgService
        .sendMessage( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Get user sent Messages list .
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getUserMessages = ( req, res, next ) => {
    return msgService
        .getUserMessages( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Get Message list .
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getMessageDetail = ( req, res, next ) => {
    return msgService
        .getMessageDetail( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const messageController = {
    sendMessage,
    getUserMessages,
    getMessageDetail
};

export default messageController;
