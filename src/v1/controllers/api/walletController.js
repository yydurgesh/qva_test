import HttpStatus from "http-status-codes";
import * as walletService from "~/v2/services/walletService";
import { sendResponse } from "~/middlewares/responseHandler";

/**
 *  addToWallet.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const addToWallet = ( req, res, next ) => {
    return walletService
        .addToWallet( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 *  wallet transaction history.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const walletTransactionHistory = ( req, res, next ) => {
    return walletService
        .walletTransactionHistory( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 *  wallet transaction history.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const addWalletAmount = ( req, res, next ) => {
    return walletService
        .addWalletAmount( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};


/**
 *  wallet transaction list.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const walletTransactionList = ( req, res, next ) => {
    return walletService
        .walletTransactionList( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const walletController = {
    addToWallet,
    walletTransactionHistory,
    addWalletAmount,
    walletTransactionList
    
};

export default walletController;
