import HttpStatus from "http-status-codes";
import { sendResponse } from "~/middlewares/responseHandler";
import * as developerService from "~/v2/services/developerService";

const sendIpOtp = ( req, res, next ) => {
    return developerService
        .sendIpOtp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
const verifyDevIpotp = ( req, res, next ) => {
    return developerService
        .verifyDevIpotp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
const ipList = ( req, res, next ) => {
    return developerService
        .ipList( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
const operatorCommisssionList = ( req, res, next ) => {
    return developerService
        .operatorCommisssionList( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
/**
* Reset password.
*
* @param {Object} req Request.
* @param {Object} res Response.
* @param {Object} next Next request.
*/
const resetDeveloperPassword = ( req, res, next ) => {
    return developerService
    .resetDeveloperPassword( req )
    .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
    .catch( ( err ) => next( err ) );
}

const checkBalance = ( req, res, next ) => {
 
    return developerService
        .checkBalance( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
const countryWiseOperator = ( req, res, next ) => {
    return developerService
        .countryWiseOperator( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const developerController = {
    
    sendIpOtp,
    verifyDevIpotp,
    ipList,
    resetDeveloperPassword,
    operatorCommisssionList,
    checkBalance,
    countryWiseOperator
  
};

export default developerController;
