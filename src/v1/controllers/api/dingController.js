import HttpStatus from "http-status-codes";

import * as dingService from "~/v1/services/dingService";
import { sendResponse } from "~/middlewares/responseHandler";

import { json } from "express";


/**
 * This api is responsible to get error list
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const GetErrorCodeDescriptions = ( req, res, next ) => {
    return dingService
        .GetErrorCodeDescriptions( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
/**
 * This api is responsible to get resigon list
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const GetRegions = ( req, res, next ) => {
    return dingService
        .GetRegions( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * This api is responsible to get country list
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const GetCountries = ( req, res, next ) => {
    return dingService
        .GetCountries( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const GetProviders = ( req, res, next ) => {
    return dingService
        .GetProviders( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const getproducts = ( req, res, next ) => {
    return dingService
        .getproducts( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const SendTransfer = ( req, res, next ) => {
    return dingService
        .SendTransfer( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const getProfiderInfo = ( req, res, next ) => {
    return dingService
        .getProfiderInfo( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};


/*
* Add promotions to db table creat by durgesh
*/
const addPromotion = ( req, res, next ) => {
    return dingService
     .addPromotions( req )
     .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
     .catch( ( err ) => next( err ) );
};

/*
* Add providers to db table creat by durgesh
*/
const addProviders = ( req, res, next ) => {
    return dingService
     .addProviders( req )
     .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
     .catch( ( err ) => next( err ) );
};

/*
* Add continent wise promotions to db table creat by durgesh
*/
const addContinentPromotion = ( req, res, next ) => {
    return dingService
     .addContinentPromotion( req )
     .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
     .catch( ( err ) => next( err ) );
};

/**
 * Add plans and top up to db
 * @param  req 
 * @param  res 
 * @param next 
 */
const addProducts = ( req, res, next ) => {
    return dingService
     .addProducts( req )
     .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
     .catch( ( err ) => next( err ) );
};

/**
 * Add all countries
 * @param  req 
 * @param  res 
 * @param next 
 */
const addCountries = ( req, res, next ) => {
    return dingService
     .addCountries( req )
     .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
     .catch( ( err ) => next( err ) );
};


const dingController = {
    
    GetErrorCodeDescriptions,
    GetRegions,
    GetCountries,
    GetProviders,
    getproducts,
    SendTransfer,
    getProfiderInfo,
    addProviders,
    addContinentPromotion,
    addProducts,
    addCountries,
    addPromotion
    
};

export default dingController;
