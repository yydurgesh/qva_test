import HttpStatus from "http-status-codes";
import * as transferService from "~/v2/services/transferService";
import { sendResponse } from "~/middlewares/responseHandler";

/**
 * Make Call.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const transferAmount = ( req, res, next ) => {
    return transferService
        .transferByContactNo( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * transfer by address.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const transferByAddress = ( req, res, next ) => {
    return transferService
        .transferByAddress( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * transfer by address.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const transferHisttory = ( req, res, next ) => {
    return transferService
        .transferHisttory( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * transfer by address.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const transferList = ( req, res, next ) => {
    return transferService
        .transferList( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Update transfer status. created by @hemant_solanki
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const updateTransferStatus = ( req, res, next ) => {
    return transferService
        .updateTransferStatus( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const adminTransferHisttory = ( req, res, next ) => {
    return transferService
        .adminTransferHisttory( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};


const getTransferHisttory = ( req, res, next ) => {
    return transferService
        .getTransferHisttory( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const transferController = {
    transferAmount,
    transferByAddress,
    transferHisttory,
    transferList,
    updateTransferStatus,
    adminTransferHisttory,
    getTransferHisttory
};

export default transferController;
