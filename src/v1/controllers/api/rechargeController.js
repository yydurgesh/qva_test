import HttpStatus from "http-status-codes";
import * as rechargeService from "~/v2/services/rechargeService";
import { sendResponse } from "~/middlewares/responseHandler";
import dataConstants from "~/constants/dataConstants";

import { GoodsServiceRechargeLib } from "~/libraries/GoodsServiceRechargeLib";
import { json } from "express";

/**
 * Get MSISDN Info.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getMsisdnInfo = (req, res, next) => {
    return rechargeService
        .getMsisdnInfo(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

/**
 * Get list of operators
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getOperators = (req, res, next) => {
    return rechargeService
        .getOperators(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};


/**
 * Get list of operators's products
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getProducts = (req, res, next) => {
    return rechargeService
        .getProducts(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

/**
 * This controller is responsible to complete recharge process with payment
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const makeRecharge = (req, res, next) => {
    return rechargeService
        .makeRecharge(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

const makeRechargeV2 = (req, res, next) => {
    if (req.body.payment_gateway == 1) {
        return rechargeService
            .makeRechargeV2(req)
            .then((data) => sendResponse(req, res, HttpStatus.OK, data))
            .catch((err) => next(err));
    } else {

    }
};

/**
 * This controller is responsible to complete recharge process with payment
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const makeRechargev3 = (req, res, next) => {

    return rechargeService
        .makeRechargev3(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

/**
 * This controller is responsible to complete recharge process with payment
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const makeRechargeBulk = (req, res, next) => {
    return rechargeService
        .makeRechargeBulk(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

/**
 * This api is responsible to get error list
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const GetErrorCodeDescriptions = (req, res, next) => {
    return rechargeService
        .GetErrorCodeDescriptions(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

/**
 * This api is responsible to get country list
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const GetCountries = (req, res, next) => {
    return rechargeService
        .GetCountries(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

const GetProviders = (req, res, next) => {
    return rechargeService
        .GetProviders(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

const getDingProdust = (req, res, next) => {
    return rechargeService
        .getDingProdust(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

const SendTransfer = (req, res, next) => {
    return rechargeService
        .SendTransfer(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};


/*code for  multiple recharge provider*/
const getProviderInfo = (req, res, next) => {
    return rechargeService
        .getProviderInfo(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

const getOperatorList = (req, res, next) => {

    const userType = req.headers['x-user-type'],
        consumerUser = dataConstants.USER_TYPE.USER_CONSUMER;


    return rechargeService
        .getOperatorsList(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));


};

const getProductList = (req, res, next) => {

    const userType = req.headers['x-user-type'],
        consumerUser = dataConstants.USER_TYPE.USER_CONSUMER;

    if (userType == consumerUser) {
        return rechargeService
            .getProductsList(req)
            .then((data) => sendResponse(req, res, HttpStatus.OK, data))
            .catch((err) => next(err));
    } else {
        return rechargeService
            .getProductList(req)
            .then((data) => sendResponse(req, res, HttpStatus.OK, data))
            .catch((err) => next(err));
    }
};

const rechargeProcess = (req, res, next) => {

    if (req.body.payment_method == 1) {
        return rechargeService
            .rechargeProcess(req)
            .then((data) => sendResponse(req, res, HttpStatus.OK, data))
            .catch((err) => next(err));

    } else {

        return rechargeService
            .rechargeCardProcess(req)
            .then((data) => sendResponse(req, res, HttpStatus.OK, data))
            .catch((err) => next(err));
    }

};

const rechargeHisttory = (req, res, next) => {
    return rechargeService
        .rechargeHisttory(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

const rechargeDetails = (req, res, next) => {
    return rechargeService
        .rechargeDetails(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

const userRechargeHisttory = (req, res, next) => {

    return rechargeService
        .userRechargeHisttory(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};
const rechargeByAdmin = (req, res, next) => {
    return rechargeService
        .rechargeByAdmin(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};
const rechargeMonthStatus = (req, res, next) => {
    return rechargeService
        .rechargeMonthStatus(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};
const rechargeDeveloperHistory = (req, res, next) => {
    return rechargeService
        .rechargeDeveloperHistory(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};



/**
 * Get recharge history by user id for download created by @hemant_solenki
 */
const getRechargeHistoryByUserId = (req, res, next) => {
    return rechargeService
        .getRechargeHistoryByUserId(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

const rechargeList = (req, res, next) => {
    return rechargeService
        .rechargeList(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

const userRechargeDetail = (req, res, next) => {
    return rechargeService
        .userRechargeDetail(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

const rechargeDeveloperApiHistory = (req, res, next) => {
    return rechargeService
        .rechargeDeveloperApiHistory(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

const rechargeDeveloperApiDetail = (req, res, next) => {
    return rechargeService
        .rechargeDeveloperApiDetail(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

const saveOperator = (req, res, next) => {
    return rechargeService
        .saveOperator(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

const saveProduct = (req, res, next) => {
    return rechargeService
        .saveProduct(req)
        .then((data) => sendResponse(req, res, HttpStatus.OK, data))
        .catch((err) => next(err));
};

const rechargeController = {
    getMsisdnInfo,
    getOperators,
    getProducts,
    makeRecharge,
    makeRechargeV2,
    makeRechargev3,
    makeRechargeBulk,
    GetErrorCodeDescriptions,
    GetCountries,
    GetProviders,
    getDingProdust,
    SendTransfer,
    getProviderInfo,
    getOperatorList,
    getProductList,
    rechargeProcess,
    rechargeHisttory,
    rechargeDetails,
    userRechargeHisttory,
    rechargeByAdmin,
    rechargeMonthStatus,
    rechargeDeveloperHistory,
    getRechargeHistoryByUserId,
    rechargeList,
    userRechargeDetail,
    rechargeDeveloperApiHistory,
    rechargeDeveloperApiDetail,
    saveOperator,
    saveProduct
    // rechargeDeveloperDetail

};

export default rechargeController;
