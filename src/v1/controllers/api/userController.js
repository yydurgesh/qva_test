import HttpStatus from "http-status-codes";
import { sendResponse } from "~/middlewares/responseHandler";
import * as userService from "~/v2/services/userService";

/**
 * Update user.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const updateUser = ( req, res, next ) => {
   
    return userService
        .updateUser( req, next )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
* Reset password.
*
* @param {Object} req Request.
* @param {Object} res Response.
* @param {Object} next Next request.
*/
const resetPassword = ( req, res, next ) => {
return userService
    .resetPassword( req )
    .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
    .catch( ( err ) => next( err ) );
    }
    /**
 *  user list .
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const userList = ( req, res, next ) => {
   
    return userService
        .getUserList( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
    /**
 *  get user profile.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getMyProfile = ( req, res, next ) => {
   
    return userService
        .getMyProfile( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

 /**
 *  update user profile.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const updateProfile = ( req, res, next ) => {
   
    return userService
        .updateProfile( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
 /**
 *  change password.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const changePassword = ( req, res, next ) => {
   
    return userService
        .changePassword( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
 
 /**
 *  add to customer api.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const createCustomer = ( req, res, next ) => {
    
    return userService
        .createCustomer( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
 /**
 *  change contact number api.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const changeNumber = ( req, res, next ) => {
   
    return userService
        .changeNumber( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

 /**
 *  verify contact number api.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const verifyNumber = ( req, res, next ) => {
   
    return userService
        .verifyNumber( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
const stripeFee = ( req, res, next ) => {
    return userService
        .stripeFee( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const getcommissionSetting = ( req, res, next ) => {
    return userService
        .getcommissionSetting( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
const updateCommission = ( req, res, next ) => {
    return userService
        .updateCommission( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
const getCommissionOperator = ( req, res, next ) => {
    return userService
        .getCommissionOperator( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
const updateCommissionOperator = ( req, res, next ) => {
    return userService
        .updateCommissionOperator( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
const userCommissionSetting = ( req, res, next ) => {
    return userService
        .userCommissionSetting( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const updateUserCommission = ( req, res, next ) => {
    return userService
        .updateUserCommission( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const getCountryWiseOperator = ( req, res, next ) => {
    return userService
        .getCountryWiseOperator( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const getDeveloperIp = ( req, res, next ) => {
    return userService
        .getDeveloperIp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};


const sendNotification = ( req, res, next ) => {
    return userService
        .sendNotification( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};


const updateStripeFeeTax = ( req, res, next ) => {
    return userService
        .updateStripeFeeTax( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const getStripeFeeTax = ( req, res, next ) => {
    return userService
        .getStripeFeeTax( req, req.body.user_type )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const userController = {
    updateUser,
    resetPassword,
    userList,
    getMyProfile,
    updateProfile,
    changePassword,
    createCustomer,
    changeNumber,
    verifyNumber,
    stripeFee,
    getcommissionSetting,
    updateCommission,
    getCommissionOperator,
    updateCommissionOperator,
    userCommissionSetting,
    updateUserCommission,
    getCountryWiseOperator,
    getDeveloperIp,
    sendNotification,
    updateStripeFeeTax,
    getStripeFeeTax
  
};

export default userController;
