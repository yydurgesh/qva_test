import HttpStatus from "http-status-codes";
import { sendResponse } from "~/middlewares/responseHandler";
import * as notificationService from "~/v2/services/notificationService";

/**
 *  user list .
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const notificationList = ( req, res, next ) => {
    return notificationService
        .getNotificationList( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const notificationController = {
    notificationList
}

export default notificationController;