import HttpStatus from "http-status-codes";
import { sendResponse } from "~/middlewares/responseHandler";
import * as userService from "~/v2/services/userService";
import * as walletService from "~/v2/services/walletService";
import * as developerService from "~/v2/services/developerService";
import * as rechargeService from "~/v2/services/rechargeService";
import * as transferService from "~/v2/services/transferService";
import * as authService from "~/v2/services/authService";

/**
 * Get all reseller users list.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getAllResellerUser = ( req, res, next ) => {
    return userService
        .getAllResellerUser( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Gate user Details.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getUserDetail = ( req, res, next ) => {
    return userService
        .getUserDetail( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Update user status
 * 
 * @param {Object} req Request.
 * @param {Object} res Response.
 * @param {Object} next Next request.
 */
const updateUserStatus = ( req, res, next) => {
    return userService
        .updateUserStatus( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
}

/**
 * Get all recharges list.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getAllRecharges = ( req, res, next ) => {
    return userService
        .getAllRecharges( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Get recharge payment list.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getRechargesPaymentList = ( req, res, next ) => {
    return userService
        .getRechargePaymentList( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Payment refund.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const refundRechargeAmount = ( req, res, next ) => {
    return userService
        .rechargeAmountRefund( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Add amount to wallet.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const updateWalletAmount = ( req, res, next ) => {   
    return walletService
        .updateWalletAmount( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Deduct amount to wallet.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const deductWalletAmount = ( req, res, next ) => {   
    return walletService
        .deductWalletAmount( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Get all consumer users list.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getAllConsumerUser = ( req, res, next ) => {
    return userService
        .getAllConsumerUser( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * this api is use for show recharge method list
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getRechargeMethod = ( req, res, next ) => {
    return userService
        .getRechargeMethod( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
/**
 * this api is use for set default recharge method
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const setDefaultMethod = ( req, res, next ) => {
    return userService
        .setDefaultMethod( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Add amount to wallet.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const updateDeveloperWalletAmount = ( req, res, next ) => {   
    return developerService
        .updateDeveloperWalletAmount( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Deduct amount to wallet.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const deductDeveloperWalletAmount = ( req, res, next ) => {   
    return developerService
        .deductDeveloperWalletAmount( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};


/**
 * this api is use for set default recharge method
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const sendAppLink = ( req, res, next ) => {
    return userService
        .sendAppLink( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
}

/**
 * Get rechargeStatus of a month
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const rechargeMonthStatus = ( req, res, next ) => {
    return rechargeService
        .rechargeMonthStatus( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
}


/**
 * Get rechargeStatus of a month
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const rechargeDetail = ( req, res, next ) => {
    return rechargeService
        .userRechargeDetails( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
}

/**
 * Get rechargeStatus of a month
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const userRechargeDetail = ( req, res, next ) => {
    return rechargeService
        .gatUserRechargeDetailByAdmin( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
}

/**
 * Get developer recharge data.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const developerRechargeHistory = ( req, res, next ) => {
    return rechargeService
        .developerRechargeHistory( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
}


/**
 * Get admin Details.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getAdminDetail = ( req, res, next ) => {
    return userService
        .getAdminDetail( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Get developer recharge data
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getdeveloperRechargeHistoryByAdmin = ( req, res, next ) => {
    return rechargeService
        .getdeveloperRechargeHistoryByAdmin( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
}


/**
 * Get developer recharge data
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getdeveloperRechargeDetailByAdmin = ( req, res, next ) => {
    return rechargeService
        .getdeveloperRechargeDetailByAdmin( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
}

/**
 * Get whole transaction list data
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getTransactionListByAdmin = ( req, res, next ) => {
    return walletService
        .getTransactionListByAdmin( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
}

/**
 * Get user transaction data
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getUserTransactionListByAdmin = ( req, res, next ) => {
    return walletService
        .getUserTransactionListByAdmin( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
}

/**
 * Get user transaction data
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getTransferListByAdmin = ( req, res, next ) => {
    return transferService
        .getTransferListByAdmin( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
}

/**
 * Get user data.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getUserData = ( req, res, next ) => {
    return userService
        .getUserData( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Save user recharge limit.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const saveRechargeLimit = ( req, res, next ) => {
    return rechargeService
        .saveRechargeLimit( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Get user recharge limit data.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getUserRechargesLimit = ( req, res, next ) => {
    return rechargeService
        .getUserRechargesLimit( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Delete user recharge limit data.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const deleteRechargeLimit = ( req, res, next ) => {
    return rechargeService
        .deleteRechargeLimit( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Update admin profile.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const updateAdminProfile = ( req, res, next ) => {
   
    return userService
        .updateAdminProfile( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * Update user.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const updateUserProfileByAdmin = ( req, res, next ) => {
   
    return userService
        .updateUserProfileByAdmin( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};


/**
 * send user approve otp.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const sendUserApproveOtp = ( req, res, next ) => {
   
    return authService
        .sendUserApproveOtp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

/**
 * send user approve otp.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const verifyUserApproveOtp = ( req, res, next ) => {
   
    return authService
        .verifyUserApproveOtp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const adminUserController = {
    getUserDetail, getAllResellerUser, updateUserStatus, getAllRecharges, getRechargesPaymentList, refundRechargeAmount, updateWalletAmount, deductWalletAmount,
    getAllConsumerUser,getRechargeMethod,setDefaultMethod, updateDeveloperWalletAmount, deductDeveloperWalletAmount, sendAppLink, rechargeMonthStatus, rechargeDetail, developerRechargeHistory, getAdminDetail,userRechargeDetail, getdeveloperRechargeHistoryByAdmin, getdeveloperRechargeDetailByAdmin,
    getTransactionListByAdmin, getUserTransactionListByAdmin, getTransferListByAdmin, getUserData, saveRechargeLimit, getUserRechargesLimit, deleteRechargeLimit, updateAdminProfile, updateUserProfileByAdmin, sendUserApproveOtp, verifyUserApproveOtp
};

export default adminUserController;
