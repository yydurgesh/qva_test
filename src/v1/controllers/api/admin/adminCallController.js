import HttpStatus from "http-status-codes";
import * as callService from "~/v2/services/callService";
import { sendResponse } from "~/middlewares/responseHandler";

/**
 * Get Call List.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */

const getUserCalls = ( req, res, next ) => {
    return callService
        .getUserCalls( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const getCallDetailByAdmin = ( req, res, next ) => {
    return callService
        .getCallDetails( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const adminCallController = {
    getUserCalls,
    getCallDetailByAdmin
};

export default adminCallController;
