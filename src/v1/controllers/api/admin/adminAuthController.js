import HttpStatus from "http-status-codes";
import * as authService from "~/v2/services/authService";
import { sendResponse } from "~/middlewares/responseHandler";

/**
 * Admin Login.
 *
 * @param  {object} req
 * @param  {object} res
 * @param  {Object} next Next request.
 */
const adminLogin = function( req, res, next ) {
    
    return authService
        .adminLogin( req.body )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );  
}; 

/**
 * Verify Otp for ip address.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const verifyAdminOtp = ( req, res, next ) => {
    return authService
        .verifyAdminOtp( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const adminAuthController = {
    adminLogin,
    verifyAdminOtp
};

export default adminAuthController;
