import HttpStatus from "http-status-codes";
import { sendResponse } from "~/middlewares/responseHandler";
import * as cardService from "~/v2/services/cardService";

 /**
 *  add to card api.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const addCard = ( req, res, next ) => {
   
    return cardService
        .addCard( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

 /**
 *  card list api.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const cardList = ( req, res, next ) => {
   
    return cardService
        .cardList( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
 /**
 *  set detault  card  api.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const setDetaultCard = ( req, res, next ) => {
   
    return cardService
        .setDetaultCard( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
 /**
 *  set delete  card  api.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const deleteCard = ( req, res, next ) => {
   
    return cardService
        .deleteCard( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};
 /**
 *  set create  card  api.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const createCard = ( req, res, next ) => {
   
    return cardService
        .createCard( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};


 /**
 *  Verify  card  api.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const cardVerify = ( req, res, next ) => {
    return cardService
        .verifyCard( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

 /**
 *  Verify  card  api.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const userCardVerify = ( req, res, next ) => {
    return cardService
        .verifyUserCardByAdmin( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

 /**
 *  card list api.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
const getCardListByadmin = ( req, res, next ) => {
    return cardService
        .getCardListByadmin( req )
        .then( ( data ) => sendResponse( req, res, HttpStatus.OK, data ) )
        .catch( ( err ) => next( err ) );
};

const cardController = {
    addCard,
    cardList,
    setDetaultCard,
    deleteCard,
    createCard,
    cardVerify,
    userCardVerify,
    getCardListByadmin

};

export default cardController;
