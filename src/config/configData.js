import dataConstants from "../constants/dataConstants";
import statusConstants from "../constants/statusConstants";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";

const localeService = new LocaleService( i18n ),
    configData = {
        "USER_TYPE": [],
        "USER_STATUS": []
    };

configData.USER_TYPE[ dataConstants.USER_TYPE.USER_CONSUMER ] = localeService.translate( "USER_TYPE_CONSUMER" );
configData.USER_TYPE[ dataConstants.USER_TYPE.USER_ADMIN ] = localeService.translate( "USER_TYPE_ADMIN" );

configData.USER_STATUS[ statusConstants.USER_STATUS.PENDING ] = localeService.translate( "USER_STATUS_PENDING" );
configData.USER_STATUS[ statusConstants.USER_STATUS.ACTIVE ] = localeService.translate( "USER_STATUS_ACTIVE" );
configData.USER_STATUS[ statusConstants.USER_STATUS.INACTIVE ] = localeService.translate( "USER_STATUS_INACTIVE" );

export default configData;
