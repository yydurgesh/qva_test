import path from "path";
import EmailTemplates from "email-templates";
import transport from "~/utils/transport";

const templatePath = path.join( __dirname, "../emails" ),

emailConfig = new EmailTemplates( {

"message": {
"from": process.env.FROM_SENDER_EMAIL
},
"views": { "root": templatePath },
"send": true,
"preview": false,
"transport": transport
} );

export default emailConfig;