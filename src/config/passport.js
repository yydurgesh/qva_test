import passport from "passport";
import passportJwt from "passport-jwt";
import commonConstants from "~/constants/comman";
var LocalStrategy   = require('passport-local').Strategy;
import Boom from "boom";

import * as userService from "~/v1/services/userService";


const JwtStrategy = passportJwt.Strategy,
    ExtractJwt = passportJwt.ExtractJwt,
    opts = {
        "jwtFromRequest": ExtractJwt.fromAuthHeaderWithScheme( "JWT" ),
        "secretOrKey": process.env.JWT_SECRET//commonConstants.JWT_SECRET //
    },
    passportLb = passport.use(
        "jwt",
        new JwtStrategy( opts, ( jwtPayload, done ) => {
           
            userService
                .getUser( jwtPayload.user_id )
                .then( ( user ) => {
                    
                    if(user[ 0 ].is_profile_done == 1){
                        userService
                        .checkDeviceId( jwtPayload.user_id, jwtPayload.deviceId)
                        .then( ( devie ) => {
                          
                            return done( null, user[ 0 ].user_id );
                            
                        }) 
                        .catch( ( error ) => {
                            

                            return done( error, false );
                        } );
                    }else{
                        return done( null, user[ 0 ].user_id );
                    }
                    
                } )
                .catch( ( error ) => {
                    return done( error, false );
                } );
        } )
    );

     // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    export const passports = passport.use('user-signup',new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req,  done) {
        var data = req.body ;
        userService
            .is_exist_user( data, 1 )
            .then( ( user ) => {
              
                if(user.is_email.length == 0){
                    console.log("user", user.is_email.length)
                    throw Boom.badRequest( "email number already exist" );
                  
                    
                }else if(user.is_number.length > 0){
                   
                  //  return done("Contact number already exist", false );
                    throw Boom.badRequest("Contact number already exist");
                }
                else{
                    return done( null,true );
                }
                
            } )
            .catch( ( error ) => {
                return error;
            } );
      } )
    );
    
   
export default passportLb;
