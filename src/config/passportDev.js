import passport1 from "passport";
import passportJwt1 from "passport-jwt";
import commonConstants from "~/constants/comman";

import * as developerService from "~/v2/services/developerService";
const JwtStrategy1 = passportJwt1.Strategy,
    ExtractJwt = passportJwt1.ExtractJwt,
    opts = {
        "jwtFromRequest": ExtractJwt.fromAuthHeaderWithScheme( "JWT" ),
        "secretOrKey": process.env.JWT_SECRET//commonConstants.JWT_SECRET
    },
    passportLbDev = passport1.use(
        "jwt",
        new JwtStrategy1( opts, ( jwtPayload, done ) => {
            
            developerService
                .getUser( jwtPayload.developer_id )
                .then( ( user ) => {
                    return done( null, user[ 0 ].developer_id );
                } )
                .catch( ( error ) => {
                    return done( error, false );
                } );
        } )
    );

export default passportLbDev;
