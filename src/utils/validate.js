import Joi from "joi";
import i18n from "~/config/i18n.config";


/**
 * Utility helper for Joi validation.
 *
 * @param  {object}  data
 * @param  {object}  schema
 * @returns {Promise}
 */
const validate = function( data, schema ) {
    console.log('default',i18n.getLocale());
   
    return Joi.validate( data, schema, { "abortEarly": false }, ( err ) => {
        if ( err ) {
            return Promise.reject( err );
        }

        return Promise.resolve( null );
    } );
};

export default validate;
