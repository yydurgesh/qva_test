import { Security } from "~/libraries/Security";
const securityObj = new Security();

/**
 * Convert all keys value into encryption.
 *
 * @param  {response} response
 * @param  {keys} keys
 * @returns {Object}
 */
const encryptAllKeys = function( response, keys ) {
    let i;

    for ( i = 0; i < keys.length; i++ ) {
        if ( response[ keys[ i ] ] !== undefined ) {
            response[ keys[ i ] ] = securityObj.encrypt( response[ keys[ i ] ] );
        }
    }

    return response;
};

/**
 * Convert all keys value into encryption.
 *
 * @param  {response} response
 * @param  {keys} ids
 * @returns {Object}
 */
const encryptobjectAllKeys = function( response, ids ) {

    return Promise.all(
        response.map( ( keys ) => {
            keys[ ids ] = securityObj.encrypt( keys[ ids ] );
    
            return keys;
        } )
    );
};

const responseHelper = {
    encryptAllKeys,
    encryptobjectAllKeys
};

export default responseHelper;
