import bcrypt from "bcrypt";
import commonConstants from "~/constants/comman";
import util from "~/utils/util";
/**
 * Crypt password.
 *
 * @param  {String}   password
 * @returns {String}
 */
const cryptPassword = function( password ) {
    const hash = bcrypt.hashSync( password, commonConstants.PASSWORD_SALT_ROUNDS );

    return hash;
};

/**
 * Compare password.
 *
 * @param  {String}   password
 * @param  {String}   hash
 * @returns {String}
 */
const compareSync = function( password, hash ) {
    return bcrypt.compareSync( password, hash );
};
/**
* Generate random password.
*
* @returns {String}
*/
const generateRandomPassword = function() {
	const newPassword = {};

	newPassword.password = util.generatRandomString( commonConstants.INTEGER_RANDOME_NUMBER, commonConstants.RANDOME_NUMBER_LENGTH );
	newPassword.hashPassword = bcrypt.hashSync( newPassword.password, commonConstants.PASSWORD_SALT_ROUNDS );

	return newPassword;
};

const password = {
    cryptPassword,
    compareSync,
    generateRandomPassword
};

export default password;
