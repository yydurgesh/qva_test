import nodemailer from "nodemailer";

/**
* Create nodmailer transport object.
*/

const transporter = nodemailer.createTransport(
{
"host": process.env.MAIL_HOST,
 "service": 'gmail',
"port": process.env.MAIL_PORT,
"secure": false,
"auth": {
"user": process.env.MAIL_USERNAME,
"pass": process.env.MAIL_PASSWORD
},
"logger": false,
"debug": false,
"tls": {
"rejectUnauthorized": false
}
}
// ,
// {
// "from": "Fellower <admin@fellower.com>"
// }
);

export default transporter;