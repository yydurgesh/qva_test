import convert from "xml-js";
import lodash from "lodash";
import DateTimeUtil from "~/utils/DateTimeUtil";
/**
 * Static utility class.
 */
export class convertData {

    constructor() { 
        this.options = {compact: true, ignoreComment: true, spaces: 4};
    }

    /**
    * Helper method to convert json to xml
    * 
    * @param {JSON} 
    * @returns {XML}
    */
    json2xml( jsonData ) {
       var xmlFormData = convert.json2xml(jsonData, this.options);
       return xmlFormData;
    }

    /**
    * Helper method to convert xml to JSON
    * 
    * @param {XML} 
    * @returns {JSON}
    */
    xml2json( xmlData ) {
       var jsonData = convert.xml2json(xmlData, this.options);
       return JSON.parse(jsonData);
    }

    /**
    * Helper method to convert list to JSON
    * 
    * @param {XML} 
    * @returns {JSON}
    */
    
    async dtonInfojson( List )  {
       var newArray = [];
            return new Promise((resolve) => {
                let jsonObj = {
                    "countryId":List.countryid._text,
                    "operatorId": List.operatorid._text,
                    "contactNumber":List.destination_msisdn._text,
                    "RegionCodes":''
                }

                newArray.push(jsonObj);
                resolve(jsonObj);
            });
        return newArray;
     
    }
    async dingInfojson( List )  {
       var newArray = [];
            return new Promise((resolve) => {
                let jsonObj = {
                   "countryId":List.CountryIso ? List.CountryIso: '',
                    "operatorId": List.Items ? List.Items[0].ProviderCode : '' ,
                    "contactNumber":List.AccountNumberNormalized ? List.AccountNumberNormalized :'',
                    "RegionCodes": List.Items ? List.Items[0].RegionCode : '',
                }

                newArray.push(jsonObj);
                resolve(jsonObj);
            });
        return newArray;
     
    }
    async list2json( idList, valueList ) {
    
       let idArray = idList.split(',');
       let valueArray = valueList.split(',');
       var newArray = [];
       let promises = idArray.map((id, index) => {
            return new Promise((resolve) => {
                let jsonObj = {
                    "id":id,
                    "value": valueArray[index],
                    "RegionCodes":''
                }

                newArray.push(jsonObj);
                resolve(jsonObj);
            });
       });

       return Promise.all(promises).then(() => newArray);
    }

    /**
    * Helper method to convert list to JSON
    * 
    * @param {XML} 
    * @returns {JSON}
    */
    async list2productArray( productList, currency ) {
       let producArray = productList.split(',');
       var newArray = [];
       let promises = producArray.map((product, index) => {
            return new Promise((resolve) => {
                let jsonObj = {
                    "product":product,
                    "currency": currency,
                     "skuCode":'',
                     "localizationKey":'',
                     "regionCode":''

                }

                newArray.push(jsonObj);
                resolve(jsonObj);
            });
       });

       return Promise.all(promises).then(() => newArray);
    }

    async dingOperatorjson( idList, valueList='' ) {
       let idArray = idList.Items;
       var newArray = [];
       let promises = idArray.map((opr, index) => {
            return new Promise((resolve) => {
                let jsonObj = {
                    "id":opr.ProviderCode,
                    "value": opr.Name,
                    "regionCodes":opr.RegionCodes[0],
                    "logoUrl": opr.LogoUrl,
                    "operatorCountry": opr.CountryIso
                }

                newArray.push(jsonObj);
                resolve(jsonObj);
            });
       });

       return Promise.all(promises).then(() => newArray);
    }
    async dingProductsjson( idList, valueList='' ) {
       let producArray = idList.Items;
       var newArray = [];
       let currentDateTime = DateTimeUtil.getCurrentTimeObjForDB();
       const proudctLength = producArray.length;
       for(let i =0 ; producArray.length > i; i++){
           let startVal =producArray[i].Minimum.SendValue;
           let endVal =producArray[i].Maximum.SendValue + startVal;

           let startValReceive =producArray[i].Minimum.ReceiveValue;
           let endValReceive =producArray[i].Maximum.ReceiveValue + startValReceive;

            if(producArray[i].Minimum.SendValue !== producArray[i].Maximum.SendValue){
            var productMap =  await lodash.range(startVal,endVal,startVal)
            var ReceiveMap =  await lodash.range(startValReceive,endValReceive,startValReceive)

                productMap.map((product, index) => {
                   
                        let jsonObj = {
                        "recharge_amount":product,
                        "receive_amount" :ReceiveMap[index],
                        "provider_code":producArray[i].ProviderCode,
                        "send_currency_iso": producArray[i].Maximum.SendCurrencyIso,
                        "receive_currency_iso": producArray[i].Maximum.ReceiveCurrencyIso,
                        "sku_code":producArray[i].SkuCode,
                        "localization_key": producArray[i].LocalizationKey,
                        "region_code":producArray[i].RegionCode,
                        "created_at": currentDateTime,
                        "updated_at": currentDateTime
                        }
                        newArray.push(jsonObj);
                    
                    });
            }else{

                let jsonObj = {
                    "recharge_amount":producArray[i].Minimum.SendValue,
                    "receive_amount" :producArray[i].Minimum.ReceiveValue,
                    "provider_code":producArray[i].ProviderCode,
                    "send_currency_iso": producArray[i].Maximum.SendCurrencyIso,
                    "receive_currency_iso": producArray[i].Maximum.ReceiveCurrencyIso,
                    "sku_code":producArray[i].SkuCode,
                    "localization_key": producArray[i].LocalizationKey,
                    "region_code":producArray[i].RegionCode,
                    "created_at": currentDateTime,
                    "updated_at": currentDateTime
                    }
                    newArray.push(jsonObj); 
            }
            console.log("newArray", newArray)
            // else{


            // }

        }

    }


    async dingCreateOperatorjson( idList, userId,userType,date ) {
       let idArray = idList.Items;
       var newArray = [];
       let promises = idArray.map((opr, index) => {
            return new Promise((resolve) => {
                let jsonObj = {
                    "cos_user_id":userId,
                    "country_id":opr.CountryIso,
                    "operator_id":opr.ProviderCode,
                    "operator_name": opr.Name,
                    "cos_user_type":userType,
                    "created_at":date,
                    "updated_at":date
                }

                newArray.push(jsonObj);
                resolve(jsonObj);
            });
       });

       return Promise.all(promises).then(() => newArray);
    }
    async countryConvertjson( idList,date ) {
       let idArray = idList.Items;
       var newArray = [];
       let promises = idArray.map((opr, index) => {
            return new Promise((resolve) => {
                let jsonObj = {
                    "country_id":opr.CountryIso,
                    "country_name":opr.CountryName,
                    "created_at":date,
                    }

                newArray.push(jsonObj);
                resolve(jsonObj);
            });
       });

       return Promise.all(promises).then(() => newArray);
    }

    //created by hemant

    async dingOperatorjsonToArray( idList, valueList='' ) {
       
       let idArray = idList.Items;
       var newArray = [];
       let promises = idArray.map((opr, index) => {
            return new Promise((resolve) => {
                let jsonObj = {
                    "operator_id":opr.ProviderCode,
                    "value": opr.Name,
                    "regionCodes":opr.RegionCodes[0],
                    "logoUrl": opr.LogoUrl,
                    "operatorCountry": opr.CountryIso
                }

                newArray.push(jsonObj);
                resolve(jsonObj);
            });
       });

       return Promise.all(promises).then(() => newArray);
    }
}
