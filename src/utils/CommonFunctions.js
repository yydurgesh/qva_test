import databaseConstants from "../constants/databaseConstants";
import configData from "~/config/configData";
import lodash from "lodash";
import { Security } from "~/libraries/Security";
import i18n from "~/config/i18n.config";
import { LocaleService } from "~/utils/localeService";
const localeService = new LocaleService(i18n);
/**
 * Static CommonFunctionUtil class.
 */
class CommonFunctionUtil {
    /**
     * Helper method to check array of inex value.
     *
     * @param {array} dataArray
     * @returns {Integer}.
     */
    static checkIndexValue(dataArray) {
        let checkValue;
        const a = 2,
            b = 1;

        if (dataArray.includes(a, 0) && dataArray.includes(b, 1)) {
            checkValue = 2;
        } else if (dataArray.includes(b, 0) && dataArray.includes(a, 1)) {
            checkValue = 1;
        } else if (dataArray.includes(b, 0) && dataArray.includes(b, 1)) {
            checkValue = 3;
        } else if (dataArray.includes(a, 0) && dataArray.includes(a, 1)) {
            checkValue = 4;
        }

        return checkValue;
    }

    /**
     * Helper method to return media type.
     *
     * @param {string} media
     * @returns {string}.
     */
    getMediaType(media) {
        let ext = media.split(".").pop();
        const imageType = ["jpeg", "jpg", "png", "gif"];

        if (imageType.includes(ext)) {
            ext = databaseConstants.ORDER_MEDIA_TYPE.IMAGE;
        }
        switch (ext) {
            case databaseConstants.ORDER_MEDIA_TYPE.IMAGE:
                return databaseConstants.ORDER_MEDIA_TYPE.IMAGE;
            case "pdf":
                return databaseConstants.ORDER_MEDIA_TYPE.PDF;
            case "zip":
                return databaseConstants.ORDER_MEDIA_TYPE.ZIP;
            case "psd":
                return databaseConstants.ORDER_MEDIA_TYPE.PSD;
            default:
                return "N/A";
        }
    }

    /**
     * Helper method to return user levels.
     *
     * @param {string} ratePoint
     * @returns {string}.
     */
    static userLevels(ratePoint) {
        if (ratePoint >= databaseConstants.SELLER_LEVEL_FROM_1 && ratePoint <= databaseConstants.SELLER_LEVEL_TO_1) {
            return configData.SELLER_LEVELS[databaseConstants.SELLER_LEVEL_NEW];
        } else if (
            ratePoint >= databaseConstants.SELLER_LEVEL_FROM_2 && ratePoint <= databaseConstants.SELLER_LEVEL_TO_2
        ) {
            return configData.SELLER_LEVELS[databaseConstants.SELLER_LEVEL_1];
        } else if (
            ratePoint >= databaseConstants.SELLER_LEVEL_FROM_3 && ratePoint <= databaseConstants.SELLER_LEVEL_TO_3
        ) {
            return configData.SELLER_LEVELS[databaseConstants.SELLER_LEVEL_2];
        }

        return configData.SELLER_LEVELS[databaseConstants.SELLER_TOP_RATED];
    }

    /**
     * Helper method to get values by array.
     *
     * @param {Array} dataArray
     * @param {Array} findArray
     * @returns {string}.
     */
    static async getValuesByArray(dataArray, findArray) {
        const promises = findArray.map(async (masterID) => {
            const findValue = await lodash.find(dataArray, { "project_metadata_master_id": masterID });

            return findValue;
        });

        return await Promise.all(promises);
    }


      get_banner_data() {
         
          var userDat;
          userDat = {
            Home: localeService.translate("Home"),
            Transfer: localeService.translate("Transfer"),
            Recharge: localeService.translate("Recharge"),
            Messaging: localeService.translate("Messaging"),
            Documentation: localeService.translate("Documentation"),
            Login: localeService.translate("Login"),
            English: localeService.translate("English"),
            Spanish: localeService.translate("Spanish"),
            mobile_required: localeService.translate('The mobile number field is mandatory'),
            please_enter_valid_mob: localeService.translate('Please enter a valid number.'),
            The_password_field_is_mandatory: localeService.translate('The password field is mandatory'),
            Minimum_length_character: localeService.translate('Minimum length 8 character'),
            Password_message_should_be: localeService.translate('Password_message_should_be'),
            The_email_mandatory: localeService.translate('The email field is mandatory'),
            Please_valid_email_address: localeService.translate('Please enter a valid email address'),
            The_otp_is_mandatory: localeService.translate('The otp field is mandatory'),
            The_name_field_is_mandatory: localeService.translate('The user name field is mandatory'),
            The_name_two: localeService.translate('User name length must be at least 2 characters long'),
            The_name_max: localeService.translate('User name length must be less than or equal to 255 characters long'),
            All_rights_reserved: localeService.translate("All rights reserved"),
    
            OTP_verified_successfully: localeService.translate("OTP verified successfully"),
            OTP_sent_successfully: localeService.translate("OTP sent successfully"),
            No_record_found: localeService.translate("No record found"),
            Message_history_not_found: localeService.translate("Message history not found"),
            Enter_characters: localeService.translate("Enter your message 2-250 characters"),
            Reseller_Commission: localeService.translate("Reseller Commission"),
            My_Profile: localeService.translate("My Profile"),
            at_list: localeService.translate("Please enter at least 2 characters."),
            otp_field_is_mandatory: localeService.translate("The otp field is mandatory"),
            old_password: localeService.translate("The old password field is mandatory"),
            The_confirm_password: localeService.translate("The confirm password field is mandatory"),
            Enter_confirm_password: localeService.translate("Enter confirm password same as new password"),
            Please_enter_your_email: localeService.translate("Please enter your email."),
            Recharge_amount_is_greater: localeService.translate("Recharge_amount_is_greater"),
            Please_acknowledge_you: localeService.translate("Please_acknowledge_you"),
            Please_newpassword: localeService.translate("Please enter new password"),

            // new data
            Features: localeService.translate('Features'),
            Download: localeService.translate('Download'),
    
    
    
            Language: i18n.getLocale()
        };
        return userDat ;
     }

    /**
     * Helper method to return user id.
     *
     * @param {string} request
     * @returns {string}.
     */
    getUserId(request) {
        const requestData = request.query;

        if (requestData.user_id === undefined) {

            return request.user;
        }

        return Security.decryptId(requestData.user_id);
    }

    /**
     * Helper method to return pagination offset.
     *
     * @param {string} page
     * @param {string} limit
     * @returns {string}.
     */
    getPageOffset(page, limit) {
        if (page !== undefined && page !== "") {
            const offset = (page * limit) - limit;

            return offset;
        }

        return databaseConstants.OFFSET; // set default offset
    }

    getArrySum(arrayData, keyData) {
        let ardata = lodash.sumBy(arrayData, function (key) {
            return Number(key[keyData]);
        });

        return ardata;
    }

    getDifference(firstArray, secondArray, compareBy) {
        let ardata = lodash.differenceBy(firstArray, secondArray, compareBy)
        return ardata;
    }

    getMatchedData(firstArray, secondArray, compareBy) {
        let ardata = lodash.intersectionBy(firstArray, secondArray, compareBy)
        return ardata;
    }
}

/**
 * @module
 * @type {CommonFunctionUtil}
 */
module.exports = CommonFunctionUtil;
