import winston, { format } from "winston";
import dataConstants from "~/constants/dataConstants";
import DateTimeUtil from "~/utils/DateTimeUtil";
import "winston-daily-rotate-file";

const LOG_DIR = process.env.LOG_DIR || "logs",
    LOG_LEVEL = process.env.LOG_LEVEL || "info",
    { combine, timestamp, printf, colorize } = format,
    /**
     * Create a new winston logger.
     */
    logger = winston.createLogger( {
        "transports": [
            new winston.transports.DailyRotateFile( {
                "format": format.combine(
                    colorize(),
                    timestamp(),
                    printf( ( info ) => {
                        return JSON.stringify( info.message );
                    } )
                ),
                "maxFiles": "14d",
                "level": LOG_LEVEL,
                "dirname": LOG_DIR,
                "datePattern": "YYYY-MM-DD",
                "filename": "%DATE%-batch-payment-response-debug.log"
            } )
        ]
    } );

/**
 * A writable stream for winston logger.
 */
export const logStream = {
    write( message ) {
        logger.info( message.toString() );
    }
};
const logformatter = ( type, message, title = "" ) => {
        switch ( type ) {
            case "start":
                logger.info(
                    "==================================================================================================="
                );
                logger.info( `Get Batch Response Cron run At ${DateTimeUtil.getCurrentTimeObjForCron()} ::` );
                logger.info(
                    "==================================================================================================="
                );
                break;
            case "body":
                logger.info( "*************************************" );
                logger.info( title );
                logger.info( "*************************************" );
                logger.info( { "message": message } );
                break;
            default:
                break;
        }
    },
    getBatchPaymentLogger = {
        "logger": logger,
        "logformatter": logformatter
    };

export default getBatchPaymentLogger;
