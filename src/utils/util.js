import randome from "randomatic";
const camelCase = require("lodash.camelcase");
const snakeCase = require("lodash.snakecase");
const transform = require("lodash.transform");
const crypto = require("crypto");

/**
 * Static utility class.
 */
class Util {
    /**
     * Helper method to convert an object into a snake_case format for use with knex.
     *
     * @param {Object} obj The object to convert.
     * @returns {Object} The resultant snake_case object.
     */
    static toSnake(obj) {
        return transform(
            obj,
            (result, value, key) => {
                result[snakeCase(key)] = value;
            },
            {}
        );
    }

    /**
     * Helper method to convert an object into a camelCase format for use with after knex.
     *
     * @param {Object} obj The object to convert.
     * @returns {Object} The resultant camelCase object.
     */
    static toCamel(obj) {
        return transform(
            obj,
            (result, value, key) => {
                result[camelCase(key)] = value;
            },
            {}
        );
    }
    /**
    Helper method to generate otp.

    * @param {Object} pattern The object to convert.
    * @param {Object} length The object to convert.
    * @returns {Object} The resultant snake_case object.
    */
    static generatOtp(pattern, length) {
        return randome(pattern, length);
    }

    /**
    * Helper method to generate unique Id.
    * 
    * @returns {string} Nonce has to be unique for each request.
    */
    static getNonceForDtone() {
        const nonce = (new Date()).getTime();

        return nonce;
    }

    /**
    * Helper method to generate hmac for dtone Goods and service Lib.
    * 
    * @param {string} apiKey API Key.
    * @param {string} apiSecretKey API Secret Key.
    * @param {string} nonce Nonce for unique Id.
    * @returns {string} Hash code in base64 format.
    */
    static getHmacForDtone(apiKey, apiSecretKey, nonce) {
        const hmac = crypto.createHmac("sha256", apiSecretKey).update(apiKey + nonce).digest("base64");

        return hmac;
    }

    /**
    * Helper method to generate MD% hex hash code .
    * 
    * @param {string} loginName API loginId.
    * @param {string} airTimeToken API Token.
    * @param {string} key Key for unique Id.
    * @returns {string} Hash code in base64 format.
    */
    static generateMd5HashDtone(loginName, airTimeToken, key) {
        const Md5Data = loginName + airTimeToken + key,
            md5Hash = crypto.createHash("md5").update(Md5Data).digest("hex");

        return md5Hash;
    }
    /**
    * Helper method to generate random string.
    *
    * @param {Object} pattern The object to convert.
    * @param {Object} length The object to convert.
    * @returns {Object} The resultant snake_case object.
    */
    static generatRandomString(pattern, length) {
        return randome(pattern, length);
    }
    static generateRandomSerise(min, max) {
        const rmNum = (Math.random() * (max - min) + min).toFixed(2);
        return rmNum;

    }
}

/**
 * @module
 * @type {Util}
 */
module.exports = Util;
