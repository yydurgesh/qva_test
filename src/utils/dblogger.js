import winston from "winston";
import expressWinston from "express-winston";
//import * as requestService from "~/services/requestService";

// Log the whole request and response body
expressWinston.requestWhitelist.push( "body" );
expressWinston.responseWhitelist.push( "body" );

/**
 * Create a new winston logger.
 */
const dblogger = expressWinston.logger( {
    "transports": [ new winston.transports.Http() ],
    "meta": true,
    /**
     * Getting all request.
     *
     * @param  {object}   req
     * @param  {object}   res
     * @returns {Promise}
     */
    "dynamicMeta": function( req, res ) {
//         if ( req.method === "POST" || req.method === "GET" || req.method === "PUT" || req.method === "DELETE" ) {
//             return requestService.createRequest( req, res );
//         }

        return false;
    }
} );

export default dblogger;
