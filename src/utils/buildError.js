import HttpStatus from "http-status-codes";
import Boom from "boom"; 

/**
 * Build error response for validation errors.
 *
 * @param  {Error} err
 * @returns {Object}
 */
const buildError = function( err ) {
    // Validation errors
    if ( err.isJoi ) {
        return {
            "code": HttpStatus.BAD_REQUEST,
            // "message": HttpStatus.getStatusText( HttpStatus.BAD_REQUEST ),
            "message": "Seems you have entered information incorrect. Please verify once and continue",
            "details":
                err.details && err.details.map( ( error ) => {
                    return {
                        "message": error.message,
                        "param": error.path.join( "." )
                    };
                } )
        };
    }

    // HTTP errors
    if ( err.isBoom ) {
        return {
            "code": err.output.statusCode,
            "message": err.output.payload.message || err.output.payload.error
        };
    }

    // Return INTERNAL_SERVER_ERROR for all other cases
    return {
        "code": HttpStatus.INTERNAL_SERVER_ERROR,
        // "message": HttpStatus.getStatusText( HttpStatus.INTERNAL_SERVER_ERROR )
        "message": "Something is wrong from server side, so please try later"
    };
};

export default buildError;
