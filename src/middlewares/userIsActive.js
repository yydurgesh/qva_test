import Boom from "boom";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";

import * as userService from "~/v1/services/userService";

const localeService = new LocaleService(i18n);

/**
 * Check user is active or not.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 */
exports.isActiveUser = (req, res, next) => {

    return userService
        .getUser(req.body.user_id)
        .then((data) => {

            if (data[0].status != 2) {

                throw Boom.forbidden(localeService.translate("You are inactived by admin, Please contact to admin"));
            } else {

                next();
            }
        })
        .catch((err) => next(err));
};