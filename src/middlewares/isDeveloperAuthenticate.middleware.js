var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
import commonConstants from "~/constants/comman";

function verifyTokens(req, res, next) {
  
  var token = '';
  if (req.cookies.developer_data) {
    var split = req.cookies.developer_data.split(' ');
    token = split[1];
  }
  if (token) {
    
    // verifies secret and checks exp
    jwt.verify(token, commonConstants.JWT_SECRET, function (err, decoded) {
      if (err)
        return res.redirect('/');
      
      req.userId = decoded.developer_id;
      req.current = "";
      var loginDeveloperData = decoded;//req.headers['x-access-token'];
      if (loginDeveloperData) {
        res.locals.loginDeveloperData = loginDeveloperData;

        if (req.originalUrl == "/developer-signin" || req.originalUrl == "/developer-signin?userType=developer" && req.originalUrl != "/logout") {
          return res.redirect('/developer-Profile');
        }
      }
      next();
    });

  } else {
    next();
  }
}

module.exports = verifyTokens;
