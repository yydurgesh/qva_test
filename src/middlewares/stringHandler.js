/**
 * Middleware to sanitizing and triming JSON body requests.
 *
 * @param  {Object}   request
 * @param  {Object}   response
 * @param  {Function} next
 */
export default function stringHandler( request, response, next ) {
    const requestBody = Object.keys( request.body );

    requestBody.map( ( key, index ) => {
        if ( typeof request.body[ key ] === "string" ) {
            request.body[ key ] = request.sanitize( request.body[ key ].trim() );
        }
    } );
    next();
}
