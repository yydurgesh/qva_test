var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
import envConstants from "~/constants/envConstants";
import statusConstants from "~/constants/statusConstants";
import * as authService from "~/v1/services/authService";
const constantsConfig = envConstants.JWT_SECRET;;
const isUsersAuthenticate = function( req, res, next ) {
  const token = req.session.userAuthToken;
  
  if ( token ) {
      // verifies secret and checks exp
      jwt.verify( token, constantsConfig, ( err, decoded ) => {
      
          if ( err ) {
              req.session.destroy();

              return next();
          }
          
          const loginUserData = req.session.loginUserData;
          
          if ( loginUserData ) {

              if ( req.originalUrl === "/" || req.originalUrl === "/signup" || req.originalUrl === "/login" || req.originalUrl === "/auth/reset-password" ) {
               
                authService.getWebUser( req, res, decoded.id )
                      .then( ( storeAuthData ) => {
                          
                          const userData = {
                              "id": storeAuthData[ 0 ].id,
                              
                          };

                          // Check store status active or not.
                          if ( storeAuthData[ 0 ].status != statusConstants.USER_STATUS.ACTIVE ) { // if In-active.
                           
                              req.session.destroy();

                              return res.redirect( "/" );
                          }
                          
                       
                         
                          req.user = userData;
                          req.current = "";
                          
                          return res.redirect( "/recharge" );
                      } )
                      .catch( ( error ) => {
                         
                          req.session.destroy();

                          return res.redirect( "/" );
                      } );
                  
              } else {
                  req.session.destroy();

                  return res.redirect( "/" );
              }
          } else {
              req.session.destroy();
              
              return next();
          }
      } );

  } else {
      next();
  }
};

module.exports = isUsersAuthenticate;
