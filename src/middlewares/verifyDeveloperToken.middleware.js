var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

import commonConstants from "~/constants/comman";

function verifyToken(req, res, next) {
    var token = '';
    if(req.cookies.developer_data) {
        var  split = req.cookies.developer_data.split(' ');
        token = split[1];
    }
    if(token){
    
        jwt.verify(token, commonConstants.JWT_SECRET, function (err, decoded) {
            if (err) {
                return res.redirect('/');
            } else {
                req.userId = decoded.developer_id;
                req.current = "";

                var loginDeveloperData = decoded;//req.headers['x-access-token'];
                if (loginDeveloperData) {
                    res.locals.loginDeveloperData = loginDeveloperData;
                }
            }
        next();
        });
    }else{
    return res.redirect('/');
    }
}

module.exports = verifyToken;
