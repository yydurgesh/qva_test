import envConstants from "~/constants/envConstants";
import databaseConstants from "~/constants/databaseConstants";
import statusConstants from "~/constants/statusConstants";
import jwt from "jsonwebtoken"; // used to create, sign, and verify tokens
import * as authService from "~/v1/services/authService";

const constantsConfig = envConstants.JWT_SECRET;

/**
 * Verify admin token.
 *
 * @param  {object} req
 * @param  {object} res
 * @param  {Object} next Next request.
 */
const verifyWebToken = function( req, res, next ) {

    // check auth token & session.
    const token = req.session.userAuthToken;
   
    if ( !token ) {
        return res.redirect( "/" );
    }
    
    // verifies secret and checks exp
    jwt.verify( token, constantsConfig, ( err, decoded ) => {
       
        if ( err ) {
            delete res.locals.loginUserData;
            /* Destroy session */
            req.session.destroy();
            req.user = null;
                
            return res.redirect( "/" );
        }else {
            
            authService
                .getWebUser( req, res, decoded.id )
                .then( ( storeAuthData ) => {
                    // const loginUserData = req.session.loginUserData;// req.headers['x-access-token'];
                    const userData = {
                        "id": storeAuthData[ 0 ].id,
                        
                    };

                    // Check store status active or not.
                    if ( storeAuthData[ 0 ].status !== statusConstants.USER_STATUS.ACTIVE ) { // if In-active.
                        req.session.destroy();

                        return res.redirect( "/" );
                    }

                    req.user = userData;
                    req.current = "";
                    
                    next();
                } )
                .catch( ( error ) => {
                    req.session.destroy();

                    return res.redirect( "/" );
                } );
        }
      
    } );

};

module.exports = verifyWebToken;
