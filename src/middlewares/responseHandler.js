/**
 * Middleware to sanitizing and triming JSON body requests.
 *
 * @param  {Object}   request
 * @param  {Object}   response
 * @param  {Function} next
 */
export const responseHandler = function( request, response, next ) {
    response.header( "Access-Control-Allow-Origin", "https://qvaring.com/" );
    response.header( "Access-Control-Allow-Methods", "GET,PUT,POST,DELETE" );
    response.header( "Access-Control-Allow-Headers", "Content-Type" );
    response.header( "Cache-Control", "no-cache, no-store, must-revalidate" );
    response.header( "Pragma", "no-cache" );
    response.header( "Expires", "0" );
    // response.header( "Content-Security-Policy", "script-src 'self' https://apis.google.com; style-src 'self'")
    next();
};

export const sendResponse = function( request, response, HttpStatus, data ) {
    return response.status( HttpStatus ).json( data );
};
