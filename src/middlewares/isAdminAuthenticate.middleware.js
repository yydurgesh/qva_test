var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
import commonConstants from "~/constants/comman";

function verifyTokens(req, res, next) {
  
  var token = '';
  if (req.cookies.admin_token) {
    var split = req.cookies.admin_token.split(' ');
    token = split[1];
  }
  if (token) {
    
    // verifies secret and checks exp
    jwt.verify(token, commonConstants.JWT_SECRET, function (err, decoded) {
      if (err)
        return res.redirect('/admin');
      
      req.userId = decoded.user_id;
      req.current = "";
      var loginAdminUserData = decoded;//req.headers['x-access-token'];
      if (loginAdminUserData) {
        res.locals.loginAdminUserData = loginAdminUserData;
        if (req.originalUrl == "/admin/" ||req.originalUrl == "/admin" && req.originalUrl != "/admin/logout") {
          return res.redirect('/admin/dashboard');
        }
      }
      next();
    });

  } else {
    next();
  }
}

module.exports = verifyTokens;
