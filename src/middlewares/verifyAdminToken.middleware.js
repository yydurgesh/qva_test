var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

import commonConstants from "~/constants/comman";

function verifyToken(req, res, next) {
    var token = '';
    if(req.cookies.admin_token) {
        var  split = req.cookies.admin_token.split(' ');
        token = split[1];
    }
    if(token){
    
        jwt.verify(token, commonConstants.JWT_SECRET, function (err, decoded) {
            if (err) {
                return res.redirect('/admin');
            } else {
                req.userId = decoded.user_id;
                req.current = "";

                var loginAdminUserData = decoded;//req.headers['x-access-token'];
                if (loginAdminUserData) {
                    res.locals.loginAdminUserData = loginAdminUserData;
                }
            }
        next();
        });
    }else{
    return res.redirect('/admin');
    }
}

module.exports = verifyToken;
