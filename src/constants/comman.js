/**
 * Common Constants
 *
 * @package                Mindiii
 * @subpackage             Common Constants
 * @category               Constants
 * @ShortDescription       This is responsible for common constants
 */

const commonConstants = {
    "UPPER_CASE_RANDOME_NUMBER": "A",
    "LOWER_CASE_RANDOME_NUMBER": "a",
    "INTEGER_RANDOME_NUMBER": "0",
    "ALL_RANDOME_CHARACTERS": "*",
    "HASH_ID_LENGTH": 6,
    "RANDOME_NUMBER_LENGTH": 6,
    "DEFAULT_USER_TYPE": 1,
    "DEFAULT_USER_STATUS": 1,
    "OTP_EXP_AT": 5,
    "PASSWORD_SALT_ROUNDS": 10,
    "PASSWORD_REGEX": "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$",
    "JWT_SECRET": "nodeauthsecret",
    "JWT_EXPIRE_TIME": 86400 * 30,
    "ALGORITHM": "aes-256-cbc",
    "ENCRYPTION_KEY": "abcde",
    "IV_LENGTH": 16,
    "DB_DATE_FORMAT": "YYYY-MM-DD HH:mm:ss",
    "GLOBAL_DATE_TIME_FORMAT": "MM/DD/YYYY HH:mm:ss",
    "STORAGE_PATH": "./storage/",
    "FILE_STATIC_PATH": "file",
    "ADMIN_EMAIL": "chandan.Mindiii@gmail.com",
    "ENVIRONMENT": "production",
    "FILE_UPLOAD_FOLDER": "user-image",
    "DOCUMENT_UPLOAD_FOLDER": "verify-image",
    "USER_FILE_TYPE": [ "image/png", "image/jpg", "image/jpeg" ],
    "USER_FILE_SIZE": 1000 * 1000 * 2,
    "USER_PROFILE_HEIGHT": 150,
    "USER_PROFILE_WIDTH": 150,
    "DEFAULT_USER_IMAGE": "public/user_placeholder_image.png",
    "VERIFY_FILE_UPLOAD_FOLDER": "verify-image",
    "PROFILE_MEDIA_UPLOAD_FOLDEFR": "profile",
    "AWS_S3_BASE_URL":'https://qvaring-stag.s3-us-west-1.amazonaws.com/',
    "USER_PLACEHOLDER_IMAGE": "placeholder/user_placeholder_image.png"
};

export default commonConstants;
