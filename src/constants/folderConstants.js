/**
 * Common Constants
 *
 * @package                Mindiii
 * @subpackage             Folder Constants
 * @category               Constants
 * @ShortDescription       This is responsible for folder constants
 */

let storageFolder = "";

if ( process.env.IS_S3_SUPPORT === "true" ) {
    storageFolder = process.env.NODE_ENV === "production" ? "" : "development/";
}

const folderConstants = {
    "PROFILE_MEDIA": `${storageFolder}profile_picture/`,
    "PROFILE_MEDIA_THUMB": `${storageFolder}profile_picture/thumb`,
    "EMAIL_TEMPLATE": {
        "SEND_REMINDER": "sendreminder"
    },
    "EMAIL_LOGO_ICON": 'public/email_icon/qvaring_logo.png',
    "USER_LOGIN_URL": 'signin?userType=consumer',
    "RESELLER_LOGIN_URL": 'signin?userType=reseller',
    "DEVELOPER_LOGIN_URL": 'developer-signin?userType=developer'
};

export default folderConstants;
