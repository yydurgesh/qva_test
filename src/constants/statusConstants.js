/**
 * Common Constants
 *
 * @package                Mindiii
 * @subpackage             Status Constants
 * @category               Constants
 * @ShortDescription       This is responsible for common constants
 */

const statusConstants = {
    "USER_STATUS": {
        "PENDING": 1,
        "ACTIVE": 2,
        "INACTIVE": 3
    },
    "IS_MOBILE": {
        "VERIFIED": 1,
        "NOT_VERIFIED": 2
    },
    "IS_MAIL": {
        "VERIFIED": 1,
        "NOT_VERIFIED": 2
    },
    "IS_PROFILE": {
        "COMPLETED": 1,
        "NOT_COMPLETED": 2
    },
    "MESSAGE_STATUS": {
        "SENT": 1
    },
    "CALL_STATUS": {
        "DONE": 1
    },
    "RECHARGE_STATUS": {
        "PENDING": 1,
        "FAILED": 2,
        "COMPLETED": 3
    },
    "RECHARGE_PAYMENT_STATUS": {
        "PENDING": 1,
        "FAILED": 2,
        "COMPLETED": 3,
        "REFUND_INITIATED": 4,
        "REFUND_FAILED": 5,
        "REFUND_COMPLETED": 6
    },
    "USER_APPROVAL": {
        "IS_APPROVED": 1,
        "NOT_APPROVED": 2
    }
};

export default statusConstants;
