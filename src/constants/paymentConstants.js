/**
 * Common Constants
 *
 * @package                Mindiii
 * @subpackage             Payment Constants
 * @category               Constants
 * @ShortDescription       This is responsible for payment constants
 */

const paymentConstants = {
    "DTONE_AIRTIME_HOSTURL": "https://airtime-api.dtone.com/cgi-bin/shop/topup",
    "DTONE_GOODS_SERVICE_HOST": "https://gs-api.dtone.com/v1.1/",
    "AIRTIME_API_ACTION": {
        "PING_METHOD": "ping"
    }
};

export default paymentConstants;
