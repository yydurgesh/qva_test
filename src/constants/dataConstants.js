/**
 * Common Constants
 *
 * @package                Mindiii
 * @subpackage             Status Constants
 * @category               Constants
 * @ShortDescription       This is responsible for common constants
 */

const dataConstants = {
    "USER_TYPE": {
        "USER_CONSUMER": 1,
        "USER_ADMIN": 2,
        "USER_RESELLER": 3,
        "USER_DEVELOPER": 4,
        "CONSUMER": 'User',
        "RESELLER": 'Reseller',
        "DEVELOPER": 'Developer'
    },
    "MOBILE_COUNTRYCODE_SAPERATOR": "-"  
};

export default dataConstants;
