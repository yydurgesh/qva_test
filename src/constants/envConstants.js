/**
 * E Constants
 *c
 * @package                Mindiii
 * @subpackage             Env Constants
 * @category               Constants
 * @ShortDescription       This is responsible for Env constants
 */


const envConstants = {

    "TWILIO_ID": process.env.TWILIO_ID,
    "TWILIO_TOKEN": process.env.TWILIO_TOKEN,
    "TWILIO_FROM_NUMBER": process.env.TWILIO_FROM_NUMBER,
    "ASSETS_BASE_URL": process.env.ASSETS_URL_BASE,
    "DTONE_API_KEY": process.env.DTONE_API_KEY,
    "DTONE_SECRET_KEY": process.env.DTONE_API_SECRET_KEY,
    "DTONE_AIRTIME_TOKEN": process.env.DTONE_API_TOKEN,
    "DTONE_AIRTIME_LOGIN": process.env.DTONE_API_LOGIN,
    "STRIPE_PUBLIC_KEY": process.env.STRIPE_PUBLIC_KEY,
    "STRIPE_SECRET_KEY": process.env.STRIPE_SECRET_KEY,
    "RECHARGE_ACTION": process.env.ACTION,
    "DING_API_KEY": process.env.DING_API_KEY,
    "DING_BASE_URL": process.env.DING_BASE_URL,
    "FIREBASE_SERVER_KEY": process.env.FIREBASE_SERVER_KEY,
    "FIREBASE_API_KEY": process.env.FIREBASE_API_KEY,
    "FIREBASE_AUTH_DOMAIN": process.env.FIREBASE_AUTH_DOMAIN,
    "FIREBASE_DATABASE_URL": process.env.FIREBASE_DATABASE_URL,
    "FIREBASE_PROJECT_ID": process.env.FIREBASE_PROJECT_ID,
    "FIREBASE_STORAGE_BUCKET": process.env.FIREBASE_STORAGE_BUCKET,
    "FIREBASE_MESSAGE_SENDER_ID": process.env.FIREBASE_MESSAGE_SENDER_ID,
    "FIREBASE_GCM_SENDER_ID": process.env.FIREBASE_GCM_SENDER_ID,
    "DING_API_MODE": process.env.DING_API_MODE,
    "AWS_S3_BASE_URL": process.env.S3_BASE_PATH,
    "JWT_SECRET":"nodeauthsecret"
};

export default envConstants;
