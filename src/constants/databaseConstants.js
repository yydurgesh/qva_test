/**
 * Common Constants
 *
 * @package                Mindiii
 * @subpackage             Common Constants
 * @category               Constants
 * @ShortDescription       This is responsible for common constants
 */

const databaseConstants = {
    "IS_NOT_VERIFY": 2,
    "IS_DELETED": "1",
    "DEVICE_TYPE": {
        "WEB": 1,
        "IOS": 2,
        "ANDROID": 3
    },
    "TIME_FORMAT": "YYYY-MM-DD HH:mm:ss",
    "USER_KEY": "user_id",
    "CONTACT_KEY": "contact_id",
    "CALL_CONTACT_KEY": "call_contact_id",
    "MESSAGE_CONTACT_KEY": "msg_contact_id",
    "LIMIT": 10,
    "OFFSET": 0,
    "DEFAULT_PAGE_NUMBER": 1,
    "TIMESTAMP_FORMAT": "YYYY-MM-DD HH:mm:ss A"
};

export default databaseConstants;
