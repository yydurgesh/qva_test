/**
 * @param  {object} knex
 */
export const seed = function( knex ) {
    // Deletes all existing entries
    return knex( "users" )
        .del()
        .then( () => {
            // Inserts seed entries
            return Promise.all( [
                knex( "users" ).insert( [
                    {
                        "user_type": "2",
                        "user_fullname": "yogendra chouhan",
                        "user_email": "chouhan.yogendra@fxbytes.com",
                        "user_loginname": "yogen",
                        "user_password": "yogen123"
                    },
                    {
                        "user_type": "2",
                        "user_fullname": "anshul joshi",
                        "user_email": "joshi.anshul@fxbytes.com",
                        "user_loginname": "anshul",
                        "user_password": "yogen123"
                    }
                ] )
            ] );
        } );
};
