/**
 * Admin table.
 * 
 * @param  {object} knex
 */
export const seed = function( knex ) {
    // Deletes all existing entries
    return knex( "users" )
        .del()
        .then( () => {
            // Inserts seed entries
            return Promise.all( [
                knex( "qva_admin" ).insert( [
                    {
                        "id": 1,
                        "name": "Admin QvaShop",
                        "email": "admin@qvastore.com",
                        "password": "$2b$10$jiHS.L4.MgJvLbN1NR1joO8ovmW5evzL0Msj4.MzS3W5oPvuebcmC",
                        "country_code": "+91",
                        "contact_number": "9589111142",
                        "language": "en",
                        "created_at": "2020-07-25 08:56:39",
                        "updated_at": "2020-07-25 08:56:39"
                    }
                ] )
            ] );
        } );
};
