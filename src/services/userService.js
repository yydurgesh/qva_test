import Boom from "boom";
import UserModel from "~/models/UserModel";
import { LocaleService } from "~/utils/localeService";
import databaseConstants from "~/constants/databaseConstants";
import i18n from "~/config/i18n.config";
import { DateTime } from "~/libraries/DateTime";
import DateTimeUtil from "~/utils/DateTimeUtil";

const UserModelObj = new UserModel(),
    datetimeObj = new DateTime(),
    localeService = new LocaleService( i18n );

/**
 * Get a user.
 *
 * @param  {Number|String}  userId
 * @returns {Promise}
 */
export const getUser = function( userId ) {
    return UserModelObj.fetchObj( { "id": userId } ).then( ( user ) => {
       
        if ( user.length < 1 ) {
            const data = { "error_code": "UNAUTHORIZED_USER" };
            
            throw Boom.unauthorized( localeService.translate( "UNAUTHORIZED_USER" ), data );
        }

        return user;
    } );
};

/**
 * Get a user using device id.
 *
 * @param  {Number|String}  userId
 * @param  {Number|String}  deviceId
 * @returns {Promise}
 */
export const getUserWithDevice = function( userId, deviceId ) {
    const whereUser = { 
            "qva_store.id": userId, 
            "qva_device_auth.device_id": deviceId 
        },
        mainKey = "id",
        joinkey = "id",
        jointable = "qva_device_auth";
    
    return UserModelObj.fetchJoinObj( whereUser, joinkey, jointable, mainKey ).then( ( user ) => {
        
        if ( user.length < 1 ) {
            const data = { "error_code": "UNAUTHORIZED_USER" };
            
            throw Boom.unauthorized( localeService.translate( "UNAUTHORIZED_USER" ), data );
        }

        return user;
    } );
};

/**
 * Get a user.
 *
 * @param  {object} req
 * @param  {object} res
 * @param  {Number|String}  authId
 * @returns {Promise}
 */
export const getWebUser = function( req, res, authId ) {
    return UserModelObj.fetchObj( { "id": authId } ).then( ( user ) => {
       
        if ( user.length < 1 ) {
            const token = req.session.userAuthToken;// req.headers['x-access-token'];
            
            if ( token ) {
                req.session.destroy();
                req.user = null;       
            }
            
            return res.redirect( "/" );
        }

        return user;
    } );
};

/**
 * Check user email existence.
 *
 * @param  {String}  req
 * @returns {Promise}
 */
export const checkEmail = function( req ) {
    const userEmail = req.body.email,
        whereCondition = { "email": userEmail };

    return UserModelObj.fetchObj( whereCondition ).then( ( user ) => {   
        if ( user.length > 0 ) {
            const data = { "error_code": "EMAIL_EXIST" };

            throw Boom.badRequest( localeService.translate( "EMAIL_DUPLICATE" ), data );
        }

        return user;
    } );
};

/**
 * User authentication.
 *
 * @param  {Object} req Request.
 */
export const checkUserAuth = ( req ) => {
    // Variable declaration
    const deviceId = req.headers[ "device-id" ],
        deviceType = parseInt( req.headers[ "device-type" ] ),
        deviceTimezone = req.headers[ "timezone" ],
        currentTime = DateTimeUtil.getCurrentTimeObjForDB(),
        whereCondition = { "userID": req.user.userID };

    // Add device Timezone
    if( deviceId === undefined || deviceTimezone === undefined || deviceType === undefined ) {
        const data = { "error_code": "MISSING_HEADERS" };
        
        throw Boom.badRequest( localeService.translate( "MISSING_HEADERS" ), data );
    }

    // Check device type
    if( deviceType !== databaseConstants.DEVICE_TYPE.IOS && deviceType !== databaseConstants.DEVICE_TYPE.ANDROID ) {
        const data = { "error_code": "INVALID_DEVICE_TYPE" };
        
        throw Boom.badRequest( localeService.translate( "INVALID_DEVICE_TYPE" ), data );
    }

    return UserModelObj.fetchFirstObj( whereCondition ).then( ( user ) => {
        const createdTime = datetimeObj.changeFormat( user.created_at, databaseConstants.INSERT_TIMESTAMP_FORMAT ),
            checkCreatedTime = datetimeObj.getDifference( createdTime, currentTime, "hours" );
        
        // Check email verification after 24 hours of signup.
        if ( checkCreatedTime >= 24 ) {
            // Check email verify or not.
            if ( user.email_verification_completed !== 1 ) { // if password doesn't match.
                const data = { "error_code": "INCOMPLETE_EMAIL_VERIFICATION" };

                throw Boom.badRequest( localeService.translate( "INCOMPLETE_EMAIL_VERIFICATION" ), data );
            }
        }
        
        // Check email verify or not.
        if ( user.status !== 1 ) { // if password doesn't match.
            const data = { "error_code": "USER_INACTIVE" };

            throw Boom.badRequest( localeService.translate( "USER_INACTIVE" ), data );
        }
        
        return user;
    } );
};

