import Joi from "joi";
import validate from "~/utils/validate";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
const localeService = new LocaleService( i18n ),
    SCHEMA = {
        "contact_number": Joi.number()
            .label( localeService.translate( "USER_CONTACT" ) )
            .integer()
            .positive()
            .required(),
        "dial_code": Joi.number()
            .label( localeService.translate( "Dial code required" ) )
            .required(),
            
           
        "country_code": Joi.allow(),
        "_csrf": Joi.allow()
    },

    RESENDSCHEMA = {
        "user_contact": Joi.number()
            .label( localeService.translate( "USER_CONTACT" ) )
            .integer()
            .positive()
            .required(),
        "dial_code": Joi.number()
            .label( localeService.translate( "Dial code required" ) )
            .required(),
            
           
        "country_code": Joi.allow(),
        "contact_number": Joi.allow(),
        "_csrf": Joi.allow()
    },

    OTPSCHEMA = {
        "user_contact": Joi.number()
            .label( localeService.translate( "USER_CONTACT" ) )
            .integer()
            .positive()
            .required(),
        "dial_code": Joi.number()
            .label( localeService.translate( "Dial code required" ) )
            .required(),
        "user_loginotp": Joi.number()
            .label( localeService.translate( "Verification code is required" ) )
            .required(),
            
           
        "country_code": Joi.allow(),
        "_csrf": Joi.allow()
    };

/**
 * Validate Contact Number For Otp.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const contactValidator = function( req, res, next ) {
    return validate( req.body, SCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};


/**
 * Validate resend  Number For Otp.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const resendotpValidator = function( req, res, next ) {
    return validate( req.body, RESENDSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};


/**
 * Check OTP.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const otpValidator = function( req, res, next ) {
    return validate( req.body, OTPSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};

export { contactValidator, otpValidator, resendotpValidator };
