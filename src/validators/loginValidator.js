import Joi from "joi";
import validate from "~/utils/validate";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
const localeService = new LocaleService( i18n ),
    VERIFYOTPSCHEMA = {
        "user_countrycode": Joi.string()
            .label( localeService.translate( "USER_COUNTRY_CODE" ) )
            .required(),
        "user_contact": Joi.string()
            .label( localeService.translate( "USER_CONTACT" ) )
            .required(),
        "user_loginotp": Joi.string()
            .min( 6 )
            .max( 6 )
            .required()
            .label( localeService.translate( "USER_OTP" ) )
    },
    LOGINSCHEMA = {
        "country_code": Joi.string()
            .label( localeService.translate( "USER_COUNTRY_CODE" ) )
            .required(),
        "dial_code": Joi.string()
            .label( localeService.translate( "USER_COUNTRY_CODE" ) )
            .required(),
        "user_contact": Joi.string()
            .label( localeService.translate( "USER_CONTACT" ) )
            .required(),
        "password": Joi.string()
            .required()
            .label( localeService.translate( "USER_PASSWORD" ) ),
        "is_app": Joi.string().allow(''),
        "_csrf": Joi.string().allow(''),
        
    },
    ADMINSCHEMA = {
        "email": Joi.string()
            .label( localeService.translate( "ADMIN_EMAIL" ) )
            .required(),
        "password": Joi.string().required(),
        "ipAddress": Joi.string()
    },
    DEVELOPERSCHEMA ={
        "developer_id":Joi.string()
              .required(),   
        "name": Joi.string()
            .label( localeService.translate( "USER_NAME" ) )
            .max( 255 )
            .min( 2 )
            .required(),
        "email": Joi.string()
            .label( localeService.translate( "USER_EMAIL" ) )
            .max( 50 )
            .min( 4 )
            .email()
            .required(),
        "password": Joi.string()
            .min( 5 )
            .required()
            .label( "Password" )  
    };

/**
 * Validate create/update user request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const loginOtpValidator = function( req, res, next ) {
    return validate( req.body, VERIFYOTPSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};
/**
 * Validate create/update user request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const loginValidator = function( req, res, next ) {
    return validate( req.body, LOGINSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};

/**
 * Validate create/update user request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const adminLoginValidator = function( req, res, next ) {
    return validate( req.body, ADMINSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};

/**
 * Validate create developer request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const developerValidator = function( req, res, next ) {
    return validate( req.body, DEVELOPERSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};


export { 
    loginOtpValidator,
    loginValidator,
    adminLoginValidator,
    developerValidator
    
};
