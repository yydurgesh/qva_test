import Joi from "joi";
import Boom from "boom";
import passwordValidator from  "password-validator";

import validate from "~/utils/validate";
import * as userService from "~/v1/services/userService"; 
//import * as developerService from "~/v1/services/developerService";
import { checkValidator } from "~/validators/fileValidator";
import { LocaleService } from "~/utils/localeService";
import commonConstants from "../constants/comman";
import i18n from "~/config/i18n.config";
const localeService = new LocaleService( i18n ),
    SCHEMA = {
        // "user_id": Joi.string()
        //     .label( localeService.translate( "USER_ID" ) )  
        //     .required(),
        "name": Joi.string()
            .label( localeService.translate( "USER_NAME" ) )
            .max( 255 )
            .min( 2 )
            .required(),
        "email": Joi.string()
            .label( localeService.translate( "USER_EMAIL" ) )
            .max( 50 )
            .min( 4 )
            .email()
            .required(),
        "user_contact": Joi.string()
            .min( 5 )
            .required()
            .label( "Phone number" ),
        
        "dial_code" : Joi.string()
            .min( 1 )
            .required()
            .label( "dial code" ),
        "password": Joi.string()
            .min( 8 )
            .required()
            .label( "Password" ),
        "confirm_password": Joi.string()
            .min( 8 )
            .required()
            .label( "Password" ),
       "country_code": Joi.string()
            .min( 1 )
            .required()
            .label( "country code" ),
        "X-DEVICE-TYPE": Joi.allow(''),   
        "otp": Joi.allow(''), 
        "_csrf": Joi.allow(''),          
        "userType": Joi.allow('')  ,
        "user_loginotp": Joi.allow('') ,
        "influencer": Joi.allow('')    
    },
    PROFILESCHEMA = {
        "user_id": Joi.string()
            .label( localeService.translate( "USER_ID" ) )  
            .required(),
        "name": Joi.string()
            .label( localeService.translate( "USER_NAME" ) )
            .max( 255 )
            .min( 2 )
            .required(),
        "email": Joi.string()
            .label( localeService.translate( "USER_EMAIL" ) )
            .max( 50 )
            .min( 4 )
            .email()
            .required(),
         "_csrf": Joi.allow()
         
    },
    PASSWORDSCHEMA ={
        "user_id": Joi.string()
            .label( localeService.translate( "USER_ID" ) )  
            .required(),
         "old_password": Joi.string()
            .min( 5 )
            .required()
            .label( "Old assword" ),   
        "new_password": Joi.string()
            .min( 5 )
            .required()
            .label( "New password" )      

    },
    CUSTOMERSCHEMA ={
        "user_id": Joi.string()
            .label( localeService.translate( "USER_ID" ) )  
            .required(),
        "email": Joi.string()
            .label( localeService.translate( "USER_EMAIL" ) )
            .max( 50 )
            .min( 4 )
            .email()
            .required(),

    },
    ADMINUSERDETAILSCHEMA = {
        "user_id": Joi.string()
            .label( localeService.translate( "USER_ID" ) )
            .required()
    },
     NUMBERRSCHEMA={
        "user_id": Joi.string()
            .label( localeService.translate( "USER_ID" ) )  
            .required(),
         "country_code": Joi.string()
            .required(),
         "contact_number": Joi.string()
            .required()
              
    },
    VERIFYNUMBERRSCHEMA={
        "user_id": Joi.string()
            .label( localeService.translate( "USER_ID" ) )  
            .required(),
         "country_code": Joi.string()
            .required() ,
         "contact_number": Joi.string()
            .required(),
         "verify_otp": Joi.string()
            .required()   
              
    },
    STRIPEFEESCHEMA={
        "user_id": Joi.string()
            .label( localeService.translate( "USER_ID" ) )  
            .required(),
         "amount": Joi.string()
            .required(),
         "currency": Joi.string()
            .required()
              
    },
    OPERATORCOMSCHEMA ={
		 "user_id": Joi.string()
            .label( localeService.translate( "USER_ID" ) )  
            .required(),
         "cos_id": Joi.string()
            .required(),
         "commission": Joi.string()
            .required()
    },
    METHODSCHEMA = {
        "method_id": Joi.string()
           .label( "method id" )
           .required()
    };

/**
 * Validate create user request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const userValidator = function( req, res, next ) {
    return validate( req.body, SCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};

/**
 * Validate user email existence.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const checkEmail = function( req, res, next ) {

    return userService
        .checkEmail( req.body.email, req )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};
const checkEmailValid = function( req, res, next ) {

    return userService
        .checkEmailValid( req.body.email, req )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};


const checkPassword = function( req, res, next ) {

var schema = new passwordValidator();
 
// Add properties to it
    schema
    .is().min(8)                                    // Minimum length 8
    .is().max(100)                                  // Maximum length 100
    .has().uppercase()                              // Must have uppercase letters
    .has().lowercase()                              // Must have lowercase letters
    .has().digits()   
    .has().symbols()                                // Must have at least 2 digits                          // Must have at least 2 digits
    .has().not().spaces()                           // Should not have spaces
    .is().not().oneOf(['Passw0rd', 'Password123']);
        
    if(schema.validate(req.body.password)){
      return  next()
    }else{
        return next(Boom.badRequest( localeService.translate( 'Password_message_should_be' ) ))
    }

      
};

/** 
 * Validate User Profile For update.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const profileImageValidator = function( req, res, next ) {
    
    const fileSchema = {
        "file_type": commonConstants.USER_FILE_TYPE,
        "file_size": commonConstants.USER_FILE_SIZE
    };
    
    if( req.files ) {
        const profileImage = req.files.profileImage,
            checkFileValidation = checkValidator( profileImage, fileSchema );
    }

    next();
};

/**
 * Validate create/update user request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const adminUserValidator = function( req, res, next ) {
    return validate( req.query, ADMINUSERDETAILSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};

/**
 * Check auth token user Id.
 *
 * @param  {Object} req Request.
 * @param  {Object} res Response.
 * @param  {Object} next Next request.
 * @returns
 */
const checkAdminUserTypeByAuthID = function( req, res, next ) { 
    const authUser = req.user;

    return userService
        .checkUserType( authUser )
        .then( ( ) => {
            next();
        } ).catch( ( err ) => next( err ) );
};
/**
 * Validate update user request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const userProfileValidator = function( req, res, next ) {
    return validate( req.body, PROFILESCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};

/**
 * Validate change password request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const changePassworkValidator = function( req, res, next ) {
    return validate( req.body, PASSWORDSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};

/**
 * Validate create customer request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const customerValidator = function( req, res, next ) {
    return validate( req.body, CUSTOMERSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};
/**
 * Validate method  request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const methodValidator = function( req, res, next ) {
    return validate( req.body, METHODSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};




/**
 * Validate chnage number request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const changeNumberValidator = function( req, res, next ) {
    return validate( req.body, NUMBERRSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};
/**
 * Validate verify number request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const verifyOtpValidator = function( req, res, next ) {
    return validate( req.body, VERIFYNUMBERRSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};

/**
 * Validate stripe fee request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const stripeFeeValidator = function( req, res, next ) {
    return validate( req.body, STRIPEFEESCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};
const operatorComValidator = function( req, res, next ) {
    return validate( req.body, OPERATORCOMSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};
export {
    checkEmail,
    userValidator,
    profileImageValidator,
    adminUserValidator,
    checkAdminUserTypeByAuthID,
    checkEmailValid,
    userProfileValidator,
    changePassworkValidator,
    customerValidator,
    methodValidator,
    
    changeNumberValidator,
    verifyOtpValidator,
    stripeFeeValidator,
    
    checkPassword
   
};
