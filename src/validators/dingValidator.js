import Joi from "joi";
import validate from "~/utils/validate";
import { LocaleService } from "~/utils/localeService";
import * as developerService from "~/v1/services/developerService";
import i18n from "~/config/i18n.config";
const localeService = new LocaleService( i18n ),

    SCHEMA = {
        "api-token": Joi.string()
           .required(),
       "content-type": Joi.string().allow(''),
       "cache-control": Joi.string().allow(''),
       "postman-token": Joi.string().allow(''),
       "user-agent": Joi.string().allow(''),
       "accept": Joi.string().allow(''),
       "host": Joi.string().allow(''),
       "cookie": Joi.string().allow(''),
       "accept-encoding": Joi.string().allow(''),
       "content-length": Joi.string().allow(''),
       "connection": Joi.string().allow(''),
       "x-forwarded-for":Joi.string().allow(''),
       "x-forwarded-proto":Joi.string().allow(''),
       "x-forwarded-port":Joi.string().allow(''),
       "x-amzn-trace-id":Joi.string().allow(''),
       "origin":Joi.string().allow(''),
       "sec-fetch-site":Joi.string().allow(''),
       "sec-fetch-mode":Joi.string().allow(''),
       "sec-fetch-dest":Joi.string().allow(''),
       "accept-language":Joi.string().allow(''),
       "x-real-ip":Joi.string().allow(''),
       "x-sucuri-clientip":Joi.string().allow(''),
       "x-sucuri-country":Joi.string().allow(''),
      },
      REGIONSCHEMA = {
        "countryIso": Joi.string()
            .required()
    },
     PROVIDERSCHEMA = {
        "countryIso": Joi.string()
            .required(),
        "regionCodes": Joi.string()
            .required(),
        "accountNumber": Joi.string().allow('')
        
    },
    PRODUCTSCHEMA ={
        "countryIso": Joi.string()
            .required(),
        "regionCodes": Joi.string()
            .required(),
        "providerCodes": Joi.string()
            .required(),
        "accountNumber": Joi.string().allow('')
      
    },
    PROVIDERINFOSCHEMA = {
        "accountNumber": Joi.string()
            .required()
    },
    TRANSFERSCHEMA = {
        "skuCode": Joi.string()
            .required(),
        "sendValue": Joi.number()
            .required(),
        "sendCurrencyIso": Joi.string()
            .required(),    
        "accountNumber": Joi.string()
            .required()
    };

/**
 * Validate create/update user request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const headerValidator = function( req, res, next ) {
    //console.log('req data',req.rawHeaders);return;
    return validate( req.headers, SCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};
/**
 * Validate developer tokem existence.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const checkTokenValid = function( req, res, next ) {
    return developerService
        .checkTokenValid( req.headers['api-token'], req )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};
/**
 * Validate developer checkDeveloperIp existence.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const checkDeveloperIp = function( req, res, next ) {
    return developerService
        .checkDeveloperIp(req.headers['api-token'], req )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};
/** 
 * Validate create/update user request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const regionsValidator = function( req, res, next ) {
    return validate( req.body, REGIONSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};

/**
 * Validate create/update user request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const providerValidator = function( req, res, next ) {
    return validate( req.body, PROVIDERSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};

/**
 * Validate create developer request.
 *
 * @param  {object}   req
 * @param  {object}   res
 * @param  {function} next
 * @returns {Promise}
 */
const developerValidator = function( req, res, next ) {
    return validate( req.body, DEVELOPERSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};

const productsValidator = function( req, res, next ){
     return validate( req.body, PRODUCTSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};

const transferValidator = function( req, res, next ){
     return validate( req.body, TRANSFERSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};
const porviderInfoValidator = function( req, res, next ){
     return validate( req.body, PROVIDERINFOSCHEMA )
        .then( () => next() )
        .catch( ( err ) => next( err ) );
};
export { 
    headerValidator,
    checkTokenValid,
    regionsValidator,
    providerValidator,
    productsValidator,
    transferValidator,
    porviderInfoValidator,
    checkDeveloperIp
        
};
