import Boom from "boom";
import imageSize from "image-size";
import commonConstants from "~/constants/comman";
import { LocaleService } from "~/utils/localeService";
import i18n from "~/config/i18n.config";
const localeService = new LocaleService( i18n );

/**
 * Helper function for validating files.
 *
 * @param  {Object}   fileObject
 * @param  {Object}   fileSchema
 */
export const checkValidator = function( fileObject, fileSchema ) {
    
    if ( Array.isArray( fileObject ) ) {
        let typeError = "",
            sizeError = "";

        for ( let i = 0; i < fileObject.length; i++ ) {
            
            if ( !fileSchema.file_type.includes( fileObject[ i ].mimetype ) ) {
                typeError += `${fileObject[ i ].name} is not a valid type \n`;
            }

            if ( fileObject[ i ].size > fileSchema.file_size ) {
                if ( fileSchema.file_value !== undefined ) {
                    sizeError += `${fileObject[ i ].name} file size is greater than ${fileSchema.file_value} \n`;
                } else {
                    sizeError += `${fileObject[ i ].name} file size is greater than 1mb \n`;
                }
            }
        }
        if ( typeError !== "" ) {
            throw Boom.badRequest( typeError );
        }

        if ( sizeError !== "" ) {
            throw Boom.badRequest( sizeError );
        }
    } else {
        // Check for file type
        if ( !fileSchema.file_type.includes( fileObject.mimetype ) ) {
            throw Boom.unsupportedMediaType( localeService.translate( "SUPPORTED_FILE_TYPES" ) );
        }

        if ( fileObject.size > fileSchema.file_size ) {
            if ( fileSchema.file_value !== undefined ) {
                throw Boom.badRequest( localeService.translate( "SUPPORTED_FILE_SIZE" ) );
            } else {
                throw Boom.rangeNotSatisfiable( localeService.translate( "SUPPORTED_FILE_SIZE" ) );
            }
        }

        const getImageLength = imageSize( fileObject.data ),
            imageHeight = commonConstants.USER_PROFILE_HEIGHT,
            imageWidth = commonConstants.USER_PROFILE_WIDTH;
        if( imageWidth === getImageLength.width && imageHeight === getImageLength.height ) {
            
            if ( fileSchema.file_req !== undefined ) {
                const getImageSize = imageSize( fileObject.data );

                if ( getImageSize.width < fileSchema.file_req.width || getImageSize.height < fileSchema.file_req.height ) {
                    throw Boom.badRequest(
                        `The minimum image dimensions should be: ${fileSchema.file_req.width} width and ${fileSchema.file_req.height} height`
                    );
                }
            }
        }else{
            throw Boom.unsupportedMediaType( localeService.translate( "SUPPORTED_FILE_LENGTH" ) );
        }
    }
};
