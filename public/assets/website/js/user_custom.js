

// make random device id
function makeid(length) {
    var result = '';
    var characters = 'abcdefghijklmnopqrstuvwxyz';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
 
    return result;
}

localStorage.setItem("otpverified",'');
// login signup phone validation
$("#loginPhone, #signUpPhone").keypress(function (event) {
    if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
        event.preventDefault();
    }

    if (this.value.length == 0 && event.which == 48) {
        event.preventDefault();
    }
})

$('#loginPhone').on('paste', function (event) {
    if (event.originalEvent.clipboardData.getData('Text').match(/[^\d]/)) {
        event.preventDefault();
    }
});

// sign up otp
$("form[name='send_otp']").validate({ 
    rules: {
       
        contact_number: {
            required: true,
            number: true,
           
        }
    },

    messages: {
     
        contact_number: {
          required: mobile_required,
          number: "Please enter a valid number."
        }
    },
    submitHandler: function (form) {
       
        var formData = new FormData(form);
        sendOtp(formData) ;

    }


});


//Signup otp 
// $('.send_otp').click(function () {
  
//     var formData = new FormData();
//     formData.append('contact_number','');
//     formData.append('country_code', $('#country_code').val());
//     formData.append('dial_code', $('#dial_code').val());

//     console.log("formData", formData)
//     sendOtp(formData)
// });

//send otp ajax method
function sendOtp(formData) {
    
    var url = window.location.origin + "/auth/send-otp";
   
    $.ajax({
        type: "POST",
        url: url,
        headers: {
            'Accept': 'application/json',
            "Cache-Control": "no-cache, no-store, must-revalidate",
            "Pragma": "no-cache",
            "Expires": "0",
            "X-USER-TYPE": 1,
            "X-DEVICE-TYPE" :3
        },
        processData: false,
        contentType: false,
        data: formData,
        cache: false,
        success: function (response) {

            toastr.remove();
            if (response.code == 200) {
                
                $('.sendOtp').html("Continue");
                toastr.success(response.message);

                var parent_fieldset = $("#verify_number").parents('fieldset');
                  
                    var next_step = true;
                    // navigation steps / progress steps
                    var current_active_step = $("#verify_number").parents('.form-wizard').find('.form-wizard-step.active');
                    var progress_line = $("#verify_number").parents('.form-wizard').find('.form-wizard-progress-line');
                    
                    // fields validation
                    //parent_fieldset.find('.required').each(function() {
                   
                    // fields validation
                    
                    if( next_step ) {
                        parent_fieldset.fadeOut(400, function() {
                            // change icons
                            current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                            // progress bar
                            bar_progress_ajax(progress_line, 'right');
                            // show next step
                            $("#verify_number").next().fadeIn();
                            // scroll window to beginning of the form
                            scroll_to_class_ajax( $('.form-wizard'), 20 );
                        });

                        $("#otp_form").show();
                    }
               
                return false;
            } else {
                toastr.remove();
                toastr.error(response.output.payload.message);
               
                return false;
            }
        }, error: function (textStatus, errorThrown) {
            $('.sendOtp').html("Continue");
            $('#login_form_btn1').prop('disabled', false);
            toastr.remove();
            toastr.error(textStatus.responseJSON.error.message);
        }
    });
}

//Send otp form end

$("form[name='verifyOtpForm']").validate({
    rules: {
        otp: {
            required: true,
            minlength: 6,
            maxlength: 6
            
        }
    },
    messages: {
        otp: {
            required: The_otp_is_mandatory
            
        }
    },
    submitHandler: function (form) {
       
        var formData = new FormData();
        next_step = false;
        localStorage.setItem("otpverified",'');
        console.log($('#otp').val().length)
        formData.append('user_loginotp', $('#otp').val());
        formData.append('user_contact', $('#signUpPhone').val());
        formData.append('dial_code', $('#dial_code').val())

       // var firebaseToken = $('#firebaseToken').val();
        //var deviceId = makeid(50);

        var url = window.location.origin + "/auth/verify-otp";
        var userType;

        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'Accept': 'application/json',
                "Cache-Control": "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0",
                "X-USER-TYPE": userType,
                "x-firebase-token": 'test',
                "x-device-id": '123',
                "x-user-type" :1
            },
            processData: false,
            contentType: false,
            data: formData,
            cache: false,
            success: function (response) {
                
              
                toastr.remove();
                if (response.code == 200) {
                      // go to next after verifyotp
                      if(response.attemptedCount!=undefined){
                        if(parseInt(response.attemptedCount[0])>=3){
                            toastr.error(response.message);
                            document.location = "/";
                       }
                    }else{

                    
                    $("#_csrf").val(response.csrfToken);
                    var parent_fieldset = $("#verify_signup").parents('fieldset');
                    
                    var next_step = true;
                    // navigation steps / progress steps
                    var current_active_step = $("#verify_signup").parents('.form-wizard').find('.form-wizard-step.active');
                    var progress_line = $("#verify_signup").parents('.form-wizard').find('.form-wizard-progress-line');
                    
                    // fields validation
                    //parent_fieldset.find('.required').each(function() {
                   
                    // fields validation
                    
                    if( next_step ) {
                        parent_fieldset.fadeOut(400, function() {
                            // change icons
                            current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                            // progress bar
                            bar_progress_ajax(progress_line, 'right');
                            // show next step
                            $("#verify_signup").next().fadeIn();
                            // scroll window to beginning of the form
                            scroll_to_class_ajax( $('.form-wizard'), 20 );
                        });

                        $("#details_form").show();
                    }
                   
                    $('.otpVerify').html("Confirm");
                    toastr.success(OTP_verified_successfully);
                    $('#user_token').val(response.token)

                }

                } else {
                    toastr.remove();
                    toastr.error("something went wrong");
                    return false;
                }
            }, error: function (textStatus, errorThrown) {
                $('#login_form_btn2').prop('disabled', false);
                $('.otpVerify').html("Confirm");
                toastr.remove();
                toastr.error(textStatus.responseJSON.error.message);
            }
        });
    }
});


//crop image start
var cropper = "";
$(function ($) {
    var imgInputSelector = $("input[name='profileImage']");
    var cropActionSelector = $("#preview_image");

    function onChangeImg() {

        if (cropper !== "") {
            cropper.destroy();
        }

        var image = document.getElementById("image");
        var files = $(this)[0].files;
        var file = files[0];

        if (file == undefined) {
            return false;
        }

        $('#crop_image_model').modal({
            show: true
        });

        $("#image").attr("src", window.URL.createObjectURL(file))
        options = {
            preview: '.preview'
        }

        cropper = new Cropper(image, {
            minCropBoxWidth: 150,
            minCropBoxHeight: 150,
            width: 450,
            minContainerWidth: 460,
            minContainerHeight: 400,
            center: true,
            aspectRatio: 2 / 2,
            cropBoxResizable: false,
            scalable: false
        })

        cropper.crop();
    }

    function cropImage() {
        let imgSrc = cropper.getCroppedCanvas({
            width: 150,
            height: 150,
        }).toDataURL();
        document.getElementById("pImg").src = imgSrc;
    }

    imgInputSelector.off("change");
    imgInputSelector.on("change", onChangeImg);

    cropActionSelector.off("click");
    cropActionSelector.on("click", cropImage);
});
//crop image end


"use strict";

function bar_progress_ajax(progress_line_object, direction) {
  
    var number_of_steps = progress_line_object.data('number-of-steps');
    var now_value = progress_line_object.data('now-value');
    var new_value = 0;
    if(direction == 'right') {
        new_value = now_value + ( 100 / number_of_steps );
    }
    else if(direction == 'left') {
        new_value = now_value - ( 100 / number_of_steps );
    }
    progress_line_object.attr('style', 'width: ' + new_value + '%;').data('now-value', new_value);
}


function scroll_to_class_ajax(element_class, removed_height) {
    var scroll_to = $(element_class).offset().top - removed_height;
    if($(window).scrollTop() != scroll_to) {
        $('.form-wizard').stop().animate({scrollTop: scroll_to}, 0);
    }
}


// user signup form 

$("form[name='signup_user']").validate({
    rules: {
        name: {
            required: true,
            minlength: 2,
            maxlength: 255
        },
        email: {
            required: true,
            email: true
        },
        password: {
            required: true,
            minlength: 8,
            passwordFunction : true,
            maxlength: 255
        },
        confirm_password: {
            required: true,
            equalTo: "#password"
        }

    },
    messages: {
        name: {
            required: The_name_field_is_mandatory,
            minlength: The_name_two,
            maxlength: The_name_max
        },
        email: {
            required: The_email_mandatory,
            email: Please_valid_email_address
        },
        password: {
            required: The_password_field_is_mandatory,
            minlength: Minimum_length_character,
            maxlength: "Password length must be less than or equal to 255 characters long"
        }
    },
    submitHandler: function (form) {
       
       // $('.signupbtn').html("Please Wait..."); X-DEVICE-TYPE
        
        var url = window.location.origin + "/auth/user-signup";
      
        var form = $('#signup_user')[0];
        var data = new FormData(form);
        data.append('user_contact', $('#signUpPhone').val());
        data.append('dial_code', $('#dial_code').val())
        data.append('country_code', $('#country_code').val())
        data.append('X-DEVICE-TYPE', 3)
        data.append('user_loginotp', $('#otp').val());

        if (cropper != "") {
            cropper.getCroppedCanvas({ width: 150, height: 150, }).toBlob((blob) => {
                data.set("profileImage", blob, "Chrysanthemum.png")
               
            });
        }

        var deviceId = makeid(50);
     
       $.ajax({
        
         type: "POST",
            url: url,
            headers: {
                'Accept': 'application/json',
                "Cache-Control": "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0",
               
                "x-firebase-token": 'test',
                "x-device-id": '123',
                "x-device-id": deviceId,
                "X-DEVICE-TYPE": 3,
                "x-user-type" : 1
            },
            processData: false,
            contentType: false,
            data: data,
            cache: false,
         success: function (response) {

            toastr.remove();
           
            console.log("response", response)
            if (response.code == 200) {

                toastr.success("Your account created successfully");
                setTimeout(function(){
                    document.location = "/recharge";
                }, 1000);
               
        }
       }, error: function (textStatus, errorThrown) {
            console.log('textStatus',textStatus)
            toastr.remove();
            toastr.error(textStatus.responseJSON.error.message);
        }
    });
    }
});


jQuery.validator.addMethod("passwordFunction", function(value, element) {
    // return this.optional(element) || (parseFloat(value) > 0);
     var str = value;
    
     if(str.match(/[a-z]/g) && str.match( 
         /[A-Z]/g) && str.match( 
         /[0-9]/g) && str.match( 
         /[^a-zA-Z\d]/g) && str.length >= 8) {
         toastr.remove();
     return true ;
     }else {
        
         return false;
     }
 }, Password_message_should_be)

 // on modal close cra data 

 $("#exampleModal1").on("hidden.bs.modal", function(){
    
  $("#otp_form").hide();
  $("#details_form").hide();
  $("#stp1").show();
    
});


// on login button click show first step
$("#showfirst").on("click", function(){
    $("#loginstp2").hide();
    $("#loginstp1").show();
      
  });

  $('.resend_otp').click(function () {
   
    var contact_number = $('#signUpPhone').val();
    var dial_code = $('#dial_code').val();
    var country_code = $('#country_code').val();
      resendOtp(contact_number,dial_code, country_code) ;
  });


  $('.resend_otp1').click(function () {
     
    var contact_number = $('#loginPhone').val();
    var dial_code = $('#log_dial_code').val();
    var country_code = $('#log_country_code').val();
      resendOtp(contact_number,dial_code, country_code) ;
  });

  //resend user otp
function  resendOtp(contact_number,dial_code, country_code) {
    $("#resendUserOtp").hide();
    $("#resendUserOtpDisable").show();

    var _csrf = $("#_csrf").val();

    var url = window.location.origin + "/auth/resend-otp";
    

    $.ajax({
        type: "POST",
        url: url,
        headers: {
            'Accept': 'application/json',
            "Cache-Control": "no-cache, no-store, must-revalidate",
            "Pragma": "no-cache",
            "Expires": "0",
            "X-USER-TYPE": 1,
            "X-DEVICE-TYPE" :3
        },
        data: { "country_code":  `${country_code.trim()}`, "user_contact": contact_number,  "contact_number": contact_number ,  "_csrf": _csrf,  "dial_code": dial_code  },
        cache: false,
        success: function (response) {

            toastr.remove();
            if (response.code == 200) {
                $('.sendOtp').html("Continue");
                toastr.success("Otp send successfully");
                $("#resendUserOtp").show();
                $("#resendUserOtpDisable").hide();
                $('#otp').val('');
                return false;
            }
        }, error: function (textStatus, errorThrown) {
            $('.sendOtp').html("Continue");
            $('#login_form_btn1').prop('disabled', false);
            toastr.remove();
            toastr.error(textStatus.responseJSON.error.message);
        }
    });
}

//Login form start
$("form[name='loginForm']").validate({
    
    rules: {
        user_contact: {
            required: true,
            number: true
        },
       
        password: {
            required: true,
            minlength: 8,
            passwordFunction : true,
            maxlength: 100
        }
    },
    messages: {
        user_contact: {
            required: mobile_required,
            number: please_enter_valid_mob
        },
        password: {
            required: The_password_field_is_mandatory,
            minlength: Minimum_length_character,
            maxlength: "Password length must be less than or equal to 255 characters long"
        }
    },
    submitHandler: function (form) {

        $('.loginButton').html("Please Wait...");
        
        var quickRecharge = localStorage.getItem('rechargeFrom');
        var url = window.location.origin + "/auth/login";
        //var ipAddress = $('#client_address').val();
       // var firebaseToken = $('#firebaseToken').val(); 
        var formData = new FormData();
        formData.append('user_contact', $('#loginPhone').val());
        formData.append('dial_code', $('#log_dial_code').val())
        formData.append('country_code', $('#log_country_code').val())
        formData.append('password', $('#loginpass').val())
        formData.append('_csrf', $("#_csrf").val())
        formData.append('is_app', '')
       
        
        var rechargeData = [];

        $.ajax({
            method: "POST",
            type: "POST",
            url: url,
            headers: {
                "Accept": "application/json",
                "Cache-Control": "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0",
                "X-USER-TYPE": 1,
                "X-DEVICE-TYPE" :3,
                "x-firebase-token": 'abc',
                "x-device-id" : '1222',
                "isApp" : '',
                "x-user-type" : 1

            },
            processData: false,  // Important!
            contentType: false,
            data: formData,
            dataType: 'json',
            cache: false,
            success: function (response) {
               
                toastr.remove();
                if (response.code == 200) {
                    var currentDate = new Date();
                    var ipExpireTime = response.isExpired;
                    if(ipExpireTime == 1){
                        toastr.success("Login successfully");
                        document.location = "/recharge"
                    }else{
                        $("#loginstp1").hide();
                        $("#loginstp2").show();
                    }
                
                  
                  //  $('.loginButton').html("Login");
                   
                        //document.location = "/recharge"
                    
                } else {
                    $('#loginUser').prop('disabled', false);
                    $('.loginButton').html("Login");
                    toastr.remove();
                    toastr.error("something went wrong");
                    return false;
                }
            }, error: function (textStatus, errorThrown) {
                $('#loginUser').prop('disabled', false);
                $('.loginButton').html("Login");
                toastr.remove();
                toastr.error(textStatus.responseJSON.error.message);
            }
        });
    }
});


//verify user otp form
$("form[name='verifyUserOtpForm']").validate({
    rules: {
        user_otp: "required",
    },
    messages: {
        user_otp: "The otp field is mandatory",
    },
    submitHandler: function (form) {
        $('.otpVerify').html("Please Wait...");
        $('#overlay').addClass('active');
        $('#login_form_btn2').prop('disabled', true);

        var countryCode = $('#countryCode').val();
        var firebaseToken = $('#firebaseToken').val();
        var deviceId = makeid(50);
        var quickRecharge = localStorage.getItem('rechargeFrom');
        var formData = new FormData();
        formData.append('user_contact', $('#loginPhone').val());
        formData.append('user_loginotp', $('#user_otp').val());
        formData.append('dial_code', $('#log_dial_code').val())
        formData.append('country_code', $('#log_country_code').val())
       
        formData.append('_csrf', $("#_csrf").val())
       // formData.append('is_app', '')

        var url = window.location.origin + "/auth/verify-user-otp";

        var userType;
        var rechargeData = [];

        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'Accept': 'application/json',
                "Cache-Control": "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0",
                "X-USER-TYPE": 1,
                "X-DEVICE-TYPE" :3,
                "x-firebase-token": 'abc',
                "x-device-id": deviceId
            },
            processData: false,
            contentType: false,
            data: formData,
            cache: false,
            success: function (response) {
                $('#login_form_btn2').prop('disabled', false);

                toastr.remove();
                if (response.code == 200) {
                    $('.otpVerify').html("Confirm");

                    if(response.attemptedCount!=undefined){
                        if(parseInt(response.attemptedCount[0])>=3){
                            toastr.error(response.message);
                            document.location = "/";
                       }
                    }else{

                      toastr.success("Otp verified successfully");
                      setTimeout(function () {
                        document.location = "/recharge"
                      }, 300)
                    }
                } else {
                    toastr.remove();
                    toastr.error("something went wrong");
                    return false;
                }
            }, error: function (textStatus, errorThrown) {
                $('#login_form_btn2').prop('disabled', false);
                $('.otpVerify').html("Confirm");
                toastr.remove();
                toastr.error(textStatus.responseJSON.error.message);
            }
        });
    }
});


/*
*  change language of web
*/
function langFunc(lang){

    //var lang = $(this).attr('data-value') ;
    var url = window.location.origin + "/auth/change_language";
    
    $.ajax({
    type: "POST",
    url: url,
    data: { "lang": lang },
    success: function (response) {
    location.reload();
    
    }, error: function (textStatus, errorThrown) {
    toastr.remove();
    toastr.error("Somthing went wrong123");
    return false;
    }
    });
    
}


//Resetpassword start
$("form[name='resetPassword']").validate({
    rules: {
        email: {
            required: true,
            email: true
        }
    },
    messages: {
        email: {
            required: 'Please enter your email',
            email: 'Please valid email address'
        }
    },
    submitHandler: function (form) {

        $('.resetButton').html("Please Wait...");

        var url = window.location.origin + "/auth/resetPassword";

        var formData = new FormData(form);
        formData.append('_csrf', $("#_csrf").val())
       
        //formData.append('email', $('#email').val());

        var customer = window.location.search;
       
       
        $.ajax({
            method: "POST",
            type: "POST",
            url: url,
            headers: {
                'Accept': 'application/json',
                "Cache-Control": "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0",
                "X-USER-TYPE": 1
            },
            processData: false,  // Important!
            contentType: false,
            data: formData,
            dataType: 'json',
            cache: false,
            success: function (response) {
                $('#forgotButton').prop('disabled', false);

                toastr.remove();
                if (response.code == 200) {
                    console.log("response", response)
                    $('.resetButton').html("Submit");
                    toastr.success(response.message);
                    setTimeout(function(){
                    document.location = "/";
                }, 1000);

                } else {
                    $('#forgotButton').prop('disabled', false);
                    $('.resetButton').html("Submit");
                    toastr.remove();
                    toastr.error("something went wrong");
                    return false;
                }
            }, error: function (textStatus, errorThrown) {
                $('#forgotButton').prop('disabled', false);
                toastr.remove();
               
                    toastr.error(textStatus.responseJSON.error.message);
                
            }
        });
    }
});

/**
 * get promotions
 * @param {*Continent} id 
 */
function getpromotion(id){
    $("#myTabContent").html('');
    $(".promoCntBody").html('');
    var url = window.location.origin + "/recharge/promotions-description";
    $.ajax({
        
        type: "POST",
           url: url,
           data: {'continent_id' : id},
           cache: false,
        success: function (response) {
           toastr.remove();
           if (response.code == 200) {
           
            var allData = response.data ;
            if(response.data.length == 0){
               var notfound = '<div class="tab-pane fade show active" id="promoTab" role="tabpanel" aria-labelledby="promoTab-tab"><div class="boxView promotionCntBlk">'+
               '<div class="promoCnt"><div class="promoCntHead"><div class="promoHeadLeft">'+
               ' <h2>No promotion found.</h2></div><div class="promoHeadRight">'+
               '<p></p></div></div><div class="promoCntBody" >'+
               '<div></div></div> </div>' ;
              
               $("#myTabContent").append(notfound) ;
            }
            for(let i =0; response.c_id.length > i; i++){
                var next = 'end' ;
           
            let countryData = allData.filter(function (e) {
                return e.country_id == response.c_id[i];
            });

            
            var html = '<div class="tab-pane fade show active" id="promoTab" role="tabpanel" aria-labelledby="promoTab-tab"><div class="boxView promotionCntBlk">'+
            '<div class="promoCnt"><div class="promoCntHead"><div class="promoHeadLeft">'+
            '<img src='+countryData[0].country_logo+' alt=""> <h2>'+countryData[0].country+'</h2></div><div class="promoHeadRight"><h3>'+countryData.length+' Promo</h3>'+
            '<p></p></div></div><div class="promoCntBody" id="ap'+i+'">'+
            '<div></div></div> </div>' ;
           
            $("#myTabContent").append(html) ;
          for(let x =0; countryData.length > x; x++){
            var html2 = '<div class="promoListItem">'+
            '<div class="promoTitle"><a href=""><i class="fas fa-angle-right"></i>'+
            '<h1>'+countryData[x].headline+'</h1><p>'+countryData[x].promotion_type+'</p></a></div><div class="PromoOp">'+
            '<div class="promoOpLeft"><img src='+countryData[x].providers_logo+'><div class="promoOpName"><h2>'+countryData[x].provider_name+'</h2><p>'+countryData[x].bonus_validity+'</p></div></div>'+
            '<div class="promoOpRight"><a class="btn btnGradient" href="">Send Top-up</a></div></div></div>';
           
            $("#ap"+i).append(html2) ;
          }   
            
            }
              
         }
      }, error: function (textStatus, errorThrown) {
           console.log('textStatus',textStatus)
           toastr.remove();
           toastr.error(textStatus.responseJSON.error.message);
       }
   });
}



$("form[name='updateProfile']").validate({
    rules: {
        name: {
            required: true,
            minlength: 2,
            maxlength: 255
        },
        email: {
            required: true,
            email: true
        }
    },
    messages: {
        name: {
            required: The_name_field_is_mandatory,
            minlength: The_name_two,
            maxlength: The_name_max
        },
        email: {
            required: The_email_mandatory,
            email: Please_valid_email_address
        }
    },
    submitHandler: function (form) {
      
        var formData =  new FormData(form);
       
        var url = window.location.origin + "/update-user";
        var deviceId = makeid(50);

        $.ajax({
            type: "POST",
            url: url,
            headers: {
                'Accept': 'application/json',
                "Cache-Control": "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0",
                "X-USER-TYPE": 1,
                "X-DEVICE-TYPE" :3,
                "x-firebase-token": 'abc',
                "x-device-id": deviceId
                
            },
            processData: false,
            contentType: false,
            data: formData,
            cache: false,
            success: function (response) {
                
              
                toastr.remove();
                if (response.code == 200) {
                      // go to next after verifyotp
                    
                        
                } else {
                    toastr.remove();
                    toastr.error("something went wrong");
                    return false;
                }
            }, error: function (textStatus, errorThrown) {
                $('#login_form_btn2').prop('disabled', false);
                $('.otpVerify').html("Confirm");
                toastr.remove();
                toastr.error(textStatus.responseJSON.error.message);
            }
        });
    }
});