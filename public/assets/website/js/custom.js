//  Sidebar toggle Function
function openNav($sliderName) {
    $('body').addClass("menuOpen");
    $('#'+$sliderName).toggle("slide", {direction: 'left'}, 300);
    $('.modalBackdrop').fadeIn();
}
function closeNav($sliderName) {    
    $('#'+$sliderName).toggle("slide", {direction: 'left'}, 300);
    $('body').removeClass("menuOpen");
    $('.modalBackdrop').fadeOut();
}

$(document).ready(function(){

/*=========Recharge Mobile and email show hide======*/
$("#emailBox").click(function(){
	$(this).hide();
	$("#cubaRecharge").hide();
	$("#emailRecharge").show();
	$("#cubacelBox").show();
})
$("#cubacelBox").click(function(){
	$(this).hide();
	$("#emailRecharge").hide();
	$("#cubaRecharge").show();
	$("#emailBox").show();
})

/*=========Nice Scroll For All======*/
$('.iti__country-list').niceScroll({
	cursorcolor:"#6265ac",
  	cursorwidth:"10px",
  	background:"#e8e8e8",
  	cursorborder:"none",
  	cursorborderradius:0,
  	// zindex: [1032],
  	autohidemode:false,
  	horizrailenabled: false
  	// touchbehavior: true,
});
$('.niceScrollView').niceScroll({
	cursorcolor:"#6265ac",
  	cursorwidth:"10px",
  	background:"#e8e8e8",
  	cursorborder:"none",
  	cursorborderradius:30,
  	// zindex: [1032],
  	horizrailenabled: false
});

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  // var target = $(e.target).attr("href") // activated tab
  // alert(target);
	$('.tabChangeScroll').niceScroll({
		cursorcolor:"#6265ac",
	  	cursorwidth:"10px",
	  	background:"#e8e8e8",
	  	cursorborder:"none",
	  	cursorborderradius:30,
	  	// zindex: [1032],
	  	horizrailenabled: false
	});
});

/*=========Payment Div show hide======*/
$("input[name='payments']").click(function() {
    var test = $(this).val();

    $(".paymentDiv").hide();
    $("#paymentOption" + test).show();
});


/*=========Pay Now recharge Loader Show hide======*/
$(".PayNowLoader").click(function(){
	$("#recLoader").show().delay(5000).fadeOut();
	setTimeout(function () {
		// swal("Recharge Done!", "Your recharge has been done successfully", "success")
		swal({
			  title: "Recharge Done!",
			  text: "Your recharge has been done successfully",
			  type: "success",
			  confirmButtonClass: "btnGradient",
			  confirmButtonText: "Ok"
		})
	}, 5300);
})

/*=========Stickey Sidebar======*/
// if ($(window).width() > 767) {
//   if($('.theiaStickySidebar').length > 0) {
//     $('.theiaStickySidebar').theiaStickySidebar({
//       // Settings
//       // additionalMarginTop: 30
//     });
//   }
//   if($('.UserStickySidebar').length > 0) {
//     $('.UserStickySidebar').theiaStickySidebar({
//       // Settings
//       additionalMarginTop: 85
//     });
//   }
//   if($('.eventStickySidebar').length > 0) {
//     $('.eventStickySidebar').theiaStickySidebar({
//       // Settings
//       additionalMarginTop: 106
//     });
//   }
// }


/*=========date time picker======*/
$(function () {
  $('.datepickDate').datetimepicker({
    format: 'MM/DD/YYYY',
    ignoreReadonly: true,
    widgetPositioning: {
        horizontal: 'auto',
        vertical: 'top'
    }
  });
    $('.datepickAutoPos').datetimepicker({
    format: 'MM/DD/YYYY',
    ignoreReadonly: true
  });
  $('.dateTime').datetimepicker({
    // inline: true
    ignoreReadonly: true
  });  
  $('.timeSelect').datetimepicker({
    // inline: true
    format: 'LT',
    ignoreReadonly: true
  });
});


/*=========Tool Tip Init======*/
// $.fn.bsTab = $.fn.tooltip.noConflict();
$(function () {
  $('[data-toggle="tooltip"]').tooltip({
  		trigger : 'hover',
      position: {
        my: "center bottom-20",
        at: "center top",
        using: function( position, feedback ) {
          $( this ).css( position );
          $( "<div>" )
            .addClass( "arrow" )
            .addClass( feedback.vertical )
            .addClass( feedback.horizontal )
            .appendTo( this );
        }
      }
  });
  $('[data-toggle="tooltip"]').on('focus', function () {
      $(this).tooltip("option", "hide")
  })
})


/*=========Data Tables======*/

$('#example').DataTable({
	"responsive": true,
	"ordering": false,
	"searching": false,
	"lengthChange": false
});
$('#example1').DataTable({
    "columns": [
      null,
      null,
      null,
      { "width": "20%" },
      null,
      null,
      null,
      null
    ],
    "responsive": true,
    "ordering": false,
    "searching": false,
    "lengthChange": false
});
$('a[aria-controls="tabItem1"]').on('shown.bs.tab', function (e) {

  $($.fn.dataTable.tables(true)).DataTable()
        .columns.adjust()
});


$('#example').on( 'page.dt', function () {
    $('html, body').animate({
        scrollTop: 0
    }, 200);
     
});


$("#filterOpen").click(function(){
    $("#filterView").slideToggle();
});


/*============History FIlter APply Dot Show and hide=============*/
$("#SearchApply").click(function(){
    $(".appliedFilter").show();
    $("#filterView").slideToggle();
});
$("#resetFilter").click(function(){
    $(".appliedFilter").hide();
    $("#filterView").slideToggle();
});


/*============Card Delete=============*/
$(".deleteCard").click(function(e){
    swal({
      title: "Are you sure?",
      text: "You want to remove this card!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, remove it!",
      closeOnConfirm: false
    },
    function(){
      swal({
        title :"Removed!", 
        text: "Your card has been removed.", 
        type: "success",
        confirmButtonClass: "btnGradient",
      });

    });
});


/*============== Chat Page Responsive ===========*/
$(function() {
    if($(window).width() <= 640){
        $(".chatHistoryItem").click(function(){
            $(".sideHistory").css({"flex": "0 0 0%", "-webkit-flex": "0 0 0%"});
            $(".conversation").css({"flex": "0 0 100%", "-webkit-flex": "0 0 100%"});
        });
        $(".chatBack").click(function(){
            $(".sideHistory").css({"flex": "0 0 100%", "-webkit-flex": "0 0 100%"});
            $(".conversation").css({"flex": "0 0 0%", "-webkit-flex": "0 0 0%"});
        });
    }
});

});